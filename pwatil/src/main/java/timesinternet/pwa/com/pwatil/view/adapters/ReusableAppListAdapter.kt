package timesinternet.pwa.com.pwatil.view.adapters

import android.os.Handler
import android.os.Message
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import timesinternet.pwa.com.pwatil.Constants
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwautil.databinding.AppItemBinding
import java.util.*

class ReusableAppListAdapter(openDetail : Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mReviewListAdapter = ArrayList<DetailAppModel>()
    private lateinit var handler: Handler
    private var openDetail : Boolean = false


    init {
        this.openDetail=openDetail
    }

    fun setAppList(appModel: ArrayList<DetailAppModel>, handler: Handler) {
        mReviewListAdapter.clear()
        mReviewListAdapter.addAll(appModel)
        this.handler = handler
        //notifyItemRangeInserted(0, appModel.size)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val appInfo = mReviewListAdapter[position]
        (holder as RecyclerHolderAppIcon).bind(appInfo)
    }

    override fun getItemCount(): Int {
        return mReviewListAdapter.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = AppItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerHolderAppIcon(applicationBinding)
    }


    inner class RecyclerHolderAppIcon(private val applicationBinding: AppItemBinding) : RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(appInfo: DetailAppModel) {
            applicationBinding.repositoryBottom = appInfo
            applicationBinding.appImg.setOnClickListener {
                Log.d("CLICKED", "CLICKED")
                var message = Message()
                message.arg1 = Constants.Messages.LAUNCH
                message.obj = appInfo
                handler.sendMessage(message)

            }


        }

    }

    fun onCategoryClick(view: View) {
        Log.d("CLICKED", "CLICKED")
    }

}