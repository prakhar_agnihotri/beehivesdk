package timesinternet.pwa.com.pwatil.AppStore

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import timesinternet.pwa.com.pwatil.Data.DummyData
import timesinternet.pwa.com.pwatil.DebugLog.Logs
import timesinternet.pwa.com.pwautil.R
import java.net.URL


class ViewApps : AppCompatActivity() {

    val animals: ArrayList<PWAApplication> = ArrayList()


    data class PWAApplication(val name: String, val manfest: String, var url: String)

    fun addAnimal(name: String, manfest: String, url: String) {
        animals.add( PWAApplication(name, manfest, url ) )

    }

    fun loadAppList(){
//TODO:Change IP
        doAsync {

            val _jsonArray = URL("http://pascolan.com/pwa/list.json" ).readText()
            val jsonArray = JSONArray(_jsonArray)
            for(  i in 0..(jsonArray.length() - 1)) {
                val item = jsonArray.getJSONObject(i)
                val manfest = item.getString("manfest")
//                val name = item.getString("name")
                val _url =  item.getString("url")

                val result = URL(manfest).readText()
                val name = JSONObject(result).getString("name")
                val icons = JSONObject(result).getJSONArray("icons")
                val icon = icons.getJSONObject(0).getString("src")

                var icnSource= ""
                if(icon[0] == '/'){
                    icnSource = _url + icon
                }
                else{
                    icnSource =  icon
                }
                addAnimal(name, _url, icnSource)

                // Your code here
            }

            this.onComplete {

                //rv!!.adapter = RVAdapter(animals,this@ViewApps )
            }
            uiThread {

                Log.d(Logs.DATA_LAYER, "Hitting URL")
            }



        }

    }

    fun findFields(data:  String, field: String) : String{
        val s = data.indexOf(field + "=")
        val l = data.indexOf(",", s)

        val value = data.substring(s + field.length + 1, l)
        return value

    }

    fun saveData(){
        val data = intent.getStringExtra("data")
        if(data != null){
            DummyData.userDetails.fname = findFields(data , "name")
            DummyData.userDetails.phone = findFields(data , "phoneNumber")
            DummyData.userDetails.lname = ""
        }

    }

    var rv : RecyclerView? =  null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(Logs.SDK, "STARTING SDK ACTIVITY")
        setContentView(R.layout.activity_view_apps)
        rv = findViewById<RecyclerView>(R.id.rv)

        rv!!.layoutManager = GridLayoutManager(this, 1)
        loadAppList()

        saveData()

    }
}
