package timesinternet.pwa.com.pwatil.Data

import android.app.NotificationManager
import timesinternet.pwa.com.pwatil.Models.DataModels


object DummyData {

    val userDetails : DataModels.UserData = DataModels.UserData("Anil", "Tiwari", "+91-7000601224", "rounak316@gmail.com","15-April-1993","MALE","DumyProfilePic")

    val simpleNotification : DataModels.NotificationData = DataModels.NotificationData(
            "45",
            45,
            "http://192.168.37.212/CAMERA/manifest.json",
            "This is big title",
            "this is subtitle",
            "aaa",
            "aaa",
            "ticker text",
            "any",
            "on",
            NotificationManager.IMPORTANCE_HIGH
    )




}


