package timesinternet.pwa.com.pwatil.view.fragments

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import timesinternet.pwa.com.pwautil.databinding.AppLoadingBinding
import timesinternet.pwa.com.timespwa.viewmodel.AppLoadingModel


class AppLoadingFragment : android.support.v4.app.Fragment() {

    private lateinit var binding: AppLoadingBinding
    private lateinit var appStoreHomeViewModel: AppLoadingModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = AppLoadingBinding.inflate(inflater, container, false).apply {
            appStoreHomeViewModel = AppLoadingModel(activity!!.application)


        }


        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        appStoreHomeViewModel = ViewModelProviders.of(this).get(AppLoadingModel::class.java)


    }


}
