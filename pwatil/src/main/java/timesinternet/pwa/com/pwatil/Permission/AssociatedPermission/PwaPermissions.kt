package timesinternet.pwa.com.pwatil.Permission.AssociatedPermission

import timesinternet.pwa.com.pwatil.Permission.Types.Pwa

object PwaPermissions {

    val addToHomeScreen = listOf(
          Pwa.HOMESCREEN_SHPRTCUT_ACCESS
    )


    val getLocation = listOf(
            Pwa.LOCATION_ACCESS
    )

    val getUserDetails = listOf(
            Pwa.USER_DETAIL_ACCESS
    )

    val notifyUser= listOf(
            Pwa.NOTIFICATION_ACCESS
    )

 val sendSms= listOf(
            Pwa.SMS_READ_ACCESS,
            Pwa.SMS_SEND_ACCESS
    )


    val makeCall = listOf(
            Pwa.MAKE_CALL
    )


    val readContacts = listOf(
            Pwa.CONTACTS_ACCESS
    )
    val accessCamera = listOf(
            Pwa.CAMERA_ACCESS
    )





}