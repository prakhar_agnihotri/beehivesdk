package timesinternet.pwa.com.timespwa.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class DBCreator// constructor
(private val mContext: Context) : SQLiteOpenHelper(mContext, DB_NAME, null, DATABASE_VERSION) {
    private val db: SQLiteDatabase? = null


    @Synchronized
    override fun close() {
        db?.close()
        super.close()
    }


    override fun onCreate(db: SQLiteDatabase) {


        val CREATE_APP_TABLE = ("CREATE TABLE " + TABLE_APP_DETAILS + "("
                + APP_ID + " TEXT PRIMARY KEY," +
                APP_NAME + " TEXT," +
                SERVER_UPDATED + " NUMBER DEFAULT 0," +
                IS_INSTALLED + " NUMBER DEFAULT 0 " +
                ")")

        db.execSQL(CREATE_APP_TABLE)

    }


    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {


    }

    companion object {

        val TAG = "DbUpdate"
        // All Static variables
        // Database Version
        const val DATABASE_VERSION = 1
        const val TABLE_APP_DETAILS = "AppDetails"
        const val APP_NAME = "AppName"
        const val APP_ID = "PkgName"
        const val SERVER_UPDATED = "ServerUpdated"
        const val IS_INSTALLED = "IsInstalled"

        const val DB_NAME = "PWA.sqlite"
        var dbCreator: DBCreator? = null

        @Synchronized
        fun getInstance(context: Context): DBCreator {

            // Use the application context, which will ensure that you
            // don't accidentally leak an Activity's context.
            // See this article for more information: http://bit.ly/6LRzfx
            if (dbCreator == null) {
                dbCreator = DBCreator(context.applicationContext)
            }
            return dbCreator as DBCreator
        }
    }

}