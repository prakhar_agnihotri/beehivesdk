package timesinternet.pwa.com.pwatil.AppStore

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwatil.SandBox
import timesinternet.pwa.com.pwatil.base.BaseFragment

import timesinternet.pwa.com.pwatil.view.adapters.AppCategoryAdapter
import timesinternet.pwa.com.pwatil.view.fragments.AppDetailFragment
import timesinternet.pwa.com.pwatil.viewmodel.CategoryViewModel
import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.pwautil.databinding.ActivityCategoryListBinding


class CategoryListFragment : BaseFragment(), AppCategoryAdapter.OnItemClickListener {


    lateinit var binding: ActivityCategoryListBinding
    private val categoryAppAdapter = AppCategoryAdapter(arrayListOf(), this)
    lateinit var viewModel: CategoryViewModel
    var typeOfLoadFun: Int = 0
    var titleBar: String = "Category"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_category_list, container, false);
        if (arguments != null) {
            this.typeOfLoadFun = arguments!!.getInt("data")
            titleBar = arguments!!.get("title") as String
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        binding.categoryViewModel = viewModel
        binding.executePendingBindings()
        binding.rvCategoryList.layoutManager = GridLayoutManager(activity, 3)
        binding.rvCategoryList.adapter = categoryAppAdapter
        binding.toolbarTitle.text = titleBar
        binding.backBtn.setOnClickListener { activity?.onBackPressed() }
        viewModel.appsList.observe(this,
                Observer<ArrayList<DetailAppModel>> { it?.let { categoryAppAdapter.replaceData(it) } })

        if (viewModel.appsList.value == null) {
            viewModel.loadApps(typeOfLoadFun)
        }

    }


    override fun onItemClick(position: Int) {

        var appDetailFragment = AppDetailFragment()
        var bundle = Bundle()
        var appModel= viewModel.appsList.value!![position]
        bundle.putParcelable("appInfo", appModel)
        bundle.putParcelableArrayList("appList",  viewModel.appsList.value)
        appDetailFragment.arguments = bundle
        replaceFragment(appDetailFragment)

    }


    override fun onLaunchClick(position: Int) {
        var appModel: DetailAppModel
        appModel = viewModel.appsList.value!![position]
        val intent = Intent(activity, SandBox::class.java)
        intent.putExtra("appId", "234")
        intent.putExtra("appUrl", appModel.launchUrl)
        intent.putExtra("mAppIconUrl", appModel.iconUrl)
        intent.putExtra("mAppName", appModel.appName)
        activity?.startActivity(intent)
    }
}
