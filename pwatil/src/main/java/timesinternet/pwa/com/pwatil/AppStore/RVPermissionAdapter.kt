package timesinternet.pwa.com.pwatil.AppStore

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.BitmapFactory
import android.graphics.drawable.Icon
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import timesinternet.pwa.com.pwatil.SandBox
import timesinternet.pwa.com.pwautil.R
import java.net.URL

class RVPermissionAdapter(val items: List<Pair<String, Any?>>, val context: Context , val prefs: SharedPreferences) : RecyclerView.Adapter<RVPermissionAdapter.ViewHolder>() {

    init {


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val _VIEW = LayoutInflater.from(context).inflate(R.layout.item_permission_view, parent, false)
        return ViewHolder(_VIEW)
    }



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.switch.text = items.get(position).first

        holder.switch.isChecked = items.get(position).second == true


        holder.switch.setOnClickListener {
            if(holder.switch.isChecked)
            {
                prefs.edit().putBoolean(items.get(position).first,  true).apply()

            } else if (holder.switch.isChecked == false){
                prefs.edit().putBoolean(items.get(position).first,  false).apply()
            }
        }
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }



    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val _itemView = view
        val switch = view.findViewById<Switch>(R.id.permission_switch)


    }


}

