package timesinternet.pwa.com.pwatil.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import timesinternet.pwa.com.pwatil.db.entity.AnalyticsEntity


@Dao
interface AnalyticsDao {

    @Insert
    fun insert(analytics: AnalyticsEntity)

    @Query("DELETE FROM analytics")
    fun deleteAll()

    @Query("SELECT * FROM analytics " )
    fun getAllAnalytics() : List<AnalyticsEntity>
}