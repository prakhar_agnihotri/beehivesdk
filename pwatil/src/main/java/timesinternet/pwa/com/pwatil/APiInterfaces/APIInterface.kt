package timesinternet.pwa.com.timespwa.Interfaces.APiInterfaces

import retrofit2.Call
import retrofit2.http.*
import timesinternet.pwa.com.pwatil.Models.AppDetailResponse
import timesinternet.pwa.com.pwatil.Models.AppInstallDeleteResponse
import timesinternet.pwa.com.pwatil.Models.DashboardRequest
import timesinternet.pwa.com.pwatil.Models.DashboardResponse


interface APIInterface {
    @POST("update_pckg/create_user/")
    @FormUrlEncoded
    fun savePost(@Field("data") title: String): Call<AppInstallDeleteResponse>

    @POST("update_pckg/add/")
    @FormUrlEncoded
    fun addPackage(@Field("data") title: String): Call<AppInstallDeleteResponse>

    @POST("update_pckg/remove/")
    @FormUrlEncoded
    fun removePackage(@Field("data") title: String): Call<AppInstallDeleteResponse>

    @Headers(
            "Content-Type: application/json",
            "auth_token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2IiOiIrOTE5NzE3MDAzNTc3IiwidW5tIjoiQW5pbCIsImlkIjoiYWFkYWhkaGVpaWhkYWsiLCJleHAiOjE1NjY5NjQzODR9.Xz7Yvh4XL0wUT4ynnOMG7zT2ENa4LDCI25HF3wVlr2U",
            "osid:1",
            "sdkversion:1.1",
            "Deviceid:b39e5065-1d07-434c-a72f-e9e2b37dd3b5"
    )
    @GET
    fun getAppDetails(@Url path: String): Call<AppDetailResponse>


/*
    @POST("initials.json")
    fun getDashboardApps(@Body  singlePostRequest : DashboardRequest): Call<DashboardResponse>
*/

    @Headers(
            "Content-Type: application/json",
            "auth_token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtb2IiOiIrOTE5NzE3MDAzNTc3IiwidW5tIjoiQW5pbCIsImlkIjoiYWFkYWhkaGVpaWhkYWsiLCJleHAiOjE1NjY5NjQzODR9.Xz7Yvh4XL0wUT4ynnOMG7zT2ENa4LDCI25HF3wVlr2U",
            "osid:1",
            "sdkversion:1.1",
            "Deviceid:b39e5065-1d07-434c-a72f-e9e2b37dd3b5"
    )
    @POST("/v1/initials")
    fun getDashboardApps(@Body inputBody: DashboardRequest): Call<DashboardResponse>


}