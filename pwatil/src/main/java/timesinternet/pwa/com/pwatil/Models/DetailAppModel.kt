package timesinternet.pwa.com.pwatil.Models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.NonNull
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "app_list")
class DetailAppModel() : Parcelable {

    @ColumnInfo(name = "tt")
    var templateType : Int? = null

    @PrimaryKey
    @ColumnInfo(name = "si")
    @SerializedName("si")
    @Expose
    @NonNull
    var appId: String? = null

    @ColumnInfo(name = "pin")
    var pinStatus: Int? = 0


    @ColumnInfo(name = "n")
    @SerializedName("n")
    @Expose
    var appName: String? = null

    @ColumnInfo(name = "c")
    @SerializedName("c")
    @Expose
    var category: String? = null

    @ColumnInfo(name = "sc")
    @SerializedName("sc")
    @Expose
    var subCategory: String? = null

    @ColumnInfo(name = "icu")
    @SerializedName("icu")
    @Expose
    var iconUrl: String? = null


    @Ignore
    @SerializedName("imu")
    @Expose
    var imageUrl: List<String>? = null

    @Ignore
    @SerializedName("vu")
    @Expose
    var videoUrl: List<String>? = null

    @ColumnInfo(name = "lu")
    @SerializedName("lu")
    @Expose
    var launchUrl: String? = null


    @ColumnInfo(name = "bu")
    @SerializedName("bu")
    @Expose
    var bannerUrl: String? = null

    @ColumnInfo(name = "bt")
    @SerializedName("bt")
    @Expose
    var bannerType: Int? = null


    @ColumnInfo(name = "r")
    @SerializedName("r")
    @Expose
    var rating: Double? = null

    @ColumnInfo(name = "d")
    @SerializedName("d")
    @Expose
    var downloadCount: Int? = null

    @ColumnInfo(name = "t")
    @SerializedName("t")
    @Expose
    var tagLine: String? = null

    @ColumnInfo(name = "m")
    @SerializedName("m")
    @Expose
    var metaDesc: String? = null

    @ColumnInfo(name = "ru")
    @SerializedName("ru")
    @Expose
    var reviewUrl: String? = null

    @Ignore
    @SerializedName("du")
    @Expose
    var detailUrl: String? = null


    @ColumnInfo(name = "time_stamp")
    var timestamp: Long = 0

    constructor(parcel: Parcel) : this() {
        appId = parcel.readString()
        pinStatus = parcel.readValue(Int::class.java.classLoader) as? Int
        appName = parcel.readString()
        category = parcel.readString()
        subCategory = parcel.readString()
        iconUrl = parcel.readString()
        imageUrl = parcel.createStringArrayList()
        videoUrl = parcel.createStringArrayList()
        launchUrl = parcel.readString()
        bannerUrl = parcel.readString()
        bannerType = parcel.readValue(Int::class.java.classLoader) as? Int
        rating = parcel.readValue(Double::class.java.classLoader) as? Double
        downloadCount = parcel.readValue(Int::class.java.classLoader) as? Int
        tagLine = parcel.readString()
        metaDesc = parcel.readString()
        reviewUrl = parcel.readString()
        detailUrl = parcel.readString()
        timestamp = parcel.readLong()
    }

    companion object CREATOR : Parcelable.Creator<DetailAppModel> {
        override fun createFromParcel(parcel: Parcel): DetailAppModel {
            return DetailAppModel(parcel)
        }

        override fun newArray(size: Int): Array<DetailAppModel?> {
            return arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

        parcel.writeString(appId)
        parcel.writeValue(pinStatus)
        parcel.writeString(appName)
        parcel.writeString(category)
        parcel.writeString(subCategory)
        parcel.writeString(iconUrl)
        parcel.writeStringList(imageUrl)
        parcel.writeStringList(videoUrl)
        parcel.writeString(launchUrl)
        parcel.writeString(bannerUrl)
        parcel.writeValue(bannerType)
        parcel.writeValue(rating)
        parcel.writeValue(downloadCount)
        parcel.writeString(tagLine)
        parcel.writeString(metaDesc)
        parcel.writeString(reviewUrl)
        parcel.writeString(detailUrl)
        parcel.writeLong(timestamp)
    }

    override fun describeContents(): Int {
        return 0
    }


}

