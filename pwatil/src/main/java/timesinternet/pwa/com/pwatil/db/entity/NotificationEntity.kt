package timesinternet.pwa.com.pwatil.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.sql.Timestamp

/*
* TABLE: NOTIFICATIONS
* APP_ID String
* TITLE String
* DETAILS String
* INTENT String
* TYPE String
* TRIGGER Bool // If notification was shown or not  [Default: False]
* Timestamp Time
* */

@Entity(tableName = "notifications")
data class NotificationEntity(@PrimaryKey
                              @ColumnInfo(name = "id") val notiId: Int,
                              @ColumnInfo(name = "si") val appId: Int,
                              @ColumnInfo(name = "t") val title: String,
                              @ColumnInfo(name = "d") val detail: String,
                              @ColumnInfo(name = "i") val intent: String,
                              @ColumnInfo(name = "ty") val subCategory: String,
                              @ColumnInfo(name = "status") val status: Boolean, // If notification was shown or not  [Default: False]
                              @ColumnInfo(name = "time_stamp") val timestamp: Long)