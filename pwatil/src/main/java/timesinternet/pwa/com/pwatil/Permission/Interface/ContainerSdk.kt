package timesinternet.pwa.com.pwatil.Permission.Interface

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import org.jetbrains.anko.Android
import timesinternet.pwa.com.pwatil.DebugLog.Logs
import timesinternet.pwa.com.pwatil.Permission.Callbacks.PermissionResponse
import timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol.Pwa
import timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol.Sdk
import timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol.User
import timesinternet.pwa.com.pwatil.SandBox
import java.util.jar.Manifest

open class ContainerSdk(appId: String, var activity: SandBox) : Sdk {
    override fun addToHomeScreen(signatureId : String) {
        Log.d(Logs.SDK_PERMISSION, "addToHomeScreen")
    }

    override fun getUserDetails(signatureId : String) {
        Log.d(Logs.SDK_PERMISSION, "getUserDetails")
        Core.grantUserDetails(signatureId, activity)
    }


    override fun readSms(signatureId : String) {
        Log.d(Logs.SDK_PERMISSION, "readSms")  }

    override fun log(signatureId : String, logData: String) {
        Log.d(Logs.SDK_PERMISSION, "log")}

    override fun openCamera(signatureId : String) {
        Log.d(Logs.SDK_PERMISSION, "openCamera")
        Core.openCamera(signatureId, activity)
    }


    override fun readContacts(signatureId : String) {
        Log.d(Logs.SDK_PERMISSION, "readContacts") }

    override fun phoneCall(signatureId : String) {
        Log.d(Logs.SDK_PERMISSION, "phoneCall") }

    override fun sendSms(signatureId : String, sendTo: String, text: String) {

        Log.d(Logs.SDK_PERMISSION, "sendSms" + sendTo+ " : " + text)
        Core.sendSMS(sendTo, text)
    }

    override fun runInBackground(signatureId : String) {
        Log.d(Logs.SDK_PERMISSION, "runInBackground") }

    override fun readLocation(signatureId : String) {
        Log.d(Logs.SDK_PERMISSION, "readLocation") }

    override fun notifyUser(signatureId : String, title: String, description: String) {
        Log.d(Logs.SDK_PERMISSION, "notifyUser")}

    override fun isPermittedTo(permissions: List<String>, permissionResponse: PermissionResponse) {
        for(permission in permissions){
            Log.d(Logs.SDK , "isPermittedTo(ContainerSdk)  PERMISSION" + permission)
        }

        Dexter.withActivity(activity)
                .withPermissions(permissions)
                .withListener(
                        object : PermissionListener, MultiplePermissionsListener {
                            override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                                Log.d(Logs.DATA_LAYER, "onPermissionsRationaleShouldBeShown")
                                token?.continuePermissionRequest();
                            }

                            override fun onPermissionsChecked(report: MultiplePermissionsReport?) {


                                for(_permission in report!!.grantedPermissionResponses)
                                    Log.d(Logs.DATA_LAYER, "grantedPermissionResponses: " + _permission.permissionName )
                                for(_permission in report.deniedPermissionResponses)
                                    Log.d(Logs.DATA_LAYER, "deniedPermissionResponses: " + _permission.permissionName )

                               if(report.isAnyPermissionPermanentlyDenied()){
                                 return  permissionResponse.onFailure()
                               }
                                permissionResponse.onSuccess()
                            }

                            override fun onPermissionGranted(response: PermissionGrantedResponse) {/* ... */
                                Log.d(Logs.DATA_LAYER, "onPermissionGranted")
                                permissionResponse.onSuccess()
                            }

                            override fun onPermissionDenied(response: PermissionDeniedResponse) {/* ... */
                                Log.d(Logs.DATA_LAYER, "onPermissionDenied")
                                permissionResponse.onFailure()
                            }

                            override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {/* ... */
                                token.continuePermissionRequest();
                                Log.d(Logs.DATA_LAYER, "onPermissionRationaleShouldBeShown")
                            }
                        }
                )
                .check(); }

    var SHARED_PREFS_PERMISSION : String = "PERMISSION_MANAGEMENT_SDK${appId}"
    var prefs: SharedPreferences = activity.getSharedPreferences(SHARED_PREFS_PERMISSION , 0)



    override fun askForNativePermission() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }








}