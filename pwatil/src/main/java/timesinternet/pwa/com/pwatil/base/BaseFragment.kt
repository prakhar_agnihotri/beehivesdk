package timesinternet.pwa.com.pwatil.base

import android.support.v4.app.Fragment
import timesinternet.pwa.com.pwautil.R

open class BaseFragment : android.support.v4.app.Fragment()
{
    fun replaceFragment(fragment:Fragment)
    {
        activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.app_store_container, fragment, "")
                .commit()

        /*childFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.main_developer_frag, fragment, "")
                .commit()*/
       /* activity!!.getCh
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.bb_frag, fragment, "")
                .commit()*/

    }
}
