package timesinternet.pwa.com.timespwa.Notification

import android.app.NotificationManager
import android.content.Context

private var notificationManager: NotificationManager? = null

fun initNotiManager(pContext: Context) {
    notificationManager =
            pContext.getSystemService(
                    Context.NOTIFICATION_SERVICE) as NotificationManager
}

fun getNotificationManager():NotificationManager{
    return notificationManager!!
}

fun isPWARegisteredOrNot() {
}

fun schedulePWARegistration() {

}

fun cancelNotiById() {

}