package timesinternet.pwa.com.pwatil.Data


import android.content.Context
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwatil.Models.DetailResonse
import timesinternet.pwa.com.pwatil.Utils.apiUtils.ApiUtils
import timesinternet.pwa.com.timespwa.Interfaces.APiInterfaces.APIInterface

class DetailAppRepository(val context: Context) {
    val TAG: String = "CATEGORYAPPADAPTER"

    fun getDetailedApp(onRepositoryReadyCallback: onDetailedAppReadyCallBack, typeOfLoadFun: Int,detailSuffix : String) {
        var detailedModel = DetailResonse()

        var mAPIService: APIInterface? = null
        mAPIService = ApiUtils.getAppDetailClient()

        val map = mutableMapOf<String, String>()
        map.put("id", detailSuffix)



        ApiUtils.enqueueWithRetry(mAPIService!!.getAppDetails(detailSuffix), ApiUtils.DEFAULT_RETRIES, object : Callback<timesinternet.pwa.com.pwatil.Models.AppDetailResponse> {
            override fun onResponse(call: Call<timesinternet.pwa.com.pwatil.Models.AppDetailResponse>, response: Response<timesinternet.pwa.com.pwatil.Models.AppDetailResponse>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        response.body().toString()
                        detailedModel = response.body()?.detailResponse!!
                        onRepositoryReadyCallback.onDataReady(detailedModel)

                    }
                    Log.d(TAG, "detailResponse code " + response.code() + " body of app details" + response.body().toString())
                } else {
                    Log.d(TAG, "detailResponse is unsuccessfull ")

                }
            }

            override fun onFailure(call: Call<timesinternet.pwa.com.pwatil.Models.AppDetailResponse>, t: Throwable) {
                Log.e(TAG, t.message)
            }
        })

        onRepositoryReadyCallback.onDataReady(detailedModel)
    }

    interface onDetailedAppReadyCallBack {
        fun onDataReady(data: DetailResonse)
    }


}