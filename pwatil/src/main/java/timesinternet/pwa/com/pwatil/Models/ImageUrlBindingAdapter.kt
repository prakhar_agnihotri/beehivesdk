package timesinternet.pwa.com.pwatil.Models

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import timesinternet.pwa.com.pwautil.R

object ImageUrlBindingAdapter {


    @JvmStatic
    @BindingAdapter("android:imgBanner")
    fun setImageUrlBanner(view: ImageView, url: String) {


        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.color.colorPrimary)
        requestOptions.error(R.color.colorPrimary)

        Glide.with(view.context).setDefaultRequestOptions(requestOptions).load(url).transition(DrawableTransitionOptions.withCrossFade(400)).into(view)
    }
}