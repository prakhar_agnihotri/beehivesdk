package timesinternet.pwa.com.pwatil.NetworkLayer

import android.util.Log
import com.example.proto.AnalyticsEventOuterClass
import com.example.proto.AuthenticationOuterClass
import com.example.proto.ConfiguratorOuterClass
import com.example.proto.NotificationOuterClass


import org.jetbrains.anko.doAsync
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.net.InetAddress
import java.net.Socket
import java.util.*

class SocketHandler {

    var HOST : String
    var PORT: Int
    var SOCKET : Socket?= null
    var INPUT_STREAM : InputStream?= null
    var OUTPUT_STREAM : OutputStream? = null

    var META_OUTPUT_STREAM : ByteArrayOutputStream
    var META_READ = false
    val META_BYTES = 3
    var PARTIAL_LENGTH = 0
    var PARTIAL_TYPE = 0
    val SLEEP_TIME = 3
    var TOKEN = ""

    val SocketNotifier: SocketNotifier

    constructor(Host : String, Port: Int, Token: String ,socketNotifier :  SocketNotifier ){
        HOST = Host
        PORT = Port
        this.TOKEN = TOKEN
        META_OUTPUT_STREAM = ByteArrayOutputStream()
        SocketNotifier = socketNotifier
        Connect()

    }



    fun Close(){
        SOCKET?.close()
        SOCKET = null
    }

    fun Connect(){
        Log.d("Trying to Connect", "To Server" + HOST + " " + PORT)

            try{
                SOCKET = Socket(InetAddress.getByName(HOST), PORT)
                createSocket()
            } catch (exc: Exception){
                Thread.sleep(SLEEP_TIME.toLong() * 1000)
                Reconnect()

            }


    }

    fun Reconnect(){
        Close()
        Connect()
    }

    fun bytesToUnsignedShort(byte1 : Byte, byte2 : Byte, bigEndian : Boolean) : Int {
        if (bigEndian)
            return (((byte1.toInt() and 255) shl 8) or (byte2.toInt() and 255))

        return (((byte2.toInt() and 255) shl 8) or (byte1.toInt() and 255))

    }

    fun Read(){

        INPUT_STREAM?.available()?.let { if (it >=0) {

            while(true){

                var MSG_TYPE = '0'
                var MSG_LEN = 0

                var META =  ByteArray(1)
                INPUT_STREAM?.read(META)?.let {

                    if (it == -1){
                        return
                    }

                    MSG_TYPE = META[0].toChar()

                    META =  ByteArray(2)
                    INPUT_STREAM?.read(META)?.let {

                        if (it == -1){
                            return
                        }

                        val __BYTE_OUTPUT_STREAM = ByteArrayOutputStream()

                        MSG_LEN  =  bytesToUnsignedShort(META[0], META[1], false)

                        META =  ByteArray(MSG_LEN)
                        var MSG_DATA = ByteArray(MSG_LEN)

                        INPUT_STREAM?.read(META)?.let{


                            if (it == -1){
                                return
                            }

                            __BYTE_OUTPUT_STREAM.write( Arrays.copyOfRange(META, 0, it))


                            while(__BYTE_OUTPUT_STREAM.size() < MSG_LEN){

                                META = ByteArray(MSG_LEN - __BYTE_OUTPUT_STREAM.size() )
                                INPUT_STREAM?.read(META)?.let {
                                    __BYTE_OUTPUT_STREAM.write( Arrays.copyOfRange(META, 0, it))
                                }

                            }

                        }

                        publishMessage( MSG_TYPE , __BYTE_OUTPUT_STREAM.toByteArray())



                    }


                }



            }


        }
        }


    }

    fun Write(socketFrame : SocketFrame){
        write(socketFrame)
    }

    fun write(socketFrame : SocketFrame){

        doAsync {
            try{
                OUTPUT_STREAM?.write(socketFrame.CreateFrame())
//                Log.d("WRITTING DATA", "TO SOCKET")
            } catch (exc: Exception){
//                Log.d("WRITTING DATA", "SOCKET FAILED")
            }
        }


    }

    fun sendAnalyticsEvent(event : AnalyticsEventOuterClass.AnalyticsEvent ){

        Write( SocketFrame( Constants.EVENT_ANALTICS, event.toByteArray() ) )

    }
    fun sendLoginEvent(event : AuthenticationOuterClass.Authentication ){

        Write( SocketFrame( Constants.AUTHENTICATION, event.toByteArray() ) )

    }


    fun publishMessage(messageType : Char, messageData : ByteArray ){
        Log.d("BLAZE_SOCKET MESSAGEP_TYPE" , messageType.toString() )
        Log.d("BLAZE_SOCKET MESSAGE_SIZE" , messageData.size.toString() )

        if(messageType == Constants.NOTIFICATION){
            try {
                val payload = NotificationOuterClass.Notification.parseFrom(messageData)
                SocketNotifier.onNotification(payload)
            } catch (exc: Exception ){
            }


        } else  if(messageType == Constants.AUTHENTICATION){

            try {
                val payload = AuthenticationOuterClass.Authentication.parseFrom(messageData)
                SocketNotifier.onAuth(payload)
            } catch (exc: Exception ){
            }


        }else  if(messageType == Constants.CONFIGURATOR){

            try {
                val payload = ConfiguratorOuterClass.Configurator.parseFrom(messageData)
                SocketNotifier.onConfigurator(payload)
            } catch (exc: Exception ){
            }


        } else  if(messageType == Constants.EVENT_ANALTICS){

            try {
                val payload = AnalyticsEventOuterClass.AnalyticsEvent.parseFrom(messageData)
                SocketNotifier.onAnalyticsEvent(payload)
            } catch (exc: Exception ){
            }


        }



    }

    fun createSocket() {
        INPUT_STREAM = SOCKET?.getInputStream()
        OUTPUT_STREAM = SOCKET?.getOutputStream()
        //sendLoginEvent( AuthenticationOuterClass.Authentication.newBuilder().setAppId("1").setState("dummy_state").setToken(this.TOKEN).setVersion("1").build() )
        Read()

        Log.d("CLOSED SOCKET CONNECTION", "RETRYING IN ${SLEEP_TIME} Seconds")

        Thread.sleep(SLEEP_TIME.toLong() * 1000)
        return Reconnect()
    }

}
