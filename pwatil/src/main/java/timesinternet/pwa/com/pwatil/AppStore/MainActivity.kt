package timesinternet.pwa.com.pwatil.AppStore


import android.app.PendingIntent
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.BitmapFactory
import android.graphics.drawable.Icon
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import timesinternet.pwa.com.pwatil.SandBox
import timesinternet.pwa.com.pwautil.R
import java.net.URL


class MainActivity : AppCompatActivity() {

    val ip = "http://10.150.72.222"

    fun downloadIcon(iconURL: String , title: String) {
        doAsync {
            val result = URL(iconURL).readBytes()
            val bmp = BitmapFactory.decodeByteArray(result, 0, result.size)
            val icon = Icon.createWithBitmap(bmp)
            Log.d("DOWNLOADED_ICON",icon.toString() )
            pinnedShorcut(icon, title, "NO URL")
        }

    }

    fun addToHomescreen() {

        doAsync {
            val result = URL(ip+"/TEST/manifest.json").readText()
            val name = JSONObject(result).getString("name")
            val icons = JSONObject(result).getJSONArray("icons")
            val icon = icons.getJSONObject(0).getString("src")
            Log.d("ICON", ip+"/TEST" + icon)
            downloadIcon(ip+"/TEST" + icon, name)
//            uiThread {
//                mainTextView.setText(result)
//            }
        }


    }

    fun goToActivity() {
        val intent = Intent(this, SandBox::class.java)
        startActivity( intent )
    }

    fun pinTheIcon(icon: Icon){

    }

    fun pinnedShorcut(icon: Icon, title: String , url: String){

        val mShortcutManager = getSystemService(ShortcutManager::class.java)

        if (mShortcutManager.isRequestPinShortcutSupported()) {
            val PWA_intent = Intent(this,SandBox::class.java)
            PWA_intent.putExtra("url", url)
            PWA_intent.setAction("MAIN");

            var pinShortcutInfo =ShortcutInfo.Builder(this, title)
                    .setShortLabel(title)
                    .setLongLabel("This is a demo PWA App for Be Hive")
                    .setIcon(icon)
                    .setIntent(PWA_intent )
                    .build()

            val pinnedShortcutCallbackIntent = mShortcutManager.createShortcutResultIntent(pinShortcutInfo)

            val successCallback = PendingIntent.getBroadcast(this, 0,
                    pinnedShortcutCallbackIntent, 0)

            mShortcutManager.requestPinShortcut(pinShortcutInfo,
                    successCallback.intentSender)
        }
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val webViewButton : Button = findViewById<Button>(R.id.startWV)
        val addToHomeScreen : Button = findViewById<Button>(R.id.addToHomeScreen)

        webViewButton.setOnClickListener(
                { view ->
                    goToActivity()
                })

        addToHomeScreen.setOnClickListener(
                { view ->
                    addToHomescreen()
                })
    }
}
