package timesinternet.pwa.com.pwatil.BroadcastReciever

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage


class SmsReciever : BroadcastReceiver() {
    //interface

    override fun onReceive(context: Context?, intent: Intent?) {
        val data = intent?.getExtras()

        val pdus = data?.get("pdus") as Array<Any>

        for (i in pdus.indices) {
            val format = data.getString("format")
            val smsMessage = SmsMessage.createFromPdu(pdus[i] as ByteArray, format)
                val messageBody = smsMessage?.getMessageBody()
                //Pass the message text to interface
                mListener?.messageReceived(  smsMessage.originatingAddress + ";" + messageBody)


        }
    }

    companion object {
        private var mListener: SmsListener? = null
        fun bindListener(listener: SmsListener) {

            mListener = listener
        }
    }
}