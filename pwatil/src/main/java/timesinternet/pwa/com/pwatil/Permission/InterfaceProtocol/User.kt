package timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol

interface  User {
    fun addToHomeScreen(signatureId : String)
    fun getUserDetails(signatureId : String)
    fun readSms(signatureId : String)
    fun log(signatureId : String, logData : String)
    fun openCamera(signatureId : String)
    fun readContacts(signatureId : String)
    fun phoneCall(signatureId : String)
    fun sendSms( signatureId : String, sendTo: String, text:String)
    fun runInBackground(signatureId : String)
    fun readLocation(signatureId : String)
    fun notifyUser(signatureId : String, title : String, description : String)
}