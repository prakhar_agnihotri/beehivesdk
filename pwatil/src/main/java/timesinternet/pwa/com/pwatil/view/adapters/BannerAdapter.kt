package timesinternet.pwa.com.pwatil.view.adapters

import android.os.Handler
import android.os.Message
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwautil.databinding.BannerItemBinding
import java.util.ArrayList

class BannerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var handler: Handler

    private val mReviewListAdapter = ArrayList<DetailAppModel>()

    fun setAppList(appModel: ArrayList<DetailAppModel>, handler: Handler) {
        mReviewListAdapter.addAll(appModel)
        this.handler = handler
        //notifyItemRangeInserted(0, appModel.size)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val appInfo = mReviewListAdapter[position]
        (holder as RecyclerHolderAppIcon).bind(appInfo)
    }

    override fun getItemCount(): Int {
        return mReviewListAdapter.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = BannerItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerHolderAppIcon(applicationBinding)
    }


    inner class RecyclerHolderAppIcon(private val applicationBinding: BannerItemBinding) : RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(appInfo: DetailAppModel) {
            applicationBinding.bannerModel=appInfo
            applicationBinding.imageView8.setOnClickListener {
                Log.d("CLICKED", "CLICKED")


                var message = Message()
                message.obj = appInfo
                handler.sendMessage(message)

            }
        }

    }


}
