package timesinternet.pwa.com.timespwa.Notification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import timesinternet.pwa.com.pwatil.Models.DataModels
import timesinternet.pwa.com.pwatil.SandBox
import timesinternet.pwa.com.pwatil.view.activities.BrainBaaziActivity
import timesinternet.pwa.com.pwautil.R

import timesinternet.pwa.com.timespwa.Utils.SingletonHolder


class NotiHandler private constructor(context: Context) {

    val context : Context = context

    init {
        // Init using context argument
    }

    companion object : SingletonHolder<NotiHandler, Context>(::NotiHandler)

    fun addSimpleNotification(notificationPayLoad: DataModels.NotificationData) {
        val notificationIntent = Intent(context, BrainBaaziActivity::class.java)
//        notificationIntent.putExtra("url", "")
//        notificationIntent.putExtra("appId", notificationPayLoad.appId)
//        notificationIntent.putExtra("mAppIconUrl", notificationPayLoad.appId)
//        notificationIntent.putExtra("mAppName", notificationPayLoad.appId)

        //todo add analytic for notification fired
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel("testGroup", "testChannel",
                    "some very long description", "channelId 785698")
        }
        val mBuilder = NotificationCompat.Builder(context, "channelId 785698")
                .setTicker(notificationPayLoad.tickerText)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_play_circle_filled_black_24dp)
                .setContentTitle(notificationPayLoad.title)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(notificationPayLoad.subTitle)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)

        getNotificationManager().notify(notificationPayLoad.notificationId, mBuilder.build())
    }
}

