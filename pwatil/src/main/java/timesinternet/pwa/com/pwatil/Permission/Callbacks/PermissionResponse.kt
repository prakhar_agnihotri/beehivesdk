package timesinternet.pwa.com.pwatil.Permission.Callbacks

interface PermissionResponse {
    fun onSuccess()
    fun onFailure()
}