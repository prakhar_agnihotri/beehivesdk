package timesinternet.pwa.com.pwatil.container

interface BridgeInterface {
    fun publishData(action : String?, data: String?)
}