package timesinternet.pwa.com.pwatil.AppStore

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import timesinternet.pwa.com.pwatil.Constants
import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.timespwa.Services.Alarm.AlarmHandler
import timesinternet.pwa.com.pwatil.Services.NotificationPushService
import timesinternet.pwa.com.timespwa.Services.OnlineDbSyncService

class NotificationPushActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_push)
    }


    fun startService(view: View) {
        //get notification push every 10 seconds


        Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show()
    }

    fun stopService(view: View) {
        //stop NotificationPushService
        val intent = Intent(this, NotificationPushService::class.java)
        stopService(intent)
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_SHORT).show()


    }

    fun startSyncService(view: View) {
        //stop NotificationPushService

        Toast.makeText(this, "Sync Service Started", Toast.LENGTH_SHORT).show()
        val alarmIntent = Intent(applicationContext, OnlineDbSyncService::class.java)

        AlarmHandler.getInstance(applicationContext).setAlarm(
                Constants.AlarmConstants.ALARM_SYNC_SERVICE_REQUEST_CODE,
                System.currentTimeMillis() + Constants.AlarmConstants.REPEAT_INTERVAL,
                Constants.AlarmConstants.REPEAT_INTERVAL,alarmIntent

        )

    }
}
