package timesinternet.pwa.com.pwatil.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/*
{
	"C":"", //Category
	"Sc":"", //Subcategory
	"an":"BaaziNow", // Appname (Free Text)
    "f":0,  //from Index
	"s":50   // Size

}
* */
class DashboardRequest(from: Int, size: Int, appName: String,subCategory : String, category : String) {
    @SerializedName("f")
    @Expose
    var from: Int? = null

    @SerializedName("s")
    @Expose
    var size: Int? = null


    @SerializedName("an")
    @Expose
    var appName: String? = null


    @SerializedName("Sc")
    @Expose
    var subCategory: String? = null

    @SerializedName("C")
    @Expose
    var category: String? = null


}