package timesinternet.pwa.com.pwatil.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.Context

class AppStoreViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var context: Context

    init {
        context = application
    }
}