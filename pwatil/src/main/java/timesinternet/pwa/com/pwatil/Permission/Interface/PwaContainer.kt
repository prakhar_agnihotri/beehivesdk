package timesinternet.pwa.com.pwatil.Permission.Interface

import android.graphics.Color
import android.support.design.widget.BottomSheetDialog
import android.util.Log
import timesinternet.pwa.com.pwatil.DebugLog.Logs
import timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol.Pwa
import timesinternet.pwa.com.pwatil.Permission.Types.Native

import android.support.v7.app.AppCompatActivity
import timesinternet.pwa.com.pwatil.Permission.Callbacks.PermissionResponse
import timesinternet.pwa.com.pwatil.SandBox
import timesinternet.pwa.com.pwautil.R


open class PwaContainer(appId: String, activity: SandBox) : ContainerSdk(appId, activity), Pwa {
    override fun isPermittedTo(permissions: MutableCollection<String>, permissionResponse: PermissionResponse) {
        for (permission in permissions) {
            prefs.getBoolean(permission, false)
            Log.d(Logs.SDK, "PERMISSION" + permission)
        }
    }


    override fun grant(permission: String) {
        prefs.edit().putBoolean(permission, true).apply()
    }

    override fun revoke(permission: String) {
        prefs.edit().putBoolean(permission, false).apply()
    }


    override fun isPermittedTo(permission: String, permissionResponse: PermissionResponse) {


        val isPermitted = prefs.getBoolean(permission, false)

        if (isPermitted) {
            Log.d(Logs.SDK, "PERMISSION REQUIRED: " + Native.Permissions.get(permission)!!)
            super.isPermittedTo(Native.Permissions.get(permission)!!, permissionResponse)
            return
        }
//
//        FancyAlertDialog.Builder(activity)
//                .setTitle("Permission Required")
//                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
//                .setMessage("Do you really want to permit the application to use:\n\n\n${permission}")
//                .setNegativeBtnText("Deny")
//                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
//                .setPositiveBtnText("Grant")
//
//                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
//                .setAnimation(Animation.POP)
//                .isCancellable(true)
//                .setIcon(R.drawable.ic_play_circle_filled_black_24dp, Icon.Visible)
//                .OnPositiveClicked {
//                    Log.d(Logs.SDK, "PERMISSION REQUIRED: " + Native.Permissions.get(permission)!!)
//                    super.isPermittedTo(Native.Permissions.get(permission)!!, permissionResponse)
//                    grant(permission)
//                }
//                .OnNegativeClicked {
//                    revoke(permission)
//                }
//                .build()


        Log.d(Logs.SDK, "isPermittedTo(PwaContainer)  PERMISSION" + permission)

    }


}