package timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol

import timesinternet.pwa.com.pwatil.Permission.Callbacks.PermissionResponse

interface  Pwa {
    fun isPermittedTo( permissions : MutableCollection<String>, permissionResponse: PermissionResponse)
    fun isPermittedTo(permission : String, permissionResponse: PermissionResponse)
    fun grant( permission : String)
    fun revoke( permission : String)
}