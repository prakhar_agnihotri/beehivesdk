package timesinternet.pwa.com.timespwa.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import timesinternet.pwa.com.pwatil.Constants

import timesinternet.pwa.com.timespwa.Utils.SingletonHolder
import java.util.*


class AppDbHelper private constructor(context: Context) {
    val context: Context = context
    init {

        mDBConnection = DBCreator.getInstance(context)

        if (db == null)
            db = mDBConnection?.writableDatabase
    }

    companion object : SingletonHolder<AppDbHelper, Context>(::AppDbHelper) {
        const val TABLE_APP_DETAILS = "AppDetails"
        const val PKG_NAME = "PkgName"
        var db: SQLiteDatabase? = null
        var mDBConnection: DBCreator? = null

    }

    /**
     * Lists all the pending packages which are not yet synced to servers AND ARE INSTALLED
     * Condition : [DBCreator] SERVER_UPDATED status is [Constants].SERVER_NOT_UPDATED
     *
     * @return ArrayList of Packages
     */
    val packagesToSync: ArrayList<String>
        @Synchronized get() {

            val selectQuery = "SELECT " + PKG_NAME + " FROM " + TABLE_APP_DETAILS + " WHERE " + DBCreator.SERVER_UPDATED + "= ?  AND " + DBCreator.IS_INSTALLED + "= ? "
            var cursor: Cursor? = null

            val addedPackages = ArrayList<String>()

            try {
                open()
                cursor = db!!.rawQuery(selectQuery, arrayOf(Constants.SyncConstants.SERVER_NOT_UPDATED.toString(), Constants.SyncConstants.APP_INSTALLED.toString()))
                if (cursor!!.moveToFirst()) {
                    do {

                        addedPackages.add(cursor.getString(0).toString())

                    } while (cursor.moveToNext())

                }
            } catch (e: Exception) {
                e.printStackTrace()

            } finally {
                if (cursor != null)
                    cursor.close()
                close()


                return addedPackages
            }
        }





    @Throws(SQLException::class)
    fun open() {
        // db = mDBConnection.getWritableDatabase();
    }

    fun close() {
        // mDBConnection.close();
    }


    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */


    @Synchronized
    fun appServerUpdated(pkgName: String, value: Int) {


        //changing real app status
        val serverUpdated = ContentValues()
        serverUpdated.put(DBCreator.SERVER_UPDATED, value)
        try {
            open()
            val row = db!!.update(TABLE_APP_DETAILS, serverUpdated, "$PKG_NAME='$pkgName'", null)

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            close()
        }
    }

    /**
     * Lists all the pending packages which are not yet synced to servers AND ARE UNINSTALLED
     * Condition : [DBCreator] SERVER_UPDATED status is [Constants].SERVER_NOT_UPDATED
     *
     * @return ArrayList of Packages
     */
    @Synchronized
    fun removeUninstalledPackages(): ArrayList<String> {

        val selectQuery = "SELECT " + PKG_NAME + " FROM " + TABLE_APP_DETAILS + " WHERE " + DBCreator.SERVER_UPDATED + "=" + Constants.SyncConstants.SERVER_NOT_UPDATED + " AND " + DBCreator.IS_INSTALLED + "=" + Constants.SyncConstants.APP_UNINSTALLED
        var cursor: Cursor? = null
        val removedPackages = ArrayList<String>()


        try {
            open()
            cursor = db!!.rawQuery(selectQuery, null)
            if (cursor!!.moveToFirst()) {
                do {

                    removedPackages.add(cursor.getString(0).toString())

                } while (cursor.moveToNext())

            }
        } catch (e: Exception) {
            e.printStackTrace()

        } finally {
            if (cursor != null)
                cursor.close()
            close()


            return removedPackages
        }

    }



}