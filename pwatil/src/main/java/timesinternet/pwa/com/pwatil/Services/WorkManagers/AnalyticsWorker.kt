package timesinternet.pwa.com.pwatil.Services.WorkManagers

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.util.Log
import androidx.work.Worker
import timesinternet.pwa.com.pwautil.R
import android.content.Context.NOTIFICATION_SERVICE
import android.graphics.Color
import android.support.annotation.NonNull
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit


class AnalyticsWorker : Worker() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun doWork(): Result {
        Log.i("WORK_MANAGER", "WORKING NOW....")
        workBeginNotification()
        return Result.SUCCESS
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun workBeginNotification() {
        val nm = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel()
                } else {
                    // If earlier version channel ID is not used
                    // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                    ""
                }
        val notificationBuilder = NotificationCompat.Builder(applicationContext, channelId )
        val notification = notificationBuilder.setOngoing(false)
                .setSmallIcon(R.drawable.ic_play_circle_filled_black_24dp)
                .setContentTitle("SENDING ANALYTICS")
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build()
        nm.notify(12121, notification)


    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String{
        val channelId = "my_service"
        val channelName = "My Background Service"
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_HIGH)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

}