package timesinternet.pwa.com.pwatil.db.dao


import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import timesinternet.pwa.com.pwatil.db.entity.NotificationEntity


@Dao
interface NotificationDao {
    @Insert
    fun insert(notificationEntity: NotificationEntity)

    @Query("DELETE FROM notifications")
    fun deleteAll()

    @Query("SELECT * FROM notifications ")
    fun getAllNotification(): List<NotificationEntity>
}