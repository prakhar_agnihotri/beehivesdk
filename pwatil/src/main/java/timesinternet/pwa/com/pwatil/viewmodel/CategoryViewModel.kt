package timesinternet.pwa.com.pwatil.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import timesinternet.pwa.com.pwatil.Data.CategoryAppRepository
import timesinternet.pwa.com.pwatil.Models.DetailAppModel

class CategoryViewModel : AndroidViewModel {

    constructor(application: Application) : super(application)

    //    val text = ObservableField("old data")
    val isLoading = ObservableField(false)
    var repoModel: CategoryAppRepository = CategoryAppRepository(getApplication())
    var appsList = MutableLiveData<ArrayList<DetailAppModel>>()

    var appHomeList = MutableLiveData<ArrayList<Any>>()


    fun loadApps(typeOfLoadFun: Int) {
        isLoading.set(true)

        repoModel.getCategoryAppsData(object : CategoryAppRepository.OnCateAppsReadyCallback {
            override fun onDataReady(data: ArrayList<Any>) {
                appHomeList.postValue(data)
            }


            override fun onHomeDataReady(data: ArrayList<Any>) {
                isLoading.set(false)
                appHomeList.value=data

            }


        },typeOfLoadFun)


    }


    fun pinApp(app: DetailAppModel) {
        repoModel.pinApp(app)


    }

}