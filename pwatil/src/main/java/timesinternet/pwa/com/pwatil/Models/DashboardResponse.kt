package timesinternet.pwa.com.pwatil.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DashboardResponse {
    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("response")
    @Expose
    var response: ArrayList<CategoryList>? = null

}


class CategoryList {
    @SerializedName("templatetype")
    @Expose
    var templatetype: Int? = null

    @SerializedName("appdisplaycat")
    @Expose
    var appdisplaycat: String? = null

    @SerializedName("list")
    @Expose
    val apps: ArrayList<DetailAppModel>? = null
}







