package timesinternet.pwa.com.pwatil.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import timesinternet.pwa.com.pwatil.Models.CategoryModel
import timesinternet.pwa.com.pwautil.databinding.CategoryItemBinding
import java.util.ArrayList

class CategoryAdapter(private var listener: OnCategoryClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mCategoryList = ArrayList<CategoryModel>()

    fun setAppList(categoryModel: ArrayList<CategoryModel>) {
        mCategoryList.addAll(categoryModel)
        //notifyItemRangeInserted(0, categoryModel.size)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val appInfo = mCategoryList[position]
        (holder as CategoryAdapter.RecyclerHolderCatIcon).bind(appInfo, listener)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = CategoryItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerHolderCatIcon(applicationBinding)
    }

    interface OnCategoryClickListener {
        fun onCategoryClick(position: Int)
    }

    inner class RecyclerHolderCatIcon(private val applicationBinding: CategoryItemBinding) : RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(appInfo: CategoryModel, listener: OnCategoryClickListener?) {
            applicationBinding.catModel = appInfo
            if (listener != null) {
                applicationBinding.imageView7.setOnClickListener({ _ -> listener.onCategoryClick(layoutPosition) })
            }
        }

    }

}