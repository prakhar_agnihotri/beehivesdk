package timesinternet.pwa.com.pwatil.AppStore

import android.app.Activity
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import timesinternet.pwa.com.pwautil.R
import java.util.*


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class WebView_PWA : Activity() {

    val ip = "http://10.150.72.222"
    fun loadPWA(){
        val  url =  intent.getStringExtra("url")
        Log.d("GOT INTENT", url)

    }

    fun addToHomescreen(){
        val shortcutManager = getSystemService(ShortcutManager::class.java)
        val shortcut = ShortcutInfo.Builder(this, "id1")
                .setShortLabel("Web site")
                .setLongLabel("Open the web site")
                .setIcon(Icon.createWithResource(this, R.drawable.abc_ab_share_pack_mtrl_alpha))
                .setIntent(Intent(Intent.ACTION_VIEW,
                        Uri.parse(ip+"/TEST/index.html")))
                .build()

        shortcutManager!!.dynamicShortcuts = Arrays.asList(shortcut)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_web_view__pw)

        val webVW : WebView = findViewById<WebView>(R.id.webVW)

        webVW.settings.javaScriptEnabled = true
        webVW.webViewClient = WebViewClient()


        val samplename:String

        val  url =  intent.getStringExtra("url")
        webVW.loadUrl(url)
    }


}
