package timesinternet.pwa.com.pwatil.Permission.Interface


import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.util.Log
import timesinternet.pwa.com.pwatil.DebugLog.Logs
import timesinternet.pwa.com.pwatil.Permission.Callbacks.PermissionResponse
import timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol.User
import timesinternet.pwa.com.pwatil.Permission.Types.Pwa
import timesinternet.pwa.com.pwatil.SandBox


class UserPwa(appId: String, activity: SandBox) : PwaContainer(appId, activity), User {

    override fun addToHomeScreen(signatureId : String) {
        Log.d(Logs.SDK, "addToHomeScreen()")

        super.isPermittedTo(  Pwa.HOMESCREEN_SHPRTCUT_ACCESS , object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "addToHomeScreen() onSuccess")
                super@UserPwa.addToHomeScreen(signatureId)

            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "addToHomeScreen() onFailure")
            }

        })

    }
    override fun getUserDetails(signatureId : String) {
        Log.d(Logs.SDK, "getUserDetails()")
        super.isPermittedTo(Pwa.USER_DETAIL_ACCESS, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "getUserDetails() onSuccess")
                super@UserPwa.getUserDetails(signatureId)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "getUserDetails() onFailure")
            }

        })
    }

    override fun readSms(signatureId : String) {
        Log.d(Logs.SDK, "readSms()")
        super.isPermittedTo(Pwa.SMS_READ_ACCESS, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "readSms() onSuccess")
                super@UserPwa.readSms(signatureId)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "readSms() onFailure")
            }

        })
    }

    override fun log(signatureId : String, logData: String) {
        Log.d(Logs.SDK, "log()")
        super.isPermittedTo(Pwa.LOG_DATA, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "log() onSuccess")
                super@UserPwa.log(signatureId, logData)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "log() onFailure")
            }

        })
    }

    override fun openCamera(signatureId : String) {
        Log.d(Logs.SDK, "openCamera()")
        super.isPermittedTo(Pwa.CAMERA_ACCESS, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "openCamera() onSuccess")
                super@UserPwa.openCamera(signatureId)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "openCamera() onFailure")
            }

        })
    }

    override fun readContacts(signatureId : String) {
        Log.d(Logs.SDK, "readContacts()")
        super.isPermittedTo(Pwa.CONTACTS_ACCESS, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "readContacts() onSuccess")
                super@UserPwa.readContacts(signatureId)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "readContacts() onFailure")
            }

        })
    }

    override fun phoneCall(signatureId : String) {
        Log.d(Logs.SDK, "phoneCall()")
        super.isPermittedTo(Pwa.MAKE_CALL, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "phoneCall() onSuccess")
                super@UserPwa.phoneCall(signatureId)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "phoneCall() onFailure")
            }

        })
    }

    override fun sendSms(signatureId : String, sendTo: String, text: String) {
        Log.d(Logs.SDK, "sendSms()")
        super.isPermittedTo(Pwa.SMS_SEND_ACCESS, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "sendSms() onSuccess")
                super@UserPwa.sendSms(signatureId , sendTo, text)

            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "sendSms() onFailure")
            }

        })
    }

    override fun runInBackground(signatureId : String) {
        Log.d(Logs.SDK, "runInBackground()")
        super.isPermittedTo(Pwa.RUN_IN_BACKGROUND, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "runInBackground() onSuccess")
                super@UserPwa.runInBackground(signatureId)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "runInBackground() onFailure")
            }

        })
    }

    override fun readLocation(signatureId : String) {
        Log.d(Logs.SDK, "readLocation()")
        super.isPermittedTo(Pwa.LOCATION_ACCESS, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "readLocation() onSuccess")
                super@UserPwa.readLocation(signatureId)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "readLocation() onFailure")
            }

        })
    }

    override fun notifyUser(signatureId : String, title: String, description: String) {
        Log.d(Logs.SDK, "notifyUser()")
        super.isPermittedTo(Pwa.NOTIFICATION_ACCESS, object : PermissionResponse{
            override fun onSuccess() {
                Log.d(Logs.DATA_LAYER, "notifyUser() onSuccess")
                super@UserPwa.notifyUser(title, description, signatureId)
            }

            override fun onFailure() {
                Log.d(Logs.DATA_LAYER, "notifyUser() onFailure")
            }

        })
    }




}