package timesinternet.pwa.com.pwatil.sajal

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwatil.db.DbHelper
import timesinternet.pwa.com.pwatil.db.dao.AppListDao


class TestRepository(application: Application) {
    val TAG = "dbhelper"
    private val appListDao: AppListDao
    private lateinit var listLiveData: List<DetailAppModel>

    init {
        val dbHelper = DbHelper.getInstance(application)
        appListDao = dbHelper?.appDao()!!

    }


    fun insert(app: DetailAppModel) {
        Log.d(TAG, "insert: " + app.toString());
        insertAsyncTask(appListDao).execute(app)
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: AppListDao) : AsyncTask<DetailAppModel, Void, Void>() {
        val TAG = "dbhelper"
        override fun doInBackground(vararg params: DetailAppModel): Void? {
            mAsyncTaskDao.insert(params[0])


//            val arr=ArrayList<AppListEntity>()
//            arr.add(params[0])
//            mAsyncTaskDao.insert(arr)
            return null
        }

        override fun onPostExecute(result: Void?) {
            Log.d(TAG, "onPostExecute: insertion completed");

        }
    }


    fun getAllItems() {


        async(UI) {

            Log.d(TAG, "getAllItems: i am here running in corotines")
            bg {

                listLiveData = appListDao.getAllApps()
                Log.d(TAG, "getAllItems: size of list is " + listLiveData.size)

                listLiveData.forEach { customName ->
                    Log.d(TAG, "getAllItems: " + customName.toString())
                }


            }

        }

    }

    fun updatePinnedStatus(status: Int, appId: String) {

        async(UI) {
            bg {
                appListDao.updatePinnedStatus(status, appId)
            }
        }

    }


    fun getPinnedApps(pinStatus: Int) {


        async(UI) {

            bg {

                listLiveData = appListDao.getAppsByStatus(pinStatus)
                Log.d(TAG, "getAllItems: size of list is " + listLiveData.size)

                listLiveData.forEach { customName ->
                    Log.d(TAG, "getAllItems: " + customName.toString())
                }


            }

        }

    }

}

