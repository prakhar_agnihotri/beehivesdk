package timesinternet.pwa.com.pwatil.container

import android.telephony.SmsManager
import android.util.Log
import android.webkit.JavascriptInterface
import timesinternet.pwa.com.pwatil.Data.DummyData
import timesinternet.pwa.com.pwatil.DebugLog.Logs
import timesinternet.pwa.com.pwatil.Permission.Interface.UserPwa
import timesinternet.pwa.com.pwatil.Permission.Permit


class BridgeNew(val bridgeInterfaceUserPwa: UserPwa) : BridgeProtocol {

    @JavascriptInterface
    override fun someFunction() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @JavascriptInterface
    override fun openCamera(signatureId: String) {
        Log.d(Logs.SDK_PERMISSION, "openCamera")
        bridgeInterfaceUserPwa.openCamera(signatureId)
    }

    @JavascriptInterface
    override fun log(signatureId: String, logData: String) {
        bridgeInterfaceUserPwa.log(signatureId, logData)
    }

    @JavascriptInterface
    override fun readContacts(signatureId: String) {
        bridgeInterfaceUserPwa.readContacts(signatureId)
    }

    @JavascriptInterface
    override fun phoneCall(signatureId: String) {
        bridgeInterfaceUserPwa.phoneCall(signatureId)
    }

    @JavascriptInterface
    override fun sendSms(signatureId: String, number: String, message: String) {
        bridgeInterfaceUserPwa.sendSms(signatureId, number, message)
    }

    @JavascriptInterface
    override fun runInBackground(signatureId: String) {
        bridgeInterfaceUserPwa.runInBackground(signatureId)
    }

    @JavascriptInterface
    override fun notifyUser(signatureId: String, title: String, description: String) {
        bridgeInterfaceUserPwa.notifyUser(signatureId, title, description)
    }

    @JavascriptInterface
    override fun readLocation(signatureId: String) {
        bridgeInterfaceUserPwa.readLocation(signatureId)
    }

    @JavascriptInterface
    override fun getUserDetails(signatureId: String) {
        bridgeInterfaceUserPwa.getUserDetails(signatureId)
    }

    @JavascriptInterface
    override fun readSms(signatureId: String) {
        bridgeInterfaceUserPwa.readSms(signatureId)
    }

    @JavascriptInterface
    override fun addToHomeScreen(signatureId: String) {
        bridgeInterfaceUserPwa.addToHomeScreen(signatureId)
    }


}