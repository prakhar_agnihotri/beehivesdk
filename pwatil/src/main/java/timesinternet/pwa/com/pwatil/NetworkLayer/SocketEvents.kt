package timesinternet.pwa.com.pwatil.NetworkLayer

import com.example.proto.NotificationOuterClass

interface SocketEvents {
    fun  notify(notification : NotificationOuterClass.Notification)
}