package timesinternet.pwa.com.pwatil.NetworkLayer

import android.util.Log
import com.example.proto.NotificationOuterClass


import org.jetbrains.anko.doAsync
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.Socket
import java.nio.ByteBuffer
import java.util.*

class UdpSocketHandler{

    var HOST : String
    var PORT: Int
    var SOCKET : DatagramSocket?= null
    var INPUT_STREAM : InputStream?= null
    var OUTPUT_STREAM : OutputStream? = null

    var META_OUTPUT_STREAM : ByteArrayOutputStream
    var META_READ = false
    val META_BYTES = 3
    var PARTIAL_LENGTH = 0
    var PARTIAL_TYPE = 0


    val SLEEP_TIME = 3


    val SocketNotifier: SocketNotifier

    constructor(Host : String, Port: Int ,socketNotifier :  SocketNotifier ){
        HOST = Host
        PORT = Port
        META_OUTPUT_STREAM = ByteArrayOutputStream()
        SocketNotifier = socketNotifier
        Connect()

    }

    fun Close(){
        SOCKET?.close()
        SOCKET = null
    }

    fun Connect(){
        Log.d("Trying to Connect", "To Server")

            try{
                Log.d("UDP SOCKET", "CONNECTING")
                SOCKET = DatagramSocket()
                Log.d("UDP SOCKET", "READING")
                doAsync {

                    Read()
                }

            } catch (exc: Exception){
                Log.d("UDP SOCKET", "FAILED")
                Thread.sleep(SLEEP_TIME.toLong() * 1000)
//                Reconnect()

            }


    }

    fun Reconnect(){
        Close()
        Connect()
    }

    fun bytesToUnsignedShort(byte1 : Byte, byte2 : Byte, bigEndian : Boolean) : Int {
        if (bigEndian)
            return (((byte1.toInt() and 255) shl 8) or (byte2.toInt() and 255))

        return (((byte2.toInt() and 255) shl 8) or (byte1.toInt() and 255))

    }

    fun Read(){

        val RecievedDatagramSocket = ByteArray(1024)
        val DataGramPacket = DatagramPacket(RecievedDatagramSocket, RecievedDatagramSocket.size)

        SOCKET?.connect( InetAddress.getByName(HOST) , PORT)
        SOCKET?.soTimeout = 2000
        while (true){
            try{
                SOCKET?.receive(DataGramPacket).let {
                    val MSG_TYPE = RecievedDatagramSocket[0].toChar()
                    val MSG_LEN = bytesToUnsignedShort(RecievedDatagramSocket[1], RecievedDatagramSocket[2], false)

                    val buf =    Arrays.copyOfRange(RecievedDatagramSocket, 3, MSG_LEN)

                }
            } catch (exc: Exception) {



            }

        }


    }


    fun Write(socketFrame : ByteArray){
        write(socketFrame)
    }

    fun Write(socketFrame : SocketFrame){
        write(socketFrame)
    }

    fun write(socketFrame : SocketFrame){

        doAsync {
            try{
                val datagramPacket = socketFrame.CreateFrame()
                SOCKET?.send( DatagramPacket(  datagramPacket, datagramPacket.size  , InetAddress.getByName(HOST) , PORT ) )

            } catch (exc: Exception){

            }
        }


    }

    fun write(socketFrame : ByteArray){

        doAsync {
            try{
                val datagramPacket = socketFrame
                SOCKET?.send( DatagramPacket(  datagramPacket, datagramPacket.size  , InetAddress.getByName(HOST) , PORT ) )
            } catch (exc: Exception){
            }
        }


    }




}
