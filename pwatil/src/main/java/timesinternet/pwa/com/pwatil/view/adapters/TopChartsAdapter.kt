package timesinternet.pwa.com.pwatil.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import timesinternet.pwa.com.pwatil.Models.CategoryModel
import timesinternet.pwa.com.pwatil.Models.TopChartsModel
import timesinternet.pwa.com.pwautil.databinding.CategoryItemBinding
import timesinternet.pwa.com.pwautil.databinding.TopChartsItemBinding
import java.util.ArrayList

class TopChartsAdapter(private var listener: TopChartsAdapter.OnTopChartClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    private val mCategoryList = ArrayList<TopChartsModel>()

    fun setAppList(categoryModel : ArrayList<TopChartsModel>) {
        mCategoryList.addAll(categoryModel)
        //notifyItemRangeInserted(0, categoryModel.size)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val appInfo = mCategoryList[position]
        (holder as TopChartsAdapter.RecyclerHolderCatIcon).bind(appInfo,listener)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = TopChartsItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerHolderCatIcon(applicationBinding)
    }

    interface OnTopChartClickListener {
        fun onTopChartClick(position: Int)
    }

    inner class RecyclerHolderCatIcon(private val applicationBinding: TopChartsItemBinding) : RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(appInfo: TopChartsModel,listener: OnTopChartClickListener?) {
            applicationBinding.catModel=appInfo
            if (listener != null) {
                applicationBinding.imageView8.setOnClickListener({ _ -> listener.onTopChartClick(layoutPosition) })
            }
        }

    }

}


