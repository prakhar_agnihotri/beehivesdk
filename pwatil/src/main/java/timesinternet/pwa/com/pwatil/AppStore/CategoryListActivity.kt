package timesinternet.pwa.com.pwatil.AppStore

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_category_list.*
import timesinternet.pwa.com.pwatil.Adapters.CategoryAppAdapter
import timesinternet.pwa.com.pwatil.Models.DataModels
import timesinternet.pwa.com.pwautil.R

class CategoryListActivity : AppCompatActivity() {

    // Initializing an empty ArrayList to be filled with animals
    val apps: ArrayList<DataModels.AppDetail> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_list)
        addApps()
        // Creates a vertical Layout Manager
        rv_category_list.layoutManager = LinearLayoutManager(this)

        // You can use GridLayoutManager if you want multiple columns. Enter the number of columns as a parameter.
//        rv_animal_list.layoutManager = GridLayoutManager(this, 2)

        // Access the RecyclerView Adapter and load the data into it
        rv_category_list.adapter = CategoryAppAdapter(apps, this)
    }

    // Adds animals to the empty animals ArrayList
    fun addApps() {
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps.add(DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps . add (DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps . add (DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps . add (DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps . add (DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps . add (DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps . add (DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))
        apps . add (DataModels.AppDetail("Tik Tok", "Including Musical.ly",
                "", android.R.drawable.sym_def_app_icon, 4.3))


    }
}
