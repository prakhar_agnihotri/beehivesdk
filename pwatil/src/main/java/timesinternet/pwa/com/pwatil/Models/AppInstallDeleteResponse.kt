package timesinternet.pwa.com.pwatil.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AppInstallDeleteResponse {

    @SerializedName("deviceId")
    @Expose
    var deviceID: String? = null
    @SerializedName("appId")
    @Expose
    var appId: String? = null
}