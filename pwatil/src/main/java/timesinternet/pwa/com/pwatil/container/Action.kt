package timesinternet.pwa.com.pwatil.container

object Action {

    val publishUserData: String = "publishUserData"
    val publishGpsData: String = "publishGpsData"
    val openCamera: String = "openCamera"
    val sendNotification: String = "sendNotification"
    val smsRecieved: String = "smsRecieved"
    val someFunction: String = "someFunction"
}