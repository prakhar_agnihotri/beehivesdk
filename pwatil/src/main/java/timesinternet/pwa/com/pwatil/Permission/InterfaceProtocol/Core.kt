package timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol

import android.support.v7.app.AppCompatActivity
import timesinternet.pwa.com.pwatil.Permission.Callbacks.PermissionResponse
import timesinternet.pwa.com.pwatil.SandBox

interface   Core  {
    fun openCamera(signatureId : String, activity: SandBox)
    fun grantUserDetails(signatureId : String, activity: SandBox)
}