package timesinternet.pwa.com.pwatil.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import timesinternet.pwa.com.pwatil.Models.DetailAppModel


@Dao
interface AppListDao {

    @Insert
    fun insert(appListEntity: DetailAppModel)

    @Insert
    fun insertAll(appListEntity: ArrayList<DetailAppModel>)

    @Query("DELETE FROM app_list")
    fun deleteAll()

    @Query("SELECT * FROM app_list ")
    fun getAllApps(): List<DetailAppModel>

    @Query("UPDATE app_list SET pin = :status WHERE si = :appId")
    fun updatePinnedStatus(status: Int, appId: String)


    @Query("SELECT * FROM app_list where pin = :pinStatus")
    fun getAppsByStatus(pinStatus : Int): List<DetailAppModel>

}