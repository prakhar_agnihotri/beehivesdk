package timesinternet.pwa.com.pwatil.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwatil.db.dao.AnalyticsDao
import timesinternet.pwa.com.pwatil.db.dao.AppListDao
import timesinternet.pwa.com.pwatil.db.dao.NotificationDao
import timesinternet.pwa.com.pwatil.db.entity.AnalyticsEntity
import timesinternet.pwa.com.pwatil.db.entity.AppListEntity
import timesinternet.pwa.com.pwatil.db.entity.NotificationEntity
import timesinternet.pwa.com.timespwa.Utils.SingletonHolder

@Database(entities = [NotificationEntity::class, DetailAppModel::class, AnalyticsEntity::class],
        version = 1)
abstract class DbHelper : RoomDatabase() {

    /**
     * Returns the Database Access Object to deal with notification.
     * @return the Database Access Object to deal with notification
     */
    abstract fun notificationDao(): NotificationDao

    /**
     * Returns the Database Access Object to deal with apps.
     * @return the Database Access Object to deal with apps
     */
    abstract fun appDao(): AppListDao

    /**
     * Returns the Database Access Object to deal with analytics.
     * @return the Database Access Object to deal with analytics
     */
    abstract fun analyticsDao(): AnalyticsDao



    companion object : SingletonHolder<DbHelper, Context>({
        Room.databaseBuilder(it.applicationContext,
                DbHelper::class.java, "pwa.db")
                .build()
    })


}