package timesinternet.pwa.com.timespwa.Services.Alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import timesinternet.pwa.com.timespwa.Services.OnlineDbSyncService
import timesinternet.pwa.com.timespwa.Utils.SingletonHolder
import kotlin.reflect.KClass


class AlarmHandler private constructor(context: Context) {

    val context: Context = context

    init {
        // Init using context argument
    }

    companion object : SingletonHolder<AlarmHandler, Context>(::AlarmHandler)

    /***
     * @requestCode: Request code for the alarm
     * @startMillis : When to start Alarm
     * @interval: Alarm Interval
     * @alarmIntent: Pending Indent for alarm
     *
     */
    fun setAlarm(requestCode: Int, startMillis: Long, interval: Long, alarmIntent : Intent) {
        val pIntent = PendingIntent.getService(context, requestCode, alarmIntent, 0)

        val alarm = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        // Start service every hour
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, startMillis, interval , pIntent)

    }

    /**
     * @requestCode: Cancel alarm for that requestCode
     */
    fun cancelAlarm(requestCode: Int) {
        val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, OnlineDbSyncService::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0)
        am.cancel(pendingIntent)
        pendingIntent.cancel()
    }
}

