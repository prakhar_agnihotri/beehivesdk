package timesinternet.pwa.com.timespwa.Notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.graphics.Color
import android.os.Build
import android.app.NotificationChannelGroup
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE


fun removeNotiChannel(channelId: String) {
    getNotificationManager()?.deleteNotificationChannel(channelId)
}


fun createNotificationChannel(groupName: String, channelName: String, channelDesc: String, channelId: String) {


    // The id of the group.
    val groupId = "my_group_01"
// The user-visible name of the group.
    val notificationManager = getNotificationManager()
    notificationManager.createNotificationChannelGroup(NotificationChannelGroup(groupId, groupName))


    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val name = channelName
        val description = channelDesc
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelId, name, importance)
        channel.description = description
        channel.lightColor = Color.RED
        channel.enableVibration(true)
        channel.vibrationPattern =
                longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        channel.group = groupId
        notificationManager?.createNotificationChannel(channel)
    }


}