package timesinternet.pwa.com.timespwa.view.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import timesinternet.pwa.com.pwatil.AppStore.CategoryListFragment
import timesinternet.pwa.com.pwautil.R


class AppStoreParentFragment : Fragment() {
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.supportFragmentManager
                .beginTransaction()
                .replace(R.id.frameLayout, CategoryListFragment(), "")
                .commit()
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.play_store, container, false)
    }

}