package timesinternet.pwa.com.pwatil.Permission

import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import timesinternet.pwa.com.pwatil.DebugLog.Logs


class PermissionManager( var context: Context , var permissionChannel: PermssionChannel?) {
    private var locationManager : LocationManager? = context.getSystemService(LOCATION_SERVICE) as LocationManager?;



    var nativePermission: NativePermission? = null

//    fun requestNewLocation() {
//        Log.d(Logs.CONTAINER , "requestNewLocation" + permissionChannel )
//        nativePermission  = NativePermission(context)
//        nativePermission?.startLocationUpdates(permissionChannel)
//
//
//        }

    fun invalidatePermissionChannel(permissionChannel: PermssionChannel?){
        this.permissionChannel = permissionChannel
    }


    private val locationListener: LocationListener = object : LocationListener {

        override fun onLocationChanged(location: Location) {
            Log.d(Logs.DATA_LAYER, "onLocationChanged" )
            permissionChannel?.publishLocation(location.latitude.toString() + ";" + location.longitude.toString())

        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            Log.d(Logs.DATA_LAYER, "onStatusChanged" )
        }
        override fun onProviderEnabled(provider: String) {
            Log.d(Logs.DATA_LAYER, "onProviderEnabled" )
        }
        override fun onProviderDisabled(provider: String) {
            Log.d(Logs.DATA_LAYER, "onProviderDisabled" )
        }
    }
}