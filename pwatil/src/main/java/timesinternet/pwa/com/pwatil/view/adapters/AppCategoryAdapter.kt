package timesinternet.pwa.com.pwatil.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import timesinternet.pwa.com.pwatil.Models.DetailAppModel

import timesinternet.pwa.com.pwautil.databinding.CategoryAppItemBinding

class AppCategoryAdapter(private var items: ArrayList<DetailAppModel>,
                         private var listener: OnItemClickListener) : RecyclerView.Adapter<AppCategoryAdapter.ViewHolder>() {


    fun replaceData(arrayList: ArrayList<DetailAppModel>) {
        items = arrayList
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppCategoryAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val binding = CategoryAppItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

//    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.bind(items[position], listener)
    }

    override fun getItemCount(): Int = items.size

    interface OnItemClickListener {
        fun onItemClick(position: Int)
        fun onLaunchClick(position: Int)
    }

    class ViewHolder(private var binding: CategoryAppItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(repo: DetailAppModel, listener: OnItemClickListener?) {
            binding.repository = repo
            if (listener != null) {
                binding.row.setOnClickListener({ _ -> listener.onItemClick(layoutPosition) })
                binding.launch.setOnClickListener({ _ -> listener.onLaunchClick(layoutPosition) })
            }

            binding.executePendingBindings()
        }
    }



}