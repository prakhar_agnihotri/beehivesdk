package timesinternet.pwa.com.pwatil.NetworkLayer

import android.R.attr.x

import android.R.array
import java.nio.ByteBuffer
import java.nio.ByteOrder


class SocketFrame{


    var MESSAGE_LENGTH : Int = 0
    lateinit var MESSAGE_DATA : ByteArray
    var MESSAGE_TYPE : Char = 0.toChar()

    constructor(type: Char,  data : ByteArray){
        MESSAGE_TYPE = type
        MESSAGE_LENGTH = data.size
        MESSAGE_DATA = data
    }

    constructor(type: Char,  data : String){
        MESSAGE_TYPE = type
        MESSAGE_LENGTH = data.length
        MESSAGE_DATA = data.toByteArray()
    }


    fun IntToByteArray(number: Int){

        val ret = ByteArray(2)
        ret[0] = (number and 0xff).toByte()
        ret[1] = (number shr 8 and 0xff).toByte()
    }
    fun CreateFrame( ) : ByteArray {
        val MESSAGE_LENGTH = MESSAGE_DATA.size
        val MESSAGE_TYPE = MESSAGE_TYPE.toByte()
        return byteArrayOf(MESSAGE_TYPE)  + longToBytes(MESSAGE_LENGTH.toShort())  + MESSAGE_DATA

    }

    fun longToBytes(x: Short): ByteArray {
        val buffer = ByteBuffer.allocate(2)
        buffer.order(ByteOrder.LITTLE_ENDIAN)
        buffer.putShort(x)

        return buffer.array()
    }


}