package timesinternet.pwa.com.pwatil.Models

class DataModels {


    data class UserData(
            var fname: String,
            var lname: String,
            var phone: String,
            var email: String,
            var dob: String,
            var gender: String,
            var profilePic: String

    )


    data class CameraData(
            val picUrl: String,
            val thumbUrl: String,
            val b64: String
    )

    data class GpsData(
            val lat: String,
            val lon: String
    )

    data class SmsData(
            val sender: String,
            val body: String
    )


    /*
    * @param [appId] appId to fetch from bridge.
    * @param [url] url of PWA app
    * @param [title] title of the notification.
    * @param [subTitle].
    * @param [icon] AppIcon of in case of default notification.
    *  @param [tickerIcon] icon that tickers on the notification bar.
    *  @param [notiType] big_notification or small_notification.
    *  @param [sound] notification sound on/off.
    *  @param [priority] priority of the notification.
    */

    data class NotificationData(var appId: String,
                                var notificationId: Int,
                                var url: String,
                                var title: String,
                                var subTitle: String,
                                var icon: String,
                                var tickerIcon: String,
                                var tickerText: String,
                                var notiType: String,
                                var sound: String,
                                var priority: Int)

    data class AppDetail(
            val appName: String,
            val devName: String,
            val thumbUrl: String,
            val drawable: Number,
            val rating: Number
    )

}
