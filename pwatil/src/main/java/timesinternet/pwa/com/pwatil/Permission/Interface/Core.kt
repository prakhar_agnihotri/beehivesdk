package timesinternet.pwa.com.pwatil.Permission.Interface

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v7.app.AppCompatActivity
import android.util.Log
import org.jetbrains.anko.webView
import timesinternet.pwa.com.pwatil.DebugLog.Logs
import timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol.Core
import timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol.Sdk
import timesinternet.pwa.com.pwatil.SandBox
import android.graphics.Bitmap
import android.R.attr.bitmap
import android.telephony.SmsManager
import android.util.Base64
import com.google.gson.Gson
import timesinternet.pwa.com.pwatil.Data.DummyData
import java.io.ByteArrayOutputStream
import android.widget.Toast




object Core : Core{
    override fun grantUserDetails(signatureId: String, activity: SandBox) {

        activity.publishMarshalledData(signatureId, Gson().toJson( DummyData.userDetails) )

    }

    private var notificationManager: NotificationManager? = null


    fun sendSMS(phoneNo: String, msg: String) {
        try {
            val smsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phoneNo, null, msg, null, null)
            Log.d("SENT SMS", phoneNo + " -> " + msg)
        } catch (ex: Exception) {
            Log.d("SENT SMS", "FAILED")
            ex.printStackTrace()
        }

    }


    override fun openCamera(signatureId : String, activity: SandBox) {
        startCamera(signatureId, activity)
    }



    private fun startCamera(signatureId : String,activity: SandBox) {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        takePictureIntent.putExtra("signatureId","hgfd")
        if (takePictureIntent.resolveActivity(activity.packageManager) != null) {
            activity.startActivityForResult(takePictureIntent, signatureId.toInt())
        }
    }

    fun onCameraResult(signatureId : String?,activity: SandBox) {

            Log.d( Logs.SDK, "Image Clicked: "+ signatureId!!)


    }


    private fun createNotificationChannel(id: String, name: String,
                                          description: String) {

        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(id, name, importance)

        channel.description = description
        channel.enableLights(true)
        channel.lightColor = Color.RED
        channel.enableVibration(true)
        channel.vibrationPattern =
                longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
//        notificationManager?.createNotificationChannel(channel)
    }

    fun initNotificationManager(activity: AppCompatActivity){
//        notificationManager = activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        createNotificationChannel(
                "com.ebookfrenzy.notifydemo.news",
                "NotifyDemo News",
                "Example News Channel")
    }

    fun sendNotification(activity: AppCompatActivity, title: String, content: String) {
        val notificationID = 108
        val channelID = "com.ebookfrenzy.notifydemo.news"

        val notification = Notification.Builder(activity,
                channelID)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setChannelId(channelID)
                .build()
//        notificationManager?.notify(notificationID, notification)
    }

    fun toBase64(bitmap: Bitmap) : String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
        return encoded
    }



}