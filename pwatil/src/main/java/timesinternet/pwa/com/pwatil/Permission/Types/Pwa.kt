package timesinternet.pwa.com.pwatil.Permission.Types

import timesinternet.pwa.com.pwatil.Permission.Layers.GenericPermission

object Pwa : GenericPermission(){

    val CAMERA_ACCESS = "Camera"
    val LOCATION_ACCESS = "Location Access"
    val HOMESCREEN_SHPRTCUT_ACCESS = "Add To Homescreen"
    val NOTIFICATION_ACCESS = "Notification"
    val SMS_READ_ACCESS = "Read Sms"
    val SMS_SEND_ACCESS = "Send Sms"
    val MAKE_CALL= "Call"
    val CONTACTS_ACCESS= "Contacts"
    val USER_DETAIL_ACCESS= "User Login Details"
    val LOG_DATA= "Log"
    val RUN_IN_BACKGROUND= "Run in background"


}