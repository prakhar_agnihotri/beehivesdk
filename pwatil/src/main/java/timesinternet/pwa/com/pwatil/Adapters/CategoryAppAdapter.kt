package timesinternet.pwa.com.pwatil.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.category_app_item.view.*
import org.jetbrains.anko.imageResource
import timesinternet.pwa.com.pwatil.Models.DataModels
import timesinternet.pwa.com.pwautil.R

class CategoryAppAdapter(val items: ArrayList<DataModels.AppDetail>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.category_app_item, parent, false))
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.appName?.text = items.get(position).appName
        holder?.appDev?.text = items.get(position).devName
        holder?.appRating?.text = items.get(position).rating.toString()
        holder?.appIcon?.imageResource = items.get(position).drawable.toInt()
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val appName = view.app_name
    val appDev = view.app_dev
    val appRating = view.app_rating
    val appIcon = view.app_icon
}