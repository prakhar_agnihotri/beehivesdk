package timesinternet.pwa.com.pwatil.Data

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import timesinternet.pwa.com.pwatil.db.DbHelper
import timesinternet.pwa.com.pwatil.db.dao.AnalyticsDao
import timesinternet.pwa.com.pwatil.db.entity.AnalyticsEntity

class AnalyticsRepository(application: Application) {
    val TAG = "dbhelper"
    private val analyticsDao: AnalyticsDao
    private lateinit var listLiveData: List<AnalyticsEntity>

    init {
        val dbHelper = DbHelper.getInstance(application)
        analyticsDao = dbHelper?.analyticsDao()!!

    }


    fun insert(analytics: AnalyticsEntity) {
        Log.d(TAG, "insert: " + analytics.toString());
        insertAsyncTask(analyticsDao).execute(analytics)
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: AnalyticsDao) : AsyncTask<AnalyticsEntity, Void, Void>() {
        val TAG = "dbhelper"
        override fun doInBackground(vararg params: AnalyticsEntity): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }

        override fun onPostExecute(result: Void?) {
            Log.d(TAG, "onPostExecute: insertion completed");

        }
    }


    fun getAllItems() {


        async(UI) {

            Log.d(TAG, "getAllItems: i am here running in corotines")
            bg {

                listLiveData = analyticsDao.getAllAnalytics()
                Log.d(TAG, "getAllItems: size of list is " + listLiveData.size)

                listLiveData.forEach { customName ->
                    Log.d(TAG, "getAllItems: " + customName.toString())
                }


            }

        }

    }


}