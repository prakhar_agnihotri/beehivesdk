package timesinternet.pwa.com.pwatil.DebugLog


object Logs {
    val CONTAINER = "_DEBUG_CONTAINER"
    val SDK = "_DEBUG_SDK"
    val SDK_PERMISSION = "_SDK_PERMISSION"
    val DATA_LAYER = "_DEBUG_DATA_LAYER"
    val JS_LAYER = "_DEBUG_JS_LAYER"

}