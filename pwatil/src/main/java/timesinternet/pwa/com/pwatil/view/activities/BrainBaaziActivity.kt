package timesinternet.pwa.com.pwatil.view.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import timesinternet.pwa.com.pwatil.view.fragments.BrainBaaziFragment
import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.timespwa.Services.OnlineDbSyncService

class BrainBaaziActivity : AppCompatActivity() {
    var lastShownNotificationId = 0


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.brain_baazi_home)
        startService(Intent(applicationContext, OnlineDbSyncService::class.java))

//        val myWork = PeriodicWorkRequest.Builder(AnalyticsWorker::class.java, 16, TimeUnit.MINUTES, 5, TimeUnit.SECONDS).build()
//        WorkManager.getInstance()
//                .enqueueUniquePeriodicWork("jobTag", ExistingPeriodicWorkPolicy.KEEP, myWork)
        val bbFrag = BrainBaaziFragment()
        addFragment(bbFrag)

    }


    fun replaceFragment(fragment: Fragment) {


//        isDetailedOpen = true

        supportFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.bb_frag, fragment, "")
                .commit()
    }

    fun addFragment(fragment: Fragment) {


//        isDetailedOpen = true

        supportFragmentManager
                .beginTransaction()
                .add(R.id.bb_frag, fragment, "")
                .commit()
    }


}
