package timesinternet.pwa.com.pwatil.Data


import android.content.Context
import android.util.Log
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timesinternet.pwa.com.pwatil.Constants
import timesinternet.pwa.com.pwatil.Models.*
import timesinternet.pwa.com.pwatil.Utils.apiUtils.ApiUtils
import timesinternet.pwa.com.pwatil.Utils.apiUtils.ApiUtils.enqueueWithRetry
import timesinternet.pwa.com.pwatil.db.DbHelper
import timesinternet.pwa.com.pwatil.db.dao.AppListDao
import timesinternet.pwa.com.timespwa.Interfaces.APiInterfaces.APIInterface


class CategoryAppRepository(val context: Context) {
    val TAG: String = "CATEGORYAPPADAPTER"
    private val appListDao: AppListDao

    init {
        val dbHelper = DbHelper.getInstance(context)
        appListDao = dbHelper?.appDao()!!
    }

    fun getCategoryAppsData(onRepositoryReadyCallback: OnCateAppsReadyCallback, typeOfLoadFun: Int) {

        var categoryList = ArrayList<CategoryList>()
        var list = ArrayList<DetailAppModel>()
        var listHome = ArrayList<Any>()

        var mAPIService: APIInterface? = null
        mAPIService = ApiUtils.getDashBoardClient()

        val requestParams = DashboardRequest(0, 50, "BazziNow", "trivia", "Game")


        enqueueWithRetry(mAPIService!!.getDashboardApps(requestParams), ApiUtils.DEFAULT_RETRIES, object : Callback<DashboardResponse> {
            override fun onResponse(call: Call<DashboardResponse>, response: Response<DashboardResponse>) {

                val resp = response.body()

                if (response.isSuccessful && resp != null && resp.status == 200) {

                    val categoryList = resp.response
                    if (categoryList != null && categoryList.isNotEmpty()) {

                        for (i in 0..categoryList.size - 1) {
                            val catItem = categoryList[i]
                            when (catItem.templatetype) {
                                Constants.AppStoreViewTypes.BANNER_VIEW_TYPE -> {
                                    listHome.add(HeaderItemViewType(catItem.appdisplaycat!!))
                                    val bannerViewTypeItem = BannerViewTypeItem(catItem.apps!!)
                                    listHome.add(bannerViewTypeItem)
                                    insert(catItem.apps!!, Constants.AppStoreViewTypes.BANNER_VIEW_TYPE)

                                }
                                Constants.AppStoreViewTypes.HORIZONTAL_LIST_VIEW_TYPE -> {
                                    listHome.add(HeaderItemViewType(catItem.appdisplaycat!!))
                                    val horizontalListViewType = HorizontalListViewType(catItem.apps!!)
                                    listHome.add(horizontalListViewType)
                                    insert(catItem.apps!!, Constants.AppStoreViewTypes.HORIZONTAL_LIST_VIEW_TYPE)
                                }
                                Constants.AppStoreViewTypes.HORIZONTAL_LIST_SMALL_VIEW_TYPE -> {
                                    listHome.add(HeaderItemViewType(catItem.appdisplaycat!!))
                                    val horizontalListViewType = HorizontalListViewType(catItem.apps!!)
                                    listHome.add(horizontalListViewType)
                                    insert(catItem.apps!!, Constants.AppStoreViewTypes.HORIZONTAL_LIST_SMALL_VIEW_TYPE)
                                }
                                Constants.AppStoreViewTypes.VERTICAL_LIST_ITEM_VIEW_TYPE -> {
                                    listHome.add(HeaderItemViewType(catItem.appdisplaycat!!))
                                    listHome.addAll(catItem.apps!!)
                                    insert(catItem.apps!!, Constants.AppStoreViewTypes.VERTICAL_LIST_ITEM_VIEW_TYPE)
                                }
                                Constants.AppStoreViewTypes.GRID_LIST_ITEM_VIEW_TYPE -> {
                                    listHome.add(HeaderItemViewType(catItem.appdisplaycat!!))
                                    listHome.addAll(catItem.apps!!)
                                    insert(catItem.apps!!, Constants.AppStoreViewTypes.GRID_LIST_ITEM_VIEW_TYPE)
                                }


                            }
                        }

                    }
                    getRecentlyOpenedApps(onRepositoryReadyCallback, listHome)

                    Log.d(TAG, "detailResponse code " + response.code() + " body of app details" + response.body().toString())


                } else {
                    Log.d(TAG, "detailResponse is unsuccessful")
                    // getRecentlyOpenedApps(onRepositoryReadyCallback)
                    getAppsFromDB(onRepositoryReadyCallback)
                }
            }

            override fun onFailure(call: Call<DashboardResponse>, t: Throwable) {
                Log.e(TAG, t.message)


                getAppsFromDB(onRepositoryReadyCallback)

            }
        })


    }

    fun insert(apps: ArrayList<DetailAppModel>, templateType: Int?) {
        Log.d(TAG, "insert: " + apps.toString());

        async(UI) {

            Log.d(TAG, "getAllItems: i am here running in corotines")
            bg {

              //  appListDao.deleteAll()

                apps.forEach { customName ->
                    customName.templateType = templateType
                }

                appListDao.insertAll(apps)
            }
        }

    }


    fun pinApp(app: DetailAppModel) {
        Log.d(TAG, "insert: " + app.toString());

        async(UI) {

            Log.d(TAG, "getAllItems: i am here running in corotines")
            bg {
                appListDao.updatePinnedStatus(Constants.Messages.PIN, app.appId!!)

            }
        }

    }

    fun getAppsFromDB(onRepositoryReadyCallback: OnCateAppsReadyCallback) {

        val listHome = ArrayList<Any>()
        doAsync {
            //Execute all the lon running tasks here
            var apps = appListDao.getAllApps()
            uiThread {


                listHome.add(HeaderItemViewType("Recently Used"))
                val horizontalListViewType = HorizontalListViewType(apps as ArrayList<DetailAppModel>)
                listHome.add(horizontalListViewType)

                listHome.add(HeaderItemViewType("Featured"))

                val bannerViewTypeItem = BannerViewTypeItem(apps as ArrayList<DetailAppModel>)
                listHome.add(bannerViewTypeItem)

                listHome.add(HeaderItemViewType("Other Apps"))


                listHome.addAll(apps as ArrayList<DetailAppModel>)

                onRepositoryReadyCallback.onDataReady(listHome)

            }
        }


    }

    fun getRecentlyOpenedApps(onRepositoryReadyCallback: OnCateAppsReadyCallback, listHome: ArrayList<Any>) {

        async(UI) {

            Log.d(TAG, "getAllItems: i am here running in corotines")
            bg {
                val recentApps = appListDao.getAppsByStatus(Constants.Messages.PIN)
                if (recentApps != null && recentApps.isNotEmpty()) {
                    Log.d("recentApps", ""+recentApps.size)

                    val horizontalListViewType = HorizontalListViewType(recentApps as ArrayList<DetailAppModel>)
                    listHome.add(0, horizontalListViewType)
                    listHome.add(0, HeaderItemViewType("Recently Used Apps"))

                }
                else
                {
                    Log.d("recentApps", "No Data")
                }

                onRepositoryReadyCallback.onDataReady(listHome)

            }


        }
    }


    interface OnCateAppsReadyCallback {
        fun onDataReady(data: ArrayList<Any>)
        fun onHomeDataReady(data: ArrayList<Any>)
    }

}







