package timesinternet.pwa.com.pwatil.view.adapters

import android.content.Context
import android.os.Handler
import android.os.Message
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import timesinternet.pwa.com.pwatil.Constants
import timesinternet.pwa.com.pwatil.Models.*
import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.pwautil.databinding.AppItemBinding
import timesinternet.pwa.com.pwautil.databinding.CategoryAppItemBinding
import timesinternet.pwa.com.pwautil.databinding.HeaderItemBinding
import java.util.ArrayList


class AppStoreHomeAdapter(context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var context: Context = context
    private val appStoreList = ArrayList<Any>()
    private lateinit var handler: Handler

    fun setAppList(appModel: ArrayList<Any>, handler: Handler) {
        appStoreList.clear()
        appStoreList.addAll(appModel)
        this.handler = handler
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (getItemViewType(position) == Constants.AppStoreViewTypes.BANNER_VIEW_TYPE) {
            val bannerHolder = holder as BannerHolder
            bannerHolder.setData((appStoreList.get(position) as BannerViewTypeItem).bannerList, handler)
        } else if (getItemViewType(position) == Constants.AppStoreViewTypes.HORIZONTAL_LIST_SMALL_VIEW_TYPE) {
            val smallHorizontalListHolder = holder as SmallHorizontalListHolder
            smallHorizontalListHolder.setData((appStoreList.get(position) as HorizontalListSmallViewType).horizontalList, handler)
        } else if (getItemViewType(position) == Constants.AppStoreViewTypes.HORIZONTAL_LIST_VIEW_TYPE) {
            val horizontalListHolder = holder as HorizontalListHolder
            horizontalListHolder.setData((appStoreList.get(position) as HorizontalListViewType).horizontalList, handler)
        } else if (getItemViewType(position) == Constants.AppStoreViewTypes.HEADER_ITEM_VIEW_TYPE) {
            val appInfo = appStoreList[position] as HeaderItemViewType
            (holder as RecyclerHolderHeader).bind(appInfo,position)
        } else if (getItemViewType(position) == Constants.AppStoreViewTypes.VERTICAL_LIST_ITEM_VIEW_TYPE) {
            val appInfo = appStoreList[position] as DetailAppModel
            (holder as AppStoreHomeAdapter.ExpandedHolderAppIcon).bind(appInfo)
        } else {
            val appInfo = appStoreList[position] as DetailAppModel
            (holder as AppStoreHomeAdapter.RecyclerHolderAppIcon).bind(appInfo)
        }
    }

    override fun getItemCount(): Int {
        return appStoreList.size
    }

    override fun getItemViewType(position: Int): Int {

        val obj: Any = appStoreList.get(position)
        return when (obj) {
            is BannerViewTypeItem -> Constants.AppStoreViewTypes.BANNER_VIEW_TYPE
            is HorizontalListViewType -> Constants.AppStoreViewTypes.HORIZONTAL_LIST_VIEW_TYPE
            is HorizontalListSmallViewType -> Constants.AppStoreViewTypes.HORIZONTAL_LIST_SMALL_VIEW_TYPE
            is DetailAppModel -> Constants.AppStoreViewTypes.VERTICAL_LIST_ITEM_VIEW_TYPE
            is HeaderItemViewType -> Constants.AppStoreViewTypes.HEADER_ITEM_VIEW_TYPE
            else -> Constants.AppStoreViewTypes.VERTICAL_LIST_ITEM_VIEW_TYPE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        if (viewType == Constants.AppStoreViewTypes.BANNER_VIEW_TYPE) {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view: View = layoutInflater.inflate(R.layout.app_store_hor_recycler, parent,false)
            return BannerHolder(view)
        } else if (viewType == Constants.AppStoreViewTypes.HORIZONTAL_LIST_VIEW_TYPE) {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view: View = layoutInflater.inflate(R.layout.app_store_hor_recycler, parent,false)
            return HorizontalListHolder(view)
        } else if (viewType == Constants.AppStoreViewTypes.HORIZONTAL_LIST_SMALL_VIEW_TYPE) {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view: View = layoutInflater.inflate(R.layout.app_store_hor_recycler, parent,false)
            return SmallHorizontalListHolder(view)
        } else if (viewType == Constants.AppStoreViewTypes.VERTICAL_LIST_ITEM_VIEW_TYPE) {
            val layoutInflater = LayoutInflater.from(parent.context)
            val categoryAppItemBinding = CategoryAppItemBinding.inflate(layoutInflater, parent, false)
            return ExpandedHolderAppIcon(categoryAppItemBinding)
        } else if (viewType == Constants.AppStoreViewTypes.HEADER_ITEM_VIEW_TYPE) {
            val layoutInflater = LayoutInflater.from(parent.context)
            val headerBinding = HeaderItemBinding.inflate(layoutInflater, parent, false)
            return RecyclerHolderHeader(headerBinding)
        } else if (viewType == Constants.AppStoreViewTypes.VERTICAL_LIST_ITEM_VIEW_TYPE) {
            val layoutInflater = LayoutInflater.from(parent.context)
            val applicationBinding = AppItemBinding.inflate(layoutInflater, parent, false)
            return RecyclerHolderAppIcon(applicationBinding)
        } else {
            val layoutInflater = LayoutInflater.from(parent.context)
            val applicationBinding = AppItemBinding.inflate(layoutInflater, parent, false)
            return RecyclerHolderAppIcon(applicationBinding)
        }

    }


    inner class RecyclerHolderAppIcon(private val applicationBinding: AppItemBinding) : RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(appInfo: DetailAppModel) {
            applicationBinding.repositoryBottom = appInfo
            applicationBinding.appImg.setOnClickListener {
                Log.d("CLICKED", "CLICKED")


                var message: Message = Message()
                message.obj = appInfo
                handler.sendMessage(message)

            }

        }

    }

    inner class ExpandedHolderAppIcon(private val applicationBinding: CategoryAppItemBinding) : RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(appInfo: DetailAppModel) {
            applicationBinding.repository = appInfo
            applicationBinding.row.setOnClickListener {
                Log.d("CLICKED", "CLICKED")


                var message: Message = Message()
                message.obj = appInfo
                handler.sendMessage(message)

            }

            applicationBinding.launch.setOnClickListener {
                Log.d("CLICKED", "CLICKED")


                var message = Message()
                message.arg1 = Constants.Messages.LAUNCH
                message.obj = appInfo
                handler.sendMessage(message)

            }
        }

    }

    inner class RecyclerHolderHeader(private val applicationBinding: HeaderItemBinding) : RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(appInfo: HeaderItemViewType,position: Int) {
            applicationBinding.headerModel = appInfo
            applicationBinding.position = position

        }


    }


    private inner class BannerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val mRecyclerView: RecyclerView
        private val bannerAdapter: BannerAdapter
        private val bannerLinearLayoutManager: LinearLayoutManager

        init {

            mRecyclerView = itemView.findViewById(R.id.featured_recycler)
            bannerAdapter = BannerAdapter()
            bannerLinearLayoutManager = LinearLayoutManager(context)
            bannerLinearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
            mRecyclerView.layoutManager = bannerLinearLayoutManager
            mRecyclerView.adapter = bannerAdapter

        }

        fun setData(list: ArrayList<DetailAppModel>, handler: Handler) {
            bannerAdapter.setAppList(list, handler)
        }
    }

    private inner class HorizontalListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val mRecyclerView: RecyclerView
        private val reusableAppListAdapter: ReusableAppListAdapter
        private val reusableLinearLayoutManager: LinearLayoutManager

        init {

            mRecyclerView = itemView.findViewById(R.id.featured_recycler)
            reusableAppListAdapter = ReusableAppListAdapter(true)
            reusableLinearLayoutManager = LinearLayoutManager(context)
            reusableLinearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
            mRecyclerView.layoutManager = reusableLinearLayoutManager
            mRecyclerView.adapter = reusableAppListAdapter

        }

        fun setData(list: ArrayList<DetailAppModel>, handler: Handler) {
            reusableAppListAdapter.setAppList(list, handler)
        }
    }

    private inner class SmallHorizontalListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val mRecyclerView: RecyclerView = itemView.findViewById(R.id.featured_recycler)
        private val reusableAppListAdapter: ReusableAppListAdapter = ReusableAppListAdapter(false)
        private val reusableLinearLayoutManager: LinearLayoutManager = LinearLayoutManager(context)

        init {

            reusableLinearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
            mRecyclerView.layoutManager = reusableLinearLayoutManager
            mRecyclerView.adapter = reusableAppListAdapter

        }

        fun setData(list: ArrayList<DetailAppModel>, handler: Handler) {
            reusableAppListAdapter.setAppList(list, handler)
        }
    }




}