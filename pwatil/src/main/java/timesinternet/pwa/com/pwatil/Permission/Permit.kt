package timesinternet.pwa.com.pwatil.Permission

import android.content.Context


object Permit  {

    var permissionManager : PermissionManager? = null
    var permssionChannel : PermssionChannel? = null


    fun initialize(context: Context){
        permissionManager = PermissionManager(context, permssionChannel)
    }

    fun addPermissionChannel(permssionChannel: PermssionChannel){
        this.permssionChannel = permssionChannel
        permissionManager?.invalidatePermissionChannel(permssionChannel)
    }

}