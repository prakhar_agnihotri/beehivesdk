package timesinternet.pwa.com.pwatil.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
TABLE: ANALYTICS
 * Id: (Unique Id:  )
 * APP_ID String
 * Timestamp Time
 * Data: String
 * Meta: String
 * Status: Bool
 * Type : String
 */


@Entity(tableName = "analytics")
data class AnalyticsEntity(@PrimaryKey
                           @ColumnInfo(name = "id") val analyticsId: Int,
                           @ColumnInfo(name = "si") val appId: String,
                           @ColumnInfo(name = "d") val data: String,
                           @ColumnInfo(name = "m") val meta: String,   // LAUNCH,  UNPIN,  PIN, USE, Error, Crash,  PERMISSION,  Notification
                           @ColumnInfo(name = "status") val status: Boolean, //analytics send or not
                           @ColumnInfo(name = "type") val type: String, //(HOMESCREEN, INAPP, )
                           @ColumnInfo(name = "time_stamp") val timestamp: Long)

