package timesinternet.pwa.com.pwatil.Models

import android.databinding.BaseObservable
import android.databinding.BindingAdapter
import android.widget.ImageView

import com.bumptech.glide.Glide

class CategoryModel : BaseObservable() {

    lateinit var name: String
    lateinit var imgUrl: String
    var id: Int = 0

}
