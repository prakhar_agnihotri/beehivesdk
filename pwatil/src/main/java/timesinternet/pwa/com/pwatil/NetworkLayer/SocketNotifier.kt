package timesinternet.pwa.com.pwatil.NetworkLayer

import android.util.Log
import com.example.proto.AnalyticsEventOuterClass
import com.example.proto.AuthenticationOuterClass
import com.example.proto.ConfiguratorOuterClass
import com.example.proto.NotificationOuterClass

interface SocketNotifier {

    fun  onNotification(notification : NotificationOuterClass.Notification)
    fun  onAuth(auth : AuthenticationOuterClass.Authentication)
    fun  onConfigurator(auth : ConfiguratorOuterClass.Configurator)
    fun  onAnalyticsEvent(event : AnalyticsEventOuterClass.AnalyticsEvent){
        Log.d("pop", "dsa")
    }
}