package timesinternet.pwa.com.pwatil.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AppDetailResponse {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("response")
    @Expose
    var detailResponse: DetailResonse? = null

}

