package timesinternet.pwa.com.pwatil.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.sql.Timestamp

/**
TABLE: APP_LIST
 * APP_ID String
 * PINNED Boole
 * NAME String
 * CAT String
 * SUB_CAT String
 * Icon_Url String
 * BannerType Int
 * ImageUrl String
 * VideoUrl String
 * BannerUrl String
 * Rating Float
 * Downloads Int
 * TagLine String
 * MetaDescription String
 * ratingUrl String
 * Timestamp Time
 */


@Entity(tableName = "app_list")
data class AppListEntity(@PrimaryKey
                        @ColumnInfo(name = "si") val appId: String,
                        @ColumnInfo(name = "pin") val pinStatus: Int, //pwa opened or not
                        @ColumnInfo(name = "n") val name: String,
                        @ColumnInfo(name = "c") val catergory: String,
                        @ColumnInfo(name = "sc") val subCategory: String,
                        @ColumnInfo(name = "icu") val iconUrl: String,
                        @ColumnInfo(name = "bt") val bannerType: Int,
                        @ColumnInfo(name = "imu") val imageUrl: String, // this url are separated by ~,
                        @ColumnInfo(name = "vu") val videoUrl: String, // this url are separated by ~,
                        @ColumnInfo(name = "bu") val bannerUrl: String,
                        @ColumnInfo(name = "r") val rating: Float,
                        @ColumnInfo(name = "d") val download: Int,
                        @ColumnInfo(name = "t") val tagLine: String,
                        @ColumnInfo(name = "m") val metaDesc: String,
                        @ColumnInfo(name = "ru") val reviewUrl: String,
                        @ColumnInfo(name = "time_stamp") val timestamp: Long)

