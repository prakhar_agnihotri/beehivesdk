package timesinternet.pwa.com.pwatil.view.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import kotlinx.android.synthetic.main.activity_app_detail_page.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import timesinternet.pwa.com.pwatil.Constants
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwatil.SandBox
import timesinternet.pwa.com.pwatil.base.BaseFragment
import timesinternet.pwa.com.pwatil.db.DbHelper
import timesinternet.pwa.com.pwatil.db.dao.AppListDao
import timesinternet.pwa.com.pwatil.view.adapters.ReusableAppListAdapter
import timesinternet.pwa.com.pwautil.databinding.ActivityAppDetailPageBinding
import timesinternet.pwa.com.timespwa.viewmodel.AppStoreDetailViewModel


class AppDetailFragment : BaseFragment(), Handler.Callback, AppBarLayout.OnOffsetChangedListener {
    lateinit var mContext : Context

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {

        when {
            Math.abs(verticalOffset) == appBarLayout?.totalScrollRange -> // Collapsed
                toolbar.alpha = 1.0f
            verticalOffset == 0 -> // Expanded
                toolbar.alpha = 0.0f
            else -> {
                // Somewhere in between
            }
        }
    }

   lateinit var appListDao: AppListDao



    override fun handleMessage(msg: Message?): Boolean {

        Log.d("CALLBACK", "RECEIVED")

        var appDetailFragment = AppDetailFragment()
        var bundle = Bundle()
        var appModel: DetailAppModel = msg?.obj as DetailAppModel
        bundle.putParcelable("appInfo", appModel)
        bundle.putParcelableArrayList("appList", appsList)
        appDetailFragment.arguments = bundle
        replaceFragment(appDetailFragment)
        return false
    }


    private lateinit var binding: ActivityAppDetailPageBinding
    private lateinit var appStoreHomeViewModel: AppStoreDetailViewModel
    private lateinit var appData: DetailAppModel
    private lateinit var handler: Handler
    private var tilApp: Boolean = false



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val dbHelper =  DbHelper.getInstance(mContext)
        appListDao = dbHelper?.appDao()!!

        binding = ActivityAppDetailPageBinding.inflate(inflater, container, false).apply {
            appStoreHomeViewModel = AppStoreDetailViewModel(activity!!.application)
        }
        this.appData = arguments?.get("appInfo") as DetailAppModel
        this.appsList = arguments?.get("appList") as ArrayList<DetailAppModel>

        this.tilApp = arguments?.getBoolean("tilApp", false) as Boolean

        return binding.root
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context!!
    }
    var appsList = ArrayList<DetailAppModel>()

    private lateinit var reusableAppListAdapter: ReusableAppListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        handler = Handler(this)

        appStoreHomeViewModel = ViewModelProviders.of(this).get(AppStoreDetailViewModel::class.java)
        binding.detailPage = appStoreHomeViewModel
        appStoreHomeViewModel.setBundle(appsList, appData, tilApp)
        appStoreHomeViewModel.start()
        setUpToolBar()
        setUpBottomRecycler()
        observerRecommendedApps()


        launch_button.setOnClickListener {
            val intent = Intent(activity, SandBox::class.java)
            intent.putExtra("appId", appData.appId)
            intent.putExtra("appUrl", appData.launchUrl)
            intent.putExtra("mAppIconUrl", appData.iconUrl)
            intent.putExtra("mAppName", appData.appName)
            intent.putExtra("tilApp", tilApp)

            async(UI) {
                bg {
                    appListDao.updatePinnedStatus(Constants.Messages.PIN, appData.appId!!)
                }
            }

            activity?.startActivity(intent)
        }

        appStoreHomeViewModel.isErrorLoadingApps.observe(this,
                Observer<Boolean> {
                    it?.let { isErrorLoading ->
                        if (isErrorLoading) {

                            val animation = ScaleAnimation(1f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
                            animation.duration = 200
                            animation.fillAfter = true
                            no_internet.animation = animation
                        }

                    }
                })


    }

    private fun observerRecommendedApps() {
        appStoreHomeViewModel.recommendedApps()?.observe(this, Observer<ArrayList<DetailAppModel>> {

            if (it != null) {
                reusableAppListAdapter.setAppList(it, handler)
            }

        })
    }

    inline fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG) {
        val snack = Snackbar.make(this, message, length)
        val view = snack.view
        val params = view.layoutParams as CoordinatorLayout.LayoutParams
        params.gravity = Gravity.TOP
        view.layoutParams = params
        snack.show()
    }

    private fun setUpToolBar() {


        back_btn.setOnClickListener {

            activity?.onBackPressed()

        }
    }


    fun setUpBottomRecycler() {


        reusableAppListAdapter = ReusableAppListAdapter(true)
        reusableAppListAdapter.setAppList(appsList, handler)
        val recommendedLinearLayoutManager = LinearLayoutManager(activity)
        recommendedLinearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding.bottomRecycler.layoutManager = recommendedLinearLayoutManager
        binding.bottomRecycler.adapter = reusableAppListAdapter

    }


}
