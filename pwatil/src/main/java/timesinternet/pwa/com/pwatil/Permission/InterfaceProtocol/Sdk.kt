package timesinternet.pwa.com.pwatil.Permission.InterfaceProtocol

import timesinternet.pwa.com.pwatil.Permission.Callbacks.PermissionResponse

interface   Sdk: User  {
    fun askForNativePermission()
    fun isPermittedTo( permissions: List<String>, permissionResponse: PermissionResponse)

}