package timesinternet.pwa.com.pwatil.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class DetailResonse {
    @SerializedName("appdetail")
    @Expose
    val appdetail: DetailAppModel? = null
    @SerializedName("recommended")
    @Expose
    val recommended: List<CategoryList>? = null
}