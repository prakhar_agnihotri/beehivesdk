package timesinternet.pwa.com.pwatil.AppStore.Retro

import okhttp3.Call
import retrofit2.http.GET

interface REST {

    @GET("/TEST/mainfest.json")
    fun fetchManifest(): Call

}