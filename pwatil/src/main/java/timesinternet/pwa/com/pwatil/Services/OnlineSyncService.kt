package timesinternet.pwa.com.timespwa.Services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationCompat.PRIORITY_MIN
import android.util.Log
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timesinternet.pwa.com.pwatil.Constants
import timesinternet.pwa.com.pwatil.Models.AppInstallDeleteResponse
import timesinternet.pwa.com.pwatil.Services.WorkManagers.AnalyticsWorker
import timesinternet.pwa.com.pwatil.Utils.apiUtils.ApiUtils
import timesinternet.pwa.com.pwatil.Utils.apiUtils.ApiUtils.DEFAULT_RETRIES
import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.timespwa.Interfaces.APiInterfaces.APIInterface
import timesinternet.pwa.com.timespwa.Services.Alarm.AlarmHandler
import timesinternet.pwa.com.timespwa.db.AppDbHelper
import java.util.*
import java.util.concurrent.TimeUnit


class OnlineDbSyncService : Service() {
    internal val TAG = "OnlineDbSyncService"
    private var mAPIService: APIInterface? = null
    private var removedPackages: ArrayList<String>? = null
    private var addPackages: ArrayList<String>? = null
    //this flag maintains when service will stop
    private var mCompletedCount = 0

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "OnlineDbSyncService::class: OnCreate")

        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel()
                } else {
                    // If earlier version channel ID is not used
                    // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                    ""
                }

        val notificationBuilder = NotificationCompat.Builder(this, channelId )
        val notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_play_circle_filled_black_24dp)
                .setPriority(PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build()
        startForeground(101, notification)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String{
        val channelId = "my_service"
        val channelName = "My Background Service"
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }
    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "OnlineDbSyncService::class: OnDestroy")

    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "OnlineDbSyncService::class: OnStartCommand")
        val constraints: Constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()
        Log.i("WORK_MANAGER", "WORKING NOW......")




//        mAPIService = ApiUtils.getAPIRemovePackageService()
//        removedPackages = AppDbHelper.getInstance(applicationContext).removeUninstalledPackages()
//        if (removedPackages!!.size == 0) {
//            mCompletedCount++
//        }
//
//        val compressedPackageObject = getSimpleObject(removedPackages!!)
//
//
//        addPackages = AppDbHelper.getInstance(applicationContext).packagesToSync
//
//        if (addPackages!!.size == 0) {
//            mCompletedCount++
//
//        }
//        if (mCompletedCount >= 2) {
//            stopSelf()
//
//        }
//        val addedpackageObject = getSimpleObject(addPackages!!)
//
//
//        //remove packages from Server which are uncompressed
//        var jsonObject = JSONObject()
//        var resultantJsonObj = JSONObject()
//
//        try {
//
//            jsonObject.put("uniqueToken", "deviceId")
//            jsonObject.put("pckg", compressedPackageObject)
//            resultantJsonObj.put("data", jsonObject)
//
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
//
//        if (removedPackages!!.size > 0) {
//            removePackedFromServer(jsonObject.toString())
//        }
//
//
//        //add Packages call
//        //remove packages from Server which are uncompressed
//        jsonObject = JSONObject()
//        resultantJsonObj = JSONObject()
//
//        try {
//
//            jsonObject.put("fcmID", "deviceId")
//            jsonObject.put("pckg", addedpackageObject)
//            resultantJsonObj.put("data", jsonObject)
//
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
//
//        if (addPackages!!.size > 0) {
//            addPackedFromServer(jsonObject.toString())
//        }

        return Service.START_STICKY
    }

    private fun getSimpleObject(packages: ArrayList<String>): String {
        val packageJson: String
        val mJSONArray = JSONArray(packages)
        val data = mJSONArray.toString()
        val regex = "\\[|\\]"
        packageJson = data.replace(regex.toRegex(), "")
        return "{$packageJson}"
    }


    private fun removePackedFromServer(title: String) {

        ApiUtils.enqueueWithRetry(mAPIService!!.removePackage(title), DEFAULT_RETRIES, object : Callback<AppInstallDeleteResponse> {
            override fun onResponse(call: Call<AppInstallDeleteResponse>, response: Response<AppInstallDeleteResponse>) {
                mCompletedCount++

                if (response.isSuccessful) {
                    //showResponse(detailResponse.body().toString());
                    for (appPackage in removedPackages!!) {
                        AppDbHelper.getInstance(applicationContext).appServerUpdated(appPackage, Constants.SyncConstants.SERVER_UPDATED)
                    }

                    response.code()
                } else {

                }
                if (mCompletedCount >= 2) {
                    //remove sync alarms
                    AlarmHandler.getInstance(applicationContext).cancelAlarm(Constants.AlarmConstants.ALARM_SYNC_SERVICE_REQUEST_CODE)
                    stopSelf()
                }
            }

            override fun onFailure(call: Call<AppInstallDeleteResponse>, t: Throwable) {

                mCompletedCount++
                if (mCompletedCount >= 2) {
                    stopSelf()
                }
            }
        })
    }

    private fun addPackedFromServer(title: String) {

        ApiUtils.enqueueWithRetry(mAPIService!!.addPackage(title), DEFAULT_RETRIES, object : Callback<AppInstallDeleteResponse> {
            override fun onResponse(call: Call<AppInstallDeleteResponse>, response: Response<AppInstallDeleteResponse>) {
                mCompletedCount++

                if (response.isSuccessful) {

                    //showResponse(detailResponse.body().toString());
                    for (appPackage in addPackages!!) {
                        AppDbHelper.getInstance(applicationContext).appServerUpdated(appPackage, Constants.SyncConstants.SERVER_UPDATED)
                    }

                    response.code()
                    if (mCompletedCount >= 2) {
                        //remove sync alarms
                        AlarmHandler.getInstance(applicationContext).cancelAlarm(Constants.AlarmConstants.ALARM_SYNC_SERVICE_REQUEST_CODE)
                        stopSelf()
                    }
                } else {

                }
            }

            override fun onFailure(call: Call<AppInstallDeleteResponse>, t: Throwable) {
                mCompletedCount++
                if (mCompletedCount >= 2) {
                    stopSelf()
                }
            }
        })
    }

}
