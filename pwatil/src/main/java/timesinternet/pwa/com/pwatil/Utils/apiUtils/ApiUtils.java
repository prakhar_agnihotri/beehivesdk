package timesinternet.pwa.com.pwatil.Utils.apiUtils;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timesinternet.pwa.com.timespwa.Interfaces.APiInterfaces.APIInterface;


public class ApiUtils {

    private ApiUtils() {
    }

    public static final int MAX_API_RETRY_COUNT = 5;

    public static final String BASE_URL = "http://223.165.30.63:8005/";
    public static final String ADD_PACKAGE_BASE_URL = BASE_URL;
    public static final String REMOVE_PACKAGE_BASE_URL = BASE_URL;
    public static final String GET_APP_DETAILS = "http://demo6020467.mockable.io/v1";

    public static final String DASHBOARD_URL = "http://172.24.42.103:9090/";

    public static final int DEFAULT_RETRIES = 3;

    public static <T> void enqueueWithRetry(Call<T> call, final int retryCount, final Callback<T> callback) {
        call.enqueue(new RetryableCallback<T>(call, retryCount) {
            @Override
            public void onFailure(Call<T> call, Throwable t) {
                callback.onFailure(call, t);

            }

            @Override
            public void onFinalResponse(Call<T> call, Response<T> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFinalFailure(Call<T> call, Throwable t) {
                callback.onFailure(call, t);
            }
        });
    }

    public static <T> void enqueueWithRetry(Call<T> call, final Callback<T> callback) {
        enqueueWithRetry(call, DEFAULT_RETRIES, callback);
    }

    public static boolean isCallSuccess(Response response) {
        int code = response.code();
        return (code >= 200 && code < 400);
    }

    public static APIInterface getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIInterface.class);
    }

    public static APIInterface getAPIAddPackageService() {

        return RetrofitClient.getClient(ADD_PACKAGE_BASE_URL).create(APIInterface.class);
    }

    public static APIInterface getAPIRemovePackageService() {

        return RetrofitClient.getClient(REMOVE_PACKAGE_BASE_URL).create(APIInterface.class);
    }

    public static APIInterface getAppDetailClient() {

        return RetrofitClient.getClient(DASHBOARD_URL).create(APIInterface.class);
    }

    public static APIInterface getDashBoardClient() {

        return RetrofitClient.getClient(DASHBOARD_URL).create(APIInterface.class);
    }


}