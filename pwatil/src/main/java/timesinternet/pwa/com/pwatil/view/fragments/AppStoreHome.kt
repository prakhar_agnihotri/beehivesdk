package timesinternet.pwa.com.pwatil.view.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.provider.SyncStateContract
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import timesinternet.pwa.com.pwatil.Constants
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwatil.SandBox
import timesinternet.pwa.com.pwatil.base.BaseFragment
import timesinternet.pwa.com.pwatil.view.adapters.AppStoreHomeAdapter
import timesinternet.pwa.com.pwatil.viewmodel.CategoryViewModel
import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.pwautil.databinding.AppStoreBinding

class AppStoreHome : BaseFragment(), Handler.Callback {
    override fun handleMessage(msg: Message?): Boolean {

        var appModel: DetailAppModel
        if (msg != null) {
            if (msg.arg1 == Constants.Messages.LAUNCH) {
                appModel = msg?.obj as DetailAppModel
                val intent = Intent(activity, SandBox::class.java)
                intent.putExtra("appId", appModel.appName)
                intent.putExtra("appUrl", appModel.launchUrl)
                intent.putExtra("mAppIconUrl", appModel.iconUrl)
                intent.putExtra("mAppName", appModel.appName)
                intent.putExtra("tilApp", false)
                startActivity(intent)
                viewModel.pinApp(appModel)
            } else {
                var appDetailFragment = AppDetailFragment()
                var bundle = Bundle()

                appModel = msg?.obj as DetailAppModel
                bundle.putParcelable("appInfo", appModel)
                val arr = ArrayList<DetailAppModel>()
                arr.add(appModel)
                bundle.putParcelableArrayList("appList", arr)
                appDetailFragment.arguments = bundle
                replaceFragment(appDetailFragment)
            }
        }
        return true
    }

    lateinit var appStoreBinding: AppStoreBinding
    lateinit var viewModel: CategoryViewModel
    lateinit var appStoreHomeAdapter: AppStoreHomeAdapter
    var typeOfLoadFun: Int = 0
    lateinit var handler: Handler;

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        appStoreBinding = DataBindingUtil.inflate(inflater, R.layout.app_store, container, false)
        return appStoreBinding.root
    }

    override fun onResume() {
        super.onResume()

        if(appStoreHomeAdapter!=null)
            viewModel.loadApps(typeOfLoadFun)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handler = Handler(this)
        appStoreHomeAdapter = AppStoreHomeAdapter(this!!.context!!)
        val bannerLinearLayoutManager = LinearLayoutManager(activity)
        bannerLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        appStoreBinding.homeRecycler.layoutManager = bannerLinearLayoutManager
        appStoreBinding.homeRecycler.adapter = appStoreHomeAdapter



        viewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        viewModel.appHomeList.observe(this,
                Observer<ArrayList<Any>> {
                    it?.let {
                        appStoreHomeAdapter.setAppList(it, handler)
                    }
                })

        if (viewModel.appsList.value == null) {
            viewModel.loadApps(typeOfLoadFun)
        }


    }

}