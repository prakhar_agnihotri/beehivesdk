package timesinternet.pwa.com.pwatil

object Constants{

    val ccc : String = ""
    val HOST = "10.150.72.19"
    val PORT = 8080
    val UDP_PORT = 9090

    object AlarmConstants{
        const val ALARM_SYNC_SERVICE_REQUEST_CODE = 1
        const val REPEAT_INTERVAL = 1 * 1000L //30 seconds
    }
    object SyncConstants{

        const val SERVER_NOT_UPDATED = -1
        const val SERVER_UPDATED = 1
        const val APP_INSTALLED = 2
        const val APP_UNINSTALLED = 3



    }
    object HealthRequestConstants {
        const val BROADCAST_HEALTH = "health_request"
    }
    object NotificationConstants {

        const val NOTIFICATION_TYPE_DEFAULT = 1
        const val NOTIFICATION_TYPE_BIG_PICTURE_STYLE = 2
        const val NOTIFICATION_TYPE_GROUP_NOTIFICATION = 3
    }

    object PwaConstants {
        const val ANR_TIMEOUT = 5000
        const val DELAY_TIMEOUT = 6000
        //15 mb limitation each PWA
        const val MAX_CACHE_AMOUNT = 1024 * 1024 * 15


    }

    object AppStoreViewTypes {
        const val BANNER_VIEW_TYPE = 1
        const val HORIZONTAL_LIST_VIEW_TYPE = 2
        const val HORIZONTAL_LIST_SMALL_VIEW_TYPE = 3
        const val VERTICAL_LIST_ITEM_VIEW_TYPE = 4
        const val HEADER_ITEM_VIEW_TYPE = 5
        const val GRID_LIST_ITEM_VIEW_TYPE = 6
    }

    object Messages{
        const val LAUNCH = 1
        const val PIN = 2
        const val UNPIN = 1
    }



}