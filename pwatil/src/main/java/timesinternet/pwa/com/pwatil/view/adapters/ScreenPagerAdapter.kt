package timesinternet.pwa.com.pwatil.view.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.LinkedHashMap
import kotlin.collections.ArrayList
import kotlin.collections.set

// 1
class ScreenPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {


    private val mFragmentList = LinkedHashMap<String, Fragment>()
    override fun getItem(position: Int): Fragment {
        val keys = ArrayList<Fragment>(mFragmentList.values)
        return keys[position]
    }


    override fun getCount(): Int {
        return mFragmentList.size
    }


    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList[title] = fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        val keys = ArrayList(mFragmentList.keys)
        return keys[position]
    }
}