package timesinternet.pwa.com.pwatil.view.fragments

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import timesinternet.pwa.com.pwautil.R

class AddPhotoBottomDialogFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

// get the views and attach the listener
       val view = inflater.inflate(R.layout.permission_dialog, container,
                false)

        val continueToApp = view.findViewById<TextView>(R.id.continue_app)
        continueToApp.setOnClickListener(View.OnClickListener {
            dialog.cancel()
        })

        return view

    }


//    override fun setCancelable(cancelable: Boolean) {
//        val dialog = dialog
//        val touchOutsideView = dialog.window!!.decorView.findViewById<View>(android.support.design.R.id.touch_outside)
//        val bottomSheetView = dialog.window!!.decorView.findViewById<View>(android.support.design.R.id.design_bottom_sheet)
//
//        if (cancelable) {
//            touchOutsideView.setOnClickListener {
//                if (dialog.isShowing) {
//                    dialog.cancel()
//                }
//            }
//            BottomSheetBehavior.from(bottomSheetView).setHideable(true)
//        } else {
//            touchOutsideView.setOnClickListener(null)
//            BottomSheetBehavior.from(bottomSheetView).setHideable(false)
//        }
//    }

    companion object {

        fun newInstance(): AddPhotoBottomDialogFragment {
            return AddPhotoBottomDialogFragment()
        }
    }
}