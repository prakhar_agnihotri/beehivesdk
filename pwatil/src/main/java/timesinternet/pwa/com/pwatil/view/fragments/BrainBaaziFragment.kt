package timesinternet.pwa.com.pwatil.view.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import timesinternet.pwa.com.pwatil.base.BaseFragment
import timesinternet.pwa.com.pwatil.view.activities.BrainBaaziActivity
import timesinternet.pwa.com.pwatil.view.activities.TilAppsFragment
import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.pwautil.databinding.BbFragLayoutBinding

class BrainBaaziFragment : BaseFragment() {


    private lateinit var binding: BbFragLayoutBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.bb_frag_layout, container, false)

        val clickListener = View.OnClickListener {view ->

            when (view.getId()) {

                R.id.upper_layout -> openAppStore()
            }
        }

        binding.upperLayout.setOnClickListener(clickListener)


        return binding.root;
    }

    private fun openAppStore()
    {

        (activity as BrainBaaziActivity).replaceFragment(TilAppsFragment())
    }

}