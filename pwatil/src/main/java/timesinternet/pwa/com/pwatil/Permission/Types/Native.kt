package timesinternet.pwa.com.pwatil.Permission.Types

import android.Manifest
import timesinternet.pwa.com.pwatil.Permission.Layers.GenericPermission

object Native  : GenericPermission() {

    val Permissions = HashMap<String, List<String>>()


    init {
        val CAMERA_ACCESS = listOf(
                Manifest.permission.CAMERA
        )
        val LOCATION_ACCESS = listOf(
                Manifest.permission.LOCATION_HARDWARE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        )
        val HOMESCREEN_SHPRTCUT_ACCESS =listOf(
                Manifest.permission.INSTALL_SHORTCUT,
                Manifest.permission.UNINSTALL_SHORTCUT
        )
        val NOTIFICATION_ACCESS = listOf(
                Manifest.permission.ACCESS_NOTIFICATION_POLICY,
                Manifest.permission.BIND_NOTIFICATION_LISTENER_SERVICE
        )
        val SMS_READ_ACCESS = listOf(
                Manifest.permission.READ_SMS,
                Manifest.permission.RECEIVE_SMS
        )
        val SMS_SEND_ACCESS = listOf(
                Manifest.permission.SEND_SMS
        )
        val MAKE_CALL= listOf(
                Manifest.permission.CALL_PHONE
        )
        val CONTACTS_ACCESS= listOf(
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_CONTACTS
        )
        val USER_DETAIL_ACCESS= listOf<String>(
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.SEND_SMS,
                Manifest.permission.CAMERA
        )

        Permissions.put(Pwa.CAMERA_ACCESS ,  CAMERA_ACCESS)
        Permissions.put(Pwa.LOCATION_ACCESS ,  LOCATION_ACCESS)
        Permissions.put(Pwa.HOMESCREEN_SHPRTCUT_ACCESS ,  HOMESCREEN_SHPRTCUT_ACCESS)
        Permissions.put(Pwa.NOTIFICATION_ACCESS ,  NOTIFICATION_ACCESS)
        Permissions.put(Pwa.SMS_READ_ACCESS ,  SMS_READ_ACCESS)
        Permissions.put(Pwa.SMS_SEND_ACCESS ,  SMS_SEND_ACCESS)
        Permissions.put(Pwa.MAKE_CALL ,  MAKE_CALL)
        Permissions.put(Pwa.CONTACTS_ACCESS ,  CONTACTS_ACCESS)
        Permissions.put(Pwa.USER_DETAIL_ACCESS ,  USER_DETAIL_ACCESS)
    }



}