package timesinternet.pwa.com.pwatil.Services

import android.app.ActivityManager
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import timesinternet.pwa.com.pwatil.Constants

/**
 *
 * Basic flow is to wait for
 *
 */
class PWAANRCheckerService : Service() {

    private val TAG = "PWAANRCheckerService"

    //check every 5 sec
    private val TIME_INTERVAL_CHECK = 5000

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "PWAANRCheckerService::class: OnCreate")

    }


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "PWAANRCheckerService::class: OnStartCommand")

        return Service.START_NOT_STICKY
    }


    class HealthReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("HealthReceiver", "onReceive:HealthReceiver ")

            isActiveFlag.status = true
        }
    }

    object isActiveFlag {

        var status : Boolean = true
    }

    private fun startANRHandler() {

        //send broadcast
        if( !isActiveFlag.status){
            //show ANR
            Log.d("HealthReceiver","ANR_DETECTED")
            Toast.makeText(applicationContext, "ANR DETECTED: KILLING",Toast.LENGTH_SHORT).show()
            val pid = android.os.Process.myPid()
            val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            var currentProcName = ""
            for (processInfo in manager.runningAppProcesses) {

                currentProcName = processInfo.processName
                Log.d("PROCESS_CHECK","" + currentProcName + " : " + processInfo.pid)
                if( currentProcName.toString().contains("SandBox")){

                    android.os.Process.killProcess( processInfo.pid)

                }

            }
            stopSelf()
        }
        else{
            Log.d("HealthReceiver","ANRNOT_DETECTED")

        }
        //intentionally change it's status to let receiver change it
        isActiveFlag.status = false
        //send  broadcast to Sanbox to call getHealthFunction and wait

        val intent = Intent()
        intent.action = Constants.HealthRequestConstants.BROADCAST_HEALTH
        sendBroadcast(intent)



    }



}
