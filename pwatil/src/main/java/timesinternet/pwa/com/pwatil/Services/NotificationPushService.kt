package timesinternet.pwa.com.pwatil.Services

import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.proto.AnalyticsEventOuterClass
import com.example.proto.AuthenticationOuterClass
import com.example.proto.ConfiguratorOuterClass
import com.example.proto.NotificationOuterClass
import org.jetbrains.anko.doAsync
import timesinternet.pwa.com.pwatil.App
import timesinternet.pwa.com.pwatil.Constants
import timesinternet.pwa.com.pwatil.Data.DummyData
import timesinternet.pwa.com.pwatil.Models.DataModels
import timesinternet.pwa.com.pwatil.NetworkLayer.SocketFrame
import timesinternet.pwa.com.pwatil.NetworkLayer.SocketHandler
import timesinternet.pwa.com.pwatil.NetworkLayer.SocketNotifier
import timesinternet.pwa.com.timespwa.Notification.NotiHandler
import timesinternet.pwa.com.timespwa.Notification.initNotiManager


/**
 * DUMMY PUSH NOTIFICATION SERVICE
 * This service push message every 10 seconds
 * until stops
 *
 */
class NotificationPushService : Service(), SocketNotifier {
    override fun onNotification(notification: NotificationOuterClass.Notification) {
        Log.d("NotificationPushService", "onNotification")

        NotiHandler.getInstance(App.instance).addSimpleNotification(DataModels.NotificationData(
                notification.appId,
                45,
                "http://192.168.37.212/CAMERA/manifest.json",
                notification.title,
                notification.sDesc,
                "aaa",
                "aaa",
                "ticker text",
                "any",
                "on",
                NotificationManager.IMPORTANCE_HIGH
        ))

    }

    override fun onAuth(auth: AuthenticationOuterClass.Authentication) {
        Log.d("NotificationPushService", "onAuth")
        //TODO HANDLE AUTH LATER
    }

    override fun onConfigurator(auth: ConfiguratorOuterClass.Configurator) {
        Log.d("NotificationPushService", "onConfigurator")

    }


    private val TAG = "NotificationPushService"
    private val NOTIFICATION_INTERVAL = 3000

    private val mAppId = "PDACamera"
    private val mTitle = "This is a sample Title"
    private val mBody = "This is the description of the notification"
    private val mIcon = "https://images-eu.ssl-images-amazon.com/images/I/41jFTr0I5nL.png"
    private val mTickerIcon = "https://images-eu.ssl-images-amazon.com/images/I/41jFTr0I5nL.png"
    private val mNotiType = Constants.NotificationConstants.NOTIFICATION_TYPE_DEFAULT

    lateinit var socketHandler: SocketHandler
    override fun onCreate() {

        doAsync {
            //TODO TOKEN BRAINBAAZI

            socketHandler = SocketHandler(Constants.HOST, Constants.PORT, "DUMMY", this@NotificationPushService)
            initNotiManager(applicationContext)
            //socketHandler.Write(SocketFrame('A', "Custom Message"))
        }



        //todo do this
       //AnalyticsEventOuterClass.AnalyticsEvent.newBuilder().setAppId().
        //socketHandler?.sendAnalyticsEvent()

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        pushNotification()


        return Service.START_STICKY
    }

    private val mHandler = android.os.Handler()
    private val mRunnable = java.lang.Runnable {
        Log.d(TAG, "Pushing Notification")
//        pushNotification()

    }

    private fun pushNotification() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        initNotiManager(this)
        NotiHandler.getInstance(App.instance).addSimpleNotification(DummyData.simpleNotification)
//        }

        //TODO Add Notification Here
        mHandler.postDelayed(mRunnable, NOTIFICATION_INTERVAL.toLong())
    }

    override fun onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.d(TAG, "Service onBind")
        return null
    }

}
