package timesinternet.pwa.com.timespwa.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.Intent
import android.databinding.ObservableField
import android.util.Log
import android.view.View
import timesinternet.pwa.com.pwatil.Data.DetailAppRepository
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwatil.Models.DetailResonse
import timesinternet.pwa.com.pwatil.SandBox


class AppStoreDetailViewModel(application: Application) : AndroidViewModel(application) {
    var appIcon = ObservableField<String>()
    var bannerIcon = ObservableField<String>()
    var maxDescriptionLines = ObservableField<Int>()

    var appTitle = ObservableField<String>()
    var appSubTitle = ObservableField<String>()
    var topText = ObservableField<String>()
    var appRating = ObservableField<String>()
    var appDownload = ObservableField<String>()
    var appCategory = ObservableField<String>()
    var appDetailed = ObservableField<String>()
    var isDetailTextAvailable = ObservableField<Boolean>()
    var isDetailTextVisible = ObservableField<Boolean>()
    var isBannerVisible = ObservableField<Boolean>()

    var appId: String = "randomId"
    var appurl: String = "randomId"
    var appIconuUrl: String = "randomId"
    var appNameString: String = "randomId"
    var tilApp: Boolean = false
    val isLoading = ObservableField(false)
    var isErrorLoadingApps = MutableLiveData<Boolean>()

    var context: Context
    var repoModel: DetailAppRepository = DetailAppRepository(getApplication())
    lateinit var appModel: DetailAppModel
    lateinit var appModelTrimmed: DetailAppModel //for detailed recycler
    lateinit var appList: List<DetailAppModel>
    var appListLiveData: MutableLiveData<ArrayList<DetailAppModel>> = MutableLiveData()

    init {
        context = application
    }

    override fun onCleared() {
        super.onCleared()
    }

    fun start() {
        loadApps(0)

    }

    fun loadApps(typeOfLoadFun: Int) {
        isLoading.set(true)
        if (appModel.detailUrl == null) {


            isLoading.set(false)
            isErrorLoadingApps.value = true
            isErrorLoadingApps.postValue(true)




            return
        }

        repoModel.getDetailedApp(object : DetailAppRepository.onDetailedAppReadyCallBack {
            override fun onDataReady(data: DetailResonse) {
                isLoading.set(false)
                if (data.appdetail != null) {
                    setBundle(data.recommended!![0].apps!!, data.appdetail, true)
                    appListLiveData.postValue(data.recommended!![0].apps!!)
                }
            }
        }, typeOfLoadFun, appModel.detailUrl!!)



    }

    fun recommendedApps(): MutableLiveData<ArrayList<DetailAppModel>> {
        return appListLiveData
    }

    fun launchPWA(view: View) {

        val intent = Intent(view.context, SandBox::class.java)
        intent.putExtra("appId", appId)
        intent.putExtra("appUrl", appurl)
        intent.putExtra("mAppIconUrl", appIconuUrl)
        intent.putExtra("mAppName", appNameString)
        intent.putExtra("tilApp", tilApp)

        view.context.startActivity(intent)
    }

        fun readMoreCLicked(view: View) {
            maxDescriptionLines.set(40)
            isDetailTextAvailable.set(false)
        }

        fun goBack(view: View) {

        }


        fun setBundle(appList: ArrayList<DetailAppModel>, appModel: DetailAppModel, tilApp: Boolean) {
            this.appModel = appModel
            this.appList = appList
            maxDescriptionLines.set(3)
            Log.d("RECUVED", "dsfd")
            bannerIcon.set(appModel.bannerUrl)
            appTitle.set(appModel.appName)
            appSubTitle.set(appModel.tagLine)
            topText.set("Top 20 in " + appModel.category)
            appRating.set(appModel.rating.toString())
            if (appModel.downloadCount != null)
                appDownload.set(appModel.downloadCount.toString())
            appCategory.set(appModel.category)
            appurl = appModel.launchUrl!!
            appIconuUrl = appModel.iconUrl!!
            appNameString = appModel.appName!!

            appIcon.set(appModel.iconUrl)
            appDetailed.set(appModel.metaDesc)

            appId = appModel.appName!!
            this.tilApp = tilApp


            if (appModel.metaDesc == null) {
                return
            }
            if (appModel.metaDesc!!.isEmpty()) {
                isDetailTextAvailable.set(false)
            } else {
                isDetailTextAvailable.set(true)
                isDetailTextVisible.set(true)
            }
            if (appModel.bannerUrl!!.isEmpty()) {
                isBannerVisible.set(false)
            } else {
                isBannerVisible.set(true)
            }
            if (appModel.metaDesc!!.length < 200) {
                isDetailTextAvailable.set(false)

            }

        }
    }