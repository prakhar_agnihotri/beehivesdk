package timesinternet.pwa.com.pwatil


import android.annotation.TargetApi
import android.app.Activity
import android.app.Notification
import android.app.NotificationManager
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.webkit.*
import android.widget.ImageView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.container_web.*
import org.json.JSONObject
import timesinternet.pwa.com.pwatil.AppStore.ViewAppsPermission
import timesinternet.pwa.com.pwatil.BroadcastReciever.SmsListener
import timesinternet.pwa.com.pwatil.BroadcastReciever.SmsReciever
import timesinternet.pwa.com.pwatil.Data.DummyData
import timesinternet.pwa.com.pwatil.DebugLog.Logs
import timesinternet.pwa.com.pwatil.Models.DataModels
import timesinternet.pwa.com.pwatil.Permission.Interface.Core
import timesinternet.pwa.com.pwatil.Permission.Interface.UserPwa
import timesinternet.pwa.com.pwatil.Permission.NativePermission
import timesinternet.pwa.com.pwatil.Permission.Permit
import timesinternet.pwa.com.pwatil.Permission.PermssionChannel
import timesinternet.pwa.com.pwatil.Utils.webInterceptors.WebViewCacheInterceptor
import timesinternet.pwa.com.pwatil.Utils.webInterceptors.WebViewCacheInterceptorInst
import timesinternet.pwa.com.pwatil.container.Action
import timesinternet.pwa.com.pwatil.container.BridgeInterface
import timesinternet.pwa.com.pwatil.container.BridgeNew
import timesinternet.pwa.com.pwatil.container.Container
import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.timespwa.Notification.NotiHandler
import timesinternet.pwa.com.timespwa.Notification.initNotiManager
import java.io.File


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class SandBox : AppCompatActivity(), BridgeInterface, PermssionChannel, SmsListener {

    private var notificationManager: NotificationManager? = null
    private var permissionToRecordAccepted = false
    override fun messageReceived(messageBody: String?) {

        publishData(Action.smsRecieved, messageBody + "")
    }

    lateinit var prefs: SharedPreferences


    val REQUEST_IMAGE_CAPTURE = 1

    private fun startCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        }
    }

    override fun publishLocation(payload: String) {
        Log.d(Logs.CONTAINER, "LOCATION INTERFACE WORKING")
        publishData(Action.publishGpsData, payload)
    }

    @JavascriptInterface
    override fun publishData(action: String?, data: String?) {
        Log.d(Logs.CONTAINER, "PUBLISHING DATA")

        if (action == Action.openCamera) {
            startCamera()
        } else if (action == Action.sendNotification) {

            sendNotification("PWA App Notification", data + "")
        } else if (action == Action.someFunction) {
            Log.d("bridge", "some function called")
        } else if (action == Action.publishUserData) {


            val data_json = JSONObject()
            data_json.put("fname", DummyData.userDetails.fname)
            data_json.put("phone", DummyData.userDetails.phone)
            data_json.put("lname", DummyData.userDetails.lname)

            runOnUiThread {
                container.loadUrl("javascript:onPublishData('${action}' , '${("" + data_json.toString()!!).replace("'", "\\'")}')")
            }
        } else {
            runOnUiThread {
                container.loadUrl("javascript:onPublishData('${action}' , '${("" + data!!).replace("'", "\\'")}')")
            }
        }


    }


    private lateinit var container: WebView
    private lateinit var imageIcon: ImageView


    @RequiresApi(Build.VERSION_CODES.O)
    fun sendNotification(title: String, content: String) {
        val notificationID = 108
        val channelID = "com.ebookfrenzy.notifydemo.news"


        val notification = Notification.Builder(this@SandBox,
                channelID)
                .setContentTitle(title)

                .setContentText(content)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setChannelId(channelID)
                .build()
        notificationManager?.notify(notificationID, notification)
    }

    fun findFields(data: String, field: String): String {
        val s = data.indexOf(field + "=")
        val l = data.indexOf(",", s)

        val value = data.substring(s + field.length + 1, l)
        return value

    }

    private var mAppId: String = ""
    private var mAppUrl: String = ""
    private var mAppIconUrl: String = ""
    private var mAppName: String = ""
    private var tilApp: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.container_web)
        mAppId = intent.getStringExtra("appId")
        mAppUrl = intent.getStringExtra("appUrl")
        mAppIconUrl = intent.getStringExtra("mAppIconUrl")
        mAppName = intent.getStringExtra("mAppName")
        tilApp = intent.getBooleanExtra("tilApp", false);

        prefs = getSharedPreferences("AppPermission", 0)
        app_name_toolbar.text = mAppName
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            initNotiManager(this)
//            NotiHandler.getInstance(App.instance).addSimpleNotification(DummyData.simpleNotification)
        }

        container = findViewById<WebView>(R.id.container)

        Log.d(Logs.SDK, "initNotificationManager")
        initPermssionChannel()
        Log.d(Logs.SDK, "initPermssionChannel")
        initContainerSettingsAndCacheLimits()
        Log.d(Logs.SDK, "initContainerSettings")
        loadApp()



        removeLoading()

        //uncomment this service to detect ANR Status
        /* val anrIntent = Intent(applicationContext, PWAANRCheckerService::class.java)
         startService(anrIntent)*/

    }

    private fun removeLoading() {
        linear_loading.visibility = View.GONE
        /*val bottomSheetDialog: BottomSheetDialog = BottomSheetDialog(this@SandBox);
        bottomSheetDialog.setContentView(R.layout.permission_dialog);
        bottomSheetDialog.show();*/
        if (!tilApp && !prefs.getBoolean("opened", false) && !prefs.getBoolean(mAppName, false)) {
            prefs.edit().putBoolean("opened", true).apply()

            permission_container.visibility = View.VISIBLE
            continue_app.setOnClickListener(View.OnClickListener {
                permission_container.visibility = View.GONE
                prefs.edit().putBoolean(mAppName, true).apply()

            })
        }
    }

    fun initPermssionChannel() {
        Permit.addPermissionChannel(this)
        registerSMS()
    }

    fun layerInit(): BridgeNew {

        val userPwa = UserPwa("${intent.getStringExtra("appId")}", this)

        return BridgeNew(userPwa)
    }


    //  Initialize container_web settings
    private fun initContainerSettingsAndCacheLimits() {
        val _container = Container()
        val bridge = layerInit()
        container.addJavascriptInterface(bridge, "TilBridge")
        container.settings.javaScriptEnabled = true
        container.settings.javaScriptCanOpenWindowsAutomatically = false
        container.settings.domStorageEnabled = true
        container.webChromeClient = _container
        container.webViewClient = WebViewClient()
        container.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {

                injectJavaScriptTimer(container)

                super.onPageFinished(view, url)

            }
        }
        container.settings.allowFileAccess = true
        container.settings.setAppCacheEnabled(true)
        container.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        val builder = WebViewCacheInterceptor.Builder(this)

        //every PWA will have its own cacheDIR ie. /data/data/cache/PWAIDAPP
        builder.setCachePath(File(this.cacheDir, mAppId))
                .setCacheSize(Constants.PwaConstants.MAX_CACHE_AMOUNT.toLong())

        WebViewCacheInterceptorInst.getInstance().init(builder)


    }

    private fun injectJavaScriptTimer(container: WebView) {
        container.loadUrl(
                "javascript:(function() {  var seconds_left = 100000000000;\n" +
                        "    var interval = setInterval(function() {\n" +
                        "    \n" +
                        "  tilBridge.getPWAHealth()  " +
                        "\n" +
                        "   \n" +
                        "    }, 1000); })()")
    }

    fun loadApp() {

        //        val  url = "http://10.150.75.249:8080/"
        //To test cache use this URL AND Check new folder in /data/data/cache/PWAIDFOLDER
        //val url = "https://meuzic.com/"

        val url = mAppUrl
        Log.d(Logs.SDK, "Opening PWA @" + url)
        container.webViewClient = object : WebViewClient() {


            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                WebViewCacheInterceptorInst.getInstance().loadUrl(container, request.url.toString())
                return true
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                WebViewCacheInterceptorInst.getInstance().loadUrl(container, url)
                return true
            }

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {
                return WebViewCacheInterceptorInst.getInstance().interceptRequest(request)
            }

            override fun shouldInterceptRequest(view: WebView, url: String): WebResourceResponse? {
                return WebViewCacheInterceptorInst.getInstance().interceptRequest(url)
            }

            override fun onPageFinished(view: WebView?, url: String?) {


                super.onPageFinished(view, url)

            }

            override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
                Log.d("SandboxWebview", "old onReceivedError" + errorCode)
                if( errorCode == -2) {
                    error.visibility = View.VISIBLE
                    container.visibility = View.GONE
                    retry.setOnClickListener(View.OnClickListener {
                        error.visibility = View.GONE
                        finish()
                    })
                }
                super.onReceivedError(view, errorCode, description, failingUrl)
            }

        }
        WebViewCacheInterceptorInst.getInstance().loadUrl(container, url)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && container.canGoBack()) {
            //appbar.visibility = View.VISIBLE
            container.goBack()
            return true
        } else {
            return super.onKeyDown(keyCode, event)
        }
    }

    override fun onDestroy() {
        prefs.edit().putBoolean("opened", false).apply()
        super.onDestroy()
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 300
    }

    fun debugging() {
        Log.d(Logs.CONTAINER, DummyData.userDetails.toString())
    }


    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionToRecordAccepted = if (requestCode == 1) {
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else {
            false
        }
        if (!permissionToRecordAccepted) finish()
    }

    fun registerSMS() {

        SmsReciever.bindListener(this)
    }


    fun publishMarshalledData(signatureId: String, data: Any) {
        var marshalledData = Gson().toJson(data)
        Log.d(Logs.DATA_LAYER, "MARSHALLED DATA ${signatureId} " + marshalledData)
        runOnUiThread {
            container.loadUrl("javascript:tilBridge.NativeSuccessCallback('${signatureId}' , ${marshalledData}  )")
        }

    }

    fun publishMarshalledError(signatureId: String) {

        container.loadUrl("javascript:tilBridge.NativeFailureCallback('${signatureId}' , {}  )")
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (data != null && resultCode == Activity.RESULT_OK) {
            val extras = data.extras
            val imageBitmap = extras!!.get("data") as Bitmap

            Core.onCameraResult(requestCode.toString(), this)


            val _data = DataModels.CameraData("url Of Pic", "url Of Thumbnail", Core.toBase64(imageBitmap))
            publishMarshalledData(requestCode.toString(), _data)
        } else {
            publishMarshalledError(requestCode.toString())
        }


    }

    fun finishSandBox(view: View) {
        finish()
    }

    fun openPermissionActivity(view: View) {
        val intent = Intent(view.context, ViewAppsPermission::class.java)
        intent.putExtra("appId", mAppId)
        intent.putExtra("mAppIconUrl", mAppIconUrl)
        intent.putExtra("mAppName", mAppName)
        view.context.startActivity(intent)
    }

}
