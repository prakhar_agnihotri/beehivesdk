package timesinternet.pwa.com.pwatil

import android.app.Application
import android.content.Intent
import timesinternet.pwa.com.pwatil.Permission.Permit
import timesinternet.pwa.com.pwatil.Services.NotificationPushService

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Permit.initialize(this)
        instance = this



        val intent = Intent(this, NotificationPushService::class.java)
        startService(intent)
    }

    companion object {
        lateinit var instance: App
            private set
    }
}