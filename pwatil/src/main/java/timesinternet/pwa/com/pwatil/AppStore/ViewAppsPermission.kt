package timesinternet.pwa.com.pwatil.AppStore

import android.Manifest
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_view_apps.*
import kotlinx.android.synthetic.main.container_web.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import timesinternet.pwa.com.pwatil.Data.DummyData
import timesinternet.pwa.com.pwatil.DebugLog.Logs
import timesinternet.pwa.com.pwautil.R
import java.net.URL


class ViewAppsPermission : AppCompatActivity() {


    var rv : RecyclerView? =  null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         val appId = intent.getStringExtra("appId")
        val appName = intent.getStringExtra("mAppName")
        val mAppIconUrl = intent.getStringExtra("mAppIconUrl")

        val SHARED_PREFS_PERMISSION : String = "PERMISSION_MANAGEMENT_SDK${appId}"
        var prefs: SharedPreferences = getSharedPreferences(SHARED_PREFS_PERMISSION , 0)

        Log.d(Logs.SDK, "STARTING SDK ACTIVITY")
        setContentView(R.layout.activity_view_apps)
        rv = findViewById<RecyclerView>(R.id.rv)
        rv!!.layoutManager = GridLayoutManager(this, 1)
        rv!!.adapter = RVPermissionAdapter(prefs.all.toList(), this, prefs)
        app_title.text = appName
        Glide.with(applicationContext).load(mAppIconUrl).into(app_icon)



    }
}
