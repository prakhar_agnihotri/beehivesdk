package timesinternet.pwa.com.pwatil.sajal

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.content_test.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timesinternet.pwa.com.pwatil.Data.AnalyticsRepository
import timesinternet.pwa.com.pwatil.Models.DashboardRequest
import timesinternet.pwa.com.pwatil.Models.DashboardResponse
import timesinternet.pwa.com.pwatil.Models.DetailAppModel
import timesinternet.pwa.com.pwatil.Utils.apiUtils.ApiUtils
import timesinternet.pwa.com.pwatil.db.entity.AnalyticsEntity
import timesinternet.pwa.com.pwatil.db.entity.AppListEntity

import timesinternet.pwa.com.pwautil.R
import timesinternet.pwa.com.timespwa.Interfaces.APiInterfaces.APIInterface


class TestActivity : AppCompatActivity() {
    val TAG ="test"
//    var common: ICommon? = null
//    var isBound = false
//    val TAG: String = "TestActivity"
//    private val serviceConn = object : ServiceConnection {
//        override fun onServiceConnected(className: ComponentName,
//                                        service: IBinder) {
//
//            common = ICommon.Stub.asInterface(service)
//
//
//            isBound = true
//        }
//
//        override fun onServiceDisconnected(name: ComponentName) {
//            isBound = false
//        }
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)



//        AnalyticsRepository(application).insert(AnalyticsEntity(1,"2234qweqwe","some_data","LAUNCH",false,"HOMESCREEN" ,System.currentTimeMillis()))


        button7.setOnClickListener{

            var obj : DetailAppModel = DetailAppModel()

            obj.appId = "qwerty12345"
            obj.appName = "TOI"
            obj.bannerType = 1


            TestRepository(application).insert(obj)




          /*  AppListEntity("qwerty1234",1,"TOI","Games",
                    "","http://roadriot.june.in/resources/splash/portrait1024.png",1,"","",
                    "http://roadriot.june.in/resources/splash/portrait1024.png",3.5f,42,"Keep track of everything popular, new and upcoming",
                    "Keep track of everything popular, new and upcoming in the world of entertainment, even when your browser is closed. Powered by The Movie Database (TMDb).",
                    "",12345678)

            TestRepository(application).insert(AppListEntity("qwerty1234",1,"TOI","Games",
                    "","http://roadriot.june.in/resources/splash/portrait1024.png",1,"","",
                    "http://roadriot.june.in/resources/splash/portrait1024.png",3.5f,42,"Keep track of everything popular, new and upcoming",
            "Keep track of everything popular, new and upcoming in the world of entertainment, even when your browser is closed. Powered by The Movie Database (TMDb).",
            "",12345678))*/



//            TestRepository(application).updatePinnedStatus(2,"qwerty1234")
//            TestRepository(application).getPinnedApps(2)
        }

//        AnalyticsRepository(application). getAllItems()
//        setSupportActionBar(toolbar)

//        bind.setOnClickListener { v ->
//            val intent = Intent("com.timespwa.ipc")
//            intent.setPackage(this.packageName)
//            bindService(intent, serviceConn, Service.BIND_AUTO_CREATE)
//
//
//        }
//
//        button4.setOnClickListener {
//            try {
//                Log.d("test", "addition is " + common?.calculate(100, 10))
//            } catch (e: RemoteException) {
//                e.printStackTrace()
//            }
//        }
//
//        fab.setOnClickListener { view ->
//            val intent = Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
//            intent.putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
//            intent.putExtra(Settings.EXTRA_CHANNEL_ID, "channelId 785698")
//            startActivity(intent)
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()
//        }
//
//        button5.setOnClickListener {
//            val intent = Intent(this, SandBox::class.java)
//            intent.putExtra("url", "https://curtkart.com/pwa/camera/")
//            startActivity(intent)
//        }

        jadooApi.setOnClickListener {
            var mAPIService: APIInterface? = null
            mAPIService = ApiUtils.getAppDetailClient()

            val map = mutableMapOf<String, String>()
            map.put("id", "90cb3c23c238dcfc")


//            ApiUtils.enqueueWithRetry(mAPIService!!.getAppDetails("jadpoo",map), ApiUtils.DEFAULT_RETRIES, object : Callback<timesinternet.pwa.com.pwatil.Models.AppDetailResponse> {
//                override fun onResponse(call: Call<timesinternet.pwa.com.pwatil.Models.AppDetailResponse>, response: Response<timesinternet.pwa.com.pwatil.Models.AppDetailResponse>) {
//
//                    if (response.isSuccessful) {
//
//                        if (response.body() != null) {
//                            response.body().toString()
//                        }
//                        Log.d(TAG, "detailResponse code " + response.code() + " body of app details" + response.body().toString())
//                    } else {
//                        Log.d(TAG, "detailResponse is unsuccessfull ")
//
//                    }
//                }
//
//                override fun onFailure(call: Call<timesinternet.pwa.com.pwatil.Models.AppDetailResponse>, t: Throwable) {
//                    Log.e(TAG, t.message)
//                }
//            })
        }

        button6.setOnClickListener {
            var mAPIService: APIInterface? = null
            mAPIService = ApiUtils.getDashBoardClient()

            val requestParams = DashboardRequest(0,50,"BazziNow","trivia","Game")



            ApiUtils.enqueueWithRetry(mAPIService!!.getDashboardApps(requestParams), ApiUtils.DEFAULT_RETRIES, object : Callback<DashboardResponse> {
                override fun onResponse(call: Call<DashboardResponse>, response: Response<DashboardResponse>) {

                    if (response.isSuccessful) {

                        if (response.body() != null) {
                            response.body().toString()

                           /*
                           * detailResponse.body().detailResponse.get(detailResponse.body().detailResponse.lastIndex).apps
                           * */

                        }
                        Log.d(TAG, "detailResponse code " + response.code() + " body of app details" + response.body().toString())


                    } else {
                        Log.d(TAG, "detailResponse is unsuccessfull ")

                    }
                }

                override fun onFailure(call: Call<DashboardResponse>, t: Throwable) {
                    Log.e(TAG, t.message)
                }
            })

        }


        button8.setOnClickListener({

        })

    }
}
