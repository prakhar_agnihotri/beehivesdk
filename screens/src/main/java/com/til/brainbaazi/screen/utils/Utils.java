package com.til.brainbaazi.screen.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ListAdapter;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.screen.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by saurabh.garg on 2/15/18.
 */

public class Utils {
    public static final String AppsFlyerInviteURLPart1 = "https://app.appsflyer.com/com.brainbaazi?pid=Referral&af_sub1=";
    public static final String AppsFlyerInviteURLPart2 = "&af_sub2=";
    public static final String AppsFlyerInviteURLPart3 = "&af_sub3=native_social";

    public static boolean isKitKatAbove() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH);
    }

    public static String getDeviceId(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
        }
        return "";
    }

    public static String getDeviceName() {
        String deviceName = "";
        try {
            deviceName = Build.MANUFACTURER + "-" + Build.PRODUCT + "-" + Build.MODEL + "-";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceName;
    }

    public static String getVersionCode(Context context) {
        String versionCode = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = pInfo.versionName + "." + pInfo.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    public static boolean checkPermission(Context context, String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isMobileNumberValid(String number) {
        boolean isValidNumber = false;
        if (!TextUtils.isEmpty(number)) {
            try {
                long valueLong = Long.parseLong(number);
                if (valueLong > 0) {
                    isValidNumber = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return isValidNumber;
    }

    public static void inviteUser(Context context, User user, String screenName) {
        if (user == null) {
            return;
        }
//        String mInvitationUrl = AppsFlyerInviteURLPart1 + user.getUserName() + AppsFlyerInviteURLPart2 + screenName + AppsFlyerInviteURLPart3;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String appName = context.getResources().getString(R.string.app_name);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, appName);
        String message = "Download " + appName + " app from play store to play BrainBaazi and win Rs.50,000 daily cash prize.\n";
        String mInvitationUrl = context.getString(R.string.invitationUrl);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message + " Use my code \"" + user.getUserName() + "\" to sign up. " + mInvitationUrl + "\n\nJeetoon main, Jeete India!");
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, "Invite your friends"));
    }

    public static void shareApp(Context context) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String appName = context.getResources().getString(R.string.app_name);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Download " + appName + " app from play store to play BrainBaazi and win Rs.50,000 daily cash prize.\n" + "https://play.google.com/store/apps/details?id=" + context.getPackageName());
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, "Share with your friends"));
    }

    public static void hideKeyboard(Context context) {
        try {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception ignored) {

        }
    }

    public static boolean isInternetConnected(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception ignored) {

        }
        return false;
    }

    public static void showKeyboard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {

        }
    }

    public static int[] getDeviceDimens(Context context) {
        int[] dimen = new int[2];
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        dimen[0] = displaymetrics.widthPixels;
        dimen[1] = displaymetrics.heightPixels;
        return dimen;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static float convertDipToPx(Context context, int dp) {
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    public static void openAppPlayStore(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String appPackageName = context.getPackageName();
        try {
            intent.setData(Uri.parse("market://details?id=" + appPackageName));
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException anfe) {
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
            context.startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static File createImageFile(Context context) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    public static boolean eligibleForEarnExtraLife(String lifeEarningList, String currentUsername) {
        boolean eligible = false;
        if (lifeEarningList != null) {
            currentUsername = currentUsername.toLowerCase();
            lifeEarningList = lifeEarningList.toLowerCase();

            String toMatch = "|" + currentUsername + "|";
            eligible = lifeEarningList.contains(toMatch);
        }
        return eligible;
    }

    public static int measurePopupMenuWidth(Context context, ListAdapter adapter) {
        int width = 0;
        View itemView = null;
        int itemType = 0;
        final int widthMeasureSpec =
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec =
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = adapter.getCount();
        ViewGroup mMeasureParent = null;
        for (int i = 0; i < count; i++) {
            final int positionType = adapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }
            if (mMeasureParent == null) {
                mMeasureParent = new FrameLayout(context);
            }
            itemView = adapter.getView(i, itemView, mMeasureParent);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);
            width = Math.max(width, itemView.getMeasuredWidth());
        }
        return width;
    }

}
