package com.til.brainbaazi.screen.dialog.countryCode;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.otp.CountryModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.screen.R;

import java.util.ArrayList;

/**
 * Created by saurabh.garg on 2/22/18.
 */

public class ISDSelectionDialog extends Dialog implements View.OnClickListener{
    private CountryListModel countryListModel;
    private OnCountrySelectionListener onCountrySelectionListener;
    private String searchHint;
    private BrainbaaziStrings brainbaaziStrings;

    public ISDSelectionDialog(@NonNull Context context) {
        super(context);
    }

    public void setSearchHint(CountryListModel countryListModel, String searchHint, OnCountrySelectionListener countrySelectionListener, BrainbaaziStrings brainbaaziStrings) {
        this.onCountrySelectionListener = countrySelectionListener;
        this.countryListModel = countryListModel;
        this.searchHint = searchHint;
        this.brainbaaziStrings = brainbaaziStrings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_isd_selection);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        inflateDialogView();
    }

    private void inflateDialogView() {
        final CountryCodeRecycleAdapter isdCodeListAdapter = new CountryCodeRecycleAdapter(onCountrySelectionListener);
        isdCodeListAdapter.setCountryModelList(countryListModel.getCountryModels());

        if(brainbaaziStrings != null) {
            TextView selectCountry = findViewById(R.id.select_your_country);
            selectCountry.setText(brainbaaziStrings.otpStrings().countryText());
        }
        findViewById(R.id.cancel_action).setOnClickListener(this);
        final RecyclerView listView = findViewById(R.id.listView);
        listView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        listView.setAdapter(isdCodeListAdapter);

        final EditText inputCountryET = findViewById(R.id.inputCountryET);
        if (!TextUtils.isEmpty(searchHint)) {
            inputCountryET.setHint(searchHint);
        }
        inputCountryET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int textLength = charSequence.length();
                if (textLength == 0) {
                    isdCodeListAdapter.setCountryModelList(countryListModel.getCountryModels());
                    return;
                }
                String input = charSequence.toString().toLowerCase();
                ArrayList<CountryModel> tempArrayList = new ArrayList<>();
                for (CountryModel countryModel : countryListModel.getCountryModels()) {
                    String name = countryModel.getName().toLowerCase();
                    if (textLength <= name.length()) {
                        if (name.contains(input)) {
                            tempArrayList.add(countryModel);
                        }
                    }
                }
                isdCodeListAdapter.setCountryModelList(tempArrayList);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.cancel_action){
            dismiss();
        }
    }

    @Override
    public void dismiss() {
        onCountrySelectionListener= null;
        super.dismiss();
    }

    public static interface OnCountrySelectionListener {
        public void onCountrySelected(CountryModel countryModel);
    }
}
