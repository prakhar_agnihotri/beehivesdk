package com.til.brainbaazi.screen.dashboard;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.ListPopupWindow;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ImageViewInteractor;
import com.google.auto.factory.AutoFactory;
import com.soundcloud.android.crop.ActivityBuilder;
import com.soundcloud.android.crop.Crop;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.CommonStrings;
import com.til.brainbaazi.entity.strings.DashboardStrings;
import com.til.brainbaazi.entity.strings.ProfileStrings;
import com.til.brainbaazi.interactor.activity.ActivityPermissionResult;
import com.til.brainbaazi.interactor.activity.ActivityResult;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.dashboard.menu.MoreMenuAdapter;
import com.til.brainbaazi.screen.dashboard.menu.MoreMenuAdapter.MoreMenuItem;
import com.til.brainbaazi.screen.dialog.AddReferralCodeDialog;
import com.til.brainbaazi.screen.dialog.AddReferralSuccessDialog;
import com.til.brainbaazi.screen.dialog.ExtraLifeFullScreenDialog;
import com.til.brainbaazi.screen.dialog.ProfilePictureDialog;
import com.til.brainbaazi.screen.utils.PermissionUtils;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.dashboard.DashboardGameInfoViewState;
import com.til.brainbaazi.viewmodel.dashboard.DashboardViewModel;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;

import static android.app.Activity.RESULT_OK;

/**
 * Created by saurabh.garg on 15/02/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class DashboardScreen extends BaseScreen<DashboardViewModel> {

    @BindView(R2.id.rlConnection)
    View rlConnection;
    @BindView(R2.id.ll_dashboard)
    View ll_dashboard;
    @BindView(R2.id.menuIcon)
    View menuIcon;

    //Game Offline Mode
    @BindView(R2.id.ll_next_game)
    LinearLayout ll_next_game;
    @BindView(R2.id.tv_nextGame)
    CustomFontTextView tv_nextGame;
    @BindView(R2.id.tv_hh1)
    CustomFontTextView tv_hh1;
    @BindView(R2.id.tv_hh2)
    CustomFontTextView tv_hh2;
    @BindView(R2.id.tv_mm1)
    CustomFontTextView tv_mm1;
    @BindView(R2.id.tv_mm2)
    CustomFontTextView tv_mm2;
    @BindView(R2.id.tv_ss1)
    CustomFontTextView tv_ss1;
    @BindView(R2.id.tv_ss2)
    CustomFontTextView tv_ss2;
    @BindView(R2.id.tv_game_info)
    CustomFontTextView tv_game_info;
    @BindView(R2.id.tv_game_prize)
    CustomFontTextView tv_game_prize;

    @BindView(R2.id.ll_game_live)
    LinearLayout ll_game_live;
    @BindView(R2.id.tv_game_prize_live)
    CustomFontTextView tv_game_prize_live;
    @BindView(R2.id.playGameButton)
    ImageView playGameButton;

    @BindView(R2.id.tv_leaderboard)
    CustomFontTextView tv_leaderboard;
    @BindView(R2.id.tv_invite)
    CustomFontTextView tv_invite;

    @BindView(R2.id.addPicButton)
    View addPicButton;
    @BindView(R2.id.profilePic)
    ImageView profilePic;
    @BindView(R2.id.tv_userName)
    CustomFontTextView tv_userName;
    @BindView(R2.id.tv_balanceAmount)
    CustomFontTextView tv_balanceAmount;

    @BindView(R2.id.ll_extraLifeBox)
    LinearLayout ll_extraLifeBox;
    @BindView(R2.id.extraLifeIcon)
    ImageView heartIcon;
    @BindView(R2.id.tv_extraLivesTV)
    CustomFontTextView tv_extraLivesTV;
    @BindView(R2.id.tv_livesAmount)
    CustomFontTextView tv_livesAmount;
    @BindView(R2.id.tv_balanceTV)
    CustomFontTextView tv_balanceTV;

    private CounterClass timer;

    @OnClick(R2.id.menuIcon)
    void testMenuClick() {
        openMenuScreen();
    }

    @OnClick(R2.id.backIcon)
    void backIconCLicked() {
        onBackPressed();
    }

    @OnClick(R2.id.profilePic)
    void handleProfilePicClick() {
        openProfilePicDialog(true);
    }

    @OnClick(R2.id.addPicButton)
    void testAddPicClick() {
        openProfilePicDialog(true);
    }

    @OnClick(R2.id.playGameButton)
    void testPlayGameClick() {
        openPlayGameScreen();
    }

    @OnClick(R2.id.ll_balanceBox)
    void testBalanceClick() {
        openBalanceScreen();
    }

    @OnClick(R2.id.ll_extraLifeBox)
    void testLivesClick() {
        openUserLivesScreen();
    }

    @OnClick(R2.id.tv_leaderboard)
    void testLeaderBoardClick() {
        openLeaderBoardScreen();
    }

    @OnClick(R2.id.tv_invite)
    void testInviteClick() {
        handleInviteClick();
    }

    private ProgressDialog progressDialog;
    private Uri photoURI;
    private String profileImageURL;
    private boolean referralAdded;

    public DashboardScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_dashboard, viewGroup, false);
    }

    @Override
    protected void onBind(DashboardViewModel viewModel) {
        observeScreenResultsStatus(viewModel);
        observerOtpResponse(viewModel);
        observeDashBoardGameInfo(viewModel);
        observerUserImageResponse(viewModel);
        observeobServeStateBehaviorSubject(viewModel);
    }

    private void showLoadingUI() {
        rlConnection.setVisibility(View.GONE);
        ll_dashboard.setVisibility(View.VISIBLE);
        menuIcon.setVisibility(View.VISIBLE);
    }

    private void changeNetworkState(boolean isConnected) {
        if (isConnected) {
            rlConnection.setVisibility(View.GONE);
            ll_dashboard.setVisibility(View.VISIBLE);
            menuIcon.setVisibility(View.VISIBLE);
        } else {
            rlConnection.setVisibility(View.GONE);
            ll_dashboard.setVisibility(View.VISIBLE);
            menuIcon.setVisibility(View.VISIBLE);
        }
    }

    private void observeScreenResultsStatus(DashboardViewModel viewModel) {
        viewModel.getActivityInteractor().activityResult().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<ActivityResult>() {
            @Override
            public void accept(ActivityResult activityResult) throws Exception {
                if (activityResult == null) {
                    return;
                }
                if (activityResult.getRequestCode() == Crop.REQUEST_PICK && activityResult.getResultCode() == RESULT_OK) {
                    beginCrop(activityResult.getData().getData());
                } else if (activityResult.getRequestCode() == Crop.REQUEST_TAKE_PHOTO && activityResult.getResultCode() == RESULT_OK) {
                    beginCrop(photoURI);
                } else if (activityResult.getRequestCode() == Crop.REQUEST_CROP) {
                    try {
                        Uri selectedImageURI = Crop.getOutput(activityResult.getData());//data.getData();
                        getViewModel().uploadImageToServer(selectedImageURI);
                        profilePic.setImageURI(null);
                        profilePic.setImageURI(selectedImageURI);
                    } catch (Exception ignored) {

                    }
                }
            }
        });

        viewModel.getActivityInteractor().permissionResults().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<ActivityPermissionResult>() {
            @Override
            public void accept(ActivityPermissionResult permissionResults) throws Exception {
                if (permissionResults == null) {
                    return;
                }
                if (permissionResults.getRequestCode() == PermissionUtils.CODE_READ_STORAGE) {
                    if (permissionResults.getGrantResult().length > 0 && permissionResults.getGrantResult()[0] == PackageManager.PERMISSION_GRANTED) {
                        openProfilePicDialog(false);
                    }
                }
            }
        });
    }

    private void observerOtpResponse(DashboardViewModel viewModel) {
        DisposableOnNextObserver<OTPResponse> otpResponseObserver = new DisposableOnNextObserver<OTPResponse>() {
            @Override
            public void onNext(OTPResponse otpResponse) {
                ProfileStrings profileStrings = getBrainBaaziStrings().profileStrings();
                switch (otpResponse.event) {
                    case OTPResponse.EVENT_UPLOAD_START:
                        String message = profileStrings.uploadingText();
                        showProgressLoader(message);
                        break;
                    case OTPResponse.EVENT_UPLOAD_PROGRESS:
                        String message2 = profileStrings.uploadedText();
                        updateProgressDialog(message2 + " " + otpResponse.token + "%...");
                        break;
                    case OTPResponse.EVENT_UPLOAD_COMPLETE:
                        hideProgressDialog();
                        if (otpResponse.success) {
                            getViewModel().updateUserImageToServer(otpResponse.token);
                            profileImageURL = otpResponse.token;
                            //updateUserImageUrl(otpResponse.token);
                        } else {
                            String message3 = profileStrings.failedUpdateProfilePicture();
                            Toast.makeText(getContext(), message3, Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            }
        };
        viewModel.observeOTPResponse().observeOn(AndroidSchedulers.mainThread()).subscribe(otpResponseObserver);
        /*viewModel.observeOTPResponse().zipWith(getViewModel().observeBrainbaaziStrings(), new BiFunction<OTPResponse, BrainbaaziStrings, OTPResponse>() {
            @Override
            public OTPResponse apply(OTPResponse response, BrainbaaziStrings brainbaaziStrings) throws Exception {
                return response;
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(otpResponseObserver);*/
    }

    private void observeDashBoardGameInfo(DashboardViewModel viewModel) {
        DisposableObserver<DashboardGameInfoViewState> dashboardStateObserver = new DisposableOnNextObserver<DashboardGameInfoViewState>() {
            @Override
            public void onNext(DashboardGameInfoViewState s) {
                if (s.state() == DashboardViewModel.VIEW_STATE_LOADING) {
                    showLoadingUI();
                } else if (s.state() == DashboardViewModel.VIEW_STATE_SUCCESS) {
                    changeNetworkState(true);
                    updateDashBoardUI(s.dashboardInfo());
                    if (s.dashboardInfo().getUser() != null) {
                        updateUserInfo(s.dashboardInfo().getUser());
                        getViewModel().getAnalytics().setFireBaseUser(s.dashboardInfo().getUser());
                    }
                } else if (s.state() == DashboardViewModel.VIEW_STATE_FAILED) {
                    changeNetworkState(false);
                }
            }
        };
        addDisposable(dashboardStateObserver);
        viewModel.observeDashBoardState().distinctUntilChanged().observeOn(AndroidSchedulers.mainThread()).subscribe(dashboardStateObserver);
    }

    private void observerUserImageResponse(DashboardViewModel viewModel) {
        DisposableObserver<User> responseDisposableObserver = new DisposableOnNextObserver<User>() {
            @Override
            public void onNext(User usernameAvailableResponse) {
                handleUserImageResponse(usernameAvailableResponse);
            }
        };
        viewModel.observeUserImageResponse().observeOn(AndroidSchedulers.mainThread()).subscribe(responseDisposableObserver);
    }

    private void observeobServeStateBehaviorSubject(DashboardViewModel viewModel) {
        DisposableObserver<Integer> stateDisposableObserver = new DisposableOnNextObserver<Integer>() {
            @Override
            public void onNext(Integer state) {
                switch (state) {
                    case DashboardViewModel.VIEW_STATE_NO_NETWORK:
                        showNoNetworkMessage();
                        break;
                }
            }
        };
        viewModel.observeStateBehaviorSubject().observeOn(AndroidSchedulers.mainThread()).subscribe(stateDisposableObserver);
    }

    private void updateUserInfo(User userInfo) {
        updateUserImageUrl(userInfo.getUserImgUrl());
        setTextView(tv_userName, userInfo.getUserName(), false);
        setTextView(tv_balanceAmount, AppUtils.coolFormatWrap(userInfo.getUserBalance()), false);

        if (userInfo.getLives() > 0) {
            heartIcon.setImageResource(R.drawable.heart_filled);
        } else {
            heartIcon.setImageResource(R.drawable.ic_heart_line_icon);
        }
        tv_livesAmount.setText(String.valueOf(userInfo.getLives()));
        referralAdded = !TextUtils.isEmpty(userInfo.getReferralID());
    }

    private void updateDashBoardUI(DashboardInfo dashboardInfo) {
        long serverTime = dashboardInfo.getServerTime() != null ? dashboardInfo.getServerTime() : 0;
        if (dashboardInfo.getActive() != null && dashboardInfo.getActive() && dashboardInfo.getCurrentGameInfo() != null) {
            updateDashBoardForLive(dashboardInfo.getCurrentGameInfo(), serverTime);
        } else {
            updateDashBoardForOffline(dashboardInfo.getNextGameInfo(), serverTime);
        }
    }

    private void updateDashBoardForOffline(GameInfo gameInfo, long serverTime) {
        //Current Game is not there
        ll_game_live.setVisibility(View.GONE);
        ll_next_game.setVisibility(View.VISIBLE);

        if (gameInfo != null) {
            String value = AppUtils.formatWrap(gameInfo.getGamePrize()) + " " + getBrainBaaziStrings().dashboardStrings().prizeText();
            setTextView(tv_game_info, AppUtils.secondsToDateFormat(String.valueOf(gameInfo.getTimeStamp())), false);
            setTextView(tv_game_prize, value, false);

            if (gameInfo.getTimeStamp() != 0) {
                long nextGameTimeLeft = gameInfo.getTimeStamp() * 1000 - serverTime;
                if (this.timer != null) {
                    this.timer.cancel();
                }
                this.timer = new CounterClass(nextGameTimeLeft, 1000);
                timer.start();
            }
        }
    }

    private void updateDashBoardForLive(GameInfo currentGameInfo, long serverTime) {
        ll_game_live.setVisibility(View.VISIBLE);
        ll_next_game.setVisibility(View.GONE);
        String value = AppUtils.formatWrap(currentGameInfo.getGamePrize()) + " " + getBrainBaaziStrings().dashboardStrings().prizeText();
        setTextView(tv_game_prize_live, value, false);
    }

    private void updateUserImageUrl(String imgURL) {
        if (profilePic instanceof ImageViewInteractor) {
            ((ImageViewInteractor) profilePic).setDefault(R.drawable.ic_user_icon);
            addPicButton.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(imgURL) && !imgURL.equalsIgnoreCase("na")) {
                profileImageURL = imgURL;
                ((ImageViewInteractor) profilePic).setImageUrl(imgURL);
                addPicButton.setVisibility(View.GONE);
            }
        } else {
            profilePic.setImageResource(R.drawable.ic_user_icon);
            addPicButton.setVisibility(View.VISIBLE);
        }
    }

    private void setTextView(TextView textView, String value, boolean toHide) {
        if (!TextUtils.isEmpty(value)) {
            textView.setText(value);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(toHide ? View.GONE : View.INVISIBLE);
        }
    }

    @Override
    protected void onUnBind() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
    }

    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            //ll_next_game.setVisibility(View.GONE);
            //ll_game_live.setVisibility(View.VISIBLE);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            displayTimerTicker(millisUntilFinished);
        }
    }

    //Display ticker timer left for result display with animation
    @SuppressLint("DefaultLocale")
    private void displayTimerTicker(long millis) {
        Animation slideUpAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
        tv_hh1.clearAnimation();
        tv_hh2.clearAnimation();
        tv_mm1.clearAnimation();
        tv_mm2.clearAnimation();
        tv_ss1.clearAnimation();
        tv_ss2.clearAnimation();


        String hours_text = String.format("%02d", TimeUnit.MILLISECONDS.toHours(millis));
        Character[] charhoursArray = AppUtils.toCharacterArray(hours_text);
        if (charhoursArray != null) {
            if (charhoursArray.length > 0 && !tv_hh1.getText().toString().equals(charhoursArray[0].toString())) {
                tv_hh1.startAnimation(slideUpAnimation);
            }
            if (charhoursArray.length > 1 && !tv_hh2.getText().toString().equals(charhoursArray[1].toString())) {
                tv_hh2.startAnimation(slideUpAnimation);
            }
            tv_hh1.setText(charhoursArray[0].toString());
            tv_hh2.setText(charhoursArray[1].toString());
        }


        String mins_text = String.format("%02d", TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)));
        Character[] charMinsArray = AppUtils.toCharacterArray(mins_text);
        if (charMinsArray != null) {
            if (charMinsArray.length > 0 && !tv_mm1.getText().toString().equals(charMinsArray[0].toString())) {
                tv_mm1.startAnimation(slideUpAnimation);
            }
            if (charMinsArray.length > 1 && !tv_mm2.getText().toString().equals(charMinsArray[1].toString())) {
                tv_mm2.startAnimation(slideUpAnimation);
            }
            tv_mm1.setText(charMinsArray[0].toString());
            tv_mm2.setText(charMinsArray[1].toString());
        }


        String sec = String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        Character[] charObjectArray = AppUtils.toCharacterArray(sec);
        if (charObjectArray != null) {
            if (charObjectArray.length > 0 && !tv_ss1.getText().toString().equals(charObjectArray[0].toString())) {
                tv_ss1.startAnimation(slideUpAnimation);
            }
            if (charObjectArray.length > 1 && !tv_ss2.getText().toString().equals(charObjectArray[1].toString())) {
                tv_ss2.startAnimation(slideUpAnimation);
            }
            tv_ss1.setText(charObjectArray[0].toString());
            tv_ss2.setText(charObjectArray[1].toString());
        }
    }

    @Override
    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        if (brainBaaziStrings == null) {
            return;
        }
        DashboardStrings dashboardStrings = brainBaaziStrings.dashboardStrings();
        tv_nextGame.setText(dashboardStrings.nextGameText());
        tv_leaderboard.setText(dashboardStrings.leaderBoardText());
        tv_invite.setText(dashboardStrings.inviteText());

        tv_balanceTV.setText(dashboardStrings.balanceText());
        tv_extraLivesTV.setText(dashboardStrings.extraLivesText());
    }

    private void openProfilePicDialog(boolean checkPermission) {
        if (getViewModel().getUserInfo() == null) {
            return;
        }

        Utils.hideKeyboard(getContext());
        int type = (isUserImageAvailable() ? ProfilePictureDialog.TYPE_DASHBOARD : ProfilePictureDialog.TYPE_DASHBOARD_NO_REMOVE);
        ProfilePictureDialog profilePictureDialog = new ProfilePictureDialog(getContext(), new ProfilePictureDialog.OnProfileClickListener() {
            @Override
            public void onChoseFromGalleryClick() {
                sendAnalyticsEvent("Take New Photo", "Profile Creation", "Photo Added", "Choose From Gallery");
                ActivityBuilder activityBuilder = Crop.pickImage();
                getViewModel().getActivityInteractor().startActivityForResult(activityBuilder.getIntent(), activityBuilder.getRequestCode());
            }

            @Override
            public void onTakePictureClick() {
                sendAnalyticsEvent("Take New Photo", "Profile Creation", "Photo Added", "Take New Photo");
                dispatchTakePictureIntent();
            }

            @Override
            public void onDeleteAvatarClick() {
                sendAnalyticsEvent("Delete Avatar", "Profile Creation", "Photo Added", "Delete Avatar");
                getViewModel().updateUserImageToServer("na");
                profileImageURL = null;
                profilePic.setImageResource(R.drawable.ic_user_icon);
            }
        });
        profilePictureDialog.setUpUI(getContext(), getBrainBaaziStrings().profileStrings(), type);
        profilePictureDialog.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = Utils.createImageFile(getContext());
            } catch (IOException ignored) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                try {
                    photoURI = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".fileprovider", photoFile);
                } catch (IllegalArgumentException e) {
                    return;
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                getViewModel().getActivityInteractor().startActivityForResult(takePictureIntent, Crop.REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void handleUserImageResponse(User user) {
        if (user != null && !TextUtils.isEmpty(user.getUserImgUrl()) && !user.getUserImgUrl().equalsIgnoreCase("na")) {
            addPicButton.setVisibility(View.GONE);
        } else {
            addPicButton.setVisibility(View.VISIBLE);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getContext().getCacheDir(), "cropped"));
        ActivityBuilder builder = Crop.of(source, destination).asSquare().start(getContext());
        getViewModel().getActivityInteractor().startActivityForResult(builder.getIntent(), builder.getRequestCode());
    }

    private void showNoNetworkMessage() {
        hideProgressDialog();
        CommonStrings commonStrings = getBrainBaaziStrings().commonStrings();
        String message = commonStrings.internetNotConnected();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void showProgressLoader(String message) {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
                progressDialog.setInverseBackgroundForced(true);
                //progressDialog.setTitle("Loading");
                progressDialog.setMessage(message);
                progressDialog.setCancelable(true); // disable dismiss by tapping outside of the dialog
                progressDialog.show();
            } else {
                updateProgressDialog(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateProgressDialog(String message) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.setMessage(message);
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception ignored) {
        }
    }

    private void openUserLivesScreen() {
        User user = getViewModel().getUserInfo();
        if (user == null) {
            return;
        }
        ExtraLifeFullScreenDialog extraLifeDialog = new ExtraLifeFullScreenDialog(getContext(), user, getViewModel().getAnalytics(), getBrainBaaziStrings());
        extraLifeDialog.show();
        sendAnalyticsEvent("extra_life_screen_viewed", "Dashboard", "Extra Lives", "Viewed");
    }

    private void openPlayGameScreen() {
        getViewModel().openPlayGameScreen(true);
    }

    private void openMenuScreen() {
        User user = getViewModel().getUserInfo();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("Event Time", getViewModel().getAnalytics().getTimeStampInHHMMSSIST());
        getViewModel().cleverTapEvent("menu_clicked", data);

        MoreMenuAdapter menuAdapter = new MoreMenuAdapter(getContext(), getBrainBaaziStrings().menuStrings());
        if (!referralAdded) {
            menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_REFERRAL_CODE, R.drawable.menu_referal_coupon));
        }
       /* if (isUserImageAvailable()) {
            menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_CHANGE_PICTURE, R.drawable.menu_camera));
        } else {
            menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_ADD_PICTURE, R.drawable.menu_camera));
        }*/
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_HOW_TO_PLAY, R.drawable.menu_question));
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_FAQ, R.drawable.menu_faq));
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_RATE_US, R.drawable.menu_rate));
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_SHARE_APP, R.drawable.menu_share));
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_MORE_OPTIONS, R.drawable.ic_expand_more));
        final ListPopupWindow popupMenu = new ListPopupWindow(getContext());
        popupMenu.setAdapter(menuAdapter);
        popupMenu.setWidth(Utils.measurePopupMenuWidth(getContext(), menuAdapter));
        popupMenu.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popupMenu.dismiss();
                int menuID = (int) id;
                switch (menuID) {
                    case MoreMenuAdapter.MENU_ADD_PICTURE:
                    case MoreMenuAdapter.MENU_CHANGE_PICTURE:
                        openProfilePicDialog(true);
                        break;
                    case MoreMenuAdapter.MENU_REFERRAL_CODE:
                        sendCleverTapEvent("referral_screen_viewed");
                        openReferralDialogue();
                        break;
                    case MoreMenuAdapter.MENU_HOW_TO_PLAY:
                        sendCleverTapEvent("how_to_play_clicked");
                        sendAnalyticsEvent("How To Play", "Dialog", "How To Play", "");
                        openWebPage(getString(R.string.how_to_play), getString(R.string.url_how_play));
                        break;
                    case MoreMenuAdapter.MENU_FAQ:
                        sendCleverTapEvent("faq_clicked");
                        sendAnalyticsEvent("FAQ", "Dialog", "FAQ", "");
                        openWebPage(getString(R.string.faqs), getString(R.string.url_faq));
                        break;
                    case MoreMenuAdapter.MENU_TERMS:
                        sendCleverTapEvent("terms_clicked");
                        sendAnalyticsEvent("Terms of Use", "Dialog", "Terms of Use", "");
                        openWebPage(getString(R.string.terms_of_use), getString(R.string.url_terms));
                        break;
                    case MoreMenuAdapter.MENU_PRIVACY:
                        sendCleverTapEvent("privacy_clicked");
                        sendAnalyticsEvent("Privacy Policy", "Dialog", "Privacy Policy", "");
                        openWebPage(getString(R.string.privacy), getString(R.string.url_privacy));
                        break;
                    case MoreMenuAdapter.MENU_RATE_US:
                        sendCleverTapEvent("rate_us_clicked");
                        sendAnalyticsEvent("Rate Us", "Dialog", "Rate Us", "");
                        Utils.openAppPlayStore(getContext());
                        break;
                    case MoreMenuAdapter.MENU_SHARE_APP:
                        sendAnalyticsEvent("Share App", "Dialog", "Share App", "");
                        Utils.shareApp(getContext());
                        break;
                    case MoreMenuAdapter.MENU_MORE_OPTIONS:
                        sendCleverTapEvent("more");
                        sendAnalyticsEvent("More", "Dialog", "More", "");
                        openMoreMenuScreen();
                        break;
                }
            }
        });
        int rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getContext().getResources().getDisplayMetrics());
        int rightPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getContext().getResources().getDisplayMetrics());
        popupMenu.setAnchorView(menuIcon);
        popupMenu.setVerticalOffset(-menuIcon.getHeight());
        popupMenu.setModal(true);
        popupMenu.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        popupMenu.setDropDownGravity(Gravity.TOP | Gravity.RIGHT);
        popupMenu.setWidth(popupMenu.getWidth() + rightMargin * 2);
        popupMenu.setHorizontalOffset(-rightPadding);
        popupMenu.show();
    }

    private void openMoreMenuScreen() {
        MoreMenuAdapter menuAdapter = new MoreMenuAdapter(getContext(), getBrainBaaziStrings().menuStrings());
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_RULES, R.drawable.menu_rules));
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_TERMS, R.drawable.menu_terms_conditions));
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_PRIVACY, R.drawable.menu_privacy_policy));
        menuAdapter.add(new MoreMenuItem(MoreMenuAdapter.MENU_SIGN_OUT, R.drawable.menu_logout));
        final ListPopupWindow popupMenu = new ListPopupWindow(getContext());
        popupMenu.setAdapter(menuAdapter);
        popupMenu.setWidth(Utils.measurePopupMenuWidth(getContext(), menuAdapter));
        popupMenu.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popupMenu.dismiss();
                int menuID = (int) id;
                switch (menuID) {
                    case MoreMenuAdapter.MENU_RULES:
                        sendCleverTapEvent("rules_clicked");
                        sendAnalyticsEvent("Rules", "Dialog", "Rules", "");
                        openWebPage(getString(R.string.rules), getString(R.string.url_rules));
                        break;
                    case MoreMenuAdapter.MENU_TERMS:
                        sendCleverTapEvent("terms_clicked");
                        sendAnalyticsEvent("Terms of Use", "Dialog", "Terms of Use", "");
                        openWebPage(getString(R.string.terms_of_use), getString(R.string.url_terms));
                        break;
                    case MoreMenuAdapter.MENU_PRIVACY:
                        sendCleverTapEvent("privacy_clicked");
                        sendAnalyticsEvent("Privacy Policy", "Dialog", "Privacy Policy", "");
                        openWebPage(getString(R.string.privacy), getString(R.string.url_privacy));
                        break;
                    case MoreMenuAdapter.MENU_SIGN_OUT:
                        sendCleverTapEvent("logout");
                        sendAnalyticsEvent("Sign Out", "Dialog", "Sign Out", "");
                        getViewModel().signOutUser();
                        break;
                }
            }
        });
        int rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getContext().getResources().getDisplayMetrics());
        int rightPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getContext().getResources().getDisplayMetrics());
        popupMenu.setAnchorView(menuIcon);
        popupMenu.setVerticalOffset(-menuIcon.getHeight());
        popupMenu.setModal(true);
        popupMenu.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        popupMenu.setDropDownGravity(Gravity.TOP | Gravity.RIGHT);
        popupMenu.setWidth(popupMenu.getWidth() + rightMargin * 2);
        popupMenu.setHorizontalOffset(-rightPadding);
        popupMenu.show();
    }

    private void openReferralDialogue() {
        AddReferralCodeDialog addReferralCodeDialog = new AddReferralCodeDialog(getContext(), getViewModel().getDataRepository(), new AddReferralCodeDialog.AddReferralListener() {
            @Override
            public void referralSuccess() {
                referralAdded = true;
                showReferralSuccessDialog();
                getViewModel().loadDashboardInfo();
            }
        });
        addReferralCodeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                sendCleverTapEvent("referral_dismissed");
            }
        });
        addReferralCodeDialog.setReferralData(getViewModel().getAnalytics(), getViewModel().getUserInfo(), getBrainBaaziStrings());
        addReferralCodeDialog.setBrainBaaziStrings(getBrainBaaziStrings());
        addReferralCodeDialog.show();
    }

    private void showReferralSuccessDialog() {
        AddReferralSuccessDialog addReferralSuccessDialog = new AddReferralSuccessDialog(getContext(), getBrainBaaziStrings().dashboardStrings());
        addReferralSuccessDialog.show();
    }

    private void openWebPage(String title, String url) {
        WebData webData = WebData.builder().setTitle(title).setWebUrl(url).build();
        getViewModel().openWebPages(webData);
    }

    private void openBalanceScreen() {
        sendCleverTapEvent("balance_button_clicked");
        sendAnalyticsEvent("DASHBOARD_BALANCE", "Dashboard", "Balance", "");
        getViewModel().openUserBalanceScreen();
    }

    private void openLeaderBoardScreen() {
        sendAnalyticsEvent("Dashboard_Leaderboard", "Dashboard", "Leaderboard", "Weekly");
        getViewModel().openLeaderBoardScreen();
    }

    private void handleInviteClick() {
        User user = getViewModel().getUserInfo();
        if (user == null) {
            return;
        }
        sendCleverTapEvent("referral_intiated");
        sendAnalyticsEvent("Invite", "Dashboard", "Invite", "");
        Utils.inviteUser(getContext(), user, "dashboard_screen");
    }

    private void sendAnalyticsEvent(String mainEvent, String category, String action, String label) {
        User user = getViewModel().getUserInfo();
        if (user == null) {
            return;
        }
        getViewModel().getAnalytics().cleverTapScreenEvent(user, mainEvent);
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent(mainEvent).setCategory(category)
                .setAction(action)
                .setLabel(label)
                .setUserName(user.getUserName())
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getViewModel().sendAnalyticsEvent(gaEventModel);
    }

    private void sendCleverTapEvent(String eventName) {
        User user = getViewModel().getUserInfo();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("Event Time", getViewModel().getAnalytics().getTimeStampInHHMMSSIST());
        getViewModel().cleverTapEvent(eventName, data);
    }

    @Override
    public void resume() {
        super.resume();
        User user = getViewModel().getUserInfo();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        } else {
            data.put("username", getViewModel().getUserName());
        }
        data.put("Event Time", getViewModel().getAnalytics().getTimeStampInHHMMSSIST());
        getViewModel().cleverTapEvent("homescreen_viewed", data);
        getViewModel().logFireBaseScreen(Analytics.SCREEN_DASHBOARD);
        getViewModel().getAnalytics().cleverTapScreenEvent(user, "DashBoardScreen");
    }

    private boolean isUserImageAvailable() {
        return (!TextUtils.isEmpty(profileImageURL) && !profileImageURL.equalsIgnoreCase("na"));
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getActivityInteractor().performBackPress();
    }
}
