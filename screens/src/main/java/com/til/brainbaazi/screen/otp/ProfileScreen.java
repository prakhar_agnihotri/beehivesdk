package com.til.brainbaazi.screen.otp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainbaazi.component.network.ImageViewInteractor;
import com.google.auto.factory.AutoFactory;
import com.google.common.collect.ImmutableList;
import com.soundcloud.android.crop.ActivityBuilder;
import com.soundcloud.android.crop.Crop;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.ProfileStrings;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;
import com.til.brainbaazi.interactor.activity.ActivityPermissionResult;
import com.til.brainbaazi.interactor.activity.ActivityResult;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.dialog.ProfilePictureDialog;
import com.til.brainbaazi.screen.utils.PermissionUtils;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.otp.ProfileViewModel;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;

import static android.app.Activity.RESULT_OK;

/**
 * Created by saurabh.garg on 2/15/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class ProfileScreen extends BaseScreen<ProfileViewModel> {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.profilePic)
    ImageView profilePic;
    @BindView(R2.id.addIcon)
    View addIcon;

    @BindView(R2.id.userName)
    EditText userName;
    @BindView(R2.id.userNameStatus)
    CustomFontTextView userNameStatus;

    @BindView(R2.id.rl_suggestion)
    View suggestionLayout;
    @BindView(R2.id.textView_suggestion)
    CustomFontTextView textView_suggestion;
    @BindView(R2.id.llSuggestions)
    LinearLayout llSuggestions;

    @BindView(R2.id.referralCodeET)
    EditText referralCodeET;

    @BindView(R2.id.referralLayout)
    RelativeLayout referralLayout;

    @BindView(R2.id.txtView_have_refferal_code)
    TextView haveRefferalCode;

    @BindView(R2.id.referCodeStatus)
    CustomFontTextView referCodeStatus;
    @BindView(R2.id.applyLayout)
    View applyLayout;
    @BindView(R2.id.applyButton)
    CustomFontTextView applyButton;

    @BindView(R2.id.saveButton)
    Button saveButton;

    @OnClick(R2.id.profilePic)
    void handleProfilePicClick() {
        openProfilePicDialog(true);
    }

    @OnClick(R2.id.txtView_have_refferal_code)
    void handleHaveRefferalCodeClick() {
        referralLayout.setVisibility(View.VISIBLE);
        haveRefferalCode.setVisibility(View.GONE);
    }

    @OnClick(R2.id.applyLayout)
    void handleReferralSubmit() {
        sendReferralNameCheckRequest();
    }

    @OnClick(R2.id.saveButton)
    void handleUserSubmit() {
        submitModifiedUserData();
    }

    private ProgressDialog progressDialog;
    private Uri photoURI;
    private String profileImageURL;
    private boolean isUserNameValid = true;
    private String userNameString = "";
    private boolean referralCodeValid = false;
    private DisposableObserver<UsernameAvailableResponse> disposableCheckuserName;
    private DisposableObserver<UsernameAvailableResponse> disposableCheckReferralName;

    public ProfileScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_profile, viewGroup, false);
    }

    @Override
    protected void onBind(ProfileViewModel viewModel) {
        bindUIViews();
        observeViewState(viewModel);
        observeScreenResultsStatus(viewModel);
        observerOtpResponse(viewModel);
        observeUserInfo(viewModel);
    }

    private void bindUIViews() {
        setNavigationIcon(toolbar, R.drawable.arrow_back);

        ProfileStrings profileStrings = getBrainBaaziStrings().profileStrings();
        suggestionLayout.setVisibility(View.GONE);
        toolbar.setTitle(profileStrings.profileText());
        textView_suggestion.setText(profileStrings.suggestionText() + ":");
        referralCodeET.setHint(profileStrings.referralHintText());
        userNameStatus.setText(profileStrings.notValidText());
        applyButton.setText(profileStrings.applyText());

        userName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    submitModifiedUserData();
                    return true;
                }
                return false;
            }
        });
        updateUserImageUrl(null);
        userName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (isUserNameValid && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && userName.getText().length() > 0) {
                        userName.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.bbcolor_6dd7d7));
                    } /*else
                        userName.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.colorBlack));*/

                } else {
                    if (isUserNameValid && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        userName.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.bbcolorBlack));
                    }
                }
            }
        });

        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().startsWith("_") || charSequence.toString().startsWith(".")) {
                    if (charSequence.toString().length() > 1) {
                        userName.setText(charSequence.toString().substring(1));
                    } else {
                        userName.setText("");
                    }
                    return;
                }
                userNameString = charSequence.toString();
                if (AppUtils.isUserNameValid(userNameString) && !AppUtils.hasUpperCase(userNameString)) {
                    senduserNameCheckRequest();
                } else {
                    disableUserNameStatusText(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        referralCodeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (disposableCheckReferralName != null) {
                    disposableCheckReferralName.dispose();
                    disposableCheckReferralName = null;
                }
                referralCodeValid = false;

                if (s.toString().startsWith("_") || s.toString().startsWith(".")) {
                    if (s.toString().length() > 1) {
                        referralCodeET.setText(s.toString().substring(1));
                    } else {
                        referralCodeET.setText("");
                    }
                    return;
                }

                if (AppUtils.isUserNameValid(s.toString()) && !AppUtils.hasUpperCase(s.toString())) {
                    applyLayout.setVisibility(View.VISIBLE);
                    referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    referCodeStatus.setVisibility(View.GONE);
                } else {
                    applyLayout.setVisibility(View.GONE);
                }
                if (s.length() > 0) {
                    if (!referralCodeValid) {
                        saveButton.setEnabled(false);
                        saveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.bbblack_alfa_20));
                    }
                } else {
                    saveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorWhite));
                    saveButton.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void senduserNameCheckRequest() {
        if (disposableCheckuserName != null) {
            disposableCheckuserName.dispose();
            disposableCheckuserName = null;
        }
        disposableCheckuserName = new DisposableObserver<UsernameAvailableResponse>() {
            @Override
            public void onNext(UsernameAvailableResponse usernameAvailableResponse) {
                handleUserNameAvailabilityResponse(usernameAvailableResponse);
            }

            @Override
            public void onError(Throwable e) {
                disableUserNameStatusText(userNameString);
            }

            @Override
            public void onComplete() {

            }
        };
        addDisposable(disposableCheckuserName);
        getViewModel().checkUserNameServer(userNameString, UsernameAvailableResponse.CHECK_USERNAME).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(disposableCheckuserName);

    }

    private void sendReferralNameCheckRequest() {
        if (disposableCheckReferralName != null) {
            disposableCheckReferralName.dispose();
            disposableCheckReferralName = null;
        }
        String referral = referralCodeET.getText().toString();
        disposableCheckReferralName = new DisposableOnNextObserver<UsernameAvailableResponse>() {
            @Override
            public void onNext(UsernameAvailableResponse usernameAvailableResponse) {
                handleReferralCheckResponse(usernameAvailableResponse);
            }
        };
        getViewModel().checkUserNameServer(referral, UsernameAvailableResponse.CHECK_REFERRAL).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(disposableCheckReferralName);
        addDisposable(disposableCheckReferralName);
    }

    private void disableUserNameStatusText(String charSequence) {
        saveButton.setEnabled(false);
        isUserNameValid = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            userName.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.bbcolorBlack));
        }
        userNameStatus.setVisibility(View.GONE);
        userName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        if (charSequence.length() >= 3) {
            userNameStatus.setVisibility(View.VISIBLE);
            userNameStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorAccent));
            ProfileStrings brainBaaziStrings = getBrainBaaziStrings().profileStrings();
            if (TextUtils.isDigitsOnly(charSequence)) {
                String message = (brainBaaziStrings != null ? brainBaaziStrings.userNameDigitText() : getString(R.string.username_can_not_be_all_digits));
                userNameStatus.setText(message);
            } else if (AppUtils.hasUpperCase(charSequence.toString())) {
                String message = (brainBaaziStrings != null ? brainBaaziStrings.noUpperCaseText() : getString(R.string.uppercase_not_allowed));
                userNameStatus.setText(message);
            } else {
                String message = (brainBaaziStrings != null ? brainBaaziStrings.noSpecialCharText() : getString(R.string.special_characters_not_allowed));
                userNameStatus.setText(message);
            }
        } else if (charSequence.length() == 0) {
            userNameStatus.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(charSequence))
            userNameString = charSequence.toString();
        else
            userNameString = "";
    }

    private void observeUserInfo(ProfileViewModel viewModel) {
        DisposableObserver<User> balanceObserver = new DisposableObserver<User>() {
            @Override
            public void onNext(User user) {
                if (user == null) {
                    String message = getBrainBaaziStrings().profileStrings().apiFailureMsg();
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        viewModel.observeUserInfo().observeOn(AndroidSchedulers.mainThread()).subscribe(balanceObserver);
    }

    private void observerOtpResponse(ProfileViewModel viewModel) {
        DisposableObserver<OTPResponse> otpResponseObserver = new DisposableObserver<OTPResponse>() {
            @Override
            public void onNext(OTPResponse otpResponse) {
                ProfileStrings brainBaaziStrings = getBrainBaaziStrings().profileStrings();
                switch (otpResponse.event) {
                    case OTPResponse.EVENT_UPLOAD_START:
                        String message = brainBaaziStrings.uploadingText();
                        showProgressLoader(message);
                        break;
                    case OTPResponse.EVENT_UPLOAD_PROGRESS:
                        String message2 = brainBaaziStrings.uploadedText();
                        updateProgressDialog(message2 + " " + otpResponse.token + "%...");
                        break;
                    case OTPResponse.EVENT_UPLOAD_COMPLETE:
                        hideProgressDialog();
                        if (otpResponse.success) {
                            getViewModel().sendProfileImageEvent(userNameString);
                            profileImageURL = otpResponse.token;
//                            updateUserImageUrl(otpResponse.token);
                        } else {
                            String message3 = brainBaaziStrings.failedUpdateProfilePicture();
                            Toast.makeText(getContext(), message3, Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        viewModel.observeOTPResponse().observeOn(AndroidSchedulers.mainThread()).subscribe(otpResponseObserver);
    }

    private void observeViewState(ProfileViewModel viewModel) {
        Disposable subscribe = viewModel.observeViewState().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer state) throws Exception {
                switch (state) {
                    case ProfileViewModel.STATE_NO_NETWORK:
                        showNoNetworkMessage();
                        break;
                }
            }
        });
        addDisposable(subscribe);
    }

    private void observeScreenResultsStatus(ProfileViewModel viewModel) {
        viewModel.getActivityInteractor().activityResult().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<ActivityResult>() {
            @Override
            public void accept(ActivityResult activityResult) throws Exception {
                if (activityResult == null) {
                    return;
                }
                if (activityResult.getRequestCode() == Crop.REQUEST_PICK && activityResult.getResultCode() == RESULT_OK) {
                    beginCrop(activityResult.getData().getData());
                } else if (activityResult.getRequestCode() == Crop.REQUEST_TAKE_PHOTO && activityResult.getResultCode() == RESULT_OK) {
                    beginCrop(photoURI);
                } else if (activityResult.getRequestCode() == Crop.REQUEST_CROP) {
                    try {
                        Uri selectedImageURI = Crop.getOutput(activityResult.getData());//data.getData();
                        getViewModel().uploadImageToServer(selectedImageURI);
                        profilePic.setImageURI(null);
                        profilePic.setImageURI(selectedImageURI);
                    } catch (Exception ignored) {

                    }
                }
            }
        });

        viewModel.getActivityInteractor().permissionResults().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<ActivityPermissionResult>() {
            @Override
            public void accept(ActivityPermissionResult permissionResults) throws Exception {
                if (permissionResults == null) {
                    return;
                }
                if (permissionResults.getRequestCode() == PermissionUtils.CODE_READ_STORAGE) {
                    if (permissionResults.getGrantResult().length > 0 && permissionResults.getGrantResult()[0] == PackageManager.PERMISSION_GRANTED) {
                        openProfilePicDialog(false);
                    }
                }
            }
        });
    }

    private void handleUserNameAvailabilityResponse(UsernameAvailableResponse usernameAvailableResponse) {
        ProfileStrings profileStrings = getBrainBaaziStrings().profileStrings();
        if (!usernameAvailableResponse.isStatus()) {
            isUserNameValid = false;
            userNameStatus.setVisibility(View.VISIBLE);
            userName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_warning_icon, 0);
            String message = profileStrings.userUnavailableText();
            userNameStatus.setText(message);
            userNameStatus.setTextColor(getContext().getResources().getColor(R.color.bbcolorAccent));
            saveButton.setEnabled(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                userName.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.bbcolorAccent));
            }
            saveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.bbblack_alfa_20));
            if (usernameAvailableResponse.getSuggestions() != null)
                showSuggestion(usernameAvailableResponse.getSuggestions());
            else {
                suggestionLayout.setVisibility(View.GONE);
                llSuggestions.removeAllViews();
            }
        } else {
            isUserNameValid = true;
            if (userName.getText().length() > 2) {
                userName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_green, 0);
                userNameStatus.setVisibility(View.VISIBLE);
                String message = profileStrings.userAvailableText();
                userNameStatus.setText(message);
                userNameStatus.setTextColor(getContext().getResources().getColor(R.color.bbcolor_3faf7e));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    userName.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.bbcolor_6dd7d7));
                }
                saveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorWhite));
                saveButton.setEnabled(true);
            }
        }
    }

    private void showSuggestion(ImmutableList<String> suggestions) {
        String userNameSugg = userName.getText().toString();
        if (!TextUtils.isEmpty(userNameSugg)) {
            suggestionLayout.setVisibility(View.VISIBLE);
            llSuggestions.removeAllViews();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            for (int i = 0; i < suggestions.size(); i++) {
                final String name = suggestions.get(i);
                View suggestionView = layoutInflater.inflate(R.layout.profile_username_suggestion, null);
                ((TextView) suggestionView.findViewById(R.id.tvSuggestion)).setText(suggestions.get(i));
                suggestionView.findViewById(R.id.tvSuggestion).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userName.setText(name);
                    }
                });
                llSuggestions.addView(suggestionView);
            }
        }
    }

    private void handleReferralCheckResponse(UsernameAvailableResponse usernameAvailableResponse) {
        ProfileStrings profileStrings = getBrainBaaziStrings().profileStrings();
        if (!usernameAvailableResponse.isStatus()) {
            saveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorWhite));
            saveButton.setEnabled(true);
            referralCodeValid = true;
            applyLayout.setVisibility(View.GONE);
            referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_green, 0);
            referCodeStatus.setVisibility(View.VISIBLE);
            String message = profileStrings.referralValidText();
            referCodeStatus.setText(message);
            referCodeStatus.setTextColor(getContext().getResources().getColor(R.color.bbcolor_3faf7e));
        } else {
            //Api fail
            saveButton.setEnabled(false);
            saveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.bbblack_alfa_20));
            referralCodeValid = false;
            applyLayout.setVisibility(View.GONE);
            referCodeStatus.setVisibility(View.VISIBLE);
            referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_warning_icon, 0);
            String message = profileStrings.referralInValidText();
            referCodeStatus.setText(message);
            referCodeStatus.setTextColor(getContext().getResources().getColor(R.color.bbcolorAccent));
        }
    }

    private void submitModifiedUserData() {
        Utils.hideKeyboard(getContext());

        if (isUserNameValid && !TextUtils.isEmpty(userNameString)) {
            String referCode = "NA";
            if (referralCodeValid) {
                referCode = referralCodeET.getText().toString().toLowerCase(Locale.ENGLISH);
            }
            String imageUrl = (!TextUtils.isEmpty(profileImageURL) ? profileImageURL : "");
            UserRequestModel.Builder builder = UserRequestModel.builder();
            builder.setUserName(userNameString).setProfileImageUrl(imageUrl).setReferralId(referCode);
            builder.setDeviceType("A").setDeviceId(Utils.getDeviceId(getContext()));
            builder.setAcquistionSource("");
            if (getViewModel().getPhoneNumber() != null) {
                builder.setMobileNumber(getViewModel().getPhoneNumber().getPhoneNumber());
                builder.setCountryCode(getViewModel().getPhoneNumber().getCountryModel().getCode());
            }
            //getViewModel().submitModifiedUserData(builder.build());
        } else {
            ProfileStrings profileStrings = getBrainBaaziStrings().profileStrings();
            if (TextUtils.isEmpty(userNameString)) {
                String message = profileStrings.checkUserName();
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            } else if (userNameString.length() < 3) {
                String message = profileStrings.minimumCharText();
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            } else if (userNameString.length() > 15) {
                String message = profileStrings.maximumCharText();
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void openProfilePicDialog(boolean checkPermission) {
        Utils.hideKeyboard(getContext());
        ProfilePictureDialog profilePictureDialog = new ProfilePictureDialog(getContext(), new ProfilePictureDialog.OnProfileClickListener() {
            @Override
            public void onChoseFromGalleryClick() {
                ActivityBuilder activityBuilder = Crop.pickImage();
                getViewModel().getActivityInteractor().startActivityForResult(activityBuilder.getIntent(), activityBuilder.getRequestCode());
            }

            @Override
            public void onTakePictureClick() {
                dispatchTakePictureIntent();
            }

            @Override
            public void onDeleteAvatarClick() {
            }
        });
        profilePictureDialog.setUpUI(getContext(), getBrainBaaziStrings().profileStrings(), ProfilePictureDialog.TYPE_PROFILE);
        profilePictureDialog.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (getContext() != null && takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = Utils.createImageFile(getContext());
            } catch (IOException ignored) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                try {
                    photoURI = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".fileprovider", photoFile);
                } catch (IllegalArgumentException e) {
                    return;
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                getViewModel().getActivityInteractor().startActivityForResult(takePictureIntent, Crop.REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getContext().getCacheDir(), "cropped"));
        ActivityBuilder builder = Crop.of(source, destination).asSquare().start(getContext());
        getViewModel().getActivityInteractor().startActivityForResult(builder.getIntent(), builder.getRequestCode());
    }

    private void updateUserImageUrl(String imgURL) {
        if (profilePic instanceof ImageViewInteractor) {
            ((ImageViewInteractor) profilePic).setDefault(R.drawable.ic_user_icon);
            addIcon.setVisibility(View.VISIBLE);
            ((ImageViewInteractor) profilePic).setImageUrl(imgURL);
            if (!TextUtils.isEmpty(imgURL)) {
                profileImageURL = imgURL;
                addIcon.setVisibility(View.GONE);
            }
        } else {
            profilePic.setImageResource(R.drawable.ic_profile);
            addIcon.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        toolbar.setTitle(brainBaaziStrings.profileStrings().profileText());
    }

    private void showNoNetworkMessage() {
        hideProgressDialog();
        String message = getBrainBaaziStrings().commonStrings().internetNotConnected();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void showProgressLoader(String message) {
        try {
            progressDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            progressDialog.setInverseBackgroundForced(true);
            //progressDialog.setTitle("Loading");
            progressDialog.setMessage(message);
            progressDialog.setCancelable(true); // disable dismiss by tapping outside of the dialog
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateProgressDialog(String message) {
        if (progressDialog != null) {
            progressDialog.setMessage(message);
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    protected void onUnBind() {

    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        if (getContext() != null)
            Utils.hideKeyboard(getContext());
        getViewModel().getActivityInteractor().performBackPress();
    }

    @Override
    public void resume() {
        super.resume();
        Map<String, Object> data = new HashMap<>();
        data.put("Phone", getViewModel().getPhoneNumber().getPhoneNumber());
        getViewModel().cleverTapEvent("profile_creation_viewed", data);

        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("profile_creation_viewed").build();
        getViewModel().logFireBaseScreen(gaEventModel, null);
    }
}
