package com.til.brainbaazi.screen.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.viewmodel.BaseScreenModel;
import com.til.brainbaazi.viewmodel.BaseViewModel;


/**
 * Created by prashant.rathore on 02/02/18.
 */
public class ScreenController<VM extends BaseScreenModel> {

    static final int STATE_FRESH = 0;
    static final int STATE_CREATE = 1;
    static final int STATE_START = 2;
    static final int STATE_RESUME = 3;
    static final int STATE_PAUSE = 4;
    static final int STATE_STOP = 5;
    static final int STATE_DESTROY = 6;


    private final VM viewModel;
    private final ScreenFactory screenFactory;

    private BaseScreen<VM> boundedView = null;
    private SegmentInfo segmentInfo;

    private static final String KEY_VIEW_STATE = "viewState";
    private static final String KEY_VIEWMODEL_STATE = "viewModelState";
    private Context context;
    private LayoutInflater layoutInflater;

    int currentState = STATE_FRESH;

    public ScreenController(SegmentInfo segmentInfo, VM viewModel, ScreenFactory screenFactory) {
        this.screenFactory = screenFactory;
        this.segmentInfo = segmentInfo;
        this.viewModel = viewModel;
    }

    void attach(Context context, LayoutInflater layoutInflater) {
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    SegmentInfo getSegmentInfo() {
        return segmentInfo;
    }

    public BaseScreen<VM> createView(ViewGroup parentView) {
        return (BaseScreen<VM>) screenFactory.create(context, layoutInflater, parentView);
    }

    public BaseScreen getBoundedView() {
        return boundedView;
    }


    public void onCreate() {
        currentState = STATE_CREATE;
        viewModel.setParams(segmentInfo.getParams());
        viewModel.onCreate();
        Bundle savedState = segmentInfo.getSavedState();
        viewModel.restoreState(savedState.getBundle(KEY_VIEWMODEL_STATE));
    }

    public void bindView(BaseScreen<VM> view) {
        boundedView = view;
        boundedView.bindViewModel(viewModel);
        Bundle savedState = segmentInfo.getSavedState();
        boundedView.restoreState(savedState.getBundle(KEY_VIEW_STATE));
    }

    public void onStart() {
        currentState = STATE_START;
        viewModel.willShow();
        boundedView.willShow();
    }

    public void onResume() {
        currentState = STATE_RESUME;
        boundedView.resume();
        viewModel.resume();
    }

    public void onPause() {
        currentState = STATE_PAUSE;
        viewModel.pause();
        boundedView.pause();
        Bundle viewState = new Bundle();
        Bundle viewModleState = new Bundle();

        viewModel.saveState(viewModleState);
        boundedView.saveState(viewState);

        segmentInfo.getSavedState().putBundle(KEY_VIEWMODEL_STATE, viewModleState);
        segmentInfo.getSavedState().putBundle(KEY_VIEW_STATE, viewState);
    }

    public void onStop() {
        currentState = STATE_STOP;
        boundedView.willHide();
        viewModel.willHide();
    }

    public void unBindView() {
        boundedView.unBindViewModel();
        boundedView = null;
    }

    public void onDestroy() {
        currentState = STATE_DESTROY;
        viewModel.onDestroy();
    }

    void dettach() {
        this.context = null;
        this.layoutInflater = null;
    }

    public boolean handleBackPressed() {
        return viewModel.handleBackPressed();
    }

}
