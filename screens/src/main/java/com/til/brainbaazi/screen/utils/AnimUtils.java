package com.til.brainbaazi.screen.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.til.brainbaazi.screen.customViews.base.AnimatableView;


/**
 * Created by saurabh.garg on 2/16/18.
 */

public class AnimUtils {

    public static void videoFullScreenAnimation(Context context, final AnimatableView view, int animDuration, GradientDrawable gradientDrawable) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        ObjectAnimator cornerAnimation = ObjectAnimator.ofFloat(gradientDrawable, "cornerRadius", 1000, 0f);
        ValueAnimator widthAnimator = ValueAnimator.ofInt(view.getMeasuredWidth(), width / 3, width / 2, width);
        ValueAnimator heightAnimator = ValueAnimator.ofInt(view.getMeasuredHeight(), width / 3, width / 2, height);

        heightAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = val;
                view.setLayoutParams(layoutParams);
            }
        });


        widthAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.width = val;
                view.setLayoutParams(layoutParams);
            }
        });
        widthAnimator.setDuration(300);
        heightAnimator.setDuration(300);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(cornerAnimation, widthAnimator, heightAnimator);
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setRadius(0);
                setMargins(view, 0, 0, 0, 0);
                view.invalidate();

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        set.start();
    }

    public static void reverseFullScreenAnimation(final Context context, final AnimatableView view, int animDuration, GradientDrawable gradientDrawable) {
        ValueAnimator widthAnimator = ValueAnimator.ofInt(view.getMeasuredWidth(), (int) convertDipToPx(context, 60));
        ValueAnimator heightAnimator = ValueAnimator.ofInt(view.getMeasuredHeight(), (int) convertDipToPx(context, 60));


        ObjectAnimator cornerAnimation =
                ObjectAnimator.ofFloat(gradientDrawable, "cornerRadius", 0, 1000);
        heightAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = val;
                view.setRadius(convertDipToPx(context, 40));
                setMargins(view, 0, (int) convertDipToPx(context, 65), 0, 0);
                view.setLayoutParams(layoutParams);
            }
        });


        widthAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.width = val;
                view.setLayoutParams(layoutParams);
            }
        });
        widthAnimator.setDuration(200);
        heightAnimator.setDuration(200);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(cornerAnimation, widthAnimator, heightAnimator);
        set.start();
    }

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    private static float convertDipToPx(Context context, int dp) {
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }


}
