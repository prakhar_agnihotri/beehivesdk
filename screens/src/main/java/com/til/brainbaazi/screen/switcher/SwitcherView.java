package com.til.brainbaazi.screen.switcher;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.til.brainbaazi.screen.BaseScreen;

/**
 * Created by prashant.rathore on 15/02/18.
 */

public class SwitcherView extends BaseScreen<SwitcherViewModel> {


    public SwitcherView(Context context, LayoutInflater layoutInflater, ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return null;
    }

    @Override
    protected void onBind(SwitcherViewModel viewModel) {

    }

    @Override
    protected void onUnBind() {

    }
}
