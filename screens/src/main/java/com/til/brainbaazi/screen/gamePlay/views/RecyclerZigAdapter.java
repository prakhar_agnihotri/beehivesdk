package com.til.brainbaazi.screen.gamePlay.views;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.brainbaazi.component.network.ImageViewInteractor;
import com.til.brainbaazi.entity.game.event.WinUser;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.utils.AppUtils;

/**
 * Created by saurabh.garg on 2/21/18.
 */

public class RecyclerZigAdapter extends RecyclerView.Adapter<RecyclerZigAdapter.ViewHolder> {

    private Winner values;

    public RecyclerZigAdapter(Winner myDataset) {
        this.values = myDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.zig_zag_item_top_winners, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        setProfileData(values.getWinUserList().get(position), holder.profielImage, holder.profileName);
        holder.winnerPrizeMoney.setText(holder.winnerPrizeMoney.getContext().getString(R.string.currencySymbol) + AppUtils.coolFormatWrap(values.getPrizeAmount()));
    }

    private void setProfileData(WinUser winUser, ImageView profielImage, CustomFontTextView profileName) {
        profileName.setText(winUser.getName());
        if (profielImage instanceof ImageViewInteractor) {
            profielImage.setImageResource(R.drawable.ic_profile);
            ((ImageViewInteractor) profielImage).setDefault(R.drawable.ic_user_icon);
            ((ImageViewInteractor) profielImage).setImageUrl(winUser.getImgUrl());
        } else {
            profielImage.setImageResource(R.drawable.ic_profile);
        }
    }

    protected void updateUserImageUrl(ImageView ivUserImage, String imageUrl) {
        if (ivUserImage instanceof ImageViewInteractor) {
            ((ImageViewInteractor) ivUserImage).setDefault(R.drawable.ic_user_icon);
            ((ImageViewInteractor) ivUserImage).setImageUrl(imageUrl);
        } else {
            ivUserImage.setImageResource(R.drawable.ic_profile);
        }
    }

    @Override
    public int getItemCount() {
        if (values != null) {
            return (values.getWinUserList().size());
        } else {
            return 0;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CustomFontTextView profileName;
        private final CustomFontTextView winnerPrizeMoney;
        private final ImageView profielImage;
        private final LinearLayout linearLayout;

        public ViewHolder(View v) {
            super(v);
            profileName = v.findViewById(R.id.profileName);
            winnerPrizeMoney = v.findViewById(R.id.winnerPrizeMoney);
            profielImage = v.findViewById(R.id.winnerProfilePic);
            linearLayout = v.findViewById(R.id.row_linear_parent);
        }
    }


}
