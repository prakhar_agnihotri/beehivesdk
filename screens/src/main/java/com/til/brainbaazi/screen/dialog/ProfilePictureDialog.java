package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;

import com.til.brainbaazi.entity.strings.ProfileStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

/**
 * Created by saurabh.garg on 2/24/18.
 */

public class ProfilePictureDialog extends BottomSheetDialog implements View.OnClickListener {
    public static final int TYPE_PROFILE = 1;
    public static final int TYPE_DASHBOARD = 2;
    public static final int TYPE_DASHBOARD_NO_REMOVE = 3;

    private OnProfileClickListener onProfileClickListener;

    public ProfilePictureDialog(@NonNull Context context, @NonNull OnProfileClickListener profileClickListener) {
        super(context);
        this.onProfileClickListener = profileClickListener;
    }

    public void setUpUI(Context context, ProfileStrings profileStrings, int dialogType) {
        View view = View.inflate(context, R.layout.dialog_profile_change, null);
        CustomFontTextView takeNewPhotoTV = view.findViewById(R.id.takeNewPhotoTV);
        CustomFontTextView choseFromGalleryTV = view.findViewById(R.id.choseFromGalleryTV);
        CustomFontTextView deleteAvatarTV = view.findViewById(R.id.deleteAvatarTV);

        takeNewPhotoTV.setText(profileStrings.takeNewPhotoText());
        choseFromGalleryTV.setText(profileStrings.choseGalleryText());
        deleteAvatarTV.setText(profileStrings.removeAvatarText());

        if (dialogType == TYPE_PROFILE || dialogType == TYPE_DASHBOARD_NO_REMOVE) {
            deleteAvatarTV.setVisibility(View.GONE);
        } else {
            deleteAvatarTV.setVisibility(View.VISIBLE);
            deleteAvatarTV.setOnClickListener(this);
        }
        takeNewPhotoTV.setOnClickListener(this);
        choseFromGalleryTV.setOnClickListener(this);
        setContentView(view);
    }

    @Override
    public void onClick(View v) {
        if (v == findViewById(R.id.takeNewPhotoTV)) {
            onProfileClickListener.onTakePictureClick();

        } else if (v == findViewById(R.id.choseFromGalleryTV)) {
            onProfileClickListener.onChoseFromGalleryClick();

        } else if (v == findViewById(R.id.deleteAvatarTV)) {
            onProfileClickListener.onDeleteAvatarClick();

        }
        dismiss();
    }

    @Override
    protected void onStop() {
        super.onStop();
        onProfileClickListener = null;
    }

    public static interface OnProfileClickListener {
        void onChoseFromGalleryClick();

        void onTakePictureClick();

        void onDeleteAvatarClick();
    }

}
