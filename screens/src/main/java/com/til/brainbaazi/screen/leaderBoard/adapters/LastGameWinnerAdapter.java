package com.til.brainbaazi.screen.leaderBoard.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.brainbaazi.component.network.ImageViewInteractor;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardUser;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class LastGameWinnerAdapter extends RecyclerView.Adapter<LastGameWinnerAdapter.ViewHolder> {

    private List<LeaderBoardUser> mValues;
    private long amountPerUser = -1;

    public void addValues(List<LeaderBoardUser> values, boolean toClear) {
        if (this.mValues == null || toClear) {
            this.mValues = new ArrayList<>();
        }
        this.mValues.addAll(values);
        notifyDataSetChanged();
    }

    public void setAmountPerUser(long amountPerUser) {
        this.amountPerUser = amountPerUser;
    }

    @Override
    public LastGameWinnerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.zig_zag_item_top_winners, parent, false);
        return new LastGameWinnerAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LastGameWinnerAdapter.ViewHolder holder, int position) {
        LeaderBoardUser leaderBoardUser = mValues.get(position);
        setProfileData(leaderBoardUser, holder.profielImage, holder.profileName);
        holder.winnerPrizeMoney.setText(holder.winnerPrizeMoney.getContext().getString(R.string.currencySymbol) + AppUtils.coolFormatWrap(amountPerUser));
    }

    private void setProfileData(LeaderBoardUser winUser, ImageView profielImage, CustomFontTextView profileName) {
        profileName.setText(winUser.getUserName());
        if (profielImage instanceof ImageViewInteractor) {
            ((ImageViewInteractor) profielImage).setDefault(R.drawable.ic_user_icon);
            ((ImageViewInteractor) profielImage).setImageUrl(winUser.getImgUrl());
        } else {
            profielImage.setImageResource(R.drawable.ic_profile);
        }
    }

    protected void updateUserImageUrl(ImageView ivUserImage, String imageUrl) {
        if (ivUserImage instanceof ImageViewInteractor) {
            ((ImageViewInteractor) ivUserImage).setDefault(R.drawable.ic_user_icon);
            ((ImageViewInteractor) ivUserImage).setImageUrl(imageUrl);
        } else {
            ivUserImage.setImageResource(R.drawable.ic_profile);
        }
    }

    @Override
    public int getItemCount() {
        return mValues != null ? mValues.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CustomFontTextView profileName;
        private final CustomFontTextView winnerPrizeMoney;
        private final ImageView profielImage;
        private final LinearLayout linearLayout;

        public ViewHolder(View v) {
            super(v);
            profileName = v.findViewById(R.id.profileName);
            winnerPrizeMoney = v.findViewById(R.id.winnerPrizeMoney);
            profielImage = v.findViewById(R.id.winnerProfilePic);
            linearLayout = v.findViewById(R.id.row_linear_parent);
        }
    }
}
