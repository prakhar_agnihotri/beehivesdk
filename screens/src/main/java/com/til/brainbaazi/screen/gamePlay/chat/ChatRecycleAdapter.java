package com.til.brainbaazi.screen.gamePlay.chat;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class ChatRecycleAdapter extends RecyclerView.Adapter<ChatRecycleAdapter.ChatViewHolder> {
    private List<ChatMsg> chatMsgList = new ArrayList<>(20);

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chat, parent, false);
        return new ChatViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        ChatMsg chatObject = chatMsgList.get(position);
        holder.chatMessage.setText(Html.fromHtml(holder.chatMessage.getContext().getString(R.string.chat_message, chatObject.getName(), chatObject.getMessage())));
        holder.uName.setText(String.valueOf(chatObject.getName().charAt(0)));
    }

    @Override
    public int getItemCount() {
        return chatMsgList != null ? chatMsgList.size() : 0;
    }

    public static class ChatViewHolder extends RecyclerView.ViewHolder {
        public final CustomFontTextView chatMessage;
        public final CustomFontTextView uName;

        public ChatViewHolder(View view) {
            super(view);
            chatMessage = view.findViewById(R.id.chatMessage);
            uName = view.findViewById(R.id.user_name);
        }
    }

    public List<ChatMsg> getChatMsgList() {
        return chatMsgList;
    }

    public void addChatMsgList(ChatMsg chatMsg) {
        if (chatMsgList == null) {
            chatMsgList = new ArrayList<>();
        }
        int currentIndex = chatMsgList.size();
        chatMsgList.add(chatMsg);
        notifyItemInserted(currentIndex);
        if (currentIndex >= 20) {
            chatMsgList.remove(0);
            notifyItemRemoved(0);
        }
    }

    public void clearData() {
        if (chatMsgList != null) {
            chatMsgList.clear();
        }
    }
}
