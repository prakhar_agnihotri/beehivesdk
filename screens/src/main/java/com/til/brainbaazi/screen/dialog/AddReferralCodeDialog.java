package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.repo.DataRepository;
import com.til.brainbaazi.entity.ReferralResponse;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.DashboardStrings;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.utils.AppUtils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by arpit.toshniwal on 04/03/18.
 */

public class AddReferralCodeDialog extends BaseDialog {

    private EditText referralCodeET;
    private TextView error;
    private Button cancelBtn, addBtn;
    private DataRepository dataRepository;
    private CompositeDisposable compositeDisposable;
    private AddReferralListener mAddReferralListener;
    private Analytics analytics;
    private User user;
    private DashboardStrings dashboardStrings;

    public AddReferralCodeDialog(@NonNull Context context, DataRepository dataRepository, @NonNull AddReferralListener addReferralListener) {
        super(context);
        this.dataRepository = dataRepository;
        this.compositeDisposable = new CompositeDisposable();
        this.mAddReferralListener = addReferralListener;
    }

    public void setReferralData(Analytics analytics, User user, BrainbaaziStrings brainBaaziStrings) {
        this.analytics = analytics;
        this.user = user;
        this.dashboardStrings = brainBaaziStrings.dashboardStrings();
    }


    @Override
    public int getDialogLayout() {
        return R.layout.dialog_add_referral_code;
    }

    @Override
    public void inflateDialogView() {
        referralCodeET = findViewById(R.id.referralCodeET);
        cancelBtn = findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(this);
        addBtn = findViewById(R.id.addBtn);
        addBtn.setOnClickListener(this);
        addBtn.setEnabled(false);
        addBtn.setAlpha(0.4f);
        error = findViewById(R.id.error);
        referralCodeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (charSequence.toString().startsWith("_") || charSequence.toString().startsWith(".")) {
                    if (charSequence.toString().length() > 1) {
                        referralCodeET.setText(charSequence.toString().substring(1));
                    } else {
                        referralCodeET.setText("");
                    }
                    return;
                }
                error.setVisibility(View.INVISIBLE);
                String userNameString = charSequence.toString();
                if (AppUtils.isUserNameValid(userNameString) && !AppUtils.hasUpperCase(userNameString)) {
                    addBtn.setEnabled(true);
                    addBtn.setAlpha(1.0f);
                } else {
                    addBtn.setEnabled(false);
                    addBtn.setAlpha(0.4f);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cancelBtn) {
            dismiss();
        } else if (view.getId() == R.id.addBtn) {
            addReferralCode();
        } else
            super.onClick(view);
    }

    private void addReferralCode() {
        DisposableObserver disposableCheckuserName = new DisposableObserver<UsernameAvailableResponse>() {
            @Override
            public void onNext(UsernameAvailableResponse usernameAvailableResponse) {
                if (!usernameAvailableResponse.isStatus()) {
                    hitReferralApi(referralCodeET.getText().toString().toLowerCase(Locale.ENGLISH));
                } else {
                    error.setVisibility(View.VISIBLE);
                    referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_warning_icon, 0);
                }
            }

            @Override
            public void onError(Throwable e) {
                error.setVisibility(View.VISIBLE);
                referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_warning_icon, 0);
            }

            @Override
            public void onComplete() {

            }
        };
        compositeDisposable.add(disposableCheckuserName);
        dataRepository.loadUsernameAvailableResponse(referralCodeET.getText().toString().toLowerCase()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(disposableCheckuserName);
    }

    private void hitReferralApi(final String referralCode) {
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("referral_code", referralCode);
        analytics.cleverTapEvent("Add Referral Code", data);

        DisposableObserver disposableCheckuserName = new DisposableObserver<Response<ReferralResponse>>() {
            @Override
            public void onNext(Response<ReferralResponse> responseResponse) {
                if (responseResponse.success()) {
                    ReferralResponse referralResponse = responseResponse.value();
                    if (TextUtils.isEmpty(referralResponse.getError())) {
                        mAddReferralListener.referralSuccess();
                        dismiss();
                    } else {
                        error.setVisibility(View.VISIBLE);
                        referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_warning_icon, 0);
                    }
                } else {
                    error.setVisibility(View.VISIBLE);
                    referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_warning_icon, 0);
                }
            }

            @Override
            public void onError(Throwable e) {
                error.setVisibility(View.VISIBLE);
                referralCodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.text_warning_icon, 0);
            }

            @Override
            public void onComplete() {

            }
        };
        compositeDisposable.add(disposableCheckuserName);
        dataRepository.addReferral(referralCode).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(disposableCheckuserName);
    }

    @Override
    public void dismiss() {
        compositeDisposable.dispose();
        super.dismiss();
    }

    @Override
    public void cancel() {
        compositeDisposable.dispose();
        super.cancel();
    }

    public interface AddReferralListener {
        void referralSuccess();
    }
}
