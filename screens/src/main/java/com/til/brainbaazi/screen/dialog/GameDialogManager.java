package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.brainbaazi.component.Analytics;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.dialog.coachmarks.DataSaverInfoDialog;
import com.til.brainbaazi.utils.AppUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class GameDialogManager implements Runnable {

    public static final int NONE = -1;

    public static final int SHOW_ELIMINATION_DIALOG = 1;

    public static final int SHOW_ELIMINATION_ON_MISS_QUESTION = 2;

    public static final int SHOW_LIFE_USED_DIALOG = 3;

    public static final int SHOW_HIGH_SCORE_DIALOG = 4;

    public static final int SHOW_YOU_WON_DIALOG = 5;

    public static final int SHOW_YOU_ARE_LATE_DIALOG = 6;

    public static final int SHOW_EARN_LIFE_DIALOG = 7;

    public static final int SHOW_EXTRA_LIFE_DIALOG = 8;

    private static final String TAG = "GameDialogManager";
    private Context context;
    private User mUser;
    private View mBackgroundView;
    private FrameLayout viewContainer;
    private long prizeAmount;
    private int _currentDialogState = GameDialogManager.NONE;
    private int _futureDialogState = GameDialogManager.NONE;
    private boolean _gameThreadRunning;
    private Thread _gameThread;
    private BrainbaaziStrings brainBaaziStrings;
    private final String gameID;
    private final Analytics analytics;
    private int question;
    private boolean dialogToShow = true;

    public GameDialogManager(Context activityContext, Analytics analytics, long gameID, long prizeAmt, User user, View backgroundView, FrameLayout viewContainer) {
        this.context = activityContext;
        this.analytics = analytics;
        this.prizeAmount = prizeAmt;
        this.mUser = user;
        this.mBackgroundView = backgroundView;
        this.viewContainer = viewContainer;
        this.gameID = String.valueOf(gameID);
    }

    public void setUser(User user) {
        this.mUser = user;
    }

    public void setPrizeAmount(long prizeAmount) {
        this.prizeAmount = prizeAmount;
    }

    public void setBrainBaaziStrings(BrainbaaziStrings brainBaaziStrings) {
        this.brainBaaziStrings = brainBaaziStrings;
    }

    public void setQuestion(int question) {
        this.question = question;
    }

    public void setDialogToShow(boolean dialogToShow) {
        this.dialogToShow = dialogToShow;
    }

    public void requestStateUpdate(int nextDialogState) {
        if (_currentDialogState != _futureDialogState) {
            Log.d(TAG, "Future dialog state is not served yet. Are you sure to skip the currently allotted future state");
        }
        _futureDialogState = nextDialogState;
    }

    public void onUpdate() {
        if (_currentDialogState != _futureDialogState) {
            updateDialogState();
        }
    }

    private void updateDialogState() {
        if (_futureDialogState == _currentDialogState || !dialogToShow) {
            return;
        }
        switch (_futureDialogState) {
            // Elimination
            case SHOW_ELIMINATION_DIALOG: {
                handleEliminateUserState();
                break;
            }

            // Elimination on missing question
            case SHOW_ELIMINATION_ON_MISS_QUESTION: {
                handleShowEliminationOnMissQuestion();
                break;
            }

            // Consume Life
            case SHOW_LIFE_USED_DIALOG: {
                handleConsumeLifeState();
                break;
            }

            // High Score
            case SHOW_HIGH_SCORE_DIALOG: {
                handleShowHighScoreState();
                break;
            }

            // You Won
            case SHOW_YOU_WON_DIALOG: {
                handleYouWonState();
                break;
            }

            // You are late
            case SHOW_YOU_ARE_LATE_DIALOG: {
                handleGameLateState();
                break;
            }

            // Earn life
            case SHOW_EARN_LIFE_DIALOG: {
                handleEarnLifeState();
                break;
            }

            //Get Extra Life
            case SHOW_EXTRA_LIFE_DIALOG: {
                handleExtraLifeState();
                break;
            }
        }

        this._currentDialogState = this._futureDialogState;
    }

    private void handleEliminateUserState() {
        try {
            mBackgroundView.setVisibility(View.VISIBLE);
            final EliminateDialog eliminateDialog = new EliminateDialog(context, mUser, analytics, gameID, question);
            eliminateDialog.setBrainBaaziStrings(brainBaaziStrings);
            eliminateDialog.show();

            Map<String, Object> data = new HashMap<>();
            data.put("username", mUser.getUserName());
            data.put("Phone", mUser.getPhoneNumber());
            data.put("game_id", gameID);
            data.put("question_id", question);
            data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
            analytics.cleverTapEvent("eliminated_screen_view_event", data);

            Bundle bundle = new Bundle();
            bundle.putString("username", mUser.getUserName());
            bundle.putString("action", "no");
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("extra_life_screen_viewed").build();
            analytics.logFireBaseScreen(gaEventModel, bundle);

            String category = "Game Play - " + gameID;
            gaEventModel = GaEventModel.builder().setMainEvent("Eliminated popup show").setCategory(category)
                    .setAction("Eliminated" + question)
                    .setLabel("Show")
                    .setUserName(mUser.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            analytics.logFireBaseEvent(gaEventModel);
        } catch (Exception ignored) {

        }
    }

    private void handleShowEliminationOnMissQuestion() {
        try {
            LateEliminateDialog lateEliminateDialog = new LateEliminateDialog(context, mUser, analytics, gameID, question);
            lateEliminateDialog.setBrainBaaziStrings(brainBaaziStrings);
            lateEliminateDialog.show();

            Bundle bundle = new Bundle();
            bundle.putString("username", mUser.getUserName());
            bundle.putString("action", "no");
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("extra_life_screen_viewed").build();
            analytics.logFireBaseScreen(gaEventModel, bundle);
        } catch (Exception ignored) {

        }
    }

    private void handleConsumeLifeState() {
        try {
            Map<String, Object> data = new HashMap<>();
            data.put("username", mUser.getUserName());
            data.put("Phone", mUser.getPhoneNumber());
            data.put("game_id", gameID);
            data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
            analytics.cleverTapEvent("extra_life_screen_viewed", data);

            data.put("action", "yes");
            analytics.cleverTapEvent("extra_life_used", data);

            Bundle bundle = new Bundle();
            bundle.putString("username", mUser.getUserName());
            bundle.putString("action", "yes");
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("extra_life_screen_viewed").build();
            analytics.logFireBaseScreen(gaEventModel, bundle);

            mBackgroundView.setVisibility(View.GONE);
            View lifeView = View.inflate(context, R.layout.view_live_extra, null);
            viewContainer.removeAllViews();
            viewContainer.addView(lifeView);

            viewContainer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewContainer.removeAllViews();
                    mBackgroundView.setVisibility(View.VISIBLE);
                }
            }, 2000);
        } catch (Exception ignored) {

        }
    }

    private void handleShowHighScoreState() {
        try {
            mBackgroundView.setVisibility(View.VISIBLE);
            HighScoreDialog highScoreDialog = new HighScoreDialog(context, mUser, 5);
            highScoreDialog.setBrainBaaziStrings(brainBaaziStrings);
            highScoreDialog.show();
        } catch (Exception ignored) {

        }
    }

    private void handleYouWonState() {
        try {
            mBackgroundView.setVisibility(View.VISIBLE);
            final YouWonDialog youWonDialog = new YouWonDialog(context, mUser, prizeAmount);
            youWonDialog.setBrainBaaziStrings(brainBaaziStrings);
            youWonDialog.show();

            Map<String, Object> data = new HashMap<>();
            data.put("username", mUser.getUserName());
            data.put("Phone", mUser.getPhoneNumber());
            data.put("amount", prizeAmount);
            data.put("game_id", gameID);
            data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
            analytics.cleverTapEvent("money_won", data);

            String category = "Game Play - " + gameID;
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("money_won").setCategory(category)
                    .setAction("money_won")
                    .setLabel(String.valueOf(prizeAmount))
                    .setUserName(mUser.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            analytics.logFireBaseEvent(gaEventModel);
        } catch (Exception ignored) {

        }
    }

    private void handleGameLateState() {
        try {
            Map<String, Object> data = new HashMap<>();
            data.put("username", mUser.getUserName());
            data.put("Phone", mUser.getPhoneNumber());
            data.put("game_id", gameID);
            data.put("message", context.getString(R.string.lateText));
            data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
            analytics.cleverTapEvent("late_screen_view_event", data);

            mBackgroundView.setVisibility(View.VISIBLE);
            final LateDialog lateDialog = new LateDialog(context, mUser, analytics, gameID, question);
            lateDialog.setBrainBaaziStrings(brainBaaziStrings);
            lateDialog.show();
        } catch (Exception ignored) {

        }
    }

    private void handleEarnLifeState() {
        try {
            mBackgroundView.setVisibility(View.VISIBLE);
            EarnLifeDialog earnLifeDialog = new EarnLifeDialog(context, mUser);
            earnLifeDialog.setBrainBaaziStrings(brainBaaziStrings);
            earnLifeDialog.show();
        } catch (Exception ignored) {

        }
    }

    private void handleExtraLifeState() {
        mBackgroundView.setVisibility(View.VISIBLE);
        showEarnExtraLifeDialog();
    }

    public void showEarnExtraLifeDialog() {
//        ExtraLifeDialog extraLifeDialog = new ExtraLifeDialog(context, mUser, analytics);
//        extraLifeDialog.setBrainBaaziStrings(brainBaaziStrings);
//        extraLifeDialog.show();
    }

    public void showStreamModeCoachmarkDialog() {
        DataSaverInfoDialog dataSaverInfoDialog = new DataSaverInfoDialog(context);
        dataSaverInfoDialog.setBrainBaaziStrings(brainBaaziStrings);
        dataSaverInfoDialog.show();
    }

    public void showLifeCoachmarkDialog() {
        ExtraLifeInfoDialog earnLifeDialog = new ExtraLifeInfoDialog(context);
        earnLifeDialog.setBrainBaaziStrings(brainBaaziStrings);
        earnLifeDialog.show();
    }

    private final Handler _gameThreadHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            onUpdate();
        }
    };

    public void startThread() {
        _gameThreadRunning = true;
        _gameThread = new Thread(this);
        _gameThread.start();
    }

    public void stopThread() {
        _gameThreadRunning = false;
        boolean retry = true;
        while (retry) try {
            _gameThread.join();
            retry = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (_gameThreadRunning) {
            try {
                Thread.sleep(100);
                _gameThreadHandler.sendMessage(_gameThreadHandler.obtainMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
