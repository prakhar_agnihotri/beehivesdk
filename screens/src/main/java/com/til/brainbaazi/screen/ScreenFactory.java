package com.til.brainbaazi.screen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.til.brainbaazi.viewmodel.BaseScreenModel;
import com.til.brainbaazi.viewmodel.BaseViewModel;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 15/02/18.
 */

public interface ScreenFactory {
    BaseScreen<?> create(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView);
}
