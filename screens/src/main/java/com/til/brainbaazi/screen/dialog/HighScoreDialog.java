package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.WinCountFrameLayout;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class HighScoreDialog extends BaseDialog {
    private User user;
    private int level;
    private long lastClickMillisResult;
    private long THRESHOLD_MILLIS = 1500L;

    public HighScoreDialog(@NonNull Context context, @NonNull User user, int level) {
        super(context);
        setDelayDismiss(5000);
        this.user = user;
        this.level = level;
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_high_score;
    }

    @Override
    public void inflateDialogView() {
//        ImageView closeButton = findViewById(R.id.closeButton);
//
//        CustomFontTextView youWonHead = findViewById(R.id.youWonHead);
//        WinCountFrameLayout winCountFrameLayout = findViewById(R.id.rl);
//        CustomFontTextView numberOfCorrectAnswers = findViewById(R.id.numberOfCorrectAnswers);
//
//        Button shareButton = findViewById(R.id.shareButton);
//        CustomFontTextView bestSoFar = findViewById(R.id.bestSoFar);
//        CustomFontTextView continueText = findViewById(R.id.continueText);
//
//        if (getBrainBaaziStrings() != null) {
//            BrainBaaziStrings brainBaaziStrings = getBrainBaaziStrings();
//
//            youWonHead.setText(brainBaaziStrings.getNewHighScoreText());
//            shareButton.setText(brainBaaziStrings.getShareText());
//            bestSoFar.setText(brainBaaziStrings.getBestSoFar());
//            continueText.setText(brainBaaziStrings.getContinueWatching());
//
//            String correctAnswer = brainBaaziStrings.getYouGotCorrectText();
//            correctAnswer = correctAnswer.replaceAll("<correct>", String.valueOf(level));
//            numberOfCorrectAnswers.setText(correctAnswer);
//
//        }
//
//        winCountFrameLayout.setViewCount(11);
//        winCountFrameLayout.setWinCount(level);
//        winCountFrameLayout.setUserImage(user.getUserImgUrl());
//
//        closeButton.setOnClickListener(this);
//        shareButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.closeButton || id == R.id.continueText) {
            dismiss();
        } else if (id == R.id.shareWonButton) {
            long now = SystemClock.elapsedRealtime();
            if (now - lastClickMillisResult > THRESHOLD_MILLIS) {
                Utils.inviteUser(view.getContext(), user, "game_screen");
            }
            lastClickMillisResult = now;
        }
    }
}
