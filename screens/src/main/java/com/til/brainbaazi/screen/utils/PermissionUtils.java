package com.til.brainbaazi.screen.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.screen.dialog.permission.SMSPermissionDialog;

/**
 * Created by saurabh.garg on 11/16/16.
 */

public class PermissionUtils {
    public static final String KEY_PERMISSION_DIALOG = "permission_dialog";

    public static final int PERMISSION_SMS = 1000;
    public static final int PERMISSION_READ_STORAGE = 1000;

    public static final int CODE_READ_SMS = 1000;
    public static final int CODE_READ_STORAGE = 1001;

    public interface OnPermissionListener {
        public void dialogAlreadyShown();

        public void dialogAllowed();

        public void dialogDenied();
    }

    public static void showUserPermissionText(Context context, SharedPreferences sharedPreferences, int permissionType, BrainbaaziStrings brainBaaziStrings, OnPermissionListener permissionListener) {
        if (context == null) {
            return;
        }
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(KEY_PERMISSION_DIALOG, Context.MODE_PRIVATE);
        }
        String key = KEY_PERMISSION_DIALOG + "_" + permissionType;
        if (sharedPreferences.getBoolean(key, true)) {
            //dialog shown
            sharedPreferences.edit().putBoolean(key, false).apply();
            showUserPermissionDialog(context, permissionType, permissionListener, brainBaaziStrings);
        } else {
            permissionListener.dialogAlreadyShown();
        }
    }

    private static void showUserPermissionDialog(final Context context, int permissionType, final OnPermissionListener listener, BrainbaaziStrings brainBaaziStrings) {
        if (permissionType == PERMISSION_SMS) {
            SMSPermissionDialog smsPermissionDialog = new SMSPermissionDialog(context, listener);
            smsPermissionDialog.setBrainBaaziStrings(brainBaaziStrings);
            smsPermissionDialog.show();
        }
    }
}
