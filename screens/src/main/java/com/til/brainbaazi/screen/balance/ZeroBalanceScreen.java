package com.til.brainbaazi.screen.balance;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainbaazi.component.Analytics;
import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.PaymentStrings;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.viewmodel.balance.ZeroBalanceViewModel;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by saurabh.garg on 2/15/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class ZeroBalanceScreen extends BaseScreen<ZeroBalanceViewModel> {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.tv_wallet_empty)
    CustomFontTextView tv_wallet_empty;
    @BindView(R2.id.tv_win_cash)
    CustomFontTextView tv_win_cash;


    public ZeroBalanceScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_balance_zero, viewGroup, false);
    }

    @Override
    protected void onBind(ZeroBalanceViewModel viewModel) {
        toolbar.setTitle(getContext().getString(R.string.cash_out));
        setNavigationIcon(toolbar, R.drawable.arrow_back);
        DisposableObserver<User> balanceObserver = new DisposableObserver<User>() {
            @Override
            public void onNext(User user) {
                if (user != null)
                    updateDashBoardUI(user);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        viewModel.observeUserInfo().observeOn(AndroidSchedulers.mainThread()).subscribe(balanceObserver);
    }

    private void updateDashBoardUI(User user) {
    }

    @Override
    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        PaymentStrings paymentStrings = brainBaaziStrings.paymentStrings();
        toolbar.setTitle(paymentStrings.cashoutText());
        tv_wallet_empty.setText(paymentStrings.walletEmptyText());
        tv_win_cash.setText(paymentStrings.winCashText());
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getActivityInteractor().performBackPress();
    }

    @Override
    protected void onUnBind() {

    }

    @Override
    public void resume() {
        super.resume();
        getViewModel().getAnalytics().cleverTapScreenEvent(getViewModel().getUser(), "balance_dialog_viewed");
        getViewModel().logFireBaseScreen(Analytics.SCREEN_ZERO_BALANCE_SCREEN);
    }
}
