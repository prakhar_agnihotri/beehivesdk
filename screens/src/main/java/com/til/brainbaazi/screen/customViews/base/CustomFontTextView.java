package com.til.brainbaazi.screen.customViews.base;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.utils.FontManager;

/**
 * Created by saurabh.garg on 2/15/18.
 */

public class CustomFontTextView extends AppCompatTextView {

    private String fontName;

    public CustomFontTextView(Context context) {
        this(context, null);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTextView(context, attrs, defStyleAttr);
    }

    private void initTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView, defStyleAttr, 0);
        try {
            this.fontName = a.getString(R.styleable.CustomFontTextView_ctv_font);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            a.recycle();
        }
        setFont(fontName);
    }

    public void setFont(String fontFamily) {
        if (!TextUtils.isEmpty(fontFamily)) {
            Typeface typeFace = FontManager.getInstance(getContext()).getTypeFace(fontFamily, getTypeface());
            setTypeface(typeFace);
        }
    }
}
