package com.til.brainbaazi.screen.gamePlay.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.AnswerOptions;
import com.til.brainbaazi.entity.game.response.GameResponse;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

/**
 * Created by saurabh.garg on 3/5/18.
 */

public class StreamTextModeView extends RelativeLayout {
    private CustomFontTextView viewTop;
    private CustomFontTextView tvTitle;

    private View llStats;
    private CustomFontTextView tv_previous_states;
    private CustomFontTextView tvCorrectAnsCount;
    private CustomFontTextView tvInorrectAnsCount;
    private CustomFontTextView tvExtraLifeCount;

    private GameplayStrings gameplayStrings;

    public StreamTextModeView(Context context) {
        this(context, null);
    }

    public StreamTextModeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StreamTextModeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void setBrainBaaziStrings(BrainbaaziStrings brainBaaziStrings) {
        if (brainBaaziStrings != null) {
            this.gameplayStrings = brainBaaziStrings.gameplayStrings();
            viewTop.setText(gameplayStrings.textOnlyModeText());
        }
    }

    private void initView() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_stream_text_mode, this);

        viewTop = findViewById(R.id.viewTop);
        tvTitle = findViewById(R.id.tvTitle);
        llStats = findViewById(R.id.llStats);

        tv_previous_states = findViewById(R.id.tv_previous_states);
        tvCorrectAnsCount = findViewById(R.id.tvCorrectAnsCount);
        tvInorrectAnsCount = findViewById(R.id.tvInorrectAnsCount);
        tvExtraLifeCount = findViewById(R.id.tvExtraLifeCount);

        if (gameplayStrings != null) {
            viewTop.setText(gameplayStrings.textOnlyModeText());
            tv_previous_states.setText(gameplayStrings.previousQuestionStatsText());
            setTvTitle(gameplayStrings.waitQuestionText());
        } else {
            viewTop.setText(getContext().getString(R.string.text_only_mode));
            tv_previous_states.setText(getContext().getString(R.string.prev_ques_stats));
            setTvTitle(getContext().getString(R.string.wait_question));
        }
        hideGameStats();
    }

    public void updateGameStatesData(GameResponse gameEvents) {
        switch (gameEvents.eventType() & GameResponse.MASK_SHOW_BITS) {
            case GameResponse.TYPE_SHOW_QUESTION:
                handleShowQuestion();
                break;
            case GameResponse.TYPE_SHOW_ANSWER:
                handleShowAnswer(gameEvents);
                break;
            case GameResponse.TYPE_SHOW_WINNER:
                handleShowWinner();
                break;
            case GameResponse.TYPE_SHOW_GAME_END:
                hanldeGameEnd();
                break;
        }
    }

    private void handleShowQuestion() {
        hideGameStats();
        setTvTitle(gameplayStrings.waitAnswerText());
    }

    private void handleShowAnswer(GameResponse gameEvents) {
        showGameStats(gameEvents);
    }

    private void handleShowWinner() {
        hideGameStats();
        setTvTitle(gameplayStrings.seeNextGameText());
    }

    private void hanldeGameEnd() {
        hideGameStats();
        setTvTitle("");
    }

    private void showGameStats(GameResponse gameEvents) {
        llStats.setVisibility(VISIBLE);
        tvTitle.setVisibility(GONE);

        QuestionInfo questionInfo = (QuestionInfo) gameEvents.value();
        Answer answer = questionInfo.answer();
        int correctCount = 0;
        int incorrectCount = 0;
        final boolean isLastQuestion = questionInfo.question().isLastQuestion();
        for (int i = 0; i < answer.getAnswerOptions().size(); i++) {
            AnswerOptions option = answer.getAnswerOptions().get(i);
            if (option.optionId().equalsIgnoreCase("" + answer.getCorrectAnswerId())) {
                correctCount = (int) option.count();
            } else {
                incorrectCount = incorrectCount + (int) option.count();
            }
        }

        String correctAnswerText = gameplayStrings.correctAnswerText() + getPersonPeopleText(correctCount);
        String incorrectAnswerText = gameplayStrings.incorrectAnswerText() + getPersonPeopleText(incorrectCount);
        String lifeUsedText = gameplayStrings.usedExtraLifeText() + getPersonPeopleText(answer.getLifeUsedCount());

        tvCorrectAnsCount.setText(correctAnswerText);
        tvInorrectAnsCount.setText(incorrectAnswerText);
        tvExtraLifeCount.setText(lifeUsedText);

        postDelayed(new Runnable() {
            @Override
            public void run() {
                hideGameStats();
                setTvTitle(isLastQuestion ? gameplayStrings.generateResultsText() : gameplayStrings.waitQuestionText());
            }
        }, 13000);
    }

    private String getPersonPeopleText(int count) {
        String text = (count > 1 ? gameplayStrings.peopleText() : gameplayStrings.personText());
        return (" : " + count + " " + text);
    }

    public void hideGameStats() {
        tvCorrectAnsCount.setText("");
        tvInorrectAnsCount.setText("");
        tvExtraLifeCount.setText("");
        llStats.setVisibility(GONE);
    }


    private void setTvTitle(String value) {
        tvTitle.setText(value);
        tvTitle.setVisibility(VISIBLE);
    }
}
