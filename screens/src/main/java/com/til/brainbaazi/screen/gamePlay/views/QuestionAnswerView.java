package com.til.brainbaazi.screen.gamePlay.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainbaazi.component.Analytics;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.AnswerOptions;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.event.QuestionOptions;
import com.til.brainbaazi.entity.game.response.GameResponse;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.AnimatableView;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.AnimUtils;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.viewmodel.gamePlay.GameViewState;
import com.til.slikeplayer.slikestream.StreamingMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saurabh.garg on 2/16/18.
 */

public class QuestionAnswerView extends RelativeLayout implements View.OnClickListener {

    private Analytics analytics;
    private String gameID;
    private User user;

    private ArrayList<CustomFontTextView> tvOptions;
    private ArrayList<CustomFontTextView> tvAnswerOptions;
    private ArrayList<CustomFontTextView> tvOptionCounts;
    private ArrayList<ProgressBar> optionpBars;

    private ImageView bb_ivStreamMode;
    private ImageView favIV;
    private CustomFontTextView questionNumber;
    private CustomFontTextView totalNumberOfPeople;

    private CustomFontTextView btnAlert;
    private ProgressBar timerProgBar;
    private CustomFontTextView textViewTimer;
    private CustomFontTextView textQuestion;
    private CustomFontTextView tv_digi_mode;
    private View questionLayout;
    private View answerLayout;

    private View ll_life_used;
    private CustomFontTextView tv_life_used;

    private Handler timerProgressViewHandler = new Handler();
    private HashMap<Integer, String> optionListMap = new HashMap<>();
    private int timerProgressStatus = 0;
    private int selectedOption = -1;
    private int correctAnswerPos;
    private int gameState;
    private int userState = GameState.PLAY_STATE_PLAYING;
    private QACallback qaCallback;
    private GameplayStrings gameplayStrings;
    private boolean isDigiMode = false;

    public QuestionAnswerView(Context context) {
        this(context, null);
    }

    public QuestionAnswerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public QuestionAnswerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initQAView();
    }

    public void setAnalytics(Analytics analytics, long gameID, User user) {
        this.analytics = analytics;
        this.gameID = String.valueOf(gameID);
        this.user = user;
    }

    public void setQaCallback(QACallback qaCallback) {
        this.qaCallback = qaCallback;
    }

    public void setUserState(int userState) {
        this.userState = userState;
    }

    public void setBrainBaaziStrings(BrainbaaziStrings brainBaaziStrings) {
        this.gameplayStrings = brainBaaziStrings.gameplayStrings();
        tv_digi_mode.setText(gameplayStrings.textOnlyModeText());
    }

    private void initQAView() {
        LayoutInflater.from(getContext()).inflate(R.layout.screen_questions, this);

        tvOptions = new ArrayList<>();
        tvOptionCounts = new ArrayList<>();
        optionpBars = new ArrayList<>();
        tvAnswerOptions = new ArrayList<>();

        bb_ivStreamMode = findViewById(R.id.bb_ivStreamMode);
        favIV = findViewById(R.id.favIV);

        CustomFontTextView btnOption1 = findViewById(R.id.button_option1);
        CustomFontTextView btnOption2 = findViewById(R.id.button_option2);
        CustomFontTextView btnOption3 = findViewById(R.id.button_option3);
        tvOptions.add(btnOption1);
        tvOptions.add(btnOption2);
        tvOptions.add(btnOption3);

        CustomFontTextView btnAns1Count = findViewById(R.id.textView_ans1Count);
        CustomFontTextView btnAns2Count = findViewById(R.id.textView_ans2Count);
        CustomFontTextView btnAns3Count = findViewById(R.id.textView_ans3Count);
        tvOptionCounts.add(btnAns1Count);
        tvOptionCounts.add(btnAns2Count);
        tvOptionCounts.add(btnAns3Count);

        ProgressBar progressBar1 = findViewById(R.id.progressBar1);
        ProgressBar progressBar2 = findViewById(R.id.progressBar2);
        ProgressBar progressBar3 = findViewById(R.id.progressBar3);
        optionpBars.add(progressBar1);
        optionpBars.add(progressBar2);
        optionpBars.add(progressBar3);

        CustomFontTextView btnAnsOption1 = findViewById(R.id.btn_ansOption1);
        CustomFontTextView btnAnsOption2 = findViewById(R.id.btn_ansOption2);
        CustomFontTextView btnAnsOption3 = findViewById(R.id.btn_ansOption3);
        tvAnswerOptions.add(btnAnsOption1);
        tvAnswerOptions.add(btnAnsOption2);
        tvAnswerOptions.add(btnAnsOption3);

        btnAlert = findViewById(R.id.btn_alert);
        textQuestion = findViewById(R.id.text_question);
        questionLayout = findViewById(R.id.question_options);
        answerLayout = findViewById(R.id.answers_options);
        timerProgBar = findViewById(R.id.timerProgressBar);
        textViewTimer = findViewById(R.id.textView_timer);
        questionNumber = findViewById(R.id.questionNumber);
        totalNumberOfPeople = findViewById(R.id.textNumberPeopleQuestion);
        tv_digi_mode = findViewById(R.id.tv_digi_mode);

        ll_life_used = findViewById(R.id.ll_life_used);
        tv_life_used = findViewById(R.id.tv_life_used);

        setStreamingView(StreamingMode.DEFAULT_MODE, R.drawable.normal_mode);
        btnAlert.setVisibility(GONE);
        timerProgBar.setVisibility(GONE);
        enableSelection();
    }

    public void setStreamingView(StreamingMode streamingMode, int resourceID) {
        isDigiMode = (streamingMode == StreamingMode.DIGI_ONLY);
        Drawable normalIcon = ContextCompat.getDrawable(getContext(), resourceID).mutate();
        normalIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.bbcolor_8a000000), PorterDuff.Mode.MULTIPLY);
        bb_ivStreamMode.setImageDrawable(normalIcon);
    }

    public void setLiveGameData(GameViewState gameInfo) {
        totalNumberOfPeople.setText(AppUtils.coolFormatWrap(gameInfo.getConcurrentUserCount()));
        if (!gameInfo.getLifeAvailable()) {
            favIV.setImageResource(R.drawable.heart_grey);
        } else {
            favIV.setImageResource(R.drawable.life_red);
        }
    }

    public void setLiveGameData(GameState gameEvents) {
        totalNumberOfPeople.setText(AppUtils.coolFormatWrap(gameEvents.getConcurrentUserCount()));
        if (gameEvents.getLifeAvailable() < 1) {
            favIV.setImageResource(R.drawable.heart_grey);
        } else {
            favIV.setImageResource(R.drawable.life_red);
        }
    }

    private void enableSelection() {
        for (CustomFontTextView option : tvOptions) {
            option.setOnClickListener(this);
            option.setSelected(false);
            option.setBackgroundResource(R.drawable.question_option_background);
            option.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolor_40404a));
        }
    }

    private void disableSelection() {
        for (CustomFontTextView option : tvOptions) {
            option.setOnClickListener(disabledOptionClickListener);
        }
        if (selectedOption == -1) {
            showOptionsDisabled();
        }
    }

    private void showOptionsDisabled() {
        for (CustomFontTextView option : tvOptions) {
            option.setSelected(false);
            option.setBackgroundResource(R.drawable.question_background_disabled);
            option.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolor_8c8a8a));
        }
    }

    private void selectOption(int selectedOption) {
        int selected = selectedOption - 1;
        for (int i = 0; i < tvOptions.size(); i++) {
            CustomFontTextView option = tvOptions.get(i);
            if (i == selected) {
                option.setSelected(true);
                option.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorWhite));
            } else {
                option.setSelected(false);
                option.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolor_40404a));
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.button_option1) {
            selectedOption = 1;
            selectOption(selectedOption);
            disableSelection();
            playSound(2);
            showAnswerLockText();
        } else if (id == R.id.button_option2) {
            selectedOption = 2;
            selectOption(selectedOption);
            disableSelection();
            playSound(2);
            showAnswerLockText();
        } else if (id == R.id.button_option3) {
            selectedOption = 3;
            selectOption(selectedOption);
            disableSelection();
            playSound(2);
            showAnswerLockText();
        }
        String selectedOptionId = optionListMap.get(selectedOption);
        sendAnswerSubmitEvent("" + selectedOption);
        qaCallback.submitUserAnswer(Integer.parseInt(selectedOptionId), selectedOptionId);
    }

    private void showAnswerLockText() {
        String message = gameplayStrings.qaAnswerLockedText();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void triggerQuestion(AnimatableView view, GradientDrawable gradientDrawable, int gameState, Question questionEvent) {
        if (questionEvent != null) {
            this.gameState = GameResponse.TYPE_SHOW_QUESTION;
            Utils.hideKeyboard(getContext());
            optionListMap.clear();
            enableQuestion();
            toggleLifeUsedView(-1);
            setQuestionData(questionEvent);
            showQAView(getContext(), view, gradientDrawable);
        }
    }

    private void setQuestionData(Question questionData) {
        selectedOption = -1;
        questionNumber.setText(gameplayStrings.questionIndexText() + questionData.getQuestionNumber());
        textQuestion.setText(questionData.getText());
        adjustTextViewSize(textQuestion);
        sendQuestionServeEvent();
        try {
            //Trigger Question Screen View Event
            boolean isEliminated = false;
            if (userState == GameState.PLAY_STATE_ELIMINATED) {
                isEliminated = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < tvOptions.size(); i++) {
            QuestionOptions options = questionData.getQuestionOptions().get(i);
            if (options.text().length() > 27) {
                tvOptions.get(i).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            } else {
                tvOptions.get(i).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            }
            tvOptions.get(i).setText(options.text());
            optionListMap.put(i + 1, options.optionId());
        }

        if (userState == GameState.PLAY_STATE_PLAYING) {
            enableSelection();
        } else {
            disableSelection();
        }
        runTimerProgress(getContext());
    }

    private void sendAnswerSubmitEvent(String selectedOption) {
        String category = "Game Play - " + gameID;
        if (!TextUtils.isEmpty(selectedOption)) {
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Answer Locked").setCategory(category)
                    .setAction("Answer Locked - " + questionNumber.getText().toString())
                    .setLabel(selectedOption)
                    .setUserName(user.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            analytics.logFireBaseEvent(gaEventModel);
        } else if (userState != GameState.PLAY_STATE_ELIMINATED) {
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Answer Not Selected").setCategory(category)
                    .setAction("Question Served - " + questionNumber.getText().toString())
                    .setLabel("")
                    .setUserName(user.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            analytics.logFireBaseEvent(gaEventModel);
        }
        if (userState != GameState.PLAY_STATE_ELIMINATED) {
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Answer Submitted").setCategory(category)
                    .setAction("Question Served - " + questionNumber.getText().toString())
                    .setLabel(selectedOption)
                    .setUserName(user.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            analytics.logFireBaseEvent(gaEventModel);
        }
    }

    public void triggerAnswer(AnimatableView view, GradientDrawable gradientDrawable, int gameState, QuestionInfo answerEvent) {
        // Get last question from shared preferences
        this.gameState = GameResponse.TYPE_SHOW_ANSWER;
        Utils.hideKeyboard(getContext());
        enableAnswer();
        setAnswerData(answerEvent);
        showQAView(getContext(), view, gradientDrawable);
    }

    private void setAnswerData(QuestionInfo answerData) {
        correctAnswerPos = 0;
        textQuestion.setText(answerData.question().getText());
        adjustTextViewSize(textQuestion);
        toggleLifeUsedView(answerData.answer().getLifeUsedCount());
        int total = answerData.answer().getMaxUserCount();

        for (int i = 0; i < tvAnswerOptions.size(); i++) {
            AnswerOptions option = answerData.answer().getAnswerOptions().get(i);
            QuestionOptions qOptions = answerData.question().getQuestionOptions().get(i);
            int count = (int) option.count();
            String text = (!TextUtils.isEmpty(option.text()) ? option.text() : qOptions.text());
            tvOptionCounts.get(i).setText(AppUtils.coolFormatWrap(count));

            if (total == 0 || count == 0) {
                optionpBars.get(i).setMax(10);
                optionpBars.get(i).setProgress(10);
            } else {
                optionpBars.get(i).setMax(total);
                optionpBars.get(i).setProgress(count);
            }
            tvAnswerOptions.get(i).setText(text);
            if (text.length() > 27) {
                tvAnswerOptions.get(i).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            } else {
                tvAnswerOptions.get(i).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            }
            if (option.optionId().equalsIgnoreCase(String.valueOf(answerData.answer().getCorrectAnswerId()))) {
                correctAnswerPos = i + 1;
                optionpBars.get(i).setProgressDrawable(getResources().getDrawable(R.drawable.progress_round_green));
            } else {
                optionpBars.get(i).setProgressDrawable(getResources().getDrawable(R.drawable.progress_round));
            }
        }

        sendAnswerServeEvent();
        if (selectedOption != correctAnswerPos) {
            setIncorrectProgressBackground(selectedOption);
        }
        disableSelection();
    }

    private void toggleLifeUsedView(int count) {
        if (count > 0) {
            String text;
            text = (count == 1 ? gameplayStrings.qaUserLifeUsedSingleCountText() : gameplayStrings.qaUserLifeUsedMultipleCountText());
            text = text.replaceAll("<user>", "" + AppUtils.coolFormatWrap(count));
            tv_life_used.setText(text);
            ll_life_used.setVisibility(VISIBLE);
        } else {
            ll_life_used.setVisibility(GONE);
        }
    }

    private void doResultBtnAnimation(int option, final AnimatableView animatableView, Drawable gradientDrawable) {
        toggleVideoDigiModeText(animatableView, false);
        if (gameState == GameResponse.TYPE_SHOW_QUESTION) {
            if (selectedOption == -1) {
                String message = gameplayStrings.qaTimeUpText();
                btnAlert.setText(message);
                btnAlert.setBackground(getResources().getDrawable(R.drawable.button_round_blue));
                if (userState == GameState.PLAY_STATE_PLAYING)
                    qaCallback.submitUserAnswer(0, "0");
                else
                    qaCallback.submitUserAnswer(-1, "E");

                playSound(3);
                sendAnswerSubmitEvent(null);

                if (userState == GameState.PLAY_STATE_PLAYING) {
                    sendTimeUpEvent();
                }
            } else { // User has clicked an option
                String message = gameplayStrings.qaAnswerSubmittedText();
                btnAlert.setText(message);
                btnAlert.setBackground(getResources().getDrawable(R.drawable.button_round_corner_green));
            }
        } else if (gameState == GameResponse.TYPE_SHOW_ANSWER) {
            btnAlert.setVisibility(VISIBLE);
            if (userState == GameState.PLAY_STATE_ELIMINATED) {
                String message = gameplayStrings.qaEliminatedText();
                btnAlert.setText(message);
                btnAlert.setBackground(getResources().getDrawable(R.drawable.bg_round_incorrect));
                sendEliminationEvent();
            } else {
                if (option == correctAnswerPos) {
                    String message = gameplayStrings.qaCorrectText();
                    btnAlert.setText(message);
                    btnAlert.setBackground(getResources().getDrawable(R.drawable.bg_round_correct));
                } else {
                    String message = gameplayStrings.qaInCorrectText();
                    btnAlert.setText(message);
                    btnAlert.setBackground(getResources().getDrawable(R.drawable.bg_round_incorrect));
                    sendEliminationEvent();
                }
            }
        }

        if (userState == GameState.PLAY_STATE_PLAYING) {
            btnAlert.setVisibility(View.VISIBLE);
            btnAlert.animate().scaleXBy(.3f).scaleX(1.2f).setDuration(500).start();
            btnAlert.animate().scaleXBy(1f).scaleX(.8f).setDuration(500).start();
        }

        // Hide result button after 2 seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btnAlert.clearAnimation();
                btnAlert.setVisibility(View.GONE);
                toggleVideoDigiModeText(animatableView, true);

            }
        }, 2000);
    }

    private void sendAnswerServeEvent() {
        String event = (userState == GameState.PLAY_STATE_PLAYING ? "answer_screen_viewed" : "answer_screen_viewed_inactive");
        String question = questionNumber.getText().toString();
        Map<String, Object> data = new HashMap<>();
        data.put("username", user.getUserName());
        data.put("Phone", user.getPhoneNumber());
        data.put("game_id", gameID);
        data.put("question_level", question);
        data.put("question_id", textQuestion.getText().toString());
        data.put("question_level", question);
        if (userState == GameState.PLAY_STATE_PLAYING) {
            data.put("answer_choice", optionListMap.get(selectedOption));
            data.put("selected_option", selectedOption);
        }
        data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
        analytics.cleverTapEvent(event, data);
        analytics.cleverTapEvent("answer_submitted", data);


        Bundle bundle = new Bundle();
        bundle.putString("username", user.getUserName());
        bundle.putString("game_id", gameID);
        bundle.putString("question_level", "" + questionNumber.getText().toString());
        bundle.putString("answer_choice", "" + selectedOption);
        bundle.putString("status", (selectedOption != correctAnswerPos ? "incorrect" : "correct"));

        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("answer_selected").build();
        analytics.logFireBaseScreen(gaEventModel, bundle);
    }

    private void sendQuestionServeEvent() {
        String category = "Game Play - " + gameID;
        String question = questionNumber.getText().toString();
        Map<String, Object> data = new HashMap<>();
        data.put("username", user.getUserName());
        data.put("Phone", user.getPhoneNumber());
        data.put("game_id", gameID);
        data.put("question_level", question);
        data.put("question_id", textQuestion.getText().toString());
        data.put("eliminated", (userState != GameState.PLAY_STATE_PLAYING));
        data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
        analytics.cleverTapEvent("question_screen_viewed", data);

        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Question Served").setCategory(category)
                .setAction("Question Served - " + question)
                .setLabel("")
                .setUserName(user.getUserName())
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        analytics.logFireBaseEvent(gaEventModel);
    }

    private void sendTimeUpEvent() {
        String question = questionNumber.getText().toString();
        Map<String, Object> data = new HashMap<>();
        data.put("username", user.getUserName());
        data.put("Phone", user.getPhoneNumber());
        data.put("game_id", gameID);
        data.put("question_level", question);
        data.put("question_id", textQuestion.getText().toString());
        data.put("question_level", question);
        analytics.cleverTapEvent("question_timeup", data);

        Bundle bundle = new Bundle();
        bundle.putString("username", user.getUserName());
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("question_timeup").build();
        analytics.logFireBaseScreen(gaEventModel, bundle);
    }

    private void sendEliminationEvent() {
        Bundle bundle = new Bundle();
        bundle.putString("username", user.getUserName());
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("question_eliminated").build();
        analytics.logFireBaseScreen(gaEventModel, bundle);
    }


    //Utility Functions for Helping
    public void enableQuestion() {
        questionLayout.setVisibility(View.VISIBLE);
        answerLayout.setVisibility(View.GONE);
    }

    public void enableAnswer() {
        questionLayout.setVisibility(View.GONE);
        answerLayout.setVisibility(View.VISIBLE);
    }

    public void showQAView(Context context, AnimatableView view, GradientDrawable drawable) {
        AnimatableView animatableView = view;
        AnimUtils.reverseFullScreenAnimation(context, animatableView, 50, drawable);
        startTimer(view, drawable);
    }

    public void hideQAView(AnimatableView view, GradientDrawable drawable) {
        AnimUtils.videoFullScreenAnimation(getContext(), view, 50, drawable);
    }

    private void runTimerProgress(final Context context) {
        if (timerProgBar.getVisibility() == View.GONE) {
            timerProgBar.setVisibility(VISIBLE);
        }
        new Thread(new Runnable() {
            public void run() {
                timerProgressStatus = 0;
                while (timerProgressStatus < 100) {
                    timerProgressStatus += 1;
                    // Update the progress bar
                    timerProgressViewHandler.post(new Runnable() {
                        public void run() {
                            if (timerProgressStatus >= 50) {
                                timerProgBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(context, R.color.bbcolor_ef3a76), PorterDuff.Mode.SRC_IN);
                            } else {
                                timerProgBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(context, R.color.bbcolor_0f30ef), PorterDuff.Mode.SRC_IN);
                            }
                            timerProgBar.setProgress(timerProgressStatus);
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void startTimer(AnimatableView view, final GradientDrawable drawable) {
        final AnimatableView animatableView = view;
        int countMillisInFuture = 10000;
        textViewTimer.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolor_0f30ef));
        if (gameState == GameResponse.TYPE_SHOW_ANSWER) {
            countMillisInFuture = 500;
        }

        new CountDownTimer(countMillisInFuture, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (gameState == GameResponse.TYPE_SHOW_ANSWER) {
                    timerProgBar.setVisibility(GONE);
                    textViewTimer.setVisibility(GONE);
                    toggleVideoDigiModeText(animatableView, true);
                } else {
                    int sec = (int) millisUntilFinished / 1000;
                    if (sec <= 10) {
                        toggleVideoDigiModeText(animatableView, false);
                        textViewTimer.setVisibility(View.VISIBLE);
                        if (sec == 4) {
                            timerProgBar.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.timer_pulse_animation));
                            textViewTimer.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.timer_pulse_animation));
                            textViewTimer.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolor_ef3a76));
                        }
                        if (sec <= 5)
                            playSound(4);
                    } else {
                        textViewTimer.setVisibility(View.GONE);
                    }
                    textViewTimer.setText(String.valueOf(sec));
                }
            }

            @Override
            public void onFinish() {
                textViewTimer.setVisibility(View.GONE);
                timerProgBar.setVisibility(View.GONE);
                disableSelection();
                doResultBtnAnimation(selectedOption, animatableView, drawable);
                timerProgBar.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideQAView(animatableView, drawable);
                        if (gameState == GameResponse.TYPE_SHOW_ANSWER) {
                            hideAnswer(animatableView);
                        } else if (gameState == GameResponse.TYPE_SHOW_QUESTION) {
                            hideQuestion(animatableView);
                            qaCallback.questionGone();
                        }
                        qaCallback.showTopWhiteAppBar();
                    }
                }, 4000);
            }
        }.start();
    }

    private OnClickListener disabledOptionClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.button_option1) {
                playSound(1);
            } else if (id == R.id.button_option2) {
                playSound(1);
            } else if (id == R.id.button_option3) {
                playSound(1);
            }
        }
    };

    private void playSound(int type) {
        int resId = R.raw.timeup;
        if (type == 1) {
            resId = R.raw.disabled;
        } else if (type == 2) {
            resId = R.raw.success;
        } else if (type == 3) {
            resId = R.raw.timeup;
        } else if (type == 4) {
            resId = R.raw.tick;
        }

        MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), resId);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });
        mediaPlayer.start();
    }

    private void setIncorrectProgressBackground(int index) {
        int position = index - 1;
        for (int i = 0; i < optionpBars.size(); i++) {
            if (i == position) {
                optionpBars.get(i).setProgressDrawable(getResources().getDrawable(R.drawable.progress_round_red));
            }
        }
    }

    private void adjustTextViewSize(final TextView textQuestion) {
        try {
            textQuestion.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        int lineCount = textQuestion.getLineCount();
                        // Use lineCount here
                        if (lineCount == 2) {
                            textQuestion.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        } else if (lineCount == 3) {
                            textQuestion.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        } else if (lineCount >= 4) {
                            textQuestion.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                        }
                        textQuestion.invalidate();
                    } catch (Exception ignored) {

                    }
                }
            });
            textQuestion.requestLayout();
        } catch (Exception ignored) {

        }
    }

    private String getString(int stringID) {
        return getContext().getString(stringID);
    }

    private void hideQuestion(AnimatableView videoView) {
        if (qaCallback != null) {
            tv_digi_mode.setVisibility(INVISIBLE);
            videoView.setVisibility(VISIBLE);
            qaCallback.questionGone();
        }
    }

    private void hideAnswer(AnimatableView videoView) {
        if (qaCallback != null) {
            tv_digi_mode.setVisibility(INVISIBLE);
            videoView.setVisibility(VISIBLE);
            qaCallback.answerGone();
        }
    }

    public void toggleVideoDigiModeText(AnimatableView videoView, boolean toShow) {
        if (tv_digi_mode != null && videoView != null) {
            tv_digi_mode.setVisibility((isDigiMode && toShow) ? VISIBLE : INVISIBLE);
            videoView.setVisibility((isDigiMode || !toShow) ? INVISIBLE : VISIBLE);
        }
    }

    public interface QACallback {

        void submitUserAnswer(int userAnswer, String optionKey);

        void answerSubmitted(int questionSeq, String optionKey);

        void questionGone();

        void answerGone();

        void showTopWhiteAppBar();
    }
}
