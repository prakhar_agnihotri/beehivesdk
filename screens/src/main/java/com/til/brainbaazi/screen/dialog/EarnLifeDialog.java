package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class EarnLifeDialog extends BaseDialog {
    private User user;
    private long lastClickMillisResult;
    private long THRESHOLD_MILLIS = 1500L;

    public EarnLifeDialog(@NonNull Context context, @NonNull User user) {
        super(context);
        this.user = user;
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_got_extralife;
    }

    @Override
    public void inflateDialogView() {

        ImageView closeButton = findViewById(R.id.closeButton);
        CustomFontTextView youEliminatedHead = findViewById(R.id.youEliminatedHead);
        CustomFontTextView question = findViewById(R.id.question);

        Button shareButton = findViewById(R.id.shareButton);
        ImageView profileImage = findViewById(R.id.profile_img_post);
        CustomFontTextView continueText = findViewById(R.id.continueText);

        updateUserImageUrl(profileImage, user);
        if (getBrainBaaziStrings() != null) {
            GameplayStrings strings = getBrainBaaziStrings().gameplayStrings();

            youEliminatedHead.setText(strings.dialogCongratsExtraLife());
            shareButton.setText(getBrainBaaziStrings().commonStrings().share());
            question.setText(strings.dialogThanksExtraLifeText());
            continueText.setText(strings.continueWatching());
        }

        closeButton.setOnClickListener(this);
        continueText.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.closeButton || id == R.id.continueText) {
            dismiss();
        } else if (id == R.id.shareWonButton) {
            long now = SystemClock.elapsedRealtime();
            if (now - lastClickMillisResult > THRESHOLD_MILLIS) {
                Utils.inviteUser(view.getContext(), user, "game_screen");
            }
            lastClickMillisResult = now;
        }
    }
}
