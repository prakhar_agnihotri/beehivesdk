package com.til.brainbaazi.screen.payment;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.text.format.Formatter;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import static android.content.Context.WIFI_SERVICE;

/**
 * Created by arpit.toshniwal on 24/02/18.
 */

public class PaymentUtils {


    public static String getIpAddress(Context applicationContext){
        String ip = PaymentUtils.getWifiIPAddress(applicationContext.getApplicationContext());
        if (TextUtils.isEmpty(ip) || ip.equalsIgnoreCase("0.0.0.0")) {
            ip = PaymentUtils.getMobileIPAddress();
        }
        return ip;
    }

    public static String getWifiIPAddress(Context applicationContext) {
        WifiManager wifiMgr = (WifiManager) applicationContext.getApplicationContext().getSystemService(WIFI_SERVICE);
        if(wifiMgr != null) {
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            return Formatter.formatIpAddress(ip);
        }
        return null;
    }

    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return null;
    }
}
