package com.til.brainbaazi.screen.switcher;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

/**
 * Created by prashant.rathore on 15/02/18.
 */

public class SwitcherViewModel extends BaseScreenModel {
    public SwitcherViewModel(ConnectionManager connectionManager, DataRepository dataRepository, Analytics analytics) {
        super(connectionManager, dataRepository,analytics);
    }
}
