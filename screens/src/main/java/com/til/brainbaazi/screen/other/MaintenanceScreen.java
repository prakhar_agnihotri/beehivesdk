package com.til.brainbaazi.screen.other;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainbaazi.component.Analytics;
import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.viewmodel.splash.MaintenanceViewModel;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 2/22/18.
 */

@AutoFactory(implementing = ScreenFactory.class)
public class MaintenanceScreen extends BaseScreen<MaintenanceViewModel> {

    public MaintenanceScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_maintenance, viewGroup, false);
    }

    @Override
    protected void onBind(MaintenanceViewModel viewModel) {

    }

    @Override
    protected void onUnBind() {

    }

    @Override
    public void resume() {
        super.resume();
        getViewModel().logFireBaseScreen(Analytics.SCREEN_MAINTENANCE);
    }
}
