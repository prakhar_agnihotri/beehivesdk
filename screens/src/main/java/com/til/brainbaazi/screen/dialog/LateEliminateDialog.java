package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.brainbaazi.component.Analytics;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.utils.AppUtils;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class LateEliminateDialog extends EliminateDialog {

    public LateEliminateDialog(@NonNull Context context, @NonNull User user, Analytics analytics, String gameID, int question) {
        super(context, user, analytics, gameID, question);
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_you_eliminated_late;
    }

    @Override
    public void inflateDialogView() {
        ImageView profileImage = findViewById(R.id.profile_img_post);

        CustomFontTextView youEliminatedHead = findViewById(R.id.youEliminatedHead);
        CustomFontTextView question = findViewById(R.id.question);
        CustomFontTextView continueText = findViewById(R.id.continueText);

        findViewById(R.id.closeButton).setOnClickListener(this);
        continueText.setOnClickListener(this);
        updateUserImageUrl(profileImage, mUser);

        GameplayStrings brainBaaziStrings = getBrainBaaziStrings().gameplayStrings();
        if (brainBaaziStrings != null) {

            youEliminatedHead.setText(brainBaaziStrings.lateEliminatedGetExtraLife());
            question.setText(brainBaaziStrings.lateEliminateExtraLifeWinChanceText());
            continueText.setText(brainBaaziStrings.continueWatching());
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.closeButton) {
            String category = "Game Play - " + gameID;
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Eliminated popup close").setCategory(category)
                    .setAction("Eliminated" + question)
                    .setLabel("Close")
                    .setUserName(mUser.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            analytics.logFireBaseEvent(gaEventModel);
            dismiss();
        } else if (id == R.id.btn_continue) {
            String category = "Game Play - " + gameID;
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Eliminated popup continue").setCategory(category)
                    .setAction("Eliminated" + question)
                    .setLabel("Continue Watching")
                    .setUserName(mUser.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            analytics.logFireBaseEvent(gaEventModel);
            dismiss();
        }
    }
}
