package com.til.brainbaazi.screen.gamePlay.views;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.recycleHelper.CustomLinearLayoutManager;

/**
 * Created by saurabh.garg on 2/21/18.
 */

public class MultipleWinnersLayout extends RelativeLayout {
    private Handler multipleWinnerhandler;

    public MultipleWinnersLayout(Context context, Winner winnerModel, BrainbaaziStrings brainbaaziStrings) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.view_multiple_winner, this);
        inflateData(winnerModel, brainbaaziStrings);
    }

    private void inflateData(final Winner winnerModel, BrainbaaziStrings brainbaaziStrings) {
        final CustomFontTextView tvWinnersCount = findViewById(R.id.numOfWinText);
        final RecyclerView rvWinners = findViewById(R.id.recyclerViewZig);
        multipleWinnerhandler = new Handler();

        final RecyclerZigAdapter mAdapter = new RecyclerZigAdapter(winnerModel);
        rvWinners.setAdapter(mAdapter);
        rvWinners.setLayoutManager(new CustomLinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        tvWinnersCount.setText(winnerModel.getWinnerCount() + " " + brainbaaziStrings.leaderboardStrings().winnersText());

        final int speedScroll = 700;

        final Runnable runnable = new Runnable() {
            int count = 0;
            boolean flag = true;

            @Override
            public void run() {
                if (count < mAdapter.getItemCount()) {
                    if (count == mAdapter.getItemCount() - 1) {
                        flag = false;
                    } else if (count == 0) {
                        flag = true;
                    }
                    if (flag) count++;


                    rvWinners.smoothScrollToPosition(count);
                    multipleWinnerhandler.postDelayed(this, speedScroll);
                }
            }
        };

        multipleWinnerhandler.postDelayed(runnable, speedScroll);

        rvWinners.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (multipleWinnerhandler != null) {
                    multipleWinnerhandler.removeCallbacks(runnable);
                    multipleWinnerhandler = null;
                }
                return false;
            }
        });
    }
}
