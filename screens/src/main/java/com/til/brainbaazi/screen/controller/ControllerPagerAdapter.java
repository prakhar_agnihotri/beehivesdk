package com.til.brainbaazi.screen.controller;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.til.brainbaazi.screen.BaseScreen;

/**
 * Created by prashant.rathore on 26/02/18.
 */

public abstract class ControllerPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private ScreenController primaryItem;

    public ControllerPagerAdapter(Context context, LayoutInflater layoutInflater) {
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ScreenController screenController = instantiateScreen(position);
        screenController.attach(context, layoutInflater);
        screenController.onCreate();
        BaseScreen view = screenController.createView(container);
        container.addView(view.getView());
        screenController.bindView(view);
        screenController.onStart();
        return screenController;
    }

    protected abstract ScreenController instantiateScreen(int position);

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (primaryItem != null && primaryItem != object) {
            primaryItem.onPause();
        }
        if(primaryItem != object) {
            ScreenController screenController = (ScreenController) object;
            primaryItem = screenController;
            this.primaryItem.onResume();
        }
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ScreenController screenController = (ScreenController) object;
        screenController.onStop();
        BaseScreen boundedView = screenController.getBoundedView();
        container.removeView(boundedView.getView());
        screenController.unBindView();
        screenController.onDestroy();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        ScreenController screenController = (ScreenController) object;
        BaseScreen boundedView = screenController.getBoundedView();
        return boundedView != null && boundedView.getView() == view;
    }
}
