package com.til.brainbaazi.screen.gamePlay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ViewFlipper;

import com.brainbaazi.component.Analytics;
import com.google.android.exoplayer2.slike.Timeline;
import com.google.android.exoplayer2.slike.source.BehindLiveWindowException;
import com.google.android.exoplayer2.slike.source.TrackGroupArray;
import com.google.android.exoplayer2.slike.source.hls.playlist.HlsPlaylistTracker;
import com.google.android.exoplayer2.slike.text.Cue;
import com.google.android.exoplayer2.slike.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.slike.upstream.HttpDataSource;
import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.GameResponse;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.AnimatableView;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.dialog.DataSaverDialog;
import com.til.brainbaazi.screen.dialog.ExtraLifeInfoDialog;
import com.til.brainbaazi.screen.dialog.GameDialogManager;
import com.til.brainbaazi.screen.dialog.GameQuitDialog;
import com.til.brainbaazi.screen.gamePlay.chat.ChatView;
import com.til.brainbaazi.screen.gamePlay.views.MultipleWinnersLayout;
import com.til.brainbaazi.screen.gamePlay.views.QuestionAnswerView;
import com.til.brainbaazi.screen.gamePlay.views.SingleWinnerView;
import com.til.brainbaazi.screen.gamePlay.views.StreamTextModeView;
import com.til.brainbaazi.screen.recycleHelper.OnSwipeTouchListener;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.gamePlay.GameViewState;
import com.til.brainbaazi.viewmodel.gamePlay.LiveGamePlayViewModel;
import com.til.slikeplayer.slikestream.AspectRatioFrameLayout;
import com.til.slikeplayer.slikestream.IStreamComponentListener;
import com.til.slikeplayer.slikestream.SlikeRtmpLivePlayer;
import com.til.slikeplayer.slikestream.StreamCoreUtils;
import com.til.slikeplayer.slikestream.StreamingMode;
import com.til.slikeplayer.slikestream.TimedUpdate;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by saurabh.garg on 2/16/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class LiveGamePlayScreen extends BaseScreen<LiveGamePlayViewModel> implements
        QuestionAnswerView.QACallback,
        ChatView.ChatViewListener {

    @BindView(R2.id.parentLayout)
    ViewGroup parentLayout;
    @BindView(R2.id.ivCollapse)
    View ivCollapse;

    @BindView(R2.id.topBarWithWhiteIcon)
    View topBarWithWhiteIcon;
    @BindView(R2.id.ivBack)
    View backIcon;
    @BindView(R2.id.ivDataSaver)
    ImageView ivDataSaver;
    @BindView(R2.id.ivLife)
    ImageView lifeIcon;
    @BindView(R2.id.tvUserCount)
    CustomFontTextView tvUserCount;
    @BindView(R2.id.ivUsers)
    View ivUsers;

    @BindView(R2.id.qaView)
    QuestionAnswerView qaView;

    @BindView(R2.id.view_container)
    FrameLayout viewContainer;

    @BindView(R2.id.indicatorLayout)
    View indicatorLayout;
    @BindView(R2.id.ivChatBg)
    View chatBgView;
    @BindView(R2.id.view_chat)
    ChatView chatView;

    //Video Frame Data
    @BindView(R2.id.videoFrameLayout)
    FrameLayout videoFrameLayout;
    @BindView(R2.id.cardView_video_container)
    AnimatableView videoContainer;
    @BindView(R2.id.player_container)
    AspectRatioFrameLayout contentFrame;
    @BindView(R2.id.viewVideo)
    SurfaceView videoView;
    @BindView(R2.id.textView_timer)
    CustomFontTextView textViewTimer;
    @BindView(R2.id.progressBar)
    ProgressBar progressBar;

    @BindView(R2.id.vw_flipper)
    ViewFlipper vw_flipper;
    @BindView(R2.id.textModeView)
    StreamTextModeView streamTextModeView;

    @OnClick(R2.id.ivBack)
    void backIconClicked() {
        handleBackIconClick();
    }

    @OnClick(R2.id.ivDataSaver)
    void handleDataSaverIconClick() {
        showDataSaverDialog();
    }

    @OnClick(R2.id.ivLife)
    void openExtraLifeDialog() {
        showEarnExtraLifeDialog();
    }

    private GameDialogManager _dialogManager;
    private GradientDrawable gradientDrawable;
    private boolean chatEnabled;
    private int _answerHideDialogState = GameDialogManager.NONE;

    private RtmpWrapper rtmpWrapper;
    private TimerTask timerTask;
    private Timer timer;
    private long currentBitRate = 0;
    private long estimatedBitRate = 0;
    private long videoLatency;


    public LiveGamePlayScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_live_game, viewGroup, false);
    }

    @Override
    protected void onBind(LiveGamePlayViewModel viewModel) {
        if (rtmpWrapper != null) {
            rtmpWrapper.onDestroy();
        }
        rtmpWrapper = new RtmpWrapper(getContext(), videoView);
        bindUIData();
        observeDialogState(viewModel);
        observeGameViewState(viewModel);
        observeLiveGameEvents(viewModel);
        observerChatMsgEvents(viewModel);
        //test();
    }

//    private void test() {
//        String messages[] = {
//
//                "q_264|1400|[{\"a\":\"a\", \"e\":\"q\", \"p\":{\"qt\":\"1 Jack and ____, went up the hill. Complete the poem.Jack and ____, went up the hill. Complete the poem.\",\"qid\":264,\"lst\":false,\"gid\":1400,\"sqn\":1,\"ans\":[{\"oid\":1268,\"val\":\" \\u00E2\\u201A\\u00B9 Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill\",\"pos\":1},{\"oid\":1269,\"val\":\"Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill \",\"pos\":2},{\"oid\":1270,\"val\":\"Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will \",\"pos\":3}]}}]",
//
//                "a_1754|1399|[{\"a\":\"a\",\"p\":{\"qid\":264,\"luc\":\"0\",\"opts\":[{\"cnt\":90,\"oid\":1268},{\"cnt\":90,\"oid\":1269},{\"cnt\":90,\"oid\":1270}],\"crt\":1268},\"e\":\"a\"}]\n" +
//                        "f_22317|1399",
//
//                "q_264|1400|[{\"a\":\"a\", \"e\":\"q\", \"p\":{\"qt\":\"2 Jack and ____, went up the hill. Complete the poem.Jack and ____, went up the hill. Complete the poem.\",\"qid\":265,\"lst\":false,\"gid\":1400,\"sqn\":2,\"ans\":[{\"oid\":1268,\"val\":\" \\u00E2\\u201A\\u00B9 Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill\",\"pos\":1},{\"oid\":1269,\"val\":\"Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill \",\"pos\":2},{\"oid\":1270,\"val\":\"Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will \",\"pos\":3}]}}]",
//
//                "a_1754|1399|[{\"a\":\"a\",\"p\":{\"qid\":265,\"luc\":\"0\",\"opts\":[{\"cnt\":90,\"oid\":1268},{\"cnt\":90,\"oid\":1269},{\"cnt\":90,\"oid\":1270}],\"crt\":1268},\"e\":\"a\"}]\n" +
//                        "f_22317|1399",
//
//                "q_264|1400|[{\"a\":\"a\", \"e\":\"q\", \"p\":{\"qt\":\"3 Jack and ____, went up the hill. Complete the poem.Jack and ____, went up the hill. Complete the poem.\",\"qid\":266,\"lst\":false,\"gid\":1400,\"sqn\":3,\"ans\":[{\"oid\":1268,\"val\":\" \\u00E2\\u201A\\u00B9 Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill Jill\",\"pos\":1},{\"oid\":1269,\"val\":\"Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill Bill \",\"pos\":2},{\"oid\":1270,\"val\":\"Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will Will \",\"pos\":3}]}}]",
//
//                "a_1754|1399|[{\"a\":\"a\",\"p\":{\"qid\":266,\"luc\":\"0\",\"opts\":[{\"cnt\":90,\"oid\":1268},{\"cnt\":90,\"oid\":1269},{\"cnt\":90,\"oid\":1270}],\"crt\":1268},\"e\":\"a\"}]\n" +
//                        "f_22317|1399",
//
//                "e_1399|[{\"a\" : \"a\", \"p\" : {\"a\" : 0 , \"u\" : \"http://slike-live-s3.akamaized.net/b/bbliveslike/playlist.m3u8\",\"t\" : 1519577073, \"g\" : \"1399\"}, \"e\" : \"l\"}]",
//
//        };
//        Observable.fromArray(messages).observeOn(Schedulers.io()).subscribe(new Consumer<String>() {
//            @Override
//            public void accept(String s) throws Exception {
//                Thread.sleep(15000);
//                performCueEvent(s);
//            }
//        });
//    }

    private void performCueEvent(String cuesPoint) {
        // Check and process the cue points
        if (cuesPoint.startsWith("q_")) {
            getViewModel().sendVideoDataEvent("QUESTION_EVENT", cuesPoint, false);
            getViewModel().submitSlikeQueEvent(cuesPoint, InputEvent.TYPE_QUESTION);

        } else if (cuesPoint.startsWith("a_")) {
            getViewModel().sendVideoDataEvent("ANSWER_EVENT", cuesPoint, false);
            getViewModel().submitSlikeQueEvent(cuesPoint, InputEvent.TYPE_ANSWER);

        } else if (cuesPoint.startsWith("f_")) {
            getViewModel().sendVideoDataEvent("WINNER_EVENT", cuesPoint, false);
            getViewModel().submitSlikeQueEvent(cuesPoint, InputEvent.TYPE_WINNER);

        } else if (cuesPoint.startsWith("e_")) {
            getViewModel().sendVideoDataEvent("GAME_FINISH_EVENT", cuesPoint, false);
            getViewModel().submitSlikeQueEvent(cuesPoint, InputEvent.TYPE_GAME_END);
        }
    }

    private void bindUIData() {
        DashboardInfo dashboardInfo = getViewModel().getDashboardInfo();
        streamTextModeView.setBrainBaaziStrings(getBrainBaaziStrings());

        qaView.setVisibility(GONE);
        qaView.setQaCallback(this);
        qaView.setAnalytics(getViewModel().getAnalytics(), dashboardInfo.getCurrentGameInfo().getCurrentGameID(), dashboardInfo.getUser());
        qaView.setBrainBaaziStrings(getBrainBaaziStrings());
        chatView.initChat();
        chatView.setAbusiveString(getViewModel().getAbusiveString());
        gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(0.0f);
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setSize(videoContainer.getWidth(), videoContainer.getHeight());
        videoContainer.setBackground(gradientDrawable);
        _dialogManager = new GameDialogManager(getContext(), getViewModel().getAnalytics(), dashboardInfo.getCurrentGameInfo().getCurrentGameID(), 0L, null, videoFrameLayout, viewContainer);
        _dialogManager.setBrainBaaziStrings(getBrainBaaziStrings());
        initTouchListeners();
        sendUserData(getViewModel().getUser());
    }

    private void sendUserData(User user) {
        if (user != null) {
            chatView.setUser(user);
            _dialogManager.setUser(user);
            if (getViewModel().isStreamModeCoachmarkToShown()) {
                _dialogManager.showStreamModeCoachmarkDialog();
            } else if (user.getLives() > 0) {
                _dialogManager.showLifeCoachmarkDialog();
            }
        }
    }

    private void observerChatMsgEvents(LiveGamePlayViewModel viewModel) {
        int chatFrequencyPerMinute = getViewModel().getChatFrequency();
        chatEnabled = (chatFrequencyPerMinute != 0);
        if (chatEnabled) {
            DisposableObserver<ChatMsg> chatMsgObserver = new DisposableOnNextObserver<ChatMsg>() {
                @Override
                public void onNext(ChatMsg chatMsg) {
                    chatView.addMessage(chatMsg);
                }

            };
            addDisposable(chatMsgObserver);
            viewModel.observeChatInfo().observeOn(AndroidSchedulers.mainThread()).subscribe(chatMsgObserver);
        }
    }

    private void observeDialogState(LiveGamePlayViewModel viewModel) {
        DisposableObserver<Integer> dialogStateObserver = new DisposableOnNextObserver<Integer>() {
            @Override
            public void onNext(Integer dialogState) {
                switch (dialogState) {
                    case LiveGamePlayViewModel.STATE_DIALOG_YOU_WON:
                        if (_dialogManager != null) {
                            _dialogManager.requestStateUpdate(GameDialogManager.SHOW_YOU_WON_DIALOG);
                            _dialogManager.onUpdate();
                        }
                        break;
                    case LiveGamePlayViewModel.STATE_DIALOG_QUIT:
                        handleBackIconClick();
                        break;
                }
            }
        };
        addDisposable(dialogStateObserver);
        viewModel.observeDialogStateBehaviorSubject().observeOn(AndroidSchedulers.mainThread()).subscribe(dialogStateObserver);
    }

    private void observeGameViewState(LiveGamePlayViewModel viewModel) {
        DisposableObserver<GameViewState> observer = new DisposableObserver<GameViewState>() {
            @Override
            public void onNext(GameViewState gameViewState) {
                switch (gameViewState.getViewState()) {
                    case LiveGamePlayViewModel.VIEW_STATE_LOADING:
                        break;
                    case LiveGamePlayViewModel.VIEW_STATE_VIDEO:
                        break;
                    case LiveGamePlayViewModel.VIEW_STATE_FAILED:
                        break;
                }
                updateLiveStateInfo(gameViewState);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        addDisposable(observer);
        viewModel.observeGameViewState().observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    private void observeLiveGameEvents(LiveGamePlayViewModel viewModel) {
        DisposableObserver<GameResponse<?>> observer = new DisposableObserver<GameResponse<?>>() {
            @Override
            public void onNext(final GameResponse<?> gameEvents) {
                handleGameState(gameEvents.gameState());
                boolean showingDialog = false;
                switch (gameEvents.eventType() & GameResponse.MASK_SHOW_BITS) {
                    case GameResponse.TYPE_SHOW_QUESTION:
                        streamTextModeView.updateGameStatesData(gameEvents);
                        handleQuestionEvent(gameEvents);
                        showingDialog = true;
                        break;
                    case GameResponse.TYPE_SHOW_ANSWER:
                        streamTextModeView.updateGameStatesData(gameEvents);
                        handleAnswerEvent(gameEvents);
                        showingDialog = true;
                        break;
                    case GameResponse.TYPE_SHOW_WINNER:
                        streamTextModeView.updateGameStatesData(gameEvents);
                        handleWinnerEvent(gameEvents);
                        break;
                    case GameResponse.TYPE_USER_STATE:
                        break;
                    case GameResponse.TYPE_SHOW_GAME_END:
                        streamTextModeView.updateGameStatesData(gameEvents);
                        handleGameEndEvent();
                        break;
                }

                switch ((gameEvents.eventType() & GameResponse.MASK_USER_INFO_BITS)) {
                    case GameResponse.TYPE_USER_INFO_KICKED_OUT:
                        break;
                    case GameResponse.TYPE_USER_INFO_LATE:
                        if (showingDialog) {
                            getView().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (getView() != null) {
                                        _dialogManager.requestStateUpdate(GameDialogManager.SHOW_YOU_ARE_LATE_DIALOG);
                                    }
                                }
                            }, 4000);
                        }
                        else {
                            _dialogManager.requestStateUpdate(GameDialogManager.SHOW_YOU_ARE_LATE_DIALOG);
                        }
                        break;
                    case GameResponse.TYPE_USER_INFO_LIFE_USED:
                        if (showingDialog) {
                            getView().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (getView() != null) {
                                        handleLifeUsedEvent(gameEvents);
                                    }
                                }
                            }, 4000);
                        }
                        else
                            handleLifeUsedEvent(gameEvents);
                        break;
                    case GameResponse.TYPE_USER_INFO_ELIMINATED:
                        if (showingDialog) {
                            getView().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (getView() != null) {
                                        _dialogManager.requestStateUpdate(GameDialogManager.SHOW_ELIMINATION_DIALOG);
                                    }
                                }
                            }, 4000);
                        }
                        else
                            _dialogManager.requestStateUpdate(GameDialogManager.SHOW_ELIMINATION_DIALOG);

                        break;
                }
                _dialogManager.onUpdate();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
        addDisposable(observer);
        viewModel.observeLiveGameEvents().observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    private void handleBackIconClick() {
        Utils.hideKeyboard(getContext());
        if (chatEnabled && chatView.getVisibility() == View.VISIBLE) {
            chatView.setVisibility(View.GONE);
            chatBgView.setVisibility(View.GONE);
            showIndicators();
        } else {
            showGameQuitDialog(true);
        }
    }

    private void showGameQuitDialog(final boolean hasGameStarted) {
        GameQuitDialog gameQuitDialog = new GameQuitDialog(getContext(), hasGameStarted, new GameQuitDialog.OnDialogClickListener() {
            @Override
            public void dialogAllowed() {
                getViewModel().cleverQuestionQuitEvent("Post Question");
                getViewModel().sendQuitDialogAnalyticsEvent(hasGameStarted);
                handleGameEndEvent();
            }

            @Override
            public void dialogDenied() {
            }
        });
        gameQuitDialog.setBrainBaaziStrings(getBrainBaaziStrings());
        gameQuitDialog.show();
    }

    private void handleGameEndEvent() {
        getViewModel().setPrepareToQuit(true);
        onBackPressed();
    }

    private void showEarnExtraLifeDialog() {
        Utils.hideKeyboard(getContext());
        if (_dialogManager != null) {
            Bundle bundle = new Bundle();
            bundle.putString("username", getViewModel().getUser().getUserName());
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("extra_life_screen_viewed").build();
            getViewModel().logFireBaseScreen(gaEventModel, bundle);

            _dialogManager.showEarnExtraLifeDialog();
        }
    }

    private void updateLiveStateInfo(final GameViewState gameViewState) {
        updateUserData(gameViewState.getConcurrentUserCount(), gameViewState.getLifeAvailable());
        _dialogManager.setPrizeAmount(gameViewState.getPrizeAmount());

        if (!chatView.isChatViewShown()) {
            if (chatEnabled) {
                initTouchListeners();
                chatView.setChatFrequency(getViewModel().getChatFrequency());
                chatView.setChatViewListener(this);
                onChatViewSwipeLeft();
            } else {
                hideIndicators();
                chatView.setVisibility(GONE);
            }
            chatView.setChatViewShown(true);
        }

//        videoContainer.setVisibility(VISIBLE);
        qaView.setLiveGameData(gameViewState);


        rtmpWrapper.init(gameViewState.getLiveStreamUrl());


//        if (slikeRtmpPlayer != null) {
//            return;
//        }
//        slikeRtmpPlayer = new SlikeRtmpLivePlayer();
//        slikeRtmpPlayer.initPlayer(getContext(), new SlikeRtmpLivePlayer.IInitializePlayer() {
//            @Override
//            public void result(boolean isInitialized) {
//                if (slikeRtmpPlayer != null && videoView != null) {
//                    slikeRtmpPlayer.loadVideo(getContext(), gameViewState.getLiveStreamUrl(), videoView, 0, LiveGamePlayScreen.this);
//                    isPlayerInitialized = true;
//                }
//            }
//        });
    }

    private void updateUserData(int userCount, boolean lifeAvailable) {
        if (userCount != -1) {
            tvUserCount.setText(AppUtils.coolFormatWrap(userCount));
        } else {
            tvUserCount.setText("0");
        }

        if (!lifeAvailable) {
            lifeIcon.setImageResource(R.drawable.ic_lives_empty);
        } else {
            lifeIcon.setImageResource(R.drawable.favorite);
        }
    }

    private void handleLifeUsedEvent(GameResponse gameResponse) {
        _dialogManager.requestStateUpdate(GameDialogManager.SHOW_LIFE_USED_DIALOG);
    }

    private void handleGameState(GameState gameState) {
        if (gameState != null) {
            qaView.setUserState(gameState.getUserPlayState());
            updateUserData(gameState.getConcurrentUserCount(), (gameState.getLifeAvailable() > gameState.getLifeUsed()));
            switch (gameState.getUserPlayState()) {
                case GameState.PLAY_STATE_PLAYING:
                    break;
                case GameState.PLAY_STATE_LATE:
                    qaView.setUserState(GameState.PLAY_STATE_ELIMINATED);
                    break;
                case GameState.PLAY_STATE_ELIMINATED:
                    qaView.setUserState(GameState.PLAY_STATE_ELIMINATED);
                    break;
                case GameState.PLAY_STATE_ELIMINATED_ON_QUIT:
                    qaView.setUserState(GameState.PLAY_STATE_ELIMINATED);
                    _dialogManager.requestStateUpdate(GameDialogManager.SHOW_ELIMINATION_ON_MISS_QUESTION);
                    break;
            }
        }
    }

    private void handleQuestionEvent(GameResponse<?> gameEvents) {
        Question question = (Question) gameEvents.value();
        hideTopWhiteAppBar();
        _dialogManager.setQuestion(question.getQuestionNumber());
        videoFrameLayout.setVisibility(View.VISIBLE);
        qaView.setVisibility(VISIBLE);
        qaView.setLiveGameData(gameEvents.gameState());
        qaView.triggerQuestion(videoContainer, gradientDrawable, gameEvents.eventType(), question);
        if (Utils.isKitKatAbove()) {
            videoFrameLayout.bringToFront();
        } else {
            videoFrameLayout.bringToFront();
            parentLayout.requestLayout();
            parentLayout.invalidate();

            videoContainer.bringToFront();
            videoFrameLayout.requestLayout();
            videoFrameLayout.invalidate();
        }

        Bundle bundle = new Bundle();
        bundle.putString("username", getViewModel().getUser().getUserName());
        bundle.putString("game_id", String.valueOf(getViewModel().getDashboardInfo().getCurrentGameInfo().getCurrentGameID()));
        bundle.putString("question_level", "" + question.getQuestionNumber());

        getViewModel().getAnalytics().logFireBaseScreen(Analytics.EVENT_QUESTION_SHOWN);

        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("question_screen_viewed").build();
        getViewModel().logFireBaseScreen(gaEventModel, bundle);
    }

    private void handleAnswerEvent(GameResponse<?> gameEvents) {
        QuestionInfo questionInfo = (QuestionInfo) gameEvents.value();
        videoFrameLayout.setVisibility(View.VISIBLE);
        hideTopWhiteAppBar();
        qaView.setVisibility(VISIBLE);
        qaView.triggerAnswer(videoContainer, gradientDrawable, gameEvents.eventType(), questionInfo);
        if (Utils.isKitKatAbove()) {
            videoFrameLayout.bringToFront();
        } else {
            videoFrameLayout.bringToFront();
            parentLayout.requestLayout();
            parentLayout.invalidate();

            videoContainer.bringToFront();
            videoFrameLayout.requestLayout();
            videoFrameLayout.invalidate();
        }
        getViewModel().getAnalytics().logFireBaseScreen(Analytics.EVENT_ANSWER_SHOWN);
    }

    private void handleWinnerEvent(GameResponse<?> gameEvents) {
        Winner winner = gameEvents.gameState().getWinner();
        hideChatIfShowing();

        videoFrameLayout.setVisibility(View.GONE);
        _dialogManager.setPrizeAmount(winner.getPrizeAmount());

        if (!TextUtils.isEmpty(winner.getEarnExtraLifeList()) && Utils.eligibleForEarnExtraLife(winner.getEarnExtraLifeList(), getViewModel().getUser().getUserName())) {
            _dialogManager.requestStateUpdate(GameDialogManager.SHOW_EARN_LIFE_DIALOG);
        }
        viewContainer.bringToFront();
        if (winner.getWinUserList() != null && winner.getWinUserList().size() > 0) {
            if (winner.getWinUserList().size() == 1) {
                View singleWinnerView = new SingleWinnerView(getContext(), winner, getBrainBaaziStrings());
                viewContainer.removeAllViews();
                viewContainer.addView(singleWinnerView);
                getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        viewContainer.removeAllViews();
                        videoFrameLayout.setVisibility(View.VISIBLE);
                    }
                }, 20000);
            } else {
                View multipleWinnerView = new MultipleWinnersLayout(getContext(), winner, getBrainBaaziStrings());
                viewContainer.removeAllViews();
                viewContainer.addView(multipleWinnerView);

                getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        viewContainer.removeAllViews();
                        videoFrameLayout.setVisibility(View.VISIBLE);
                    }
                }, 20000);
            }
        } else {
            //No Winner Situation
            videoFrameLayout.setVisibility(View.GONE);
            View noWinnerView = LayoutInflater.from(getContext()).inflate(R.layout.view_no_winners, null);
            viewContainer.removeAllViews();
            viewContainer.addView(noWinnerView);

            getView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewContainer.removeAllViews();
                    videoFrameLayout.setVisibility(View.VISIBLE);
                }
            }, 20000);
        }
    }

    private void showProgress(boolean show) {
        progressBar.setVisibility(show ? VISIBLE : GONE);
    }

    @Override
    public void submitUserAnswer(int userAnswer, String optionKey) {
        if (getViewModel() != null) {
            getViewModel().submitUserAnswer(userAnswer, optionKey);
            getViewModel().getAnalytics().logFireBaseScreen(Analytics.EVENT_ANSWER_SUBMIT);
        }
    }

    @Override
    public void answerSubmitted(int questionSeq, String optionKey) {

    }

    @Override
    public void questionGone() {
        showTopWhiteAppBar();
        if (chatEnabled) {
            chatView.bringToFront();
        }
    }

    @Override
    public void answerGone() {
        showTopWhiteAppBar();
        if (_answerHideDialogState != GameDialogManager.NONE) {
            _dialogManager.requestStateUpdate(_answerHideDialogState);
            _answerHideDialogState = GameDialogManager.NONE;
        }
        if (chatEnabled) {
            chatView.bringToFront();
        }
    }

    @Override
    public void showTopWhiteAppBar() {
        if (topBarWithWhiteIcon.getVisibility() == View.INVISIBLE) {
            topBarWithWhiteIcon.setVisibility(View.VISIBLE);
            topBarWithWhiteIcon.bringToFront();
        }
    }

    public void hideTopWhiteAppBar() {
        if (topBarWithWhiteIcon.getVisibility() == View.VISIBLE) {
            topBarWithWhiteIcon.setVisibility(View.INVISIBLE);
        }
    }

    private void showIndicators() {
        if (chatView.getVisibility() != View.VISIBLE) {
            if (indicatorLayout.getVisibility() != View.VISIBLE) {
                indicatorLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private void hideIndicators() {
        if (indicatorLayout.getVisibility() == View.VISIBLE) {
            indicatorLayout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onChatViewSwipeLeft() {
        Utils.hideKeyboard(getContext());
        if (chatView.getVisibility() == View.GONE) {
            chatView.setVisibility(View.VISIBLE);
            chatBgView.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.view_in);
            chatView.startAnimation(animation);
            if (indicatorLayout.getVisibility() == View.VISIBLE) {
                hideIndicators();
            }
        }
    }

    @Override
    public void onChatViewSwipeRight() {
        Utils.hideKeyboard(getContext());
        if (chatView.getVisibility() == View.VISIBLE) {
            Utils.hideKeyboard(getContext());
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.view_out);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    chatView.setVisibility(View.GONE);
                    chatBgView.setVisibility(View.GONE);
                    showIndicators();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            chatView.startAnimation(animation);
        }
        if (indicatorLayout.getVisibility() == View.GONE || indicatorLayout.getVisibility() == View.INVISIBLE) {
            showIndicators();
            indicatorLayout.bringToFront();
        }
    }

    private void hideChatIfShowing() {
        if (chatEnabled) {
            chatView.setVisibility(View.GONE);
            chatBgView.setVisibility(View.GONE);
            hideIndicators();
        }
    }

    public void hideChatView() {
        chatView.setVisibility(View.GONE);
        chatBgView.setVisibility(View.GONE);
    }

    public void showChatView() {
        if (chatEnabled) {
            chatView.setVisibility(View.VISIBLE);
            chatBgView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        _dialogManager.setBrainBaaziStrings(brainBaaziStrings);
        qaView.setBrainBaaziStrings(getBrainBaaziStrings());
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initTouchListeners() {
        if (chatEnabled) {
            contentFrame.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onSwipeLeft() {
                    getViewModel().getAnalytics().cleverTapScreenEvent(getViewModel().getUser(), "comment_swipe");
                    getViewModel().logFireBaseScreen(Analytics.SCREEN_CHAT_SWIPE);
                    onChatViewSwipeLeft();
                }

                @Override
                public void onSwipeRight() {
                    super.onSwipeRight();
                    onChatViewSwipeRight();
                }

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideKeyboard(getContext());
                    return super.onTouch(v, event);
                }
            });

            parentLayout.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onSwipeLeft() {
                    onChatViewSwipeLeft();
                }

                @Override
                public void onSwipeRight() {
                    super.onSwipeRight();
                    onChatViewSwipeRight();
                }

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideKeyboard(getContext());
                    return super.onTouch(v, event);
                }
            });
        }

        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int currentRootViewHeight = parentLayout.getHeight();
                if (currentRootViewHeight <= (2 * Utils.getDeviceDimens(getContext())[1]) / 3) {
                    //ivCollapse.setVisibility(View.VISIBLE);
                } else {
                    //ivCollapse.setVisibility(View.GONE);
                }
                parentLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    @Override
    public void onChatViewTouch() {
        Utils.hideKeyboard(getContext());
    }

    @Override
    public void onChatMessageSend(String message) {
        Utils.hideKeyboard(getContext());
        getViewModel().getAnalytics().cleverTapScreenEvent(getViewModel().getUser(), "comment_added");
        getViewModel().submitUserChatMessage(message);
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getActivityInteractor().performBackPress();
    }

    @Override
    public void willShow() {
        super.willShow();
        rtmpWrapper.onStart();
    }

    @Override
    public void resume() {
        super.resume();
        startTimerForIAmAlive();
        getViewModel().getAnalytics().cleverTapScreenEvent(getViewModel().getUser(), "VideoActivityScreenActive");
        getViewModel().logFireBaseScreen(Analytics.SCREEN_GAMEPLAY);
        if (_dialogManager != null) {
            _dialogManager.startThread();
        }
        rtmpWrapper.onResume();
//        if (slikeRtmpPlayer != null) {
//            reconnectIfPossible(false);
//        }
    }

    @Override
    public void pause() {
        super.pause();
        stopTimerForIamAlive();
        getViewModel().getAnalytics().cleverTapScreenEvent(getViewModel().getUser(), "VideoActivityScreenInactive");
        if (_dialogManager != null) {
            _dialogManager.stopThread();
        }
        rtmpWrapper.onPause();
//        if (slikeRtmpPlayer != null && slikeRtmpPlayer.hasPlayer()) {
//            slikeRtmpPlayer.appPaused();
//            slikeRtmpPlayer.stopPlayer();
//            slikeRtmpPlayer.destroyPlayer();
//        }
    }

    @Override
    public void willHide() {
//        rtmpWrapper.onStop();
        super.willHide();
    }

    @Override
    protected void onUnBind() {
        rtmpWrapper.onDestroy();
//        playerCleanup();
    }

    private void startTimerForIAmAlive() {
        try {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    getView().post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                getViewModel().cleverIAMAliveEvent(estimatedBitRate + "", videoLatency + "", Utils.getDeviceName(), Utils.getVersionCode(getContext()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            };
            timer.scheduleAtFixedRate(timerTask, 0, 60000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopTimerForIamAlive() {
        try {
            timerTask.cancel();
            timerTask = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDataSaverDialog() {
        Utils.hideKeyboard(getContext());
        DataSaverDialog dataSaverDialog = new DataSaverDialog(getContext(), getViewModel().getAnalytics(), ivDataSaver, new StreamModeCallback() {
            @Override
            public void onStreamStateChange(StreamingMode streamMode) {
                changeSlikeStreamMode(streamMode);
            }
        });
        dataSaverDialog.setStreamingMode(rtmpWrapper.getCurrentStreamingMode(), rtmpWrapper.getRecommendedStreamingMode());
        dataSaverDialog.setBrainBaaziStrings(getBrainBaaziStrings());
        dataSaverDialog.show();
    }

    public void changeSlikeStreamMode(StreamingMode streamMode) {
        //TODO event
        //CleverTapHelper.cleverTapStreamModeChangeEvent(mUser, streamingMode.toString(), isUserGenerated, false);
        int streamDrawable = R.drawable.normal_mode;
        boolean toFlipView = false;
        boolean toShowChat = false;
        switch (streamMode) {
            case DEFAULT_MODE:
                streamDrawable = R.drawable.normal_mode;
                toFlipView = (vw_flipper.getDisplayedChild() != 0);
                toShowChat = true;
                break;

            case DATA_SAVER_MODE:
                streamDrawable = R.drawable.saver_mode;
                toFlipView = (vw_flipper.getDisplayedChild() != 0);
                toShowChat = true;
                break;

            case DIGI_ONLY:
                streamDrawable = R.drawable.text_mode;
                toFlipView = (vw_flipper.getDisplayedChild() == 0);
                toShowChat = false;
                break;
        }
        if (toFlipView) {
            vw_flipper.showNext();
            if (toShowChat) {
                showChatView();
            } else {
                hideChatView();
            }
        }
        ivDataSaver.setImageResource(streamDrawable);
        qaView.setStreamingView(streamMode, streamDrawable);
        rtmpWrapper.setCurrentStreamingMode(streamMode);
    }

    class RtmpWrapper implements SlikeRtmpLivePlayer.IInitializePlayer, IStreamComponentListener {

        private final Context context;
        SlikeRtmpLivePlayer slikeRtmpPlayer;
        String url;
        PublishSubject<RTMPEvent> eventPublisher = PublishSubject.create();
        SurfaceView videoView;
        private boolean isPlayerInitialized;
        private StreamingMode currentStreamingMode = StreamingMode.NONE;
        private StreamingMode recommendedStreamingMode = StreamingMode.DEFAULT_MODE;

        final int EVENT_IDLE = 0;
        final int EVENT_INIT = 1;
        final int EVENT_PLAY = 2;
        final int EVENT_PAUSE = 3;
        final int EVENT_STOP = 4;
        final int EVENT_DESTROY = 5;
        final int EVENT_RECONNECT = 6;
        final int EVENT_SWITCH_VIDEO = 8;

        int targetState = 0;
        int currentState = 0;
        private boolean reconnecting;


        public RtmpWrapper(Context context, SurfaceView videoView) {
            this.context = context;
            this.videoView = videoView;
            setSlikeUserData();
            slikeRtmpPlayer = new SlikeRtmpLivePlayer();
            slikeRtmpPlayer.initPlayer(context, RtmpWrapper.this);
            targetState = EVENT_INIT;
            setupEventObserver();
        }

        private void setSlikeUserData() {
            try {
                User user = getViewModel().getUser();
                if (user != null) {
                    String strName = user.getUserName();
                    String strGID = String.valueOf(getViewModel().getDashboardInfo().getCurrentGameInfo().getCurrentGameID());
                    String strEmail = "";
                    StreamCoreUtils.setUserData("&mobile=" + user.getPhoneNumber() + "&username=" + strName + "&gameid=" + strGID + "&email=" + strEmail);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        private void setupEventObserver() {
            eventPublisher.observeOn(Schedulers.io()).subscribe(new Observer<RTMPEvent>() {

                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(RTMPEvent rtmpEvent) {
                    if (currentState == targetState && currentState != EVENT_INIT) {
                        return;
                    }
//                    if (reconnecting && rtmpEvent.event != EVENT_RECONNECT && rtmpEvent.event != EVENT_SWITCH_VIDEO) {
//                        eventPublisher.onNext(rtmpEvent);
//                        return;
//                    }

                    switch (rtmpEvent.event) {
                        case EVENT_INIT:
                            slikeRtmpPlayer.loadVideo(context, rtmpEvent.url, videoView, 0, RtmpWrapper.this);
                            break;
                        case EVENT_PLAY:
                            slikeRtmpPlayer.play();
                            break;
                        case EVENT_RECONNECT:
                            if (slikeRtmpPlayer.isPlaying()) {
                                slikeRtmpPlayer.reconnect(context);
                            }
                            reconnecting = false;
                            return;
                        case EVENT_SWITCH_VIDEO:
                            if (slikeRtmpPlayer.isPlaying()) {
                                slikeRtmpPlayer.switchVideoURL(true);
                            }
                            reconnecting = false;
                            return;
                        case EVENT_PAUSE:
                            slikeRtmpPlayer.pause();
                            break;
                        case EVENT_STOP:
                            slikeRtmpPlayer.stopPlayer();
                            break;
                        case EVENT_DESTROY:
                            slikeRtmpPlayer.destroyPlayer();
                            slikeRtmpPlayer = null;
                            eventPublisher.onComplete();
                            break;
                    }
                    currentState = rtmpEvent.event;
                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
        }

        private void processNextEvent(String url) {
            if (currentState != targetState && currentState != EVENT_IDLE) {
                switch (currentState) {
                    case EVENT_INIT: {
                        switch (targetState) {
                            case EVENT_PLAY:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_PLAY));
                                break;
                            case EVENT_DESTROY:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_DESTROY));
                                break;
                        }
                    }
                    break;
                    case EVENT_PLAY:
                        switch (targetState) {
                            case EVENT_INIT:
                            case EVENT_STOP:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_PAUSE));
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_STOP));
                                break;
                            case EVENT_DESTROY:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_PAUSE));
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_STOP));
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_DESTROY));
                                break;
                        }
                        break;
                    case EVENT_PAUSE:
                        switch (targetState) {
                            case EVENT_PLAY:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_PAUSE));
                                break;
                            case EVENT_DESTROY:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_STOP));
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_DESTROY));
                                break;
                        }
                        break;
                    case EVENT_STOP:
                        switch (targetState) {
                            case EVENT_PLAY:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_INIT));
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_PLAY));
                                break;
                            case EVENT_DESTROY:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_DESTROY));
                                break;
                        }
                        break;
                    case EVENT_DESTROY:
                        switch (targetState) {
                            case EVENT_INIT:
                            case EVENT_STOP:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_DESTROY));
                                break;
                            case EVENT_PLAY:
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_PAUSE));
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_STOP));
                                eventPublisher.onNext(new RTMPEvent(url, EVENT_DESTROY));
                        }
                        break;
                }
                eventPublisher.onNext(new RTMPEvent(url, targetState));
            }
        }

        void init(String url) {
            if (!url.equals(this.url)) {
                this.url = url;
                targetState = EVENT_INIT;
                if (currentState == EVENT_INIT) {
                    eventPublisher.onNext(new RTMPEvent(url, EVENT_INIT));
                } else if (currentState != EVENT_DESTROY) {
                    processNextEvent(url);
                } else if (currentState == EVENT_INIT) {
                    eventPublisher.onNext(new RTMPEvent(url, EVENT_INIT));
                }
            }
        }

        public void onStart() {

        }

        void onResume() {
            if (slikeRtmpPlayer != null) {
                slikeRtmpPlayer.resumeActivity(getContext());
            }
            targetState = EVENT_PLAY;
            processNextEvent(url);
        }

        void onPause() {
            if (slikeRtmpPlayer != null) {
                slikeRtmpPlayer.appPaused();
            }
            targetState = EVENT_PAUSE;
            processNextEvent(url);
        }

        public void onStop() {
            targetState = EVENT_STOP;
            processNextEvent(url);
        }

        void onDestroy() {
            if (slikeRtmpPlayer != null) {
                slikeRtmpPlayer.destroyActivity(getContext());
            }
            targetState = EVENT_DESTROY;
            processNextEvent(url);
        }

        @Override
        public void result(boolean isInitialized) {
            if (targetState != EVENT_DESTROY) {
                eventPublisher.onNext(new RTMPEvent(url, EVENT_INIT));
                processNextEvent(url);
            }
        }

        @Override
        public void onCues(List<Cue> cues) {
            if (currentState != EVENT_DESTROY) {
                if (cues == null || cues.isEmpty())
                    return;
                String cuesPoint = cues.get(0).text.toString();
                if (TextUtils.isEmpty(cuesPoint)) {
                    return;
                }
                Log.d("DATA_CUE", cuesPoint);
                performCueEvent(cuesPoint);
            }
        }


        @Override
        public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        /*if (contentFrame != null) {
            float aspectRatio = height == 0 ? 1 : (width * pixelWidthHeightRatio) / height;
            contentFrame.setAspectRatio(aspectRatio);
        }*/
        }

        @Override
        public void onRenderedFirstFrame() {
            showProgress(false);
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {

        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//            if (playWhenReady && playbackState == 2) {
//                showProgress(true);
//            } else if (playbackState == 3) {
//                showProgress(false);
//            }
        }

        @Override
        public void onPositionDiscontinuity(int i) {

        }

        @Override
        public void onTimelineChanged(Timeline timeline, Object o) {

        }

        @Override
        public void onTimeUpdate(TimedUpdate timedUpdate) {
            try {
                currentBitRate = timedUpdate.currentPlayingBitrate;
                estimatedBitRate = timedUpdate.estimatedBitrate;
                videoLatency = timedUpdate.getLatency();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onBandwidthSample(int i, long l, long l1) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {

        }

        @Override
        public void onPlayerError(final Exception e) {
            if (e != null) {
                getViewModel().sendStreamErrorEvent(e.toString());
            }
            if (reconnecting) {
                return;
            }
            if (e.getCause() != null && e.getCause() instanceof HttpDataSource.InvalidResponseCodeException && ((HttpDataSource.InvalidResponseCodeException) e.getCause()).responseCode == 404) {
                reconnectIfPossible(e.getCause() instanceof HttpDataSource.InvalidResponseCodeException);
            } else if (e.getCause() != null && !(e.getCause() instanceof HlsPlaylistTracker.PlaylistStuckException) && !(e.getCause() instanceof BehindLiveWindowException)) {
                reconnectIfPossible(true);
            }
        }

        @Override
        public void onPlaybackParametersChanged(com.google.android.exoplayer2.slike.PlaybackParameters playbackParameters) {

        }

        @Override
        public void onMetadata(com.google.android.exoplayer2.slike.metadata.Metadata metadata) {

        }

        //SLIKE HELPER METHODS
        private void reconnectIfPossible(final boolean isMediaNotfound) {
            if (reconnecting) {
                return;
            }
            reconnecting = true;
            if (!isMediaNotfound)
                eventPublisher.onNext(new RTMPEvent(url, EVENT_RECONNECT));
            else
                eventPublisher.onNext(new RTMPEvent(url, EVENT_SWITCH_VIDEO));
        }

        @Override
        public void onModeChange(final StreamingMode streamingMode, boolean isUserGenerated) {
            getView().post(new Runnable() {
                @Override
                public void run() {
                    if (currentStreamingMode != streamingMode) {
                        changeSlikeStreamMode(streamingMode);
                    }
                }
            });
        }

        @Override
        public void onModeChangeSuggested(StreamingMode streamingMode) {
            if (streamingMode != currentStreamingMode) {
                //TODO event
                //CleverTapHelper.cleverTapStreamModeChangeEvent(mUser, mode.toString(), false, true);
                recommendedStreamingMode = streamingMode;
                if (streamingMode == StreamingMode.DIGI_ONLY) {

                }
            }
        }

        public StreamingMode getCurrentStreamingMode() {
            if (currentStreamingMode == StreamingMode.NONE) {
                currentStreamingMode = slikeRtmpPlayer.getStreamingMode();
            }
            return currentStreamingMode;
        }

        public StreamingMode getRecommendedStreamingMode() {
            return recommendedStreamingMode;
        }

        public void setCurrentStreamingMode(StreamingMode streamingMode) {
            if (currentStreamingMode != streamingMode) {
                currentStreamingMode = streamingMode;
                slikeRtmpPlayer.setStreamingMode(streamingMode);
            }
        }
    }

    public interface StreamModeCallback {
        void onStreamStateChange(StreamingMode streamMode);
    }

    static class RTMPEvent {

        final String url;
        final int event;

        public RTMPEvent(String url, int event) {
            this.url = url;
            this.event = event;
        }
    }

}
