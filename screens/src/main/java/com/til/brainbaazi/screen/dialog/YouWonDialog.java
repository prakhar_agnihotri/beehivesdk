package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class YouWonDialog extends BaseDialog {

    private User mUser;
    private long prizeAmount;
    private long lastClickMillisResult;
    private static final long THRESHOLD_MILLIS = 1500L;

    public YouWonDialog(@NonNull Context context, @NonNull User user, long prize) {
        super(context);
        setDelayDismiss(10000);
        this.mUser = user;
        this.prizeAmount = prize;
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_you_won;
    }

    @Override
    public void inflateDialogView() {
        ImageView ivClose = findViewById(R.id.closeButton);
        ImageView ivUserImage = findViewById(R.id.profile_img_post);


        CustomFontTextView youWonHead = findViewById(R.id.youWonHead);
        CustomFontTextView tvUserName = findViewById(R.id.profileName);
        CustomFontTextView tvPrizeAmount = findViewById(R.id.prizeMoneyFigure);
        CustomFontTextView shareText = findViewById(R.id.shareText);
        Button btShare = findViewById(R.id.shareWonButton);
        CustomFontTextView tvContinue = findViewById(R.id.continueText);

        ivClose.setOnClickListener(this);
        btShare.setOnClickListener(this);
        tvContinue.setOnClickListener(this);

        updateUserImageUrl(ivUserImage, mUser);
        if (mUser != null) {
            tvUserName.setText(mUser.getUserName());
            /*f (TextUtils.isEmpty(mUser.getUserImgUrl())) {
                if (prizeAmount > 0) {
                    tvPrizeAmount.setText(getContext().getApplicationContext().getString(R.string.currencySymbol) + AppUtils.coolFormatWrap(prizeAmount));
                } else {
                    tvPrizeAmount.setText("");
                }
            }*/
        }

        GameplayStrings brainBaaziStrings = getBrainBaaziStrings().gameplayStrings();
        if (brainBaaziStrings != null) {
            youWonHead.setText(brainBaaziStrings.youWonText());
            shareText.setText(brainBaaziStrings.congratsShareFriendsText());
            btShare.setText(getBrainBaaziStrings().commonStrings().share());
            tvContinue.setText(brainBaaziStrings.continueWatching());
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.closeButton || id == R.id.continueText) {
            dismiss();
        } else if (id == R.id.shareWonButton) {
            long now = SystemClock.elapsedRealtime();
            if (now - lastClickMillisResult > THRESHOLD_MILLIS) {
                Utils.inviteUser(view.getContext(), mUser, "game_screen");
            }
            lastClickMillisResult = now;
        }
    }
}
