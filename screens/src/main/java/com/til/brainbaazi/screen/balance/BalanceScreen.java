package com.til.brainbaazi.screen.balance;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainbaazi.component.Analytics;
import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.PaymentStrings;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.balance.BalanceViewModel;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by saurabh.garg on 2/15/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class BalanceScreen extends BaseScreen<BalanceViewModel> {

    public static final int RESULT_CODE_PAYTM = 0x0340;
    public static final int RESULT_CODE_MOBIKWICK = 0x0341;
    public static final int RESULT_CODE_AMAZON = 0x0342;

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.blockLayout)
    View blockLayout;
    @BindView(R2.id.tipLayout)
    View tipLayout;
    @BindView(R2.id.tv_tip)
    CustomFontTextView tv_tip;

    @BindView(R2.id.yourBalance)
    CustomFontTextView yourBalance;
    @BindView(R2.id.verifyNumber)
    CustomFontTextView verifyNumber;
    @BindView(R2.id.tv_cashout1)
    CustomFontTextView tv_cashout1;
    @BindView(R2.id.tv_cashout2)
    CustomFontTextView tv_cashout2;
    @BindView(R2.id.tv_cashout3)
    CustomFontTextView tv_cashout3;
    @BindView(R2.id.tv_coupons)
    CustomFontTextView tv_coupons;

    @BindView(R2.id.amount)
    CustomFontTextView amount;

    @BindView(R2.id.cashoutButtonPaytm)
    View cashoutButtonPaytm;
    @BindView(R2.id.cashoutButtonMobiwiki)
    View cashoutButtonMobiwiki;
    @BindView(R2.id.cashoutButtonAmazon)
    View cashoutButtonAmazon;


    @OnClick(R2.id.cashoutButtonPaytm)
    void handlePaytmClick() {
        startPaytmTransfer();
    }

    @OnClick(R2.id.cashoutButtonMobiwiki)
    void handleMobikwickClick() {
        startMobikwickTransfer();
    }

    @OnClick(R2.id.cashoutButtonAmazon)
    void handleAmazonClick() {
        startAmazonTransfer();
    }

    private BrainbaaziStrings brainBaaziStrings;
    private ProgressDialog progressDialog;
    private String amountToTransfer;
    private User user;
    private int requestType;
    private DisposableObserver<PaymentResponseMessage> disposablePaymentResponse;
    private DisposableObserver<BrainbaaziStrings> paymentsStringObserver;

    public BalanceScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_balance, viewGroup, false);
    }

    @Override
    protected void onBind(BalanceViewModel viewModel) {
        bindUIViews();
        observePaymentsString();
        observeUserInfo(viewModel);
    }

    private void observePaymentsString() {
        paymentsStringObserver = new DisposableOnNextObserver<BrainbaaziStrings>() {
            @Override
            public void onNext(BrainbaaziStrings brainbaaziStrings) {
                updateBrainBaziTexts(brainbaaziStrings);
            }
        };
        addDisposable(paymentsStringObserver);
        getViewModel()
                .observeBrainbaaziStrings()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(paymentsStringObserver);
    }

    private void bindUIViews() {
        toolbar.setTitle(getContext().getString(R.string.cash_out));
        setNavigationIcon(toolbar, R.drawable.arrow_back);
        configureViewBasedOnTip(false);
    }

    private void configureViewBasedOnTip(boolean isTIP) {
        tipLayout.setVisibility(isTIP ? View.VISIBLE : View.GONE);
        blockLayout.setVisibility(isTIP ? View.VISIBLE : View.GONE);

        cashoutButtonPaytm.setEnabled(!isTIP);
        cashoutButtonAmazon.setEnabled(!isTIP);
        cashoutButtonMobiwiki.setEnabled(!isTIP);
    }

    private void observeUserInfo(BalanceViewModel viewModel) {
        DisposableObserver<User> dashboardStateObserver = new DisposableObserver<User>() {
            @Override
            public void onNext(User userInfo) {
                updateUserData(userInfo);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        addDisposable(dashboardStateObserver);
        viewModel.observeUserInfo().observeOn(AndroidSchedulers.mainThread()).subscribe(dashboardStateObserver);
    }

    private void updateUserData(User user) {
        this.user = user;
        amountToTransfer = String.valueOf(user.getUserBalance());
        if (TextUtils.isEmpty(amountToTransfer) || "--".equalsIgnoreCase(amountToTransfer) || "0".equalsIgnoreCase(amountToTransfer)) {
            amountToTransfer = "0";
            cashoutButtonPaytm.setAlpha(0.4f);
            cashoutButtonMobiwiki.setAlpha(0.4f);
        }
        amount.setText(amountToTransfer);
        configureViewBasedOnTip(user.isTransactionInProgress());
    }

    private void startAmazonTransfer() {
        if (!checkBalanceInfo()) {
            return;
        }
        requestType = RESULT_CODE_AMAZON;
        getViewModel().getAnalytics().cleverBalanceEvent(user, "balance_cashout_clicked", "amazon");
        getViewModel().openAmazonPaymentScreen();
    }

    private void startPaytmTransfer() {
        if (!checkBalanceInfo()) {
            return;
        }
        requestType = RESULT_CODE_PAYTM;
        getViewModel().getAnalytics().cleverBalanceEvent(user, "balance_cashout_clicked", "paytm");
        showCashOutConfirmDialog();
    }

    private void startMobikwickTransfer() {
        if (!checkBalanceInfo()) {
            return;
        }
        requestType = RESULT_CODE_MOBIKWICK;
        getViewModel().getAnalytics().cleverBalanceEvent(user, "balance_cashout_clicked", "mobikwik");
        showCashOutConfirmDialog();
    }

    private void showCashOutConfirmDialog() {
        if (brainBaaziStrings == null) {
            return;
        }
        final Dialog cashoutConfirmDialog = new Dialog(getContext(), android.R.style.Theme_NoTitleBar);
        cashoutConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cashoutConfirmDialog.setCancelable(true);
        cashoutConfirmDialog.setContentView(R.layout.dialog_confirm_cashout);
        cashoutConfirmDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        cashoutConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.HSVToColor(100, new float[]{0f, 0f, 0f})));
        cashoutConfirmDialog.setCanceledOnTouchOutside(true);

        TextView titleTV = cashoutConfirmDialog.findViewById(R.id.sureText);
        TextView confirmEmailDesc = cashoutConfirmDialog.findViewById(R.id.confirmEmailDescTV);
        TextView confirmEmail = cashoutConfirmDialog.findViewById(R.id.confirmEmailTV);
        Button cancelButton = cashoutConfirmDialog.findViewById(R.id.cancelBtn);
        Button confirmButton = cashoutConfirmDialog.findViewById(R.id.confirmBtn);

        String title = brainBaaziStrings.paymentStrings().cashOutDialogTitle();
        String descText = brainBaaziStrings.paymentStrings().cashOutDialogMessage();

        title = title.replace("<source>", getSource());
        descText = descText.replace("<source>", getSource());
        descText = descText.replace("<amount>", amountToTransfer);

        titleTV.setText(title);
        confirmEmailDesc.setText(descText);
        confirmEmail.setText(user.getPhoneNumber());

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewModel().sendPaymentDialogAnalytics(user, "balance_cashout_cancelled", getSource(), "Confirmation Screen ", "Cancel");
                getViewModel().getAnalytics().cleverBalanceEvent(user, "balance_cashout_clicked", getSource());
                cashoutConfirmDialog.dismiss();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewModel().sendPaymentDialogAnalytics(user, "Confirm Cashout Screen", getSource(), "Confirmation Screen ", "Confirm");
                getViewModel().getAnalytics().cleverBalanceEvent(user, "balance_cashout_confirmed", getSource());
                cashoutConfirmDialog.dismiss();
                showProgressLoader();
                startUserPayment();
            }
        });
        try {
            cashoutConfirmDialog.show();
        } catch (Exception ignored) {
        }
    }

    private void showProgressLoader() {
        try {
            progressDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            progressDialog.setMessage("Processing your request...");
            progressDialog.setCancelable(true); // disable dismiss by tapping outside of the dialog
            progressDialog.show();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    private boolean checkBalanceInfo() {
        if (!Utils.isInternetConnected(getContext())) {
            String title = brainBaaziStrings.commonStrings().internetNotConnected();
            Toast.makeText(getContext(), title, Toast.LENGTH_LONG).show();
            return false;
        }
        if (AppUtils.isAmountZero(amountToTransfer)) {
            String title = brainBaaziStrings.paymentStrings().insufficientBalanceText();
            Toast.makeText(getContext(), title, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private String getSource() {
        String requestName = "";
        switch (requestType) {
            case RESULT_CODE_MOBIKWICK: {
                requestName = "MobiKwik";
                break;
            }
            case RESULT_CODE_PAYTM: {
                requestName = "Paytm";
                break;
            }
            case RESULT_CODE_AMAZON: {
                requestName = "Amazon";
                break;
            }
        }

        return requestName;
    }


    private void startUserPayment() {
        if (disposablePaymentResponse != null) {
            disposablePaymentResponse.dispose();
            disposablePaymentResponse = null;
        }
        disposablePaymentResponse = new DisposableObserver<PaymentResponseMessage>() {
            @Override
            public void onNext(PaymentResponseMessage paymentResponseMessage) {
                hideProgressDialog();
                handlePaymentResponse(paymentResponseMessage);
                getViewModel().sendPaymentResponseAnalytics(paymentResponseMessage, user, getSource(), amountToTransfer);
            }

            @Override
            public void onError(Throwable e) {
                hideProgressDialog();
                getViewModel().sendPaymentResponseAnalytics(null, user, getSource(), amountToTransfer);
            }

            @Override
            public void onComplete() {

            }
        };
        getViewModel().startPaymentService(user, getSource()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(disposablePaymentResponse);
        addDisposable(disposablePaymentResponse);
    }

    private void handlePaymentResponse(PaymentResponseMessage paymentResponseMessage) {
        getViewModel().navigateToSuccessScreen(paymentResponseMessage, getSource(), user);
    }

    private void updateBrainBaziTexts(BrainbaaziStrings brainbaaziStrings) {
        this.brainBaaziStrings = brainbaaziStrings;
        PaymentStrings paymentStrings = brainbaaziStrings.paymentStrings();
        toolbar.setTitle(paymentStrings.cashoutText());
        tv_tip.setText(paymentStrings.transactionAlreadyInProgressText());
        yourBalance.setText(paymentStrings.amountToRedeemText());
        verifyNumber.setText(paymentStrings.selectPaymentModeText());
        tv_cashout1.setText(paymentStrings.cashOutWithText());
        tv_cashout2.setText(paymentStrings.cashOutWithText());
        tv_cashout3.setText(paymentStrings.cashOutWithText());
        tv_coupons.setText(paymentStrings.couponsText());
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getActivityInteractor().performBackPress();
    }

    @Override
    protected void onUnBind() {
        getViewModel().getAnalytics().cleverTapScreenEvent(user, "balance_dialog_dismissed");
    }

    @Override
    public void resume() {
        super.resume();
        getViewModel().getAnalytics().cleverTapScreenEvent(user, "balance_dialog_viewed");
        getViewModel().logFireBaseScreen(Analytics.SCREEN_BALANCE_SCREEN);
    }
}
