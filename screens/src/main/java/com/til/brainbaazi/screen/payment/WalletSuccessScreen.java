package com.til.brainbaazi.screen.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.viewmodel.balance.BalanceViewModel;
import com.til.brainbaazi.viewmodel.payment.SuccessViewModel;
import com.til.brainbaazi.viewmodel.payment.WalletSuccessViewModel;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by prashant.rathore on 09/03/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class WalletSuccessScreen extends BaseScreen<WalletSuccessViewModel> {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.tvTitle)
    TextView tvTitle;

    @BindView(R2.id.tvUpdatedBalance)
    TextView tvUpdatedBalance;

    @BindView(R2.id.tvMessage)
    TextView tvMessage;

    @BindView(R2.id.gotohome)
    TextView gotohome;

    private ProgressDialog progressDialog;

    private User user;

    @OnClick(R2.id.gotohome)
    void getToHome() {
        getViewModel().getPaymentNavigation().proceedToHome();
    }

    public WalletSuccessScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.cashout_mobikwik_wallet_success_screen, viewGroup, false);
    }

    @Override
    protected void onBind(WalletSuccessViewModel viewModel) {
        observeUserInfo(viewModel);
        gotohome.setText(getBrainBaaziStrings().paymentStrings().goToHomeText());
        toolbar.setTitle(getBrainBaaziStrings().paymentStrings().cashoutText());
        setNavigationIcon(toolbar, R.drawable.arrow_back);

        if (!TextUtils.isEmpty(getViewModel().getAmountToTransfer())) {
            tvUpdatedBalance.setText(" " + getString(R.string.currencySymbol) + getViewModel().getAmountToTransfer());
        }
        tvTitle.setText(getBrainBaaziStrings().paymentStrings().createMobikwikText());
    }

    @Override
    protected void onUnBind() {

    }

    private void observeUserInfo(WalletSuccessViewModel viewModel) {
        DisposableObserver<User> dashboardStateObserver = new DisposableObserver<User>() {
            @Override
            public void onNext(User userInfo) {
                updateUserData(userInfo);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        addDisposable(dashboardStateObserver);
        viewModel.observeUserInfo().observeOn(AndroidSchedulers.mainThread()).subscribe(dashboardStateObserver);
    }

    private void updateUserData(User user) {
        this.user = user;
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getPaymentNavigation().proceedToHome();
    }

    private void sendRequestForTransferOfAmount() {
        showProgressLoader();
        getViewModel().getAnalytics().cleverMobiqwikCreationEvent(user, "success");
        DisposableObserver<PaymentResponseMessage> transferAmount = new DisposableObserver<PaymentResponseMessage>() {
            @Override
            public void onNext(PaymentResponseMessage paymentResponseMessage) {
                hideProgressDialog();
                if (paymentResponseMessage.isRequestSuccess()) {
                    String message = getBrainBaaziStrings().paymentStrings().mobikwikSuccessText();
                    message = message.replace("<amount>", getViewModel().getAmountToTransfer());
                    tvMessage.setText(message);

                    tvUpdatedBalance.setText(" " + getString(R.string.currencySymbol) + getString(R.string.zero_digit_text));
                }
                getViewModel().getAnalytics().cleverWalletCashoutEvent(user, getViewModel().getAmountToTransfer(), "MobiKwik", "success");
            }

            @Override
            public void onError(Throwable e) {
                getViewModel().getAnalytics().cleverWalletCashoutEvent(user, getViewModel().getAmountToTransfer(), "MobiKwik", "failed");

            }

            @Override
            public void onComplete() {

            }
        };
        getViewModel().getCompositeDisposable().add(transferAmount);
        getViewModel().getPaymentRequests().transferAmount(getViewModel().getAuthToken(), createRequestData()).observeOn(AndroidSchedulers.mainThread()).subscribe(transferAmount);
    }

    private PaymentRequest createRequestData() {
        return PaymentRequest.builder().
                setAmt(getViewModel().getAmountToTransfer()).
                setMob(getViewModel().getPhoneNumber()).
                setIp(PaymentUtils.getIpAddress(getContext().getApplicationContext())).
                setSrc("MobiKwick".toLowerCase()).
                build();
    }

    private void showProgressLoader() {
        try {
            progressDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            String message = getBrainBaaziStrings().paymentStrings().processRequestText();
            progressDialog.setMessage(message);
            progressDialog.setCancelable(true); // disable dismiss by tapping outside of the dialog
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
