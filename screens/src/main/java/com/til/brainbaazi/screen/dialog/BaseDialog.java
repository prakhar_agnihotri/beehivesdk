package com.til.brainbaazi.screen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.brainbaazi.component.network.ImageViewInteractor;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.screen.R;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public abstract class BaseDialog extends Dialog implements View.OnClickListener {
    private long delayDismiss = -1;
    private BrainbaaziStrings brainBaaziStrings;

    public BaseDialog(@NonNull Context context) {
        super(context);
    }

    public BaseDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    public void setBrainBaaziStrings(BrainbaaziStrings brainBaaziStrings) {
        this.brainBaaziStrings = brainBaaziStrings;
    }


    public BrainbaaziStrings getBrainBaaziStrings() {
        return brainBaaziStrings;
    }

    public abstract int getDialogLayout();

    public abstract void inflateDialogView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(getDialogLayout());

        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        inflateDialogView();
    }

    protected void updateUserImageUrl(ImageView ivUserImage, User user) {
        if (user != null) {
            if (ivUserImage instanceof ImageViewInteractor) {
                ((ImageViewInteractor) ivUserImage).setDefault(R.drawable.ic_user_icon);
                ((ImageViewInteractor) ivUserImage).setImageUrl(user.getUserImgUrl());
            } else {
                ivUserImage.setImageResource(R.drawable.ic_profile);
            }
        } else {
            ivUserImage.setImageResource(R.drawable.ic_profile);
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void show() {
        super.show();
        if (delayDismiss > 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (isShowing()) {
                            dismiss();
                        }
                    } catch (Exception ex) {

                    }
                }
            }, delayDismiss);
        }
    }

    public void setDelayDismiss(long delayDismiss) {
        this.delayDismiss = delayDismiss;
    }
}
