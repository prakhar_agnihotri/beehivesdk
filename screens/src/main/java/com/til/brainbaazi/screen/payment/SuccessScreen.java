package com.til.brainbaazi.screen.payment;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.viewmodel.payment.SuccessViewModel;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 09/03/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class SuccessScreen extends BaseScreen<SuccessViewModel>{

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.tv_tip)
    TextView tv_tip;

    @BindView(R2.id.success_message)
    TextView success_message;

    @BindView(R2.id.transaction_id)
    TextView transaction_id;

    @BindView(R2.id.gotohome)
    TextView gotohome;

    @OnClick(R2.id.gotohome)
    void getToHome() {
        getViewModel().getPaymentNavigation().proceedToHome();
    }

    public SuccessScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.cashout_payment_success_screen, viewGroup, false);
    }

    @Override
    protected void onBind(SuccessViewModel viewModel) {
        if (!TextUtils.isEmpty(viewModel.getAmountToTransfer())) {
            if (!TextUtils.isEmpty(viewModel.getMessage())) {
                success_message.setText(viewModel.getMessage());
            }else {
                if(getBrainBaaziStrings() != null)
                    success_message.setText(getBrainBaaziStrings().paymentStrings().paymentInitiateText());
            }
            String message = getBrainBaaziStrings().paymentStrings().orderIdText();
            transaction_id.setText(message + ": " + viewModel.getOrderId());
        }

        gotohome.setText(getBrainBaaziStrings().paymentStrings().goToHomeText());
        tv_tip.setText(getBrainBaaziStrings().paymentStrings().transactionInProgressText());
        toolbar.setTitle(getBrainBaaziStrings().paymentStrings().cashoutText());
        setNavigationIcon(toolbar, R.drawable.arrow_back);
    }

    @Override
    protected void onUnBind() {

    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getPaymentNavigation().proceedToHome();
    }

}
