package com.til.brainbaazi.screen.otp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainbaazi.component.Analytics;
import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.OtpStrings;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.viewmodel.otp.OtpVerifyViewModel;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by prashant.rathore on 22/02/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class VerifyOTPScreen extends BaseScreen<OtpVerifyViewModel> {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.enterCodeText)
    CustomFontTextView enterCodeText;
    @BindView(R2.id.phoneTV)
    CustomFontTextView phoneTV;

    @BindView(R2.id.phoneNumberEV)
    EditText phoneNumberEV;

    @BindView(R2.id.otpStatusTV)
    CustomFontTextView otpStatusTV;
    @BindView(R2.id.timerTV)
    CustomFontTextView timerTV;

    @BindView(R2.id.nextButton)
    ImageView nextButton;

    @OnClick(R2.id.phoneTV)
    void handlePhoneNumberClick() {
        getViewModel().openGenerateOTPScreen();
    }

    @OnClick(R2.id.nextButton)
    void handleSubmitClick() {
        verifyOtpPassword();
    }

    private ProgressDialog progressDialog;
    private int timer = 30;
    private String resendOTPInText, resentCodeText, codeReSentText;

    public VerifyOTPScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_otp_verify, viewGroup, false);
    }

    @Override
    protected void onBind(OtpVerifyViewModel viewModel) {
        bindUIData();
        observerSmsEvents(viewModel);
        observeViewState(viewModel);
    }

    private void bindUIData() {
        nextButton.setAlpha(0.4f);
        toolbar.setTitle(getContext().getString(R.string.enter_otp));
        setNavigationIcon(toolbar, R.drawable.arrow_back);

        resendOTPInText = getContext().getResources().getString(R.string.resend_code_in_text);
        resentCodeText = getContext().getResources().getString(R.string.resend_code_text);
        codeReSentText = getContext().getResources().getString(R.string.code_resent_to_text);

        if (getViewModel().getPhoneNumber() != null) {
            phoneTV.setText(getViewModel().getPhoneNumber().getPhoneNumber());
        }
        countDownTimer.start();
        updateTimerText();
        getView().postDelayed(new Runnable() {

            public void run() {
                phoneNumberEV.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                phoneNumberEV.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 200);

        phoneNumberEV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    verifyOtpPassword();
                    return true;
                }
                return false;
            }
        });

        phoneNumberEV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 6) {
                    nextButton.setEnabled(true);
                    nextButton.setAlpha(1.0f);
                } else {
                    nextButton.setEnabled(false);
                    nextButton.setAlpha(0.4f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void observerSmsEvents(OtpVerifyViewModel viewModel) {
        Disposable smsSubscribe = viewModel.observeSmsEvents().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<String>() {
            @Override
            public void accept(String message) throws Exception {
                if (getContext() == null || TextUtils.isEmpty(message)) {
                    return;
                }
                if (message.length() >= 6) {
                    //remove extra smsRead.contains("is your verification code.")
                    String msgStr = message.substring(0, 6);
                    phoneNumberEV.setText(msgStr);
                    GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("otp_filled").setCategory("Verify Number")
                            .setAction("OTP filled")
                            .setLabel("")
                            .setUserName("")
                            .setTimeStamp(AppUtils.getTimeStamp()).build();
                    getViewModel().sendAnalyticsEvent(gaEventModel);
                }
            }
        });
        addDisposable(smsSubscribe);
    }

    private void observeViewState(OtpVerifyViewModel viewModel) {
        Disposable subscribe = viewModel.observeViewState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer state) throws Exception {
                GaEventModel gaEventModel;
                switch (state) {
                    case OtpVerifyViewModel.STATE_INPUT:
                        gaEventModel = GaEventModel.builder().setMainEvent("otp_initiated").build();
                        getViewModel().sendAnalyticsScreen(gaEventModel, null);
                        break;
                    case OtpVerifyViewModel.STATE_VERIFING:
                        Map<String, Object> data = new HashMap<>();
                        data.put("device_id", Utils.getDeviceId(getContext()));
                        getViewModel().cleverTapEvent("otp_filled", data);

                        gaEventModel = GaEventModel.builder().setMainEvent("otp_filled").build();
                        getViewModel().sendAnalyticsScreen(gaEventModel, null);
                        handleOTPVerifying();
                        break;
                    case OtpVerifyViewModel.STATE_FAILED:
                        handleOTPFail();
                        break;
                    case OtpVerifyViewModel.STATE_SUCCESS:
                        hideProgressDialog();
                        break;
                    case OtpVerifyViewModel.STATE_RESENT:
                        handleOTPResent();
                        break;
                    case OtpVerifyViewModel.STATE_EXIT:
                        hideProgressDialog();
                        break;
                }
            }
        });
        addDisposable(subscribe);
    }

    private void handleOTPResent() {
        try {
            hideProgressDialog();
            timerTV.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorBlack));

            String finalCodeReSentText = codeReSentText;
            finalCodeReSentText = finalCodeReSentText.replaceAll("<mobile>", getViewModel().getPhoneNumber().getPhoneNumber());
            otpStatusTV.setText(finalCodeReSentText);

            otpStatusTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check, 0, 0, 0);
            otpStatusTV.setVisibility(View.VISIBLE);
            countDownTimer.start();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

    }

    private void verifyOtpPassword() {
        String password = phoneNumberEV.getText().toString();
        if (TextUtils.isEmpty(password)) {
            return;
        }
        getViewModel().verifyOtp(password);
    }

    private void handleOTPVerifying() {
        timerTV.setOnClickListener(null);
        String message = getBrainBaaziStrings().otpStrings().verifyingText();
        showProgressLoader(message);
    }

    private void handleOTPFail() {
        hideProgressDialog();
        String message = getBrainBaaziStrings().otpStrings().inCorrectOTPText();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private CountDownTimer countDownTimer = new CountDownTimer(30000, 1000) {

        @Override
        public void onTick(long millisUntilFinished) {
            timer--;
            updateTimerText();
        }

        @Override
        public void onFinish() {
            try {
                if (timerTV == null) {
                    return;
                }
                timerTV.setText(resentCodeText);
                otpStatusTV.setVisibility(View.GONE);
                timerTV.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorAccent));
                timerTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Resend OTP").setCategory("Verify Number")
                                .setAction("Resend OTP")
                                .setLabel("")
                                .setUserName("")
                                .setTimeStamp(AppUtils.getTimeStamp()).build();
                        getViewModel().sendAnalyticsEvent(gaEventModel);

                        timer = 30;
                        String message = getBrainBaaziStrings().otpStrings().resendingOTPText();
                        showProgressLoader(message);
                        getViewModel().resendOtp();
                    }
                });
            } catch (Exception e) {
                countDownTimer.cancel();
            }
        }
    };

    private void updateTimerText() {
        String digit = timer <= 9 ? "0" + timer : String.valueOf(timer);
        String timerText = resendOTPInText + " " + digit;
        timerTV.setText(timerText);
    }

    @Override
    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        OtpStrings otpStrings = brainBaaziStrings.otpStrings();
        toolbar.setTitle(otpStrings.enterOtpText());
        enterCodeText.setText(otpStrings.enterSixDigitCodeText());
        resendOTPInText = otpStrings.resendOTPInText();
        resentCodeText = otpStrings.resentCodeText();
        codeReSentText = otpStrings.codeResentToText();
    }

    @Override
    public void pause() {
        Utils.hideKeyboard(getContext());
        super.pause();
    }

    private void showProgressLoader(String message) {
        try {
            progressDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            progressDialog.setInverseBackgroundForced(true);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(true); // disable dismiss by tapping outside of the dialog
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getActivityInteractor().performBackPress();
    }

    @Override
    public void resume() {
        super.resume();
        getViewModel().logFireBaseScreen(Analytics.SCREEN_OTP_VERIFY);
    }

    @Override
    protected void onUnBind() {

    }
}
