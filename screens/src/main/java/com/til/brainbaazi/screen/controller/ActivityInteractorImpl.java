package com.til.brainbaazi.screen.controller;

import android.app.Activity;

import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.interactor.activity.ActivityPermissionResult;
import com.til.brainbaazi.interactor.activity.ActivityResult;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

abstract class ActivityInteractorImpl implements ActivityInteractor {

    private PublishSubject<ActivityPermissionResult> activityPermissionResultPublisher = PublishSubject.create();
    private PublishSubject<ActivityResult> activityResultPublisher = PublishSubject.create();

    private Activity activity;


    void publisPermissionResult(ActivityPermissionResult permissionResult) {
        activityPermissionResultPublisher.onNext(permissionResult);
    }

    void publishActivityResult(ActivityResult activityResult) {
        activityResultPublisher.onNext(activityResult);
    }

    @Override
    public Observable<? extends ActivityResult> activityResult() {
        return activityResultPublisher;
    }

    @Override
    public Observable<ActivityPermissionResult> permissionResults() {
        return activityPermissionResultPublisher;
    }

    @Override
    public abstract void requestPermission(String[] permissions, int code);
}