package com.til.brainbaazi.screen.gamePlay.chat;/*
 * Copyright (c) 2017. All rights reserved by BBDSL. Developed by Pravin Ranjan.
 */


import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.recycleHelper.LinearLayoutManagerWithSmoothScroller;
import com.til.brainbaazi.screen.recycleHelper.OnSwipeTouchListener;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;


public final class ChatView extends RelativeLayout {
    private EditText inputBox;
    private ImageView sendButton;
    private RecyclerView mChatListView;
    private View chatEditBoxLayout;
    private ImageView commentNowBtn;

    private User user;
    private ChatRecycleAdapter mChatListAdapter;
    private ChatViewListener chatViewListener;

    public String abusiveString;
    private long chatDelayInMillis;
    private boolean isChatViewShown;


    public ChatView(Context context) {
        this(context, null);
    }

    public ChatView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(getContext()).inflate(R.layout.view_chat_screen, this);
    }

    public void addMessage(ChatMsg chatMsg) {
        if (mChatListAdapter != null) {
            mChatListAdapter.addChatMsgList(chatMsg);
            mChatListView.post(new Runnable() {
                @Override
                public void run() {
                    mChatListView.getLayoutManager().smoothScrollToPosition(mChatListView, new RecyclerView.State(), mChatListAdapter.getItemCount() - 1);
                }
            });
        }
    }

    public void setAbusiveString(String abusiveString) {
        this.abusiveString = abusiveString;
    }

    public void initChat() {
        try {
            //abusiveString = AppUtils.loadFilteredText(getContext().getApplicationContext());
            mChatListView = findViewById(R.id.chatListView);
            inputBox = findViewById(R.id.inputBox);
            chatEditBoxLayout = findViewById(R.id.chatEditBoxLayout);
            commentNowBtn = findViewById(R.id.commentNowBtn);
            sendButton = findViewById(R.id.sendButton);

            mChatListAdapter = new ChatRecycleAdapter();
            LinearLayoutManagerWithSmoothScroller mLayoutManager = new LinearLayoutManagerWithSmoothScroller(getContext());
            DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
            itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider_line_chat_list));
            mChatListView.addItemDecoration(itemDecorator);
            mLayoutManager.setStackFromEnd(true);
            mChatListView.setLayoutManager(mLayoutManager);
            mChatListView.setAdapter(mChatListAdapter);
            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.hideKeyboard(v.getContext());
                    submitUserComment();
                }
            });

            commentNowBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.hideKeyboard(v.getContext());
                    if (chatEditBoxLayout.getVisibility() == View.GONE) {
                        commentNowBtn.setVisibility(View.GONE);
                        chatEditBoxLayout.setVisibility(View.VISIBLE);
                        inputBox.requestFocus();
                        Utils.showKeyboard(v.getContext(), inputBox);
                        inputBox.performClick();
                    }
                }
            });
            mChatListView.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onSwipeRight() {
                    super.onSwipeRight();
                    if (chatViewListener != null) {
                        chatViewListener.onChatViewSwipeRight();
                    }
                }

                @Override
                public void onSwipeLeft() {
                    super.onSwipeLeft();
                    if (chatViewListener != null) {
                        chatViewListener.onChatViewSwipeLeft();
                    }
                }

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (chatViewListener != null) {
                        chatViewListener.onChatViewTouch();
                    }
                    return super.onTouch(v, event);
                }
            });
        } catch (Exception e) {
        }
    }

    private void submitUserComment() {
        if (user == null) {
            return;
        }
        String msg = inputBox.getText().toString();
        if (TextUtils.isEmpty(msg)) return;

        if (TextUtils.isEmpty(msg.trim())) return;

        boolean isMatches = false;
        try {
            msg = msg.replaceAll("[-+.^:,*?]", "");
            String[] msgs = msg.split(" ");
            for (String text : msgs) {
                if (AppUtils.isMatches(text, abusiveString)) {
                    isMatches = true;
                    break;
                }
            }
        } catch (Exception ex) {
        }

//        ChatMsg.Builder builder = ChatMsg.builder().setMessage(msg).setName(user.getUserName()).setImgUrl(user.getUserImgUrl());
//        addMessage(builder.build());
        if (isMatches) {
            inputBox.setText("");
            return;
        } else if (chatViewListener != null) {
            chatViewListener.onChatMessageSend(inputBox.getText().toString());
        }

        inputBox.setText("");
        sendButton.setClickable(false);
        sendButton.setAlpha(0.2f);
        sendButton.postDelayed(new Runnable() {
            @Override
            public void run() {
                sendButton.setClickable(true);
                sendButton.setAlpha(1.0f);
            }
        }, chatDelayInMillis);
    }

    public void setChatFrequency(int chatFrequencyPerMinute) {
        this.chatDelayInMillis = (60 * 1000) / chatFrequencyPerMinute;
    }

    public void setChatViewListener(ChatView.ChatViewListener chatViewListener) {
        this.chatViewListener = chatViewListener;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isChatViewShown() {
        return isChatViewShown;
    }

    public void setChatViewShown(boolean chatViewShown) {
        isChatViewShown = chatViewShown;
    }

    public interface ChatViewListener {
        void onChatViewSwipeLeft();

        void onChatViewSwipeRight();

        void onChatViewTouch();

        void onChatMessageSend(String message);

    }
}