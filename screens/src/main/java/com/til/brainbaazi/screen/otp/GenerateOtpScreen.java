package com.til.brainbaazi.screen.otp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainbaazi.component.Analytics;
import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.otp.CountryModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.interactor.activity.ActivityPermissionResult;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.dialog.countryCode.ISDSelectionDialog;
import com.til.brainbaazi.screen.utils.PermissionUtils;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.otp.OtpGenerateViewModel;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by prashant.rathore on 15/02/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class GenerateOtpScreen extends BaseScreen<OtpGenerateViewModel> {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.isd_code_ll)
    View isd_code_ll;
    @BindView(R2.id.countryCodeTV)
    CustomFontTextView countryCodeTV;

    @BindView(R2.id.phoneNumberEV)
    EditText phoneNumberEV;

    @BindView(R2.id.nextButton)
    ImageView nextButton;

    @OnClick(R2.id.isd_code_ll)
    void handleCountryCodeClick() {
        openCountryCodeDialog();
    }

    @OnClick(R2.id.nextButton)
    void handleSubmitClick() {
        checkAndGenerateOTP();
    }


    private CountryListModel countryListModel;
    private CountryModel currentCountry = CountryModel.builder().setCode("IN").setName("India").setDialCode("+91").build();
    ;
    private ProgressDialog progressDialog;

    public GenerateOtpScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_otp_generate, viewGroup, false);
    }

    @Override
    protected void onBind(OtpGenerateViewModel viewModel) {
        bindLoginUiViews();
        //showAppPermissionDialog(viewModel);
        observerCountryCodeInfo(viewModel);
        observeViewState(viewModel);
        nextButton.setAlpha(0.4f);
        nextButton.setEnabled(false);
        getView().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getContext() != null) {
                    phoneNumberEV.requestFocus();
                    Utils.showKeyboard(getContext(), phoneNumberEV);
                }
            }
        }, 200);
    }

    private void showAppPermissionDialog(OtpGenerateViewModel viewModel) {
        //Observer Permission Changes
        viewModel.getActivityInteractor().permissionResults().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<ActivityPermissionResult>() {
            @Override
            public void accept(ActivityPermissionResult permissionResults) throws Exception {
                if (permissionResults == null) {
                    return;
                }
                if (permissionResults.getRequestCode() == PermissionUtils.CODE_READ_SMS) {
//                    if (permissionResults.getGrantResult().length > 0 && permissionResults.getGrantResult()[0] == PackageManager.PERMISSION_GRANTED) {
//                        OtpStrings brainBaaziStrings = getBrainBaaziStrings().otpStrings();
//                        String message = (brainBaaziStrings != null ? brainBaaziStrings.readSMSGrantText() : getString(R.string.read_permission_allowed));
//                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
//                    } else {
//                        BrainBaaziStrings brainBaaziStrings = getBrainBaaziStrings();
//                        String message = (brainBaaziStrings != null ? brainBaaziStrings.getReadSMSDenyText() : getString(R.string.read_permission_denied));
//                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
//                    }
                }
            }
        });

        final String permission = Manifest.permission.READ_SMS;
        if (!Utils.checkPermission(getContext(), permission))
            PermissionUtils.showUserPermissionText(getContext(), null, PermissionUtils.PERMISSION_SMS, getBrainBaaziStrings(), new PermissionUtils.OnPermissionListener() {
                @Override
                public void dialogAlreadyShown() {

                }

                @Override
                public void dialogAllowed() {
                    sendSMSDialogEvent("OK");
                    getViewModel().getActivityInteractor().requestPermission(new String[]{permission}, PermissionUtils.CODE_READ_SMS);
                }

                @Override
                public void dialogDenied() {
                    sendSMSDialogEvent("Cancel");
                }
            });
    }

    private void sendSMSDialogEvent(String action) {
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Read SMS Permission").setCategory("Verify Number")
                .setAction(action)
                .setLabel("")
                .setUserName("")
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getViewModel().logFireBaseEvent(gaEventModel);
    }

    private void observeViewState(OtpGenerateViewModel viewModel) {
        Disposable subscribe = viewModel.observeViewState().subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer state) throws Exception {
                switch (state) {
                    case OtpGenerateViewModel.STATE_INPUT:
                        break;
                    case OtpGenerateViewModel.STATE_REQUESTING:
                        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("phone_filled").build();
                        getViewModel().logFireBaseScreen(gaEventModel, null);
                        String message = getBrainBaaziStrings().otpStrings().verifyingText();
                        showProgressLoader(message);
                        break;
                    case OtpGenerateViewModel.STATE_FAILED:
                        hideProgressDialog();
                        if(getContext() != null) {
                            BrainbaaziStrings brainbaaziStrings = getBrainBaaziStrings();

                            Toast.makeText(getContext(), brainbaaziStrings.otpStrings().invalidPhoneNumber(), Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case OtpGenerateViewModel.STATE_SUCCESS:
                        sendOTPStartEvent();
                        hideProgressDialog();
                        break;
                    case OtpGenerateViewModel.STATE_NO_NETWORK:
                        showNoNetworkMessage();
                        break;
                }
            }
        });
        addDisposable(subscribe);
    }

    private void observerCountryCodeInfo(OtpGenerateViewModel viewModel) {
        DisposableObserver<CountryListModel> modelDisposableObserver = new DisposableOnNextObserver<CountryListModel>() {
            @Override
            public void onNext(CountryListModel listModel) {
                if (listModel != null && listModel.getCountryModels() != null && listModel.getCountryModels().size() > 0) {
                    countryListModel = listModel;
                    //updateCountryCodeData(listModel.getCountryModels().get(0));
                }
            }

        };
        addDisposable(modelDisposableObserver);
        viewModel.observeCountryCodeInfo().observeOn(AndroidSchedulers.mainThread()).subscribe(modelDisposableObserver);
    }

    private void bindLoginUiViews() {
        toolbar.setTitle(getContext().getString(R.string.enter_phone_number));
        setNavigationIcon(toolbar, R.drawable.arrow_back);
        phoneNumberEV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    boolean isValidNumber = Utils.isMobileNumberValid(charSequence.toString());
                    if (isValidNumber) {
                        nextButton.setEnabled(true);
                        nextButton.setAlpha(1.0f);
                    } else {
                        nextButton.setEnabled(false);
                        nextButton.setAlpha(0.4f);
                    }
                } else {
                    nextButton.setAlpha(0.4f);
                    nextButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        phoneNumberEV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    checkAndGenerateOTP();
                    return true;
                }
                return false;
            }
        });
    }

    private void checkAndGenerateOTP() {
        String inputNumber = phoneNumberEV.getText().toString();
        String countryCode = currentCountry.getDialCode();
        if (!Utils.isMobileNumberValid(inputNumber)) {
            return;
        }
        if (!Utils.isInternetConnected(getContext())) {
            showNoNetworkMessage();
            return;
        }
        getViewModel().generateOtp(inputNumber, currentCountry);
    }

    private void updateCountryCodeData(CountryModel countryModel) {
        this.currentCountry = countryModel;
        String countryDialCode = countryModel.getDialCode();
        String countryImageString = countryModel.getCode().toLowerCase(Locale.ENGLISH);
        String countryString = countryDialCode + " (" + countryModel.getName() + ")";
        countryCodeTV.setText(countryString);
    }

    private void openCountryCodeDialog() {
        if (countryListModel != null && countryListModel.getCountryModels() != null && countryListModel.getCountryModels().size() <= 1) {
            return;
        }
        final ISDSelectionDialog dialog = new ISDSelectionDialog(getContext());
        String message = getBrainBaaziStrings().otpStrings().searchText();
        dialog.setSearchHint(countryListModel, message, new ISDSelectionDialog.OnCountrySelectionListener() {
            @Override
            public void onCountrySelected(CountryModel countryModel) {
                updateCountryCodeData(countryModel);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }, getBrainBaaziStrings());
        dialog.show();
        getViewModel().logFireBaseScreen(Analytics.SCREEN_ISD_SELECTION);
    }

    private void showProgressLoader(String message) {
        try {
            progressDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            progressDialog.setInverseBackgroundForced(true);
            //progressDialog.setTitle("Loading");
            progressDialog.setMessage(message);
            progressDialog.setCancelable(true); // disable dismiss by tapping outside of the dialog
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        toolbar.setTitle(brainBaaziStrings.otpStrings().enterPhoneText());
    }

    private void showNoNetworkMessage() {
        String message = getBrainBaaziStrings().commonStrings().internetNotConnected();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getActivityInteractor().performBackPress();
    }

    @Override
    public void resume() {
        super.resume();
        getViewModel().logFireBaseScreen(Analytics.SCREEN_OTP_GENERATE);

        Map<String, Object> data = new HashMap<>();
        data.put("device_id", Utils.getDeviceId(getContext()));
        getViewModel().cleverTapEvent("verify_phone_screen", data);

        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("verify_phone_screen").build();
        getViewModel().logFireBaseScreen(gaEventModel, null);

        gaEventModel = GaEventModel.builder().setMainEvent("verify_phone_screen").setCategory("Verify Number")
                .setAction("")
                .setLabel("")
                .setUserName("")
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getViewModel().logFireBaseScreen(gaEventModel, null);
    }

    @Override
    public void pause() {
        Utils.hideKeyboard(getContext());
        super.pause();
    }

    @Override
    protected void onUnBind() {
    }

    private void sendOTPStartEvent() {
        getViewModel().sendOTPStartEvent(Utils.getDeviceId(getContext()));
    }
}
