package com.til.brainbaazi.screen.splash;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.til.brainbaazi.entity.config.LanguageOption;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saurabh.garg on 3/10/18.
 */

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageSelectionHeaderHolder> {
    private final int TYPE_HEADER = 1;
    private final int TYPE_LANG_ITEM = 2;

    private final OnLanguageSelection onLanguageSelection;
    private List<LanguageOption> mValues;
    private LanguageOption selected;

    public LanguageAdapter(OnLanguageSelection onLanguageSelection) {
        this.onLanguageSelection = onLanguageSelection;
    }

    public void addValues(List<LanguageOption> values) {
        if (this.mValues == null) {
            this.mValues = new ArrayList<>();
        }
        this.mValues.addAll(values);
        notifyDataSetChanged();
    }

    public LanguageOption getSelected() {
        return selected;
    }

    public void setSelected(LanguageOption selected) {
        this.selected = selected;
        notifyDataSetChanged();
        if (onLanguageSelection != null) {
            onLanguageSelection.onLanguageSelection(selected);
        }
    }

    @Override
    public LanguageSelectionHeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_language_header, parent, false);
            return new LanguageSelectionHeaderHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_language_item, parent, false);
            return new LanguageSelectionItemHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(LanguageSelectionHeaderHolder holder, final int position) {
        if (position == 0) {
            bindLanguageHeader(holder);
        } else if (holder instanceof LanguageSelectionItemHolder) {
            LanguageOption language = mValues.get(position - 1);
            bindLangSelectionData((LanguageSelectionItemHolder) holder, language);
        }
    }

    private void bindLanguageHeader(LanguageSelectionHeaderHolder holder) {
        String title = holder.tv_header.getContext().getString(R.string.select_language);
        if (selected != null) {
            title = selected.selectLanguageText();
        }
        holder.tv_header.setText(title);
    }

    private void bindLangSelectionData(LanguageSelectionItemHolder holder, LanguageOption language) {
        holder.setItem(language);
        holder.tv_header.setText(language.name());
        if (selected != null && language.name().equalsIgnoreCase(selected.name())) {
            holder.cb_selection.setChecked(true);
        } else {
            holder.cb_selection.setChecked(false);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return TYPE_LANG_ITEM;
    }

    public int getSpanSize(int position) {
        int type = getItemViewType(position);
        int spanSize = 1;
        switch (type) {
            case TYPE_HEADER:
                spanSize = 2;
                break;
        }
        return spanSize;
    }

    @Override
    public int getItemCount() {
        return mValues != null ? (mValues.size() + 1) : 0;
    }

    public class LanguageSelectionHeaderHolder extends RecyclerView.ViewHolder {
        public final CustomFontTextView tv_header;

        public LanguageSelectionHeaderHolder(View view) {
            super(view);
            this.tv_header = view.findViewById(R.id.tv_header);
        }
    }

    public class LanguageSelectionItemHolder extends LanguageSelectionHeaderHolder implements View.OnClickListener {
        public final View parentLayout;
        public final CheckBox cb_selection;
        public LanguageOption language;

        public LanguageSelectionItemHolder(View view) {
            super(view);
            this.parentLayout = view.findViewById(R.id.parentLayout);
            this.cb_selection = view.findViewById(R.id.cb_selection);
            this.parentLayout.setOnClickListener(this);
        }

        public void setItem(LanguageOption language) {
            this.language = language;
        }

        @Override
        public void onClick(View v) {
            setSelected(language);
        }
    }

    public interface OnLanguageSelection {
        void onLanguageSelection(LanguageOption languageOption);
    }
}
