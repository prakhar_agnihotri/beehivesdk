package com.til.brainbaazi.screen.leaderBoard.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.brainbaazi.component.network.ImageViewInteractor;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardUser;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardAdapter.RestUserViewHolder> {
    private final int TYPE_USER_TOP = 1;
    private final int TYPE_USER_REST = 2;

    private List<LeaderBoardUser> mValues;
    private long amountPerUser = -1;

    public void addValues(List<LeaderBoardUser> values, boolean toClear) {
        if (this.mValues == null || toClear) {
            this.mValues = new ArrayList<>();
        }
        this.mValues.addAll(values);
        notifyDataSetChanged();
    }

    public void setAmountPerUser(long amountPerUser) {
        this.amountPerUser = amountPerUser;
    }

    @Override
    public RestUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_USER_TOP) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_leaderboard_item_top, parent, false);
            return new TopUserViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_leaderboard_item, parent, false);
            return new RestUserViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RestUserViewHolder holder, final int position) {
        LeaderBoardUser leaderBoardUser = mValues.get(position);
        bindUserAdapterData(holder, leaderBoardUser);
        bindUserRankImage(holder.profile_img_post.getContext(), holder, position);
    }

    private void bindUserRankImage(Context context, RestUserViewHolder holder, int rank) {
        if (holder instanceof TopUserViewHolder) {
            TopUserViewHolder viewHolder = (TopUserViewHolder) holder;
            if (rank == 0) {
                viewHolder.img_rank.setImageResource(R.drawable.badge1);
                viewHolder.user_prize.setTextColor(context.getResources().getColor(R.color.bbcolorWarning));
            } else if (rank == 1) {
                viewHolder.img_rank.setImageResource(R.drawable.badge2);
                viewHolder.user_prize.setTextColor(context.getResources().getColor(R.color.bbcolor_f66092));
            } else if (rank == 2) {
                viewHolder.img_rank.setImageResource(R.drawable.badge3);
                viewHolder.user_prize.setTextColor(context.getResources().getColor(R.color.bbcolor_6b7fff));
            }
        }
    }

    private void bindUserAdapterData(RestUserViewHolder holder, LeaderBoardUser leaderBoardUser) {
        holder.user_name.setText(leaderBoardUser.getUserName());
        if (amountPerUser > 0) {
            holder.user_prize.setText(getPrizeAmount(leaderBoardUser));
            setTextViewData(holder.user_rank, null);
        } else {
            holder.user_prize.setText(getPrizeAmount(leaderBoardUser));
            setTextViewData(holder.user_rank, String.valueOf(leaderBoardUser.getUserRank()));
        }
        setImageView(holder.profile_img_post, leaderBoardUser.getImgUrl());
    }


    private String getPrizeAmount(LeaderBoardUser leaderBoardUser) {
        long amount = (amountPerUser > 0 ? amountPerUser : leaderBoardUser.getPrize());
        return ("₹" + String.valueOf(amount));
        //return AppUtils.coolFormat(amount);
    }

    private void setImageView(final ImageView profileImageView, String photoUrl) {
        profileImageView.setImageResource(R.drawable.ic_profile);
        if (profileImageView instanceof ImageViewInteractor) {
            ((ImageViewInteractor) profileImageView).setDefault(R.drawable.ic_user_icon);
            ((ImageViewInteractor) profileImageView).setImageUrl(photoUrl);
        }
    }

    private void setTextViewData(CustomFontTextView view, String text) {
        if (view != null) {
            if (!TextUtils.isEmpty(text)) {
                view.setText(text);
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isLeaderBoardWinners() && position < 3) {
            return TYPE_USER_TOP;
        }
        return TYPE_USER_REST;
    }

    private boolean isLeaderBoardWinners() {
        return (amountPerUser < 0);
    }

    @Override
    public int getItemCount() {
        return mValues != null ? mValues.size() : 0;
    }

    public class TopUserViewHolder extends RestUserViewHolder {
        public final ImageView img_rank;

        public TopUserViewHolder(View view) {
            super(view);
            img_rank = view.findViewById(R.id.img_rank);
        }
    }

    public class RestUserViewHolder extends RecyclerView.ViewHolder {
        public final CustomFontTextView user_name, user_rank, user_prize;
        public final ImageView profile_img_post;

        public RestUserViewHolder(View view) {
            super(view);
            profile_img_post = view.findViewById(R.id.profile_img_post);
            user_name = view.findViewById(R.id.user_name);
            user_rank = view.findViewById(R.id.user_rank);
            user_prize = view.findViewById(R.id.user_prize);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + user_prize.getText() + "'";
        }
    }
}
