package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.CommonStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

/**
 * Created by saurabh.garg on 2/26/18.
 */

public class GameQuitDialog extends BaseDialog {
    private boolean hasGameStarted;
    private OnDialogClickListener onDialogClickListener;

    public GameQuitDialog(@NonNull Context context, boolean hasGameStarted, OnDialogClickListener onDialogClickListener) {
        super(context);
        this.hasGameStarted = hasGameStarted;
        this.onDialogClickListener = onDialogClickListener;
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_quit_game;
    }

    @Override
    public void inflateDialogView() {
        CustomFontTextView sureText = findViewById(R.id.sureText);
        Button cancel = findViewById(R.id.cancelBtn);
        Button quitBtn = findViewById(R.id.quitBtn);

        CommonStrings commonStrings = getBrainBaaziStrings().commonStrings();
        sureText.setText(hasGameStarted ? commonStrings.exitDialogGameStartedText() : commonStrings.exitDialogGameBeginText());
        cancel.setText(commonStrings.cancelText());
        quitBtn.setText(commonStrings.quitText());

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogClickListener.dialogDenied();
                dismiss();
            }
        });

        quitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onDialogClickListener.dialogAllowed();
            }
        });
    }

    public interface OnDialogClickListener {
        public void dialogAllowed();

        public void dialogDenied();
    }
}
