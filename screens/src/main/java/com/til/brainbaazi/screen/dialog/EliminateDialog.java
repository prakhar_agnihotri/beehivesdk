package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.brainbaazi.component.Analytics;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class EliminateDialog extends BaseDialog {

    protected final Analytics analytics;
    protected final String gameID;
    protected final int question;
    protected final User mUser;

    private long lastClickMillisResult;
    private static final long THRESHOLD_MILLIS = 1500L;

    public EliminateDialog(@NonNull Context context, @NonNull User user, Analytics analytics, String gameID, int question) {
        super(context);
        this.mUser = user;
        this.analytics = analytics;
        this.gameID = gameID;
        this.question = question;
        setDelayDismiss(10000);
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_you_eliminated;
    }

    @Override
    public void inflateDialogView() {
        ImageView profileImage = findViewById(R.id.profile_img_post);

        CustomFontTextView youEliminatedHead = findViewById(R.id.youEliminatedHead);
        CustomFontTextView question = findViewById(R.id.question);
        Button shareButton = findViewById(R.id.shareButton);
        CustomFontTextView continueText = findViewById(R.id.continueText);

        findViewById(R.id.closeButton).setOnClickListener(this);
        continueText.setOnClickListener(this);
        shareButton.setOnClickListener(this);

        updateUserImageUrl(profileImage, mUser);
        if (getBrainBaaziStrings() != null) {
            GameplayStrings strings = getBrainBaaziStrings().gameplayStrings();

            youEliminatedHead.setText(strings.youAreEliminatedText());
            shareButton.setText(strings.dialogGetExtraLifeText());
            question.setText(strings.continuePlayToEarnText());
            continueText.setText(strings.continueWatching());
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.closeButton) {
            dismiss();
        } else if (id == R.id.continueText) {
            Map<String, Object> data = new HashMap<>();
            data.put("username", mUser.getUserName());
            data.put("Phone", mUser.getPhoneNumber());
            data.put("game_id", gameID);
            data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
            analytics.cleverTapEvent("eliminated_continue_watch_event", data);

            dismiss();
        } else if (id == R.id.shareButton) {
            Map<String, Object> data = new HashMap<>();
            data.put("username", mUser.getUserName());
            data.put("Phone", mUser.getPhoneNumber());
            data.put("game_id", gameID);
            data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
            analytics.cleverTapEvent("eliminated_share_event", data);

            long now = SystemClock.elapsedRealtime();
            if (now - lastClickMillisResult > THRESHOLD_MILLIS) {
                Utils.inviteUser(view.getContext(), mUser, "game_screen");
            }
            lastClickMillisResult = now;
        }
    }
}
