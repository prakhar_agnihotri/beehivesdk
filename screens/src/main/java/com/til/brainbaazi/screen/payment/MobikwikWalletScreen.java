package com.til.brainbaazi.screen.payment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.entity.strings.OtpStrings;
import com.til.brainbaazi.entity.strings.PaymentStrings;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.payment.MobikiwikWalletModel;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by prashant.rathore on 09/03/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class MobikwikWalletScreen extends BaseScreen<MobikiwikWalletModel> {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.enterEmailText)
    TextView enterEmailText;

    @BindView(R2.id.enterCodeText)
    TextView enterCodeText;

    @BindView(R2.id.email_layout)
    View cashout_mobiwiki_email;

    @BindView(R2.id.otp_layout)
    View cashout_mobiwiki_otp;

    @BindView(R2.id.etEmail)
    EditText etEmail;

    @BindView(R2.id.nextButtonotp)
    TextView nextButtonotp;

    @BindView(R2.id.phoneNumberEV)
    EditText phoneNumberEV;

    @BindView(R2.id.verifyButton)
    Button verifyButton;

    @BindView(R2.id.phoneTV)
    TextView phoneTV;

    @BindView(R2.id.timerTV)
    TextView timerTV;

    @BindView(R2.id.otpStatusTV)
    TextView otpStatusTV;

    @BindView(R2.id.nextButton)
    TextView nextButtonEmail;

    @BindView(R2.id.ivDisabled)
    View ivDisabled;

    @OnClick(R2.id.nextButton)
    void nextButtonEmailClicked() {
        if (isValidEmail(etEmail.getText().toString())) {
            email = etEmail.getText().toString();
            getViewModel().generateOTP();
        }
    }

    @OnClick(R2.id.nextButtonotp)
    void nextButtonOTPClicked() {
        if (!TextUtils.isEmpty(phoneNumberEV.getText().toString()))
            getViewModel().verifyOTP(email, phoneNumberEV.getText().toString());
    }

    @OnClick(R2.id.timerTV)
    void timerTextViewClicked() {
        phoneNumberEV.setText("");
        getViewModel().regenerateOTP();
    }

    private int timer = 30;
    private String email;

    private ProgressDialog progressDialog;
    private String resendOTPInText, resentCodeText, codeSentText;

    public MobikwikWalletScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.cashout_mobiwiki_create_screen, viewGroup, false);
    }

    @Override
    protected void onBind(MobikiwikWalletModel viewModel) {
        toolbar.setTitle(getBrainBaaziStrings().paymentStrings().cashoutText());
        setNavigationIcon(toolbar, R.drawable.arrow_back);

        OtpStrings otpStrings = getBrainBaaziStrings().otpStrings();
        resendOTPInText = otpStrings.resendOTPInText();
        resentCodeText = otpStrings.resentCodeText();
        codeSentText = otpStrings.codeSentText();

        enterEmailText.setText(getBrainBaaziStrings().paymentStrings().enterEmailText());
        enterCodeText.setText(otpStrings.enterOtpText());
        verifyButton.setText(otpStrings.verifyingText());

        addTextWatcherForEmail();
        initListenersForOTP();
        observerViewState(viewModel);
        observePaymentResponse(viewModel);
    }

    private void observePaymentResponse(MobikiwikWalletModel viewModel) {
        DisposableOnNextObserver<PaymentResponseMessage> disposableOnNextObserver = new DisposableOnNextObserver<PaymentResponseMessage>() {
            @Override
            public void onNext(PaymentResponseMessage paymentResponseMessage) {
                if (paymentResponseMessage.getRequestType() == 2) {
                    handleRegenerateOTP(paymentResponseMessage);
                } else if (paymentResponseMessage.getRequestType() == 1) {
                    handleGenerateOtp(paymentResponseMessage);
                }
            }
        };
        addDisposable(disposableOnNextObserver);
        viewModel.observerPaymentResponse().subscribe(disposableOnNextObserver);
    }

    private void handleGenerateOtp(PaymentResponseMessage paymentResponseMessage) {
        if (paymentResponseMessage.isRequestSuccess()) {
            if (paymentResponseMessage.getStatusCode().equals("0")) {
                getViewModel().openSuccessScreen();
            }
        } else {
            if ("164".equals(paymentResponseMessage.getStatusCode())) {
                showErrorDialog("The 6 digit code you entered is incorrect. Please try again.", paymentResponseMessage.getStatusCode());
            } else {
                showErrorDialog(paymentResponseMessage.getStatusDescription(), paymentResponseMessage.getStatusCode());
            }
        }
    }

    private void handleRegenerateOTP(PaymentResponseMessage paymentResponseMessage) {
        try {
            if (paymentResponseMessage.isRequestSuccess()) {
                if (paymentResponseMessage.getStatusCode().equals("0")) {
                    timerTV.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorBlack));
                    timerTV.setOnClickListener(null);
                    otpStatusTV.setText(codeSentText);
                    otpStatusTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check, 0, 0, 0);
                    otpStatusTV.setVisibility(View.VISIBLE);
                    countDownTimer.start();
                }
            } else {
                if (paymentResponseMessage.getStatusCode() != null) {
                    Toast.makeText(getContext(), paymentResponseMessage.getStatusDescription(), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            String message = getBrainBaaziStrings().paymentStrings().unableToProcess_requestText();
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    private void observerViewState(MobikiwikWalletModel viewModel) {
        Disposable subscribe = viewModel.observeViewState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                switch (integer) {
                    case MobikiwikWalletModel.STATE_LOADING:
                        showProgressLoader();
                        break;
                    case MobikiwikWalletModel.STATE_REMOVE_LOADING:
                        hideProgressDialog();
                        break;
                    case MobikiwikWalletModel.STATE_SHOW_OTP_SCREEN:
                        switchLayouts();
                        break;
                    case MobikiwikWalletModel.STATE_SHOW_ERROR_TOAST:
                        PaymentStrings brainBaaziStrings = getBrainBaaziStrings().paymentStrings();
                        String message = brainBaaziStrings.unableToProcess_requestText();
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        break;
                    case MobikiwikWalletModel.STATE_SHOW_EMAIL_SCREEN:
                        try {
                            timer = 30;
                            countDownTimer.cancel();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        switchLayouts();
                        break;
                }
            }
        });
        addDisposable(subscribe);
    }

    @Override
    protected void onUnBind() {
        hideProgressDialog();
        Utils.hideKeyboard(getContext());
    }

    private void showProgressLoader() {
        try {
            String message = getBrainBaaziStrings().paymentStrings().processRequestText();
            progressDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(true); // disable dismiss by tapping outside of the dialog
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addTextWatcherForEmail() {
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (isValidEmail(etEmail.getText().toString())) {
                    nextButtonEmail.setEnabled(true);
                    nextButtonEmail.setAlpha(1.0f);
                } else {
                    nextButtonEmail.setEnabled(false);
                    nextButtonEmail.setAlpha(0.5f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    nextButtonEmail.performClick();
                    return true;
                }
                return false;
            }
        });
    }

    private void switchLayouts() {
        getViewModel().setForOtpScreen(!getViewModel().isForOtpScreen());
        if (getViewModel().isForOtpScreen()) {
            cashout_mobiwiki_email.setVisibility(View.GONE);
            cashout_mobiwiki_otp.setVisibility(View.VISIBLE);
            inflateOTPdata();
            countDownTimer.start();
            updateTimerText();

            phoneNumberEV.postDelayed(new Runnable() {

                public void run() {
                    phoneNumberEV.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                    phoneNumberEV.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
                }
            }, 200);
        } else {
            phoneNumberEV.setText("");
            cashout_mobiwiki_email.setVisibility(View.VISIBLE);
            cashout_mobiwiki_otp.setVisibility(View.GONE);
        }
    }

    private void inflateOTPdata() {
        phoneTV.setText(getViewModel().getPhoneNumber());
        phoneNumberEV.setHint(getString(R.string.cashout_enter_otp_text));
        verifyButton.setText(getString(R.string.cashout_verify_otp_text));
    }

    private void initListenersForOTP() {
        phoneNumberEV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    nextButtonOTPClicked();
                    return true;
                }
                return false;
            }
        });

        phoneNumberEV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("Arpit", "charSequence: " + charSequence.length());
                if (charSequence.length() == 6) {
                    nextButtonotp.setEnabled(true);
                    ivDisabled.setVisibility(View.GONE);
                } else {
                    nextButtonotp.setEnabled(false);
                    ivDisabled.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getViewModel().isForOtpScreen()) {
            try {
                countDownTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            switchLayouts();
        } else {
            getViewModel().getActivityInteractor().performBackPress();
        }
    }

    private boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private CountDownTimer countDownTimer = new CountDownTimer(30000, 1000) {

        public void onTick(long millisUntilFinished) {
            timer--;
            updateTimerText();
        }

        public void onFinish() {
            try {
                timerTV.setText(resentCodeText);
                otpStatusTV.setVisibility(View.GONE);

                timerTV.setTextColor(ContextCompat.getColor(getContext(), R.color.bbcolorAccent));
                timerTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        timer = 30;
                        timerTextViewClicked();
                    }
                });
            } catch (Exception e) {
                countDownTimer.cancel();
            }
        }

    };

    private void updateTimerText() {
        String digit = timer <= 9 ? "0" + timer : String.valueOf(timer);
        String timerText = resendOTPInText + " " + digit;
        timerTV.setText(timerText);
    }

    private void showErrorDialog(String message, final String statusCode) {
        final Dialog errorDialog = new Dialog(getContext(), android.R.style.Theme_Light_NoTitleBar);
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.setCancelable(true);
        errorDialog.setContentView(R.layout.cashout_dialog_incorrect_otp);

        errorDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.HSVToColor(100, new float[]{0f, 0f, 0f})));

        TextView tvTitle = errorDialog.findViewById(R.id.tvTitle);
        TextView tvMessage = errorDialog.findViewById(R.id.tvMessage);
        Button btnOk = errorDialog.findViewById(R.id.btnOk);

        tvMessage.setText(message);
        PaymentStrings paymentStrings = getBrainBaaziStrings().paymentStrings();
        if (statusCode.equals("152")) {
            String titleMessage = paymentStrings.invalidEmailText();
            tvTitle.setText(titleMessage);
        } else {
            String titleMessage = getBrainBaaziStrings().otpStrings().inCorrectOTPText();
            tvTitle.setText(titleMessage);
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (statusCode.equals("152")) {
                    errorDialog.dismiss();
                    onBackPressed();
                } else {
                    errorDialog.dismiss();
                }
            }
        });
        errorDialog.show();
    }
}
