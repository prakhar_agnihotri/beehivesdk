package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.brainbaazi.component.Analytics;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class LateDialog extends BaseDialog {
    private final Analytics analytics;
    private final String gameID;
    private final int question;
    private final User mUser;

    public LateDialog(@NonNull Context context, @NonNull User user, Analytics analytics, String gameID, int question) {
        super(context);
        this.mUser = user;
        this.analytics = analytics;
        this.gameID = gameID;
        this.question = question;
        setDelayDismiss(10000);
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_you_are_late;
    }

    @Override
    public void inflateDialogView() {

        CustomFontTextView youWonHead = findViewById(R.id.youWonHead);
        CustomFontTextView shareText = findViewById(R.id.shareText);
        Button btn_continue = findViewById(R.id.btn_continue);

        GameplayStrings brainBaaziStrings = getBrainBaaziStrings().gameplayStrings();
        youWonHead.setText(brainBaaziStrings.youAreLateTitle());
        shareText.setText(brainBaaziStrings.youAreLateTitleMsg());
        btn_continue.setText(brainBaaziStrings.continueText());

        findViewById(R.id.closeButton).setOnClickListener(this);
        btn_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.closeButton) {
            dismiss();
        } else if (id == R.id.btn_continue) {
            Map<String, Object> data = new HashMap<>();
            data.put("username", mUser.getUserName());
            data.put("Phone", mUser.getPhoneNumber());
            data.put("game_id", gameID);
            data.put("Event Time", analytics.getTimeStampInHHMMSSIST());
            analytics.cleverTapEvent("late_continue_watch_event", data);

            dismiss();
        }
    }
}
