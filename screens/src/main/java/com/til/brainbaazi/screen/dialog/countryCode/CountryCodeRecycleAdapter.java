package com.til.brainbaazi.screen.dialog.countryCode;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.til.brainbaazi.entity.otp.CountryModel;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.dialog.countryCode.ISDSelectionDialog.OnCountrySelectionListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class CountryCodeRecycleAdapter extends RecyclerView.Adapter<CountryCodeRecycleAdapter.CountryCodeViewHolder> {

    private List<CountryModel> countryModelList = new ArrayList<>();
    private OnCountrySelectionListener onCountrySelectionListener;

    public CountryCodeRecycleAdapter(OnCountrySelectionListener countrySelectionListener) {
        this.onCountrySelectionListener = countrySelectionListener;
    }

    @Override
    public CountryCodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_isd_codelist, parent, false);
        return new CountryCodeViewHolder(itemView);
    }

    public void setCountryModelList(List<CountryModel> countryModelList) {
        this.countryModelList = countryModelList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(CountryCodeViewHolder holder, int position) {
        CountryModel countryModel = countryModelList.get(position);
        holder.countryCodeTV.setText(countryModel.getName() + "(" + countryModel.getDialCode() + ")");

        holder.ll_parent.setTag(countryModel);
        holder.ll_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountryModel model = (CountryModel) v.getTag();
                if (model != null && onCountrySelectionListener != null) {
                    onCountrySelectionListener.onCountrySelected(model);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return countryModelList != null ? countryModelList.size() : 0;
    }

    public static class CountryCodeViewHolder extends RecyclerView.ViewHolder {
        private final View ll_parent;
        private final CustomFontTextView countryCodeTV;

        public CountryCodeViewHolder(View view) {
            super(view);
            ll_parent = view.findViewById(R.id.ll_parent);
            countryCodeTV = view.findViewById(R.id.countryCodeTV);
        }
    }
}
