package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.til.brainbaazi.entity.strings.DashboardStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

/**
 * Created by saurabh.garg on 08/03/18.
 */

public class AddReferralSuccessDialog extends BaseDialog {

    private final DashboardStrings dashboardStrings;

    public AddReferralSuccessDialog(@NonNull Context context, @NonNull DashboardStrings dashboardStrings) {
        super(context);
        this.dashboardStrings = dashboardStrings;
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_add_referral_success;
    }

    @Override
    public void inflateDialogView() {
        CustomFontTextView tv_referral_applied = findViewById(R.id.tv_referral_applied);
        CustomFontTextView tv_got_life = findViewById(R.id.tv_got_life);
        Button btn_done = findViewById(R.id.btn_done);

        tv_referral_applied.setText(dashboardStrings.referralCodeAppliedText());
        tv_got_life.setText(dashboardStrings.referralGotLifeText());
        btn_done.setText(dashboardStrings.doneText());

        btn_done.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_done) {
            dismiss();
        } else {
            super.onClick(view);
        }
    }
}
