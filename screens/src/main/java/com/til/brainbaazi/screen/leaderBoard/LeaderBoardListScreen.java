package com.til.brainbaazi.screen.leaderBoard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardUser;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.leaderBoard.adapters.LeaderBoardAdapter;
import com.til.brainbaazi.screen.recycleHelper.LinearLayoutManagerWithSmoothScroller;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardListViewModel;

import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by saurabh.garg on 2/21/18.
 */

@AutoFactory(implementing = ScreenFactory.class)
public class LeaderBoardListScreen extends BaseScreen<LeaderBoardListViewModel> {

    @BindView(R2.id.list)
    RecyclerView list;

    private LeaderBoardAdapter leaderBoardAdapter;

    public LeaderBoardListScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_leaderboard_list, viewGroup, false);
    }

    @Override
    protected void onBind(LeaderBoardListViewModel viewModel) {
        bindUIData(viewModel);
        observeGameViewState(viewModel);
    }

    private void bindUIData(LeaderBoardListViewModel viewModel) {
        LinearLayoutManagerWithSmoothScroller mLayoutManager = new LinearLayoutManagerWithSmoothScroller(getContext());
        list.setLayoutManager(mLayoutManager);
        leaderBoardAdapter = new LeaderBoardAdapter();
        list.setAdapter(leaderBoardAdapter);
    }

    private void observeGameViewState(LeaderBoardListViewModel viewModel) {
        DisposableObserver<List<LeaderBoardUser>> listModelDisposableObserver = new DisposableOnNextObserver<List<LeaderBoardUser>>() {
            @Override
            public void onNext(List<LeaderBoardUser> users) {
                updateLeaderBoardData(users);
            }
        };
        addDisposable(listModelDisposableObserver);
        viewModel.observeLeaderBoarModel().observeOn(AndroidSchedulers.mainThread()).subscribe(listModelDisposableObserver);
    }

    private void updateLeaderBoardData(List<LeaderBoardUser> items) {
        leaderBoardAdapter.addValues(items, true);
    }

    @Override
    protected void onUnBind() {

    }
}
