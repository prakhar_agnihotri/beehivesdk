package com.til.brainbaazi.screen.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.interactor.activity.ActivityPermissionResult;
import com.til.brainbaazi.interactor.activity.ActivityResult;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.viewmodel.BaseViewModel;

/**
 * Created by prashant.rathore on 23/02/18.
 */

public abstract class ScreenControllerActivity extends Activity {

    private ScreenController screenController;
    private BaseScreen screenView;

    private ActivityInteractorImpl activityInteractor = new ActivityInteractorImpl() {

        @Override
        public void requestPermission(String[] permissions, int requestCode) {
            ActivityCompat.requestPermissions(ScreenControllerActivity.this, permissions, requestCode);
        }

        @Override
        public void startActivityForResult(Intent intent, int requestCode) {
            ScreenControllerActivity.this.startActivityForResult(intent, requestCode);
        }

        @Override
        public void performBackPress() {
            ScreenControllerActivity.this.onBackPressed();
        }
    };

    private Handler mHandler = new Handler();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        SegmentInfo segmentInfo = savedInstanceState == null ? (SegmentInfo) getIntent().getParcelableExtra("SEGMENT_INFO") : (SegmentInfo) savedInstanceState.getParcelable("SEGMENT_INFO");
        segmentInfo = segmentInfo == null ? provideDefaultScreenInfo() : segmentInfo;
        this.screenController = provideController(segmentInfo);
        this.screenController.attach(this, LayoutInflater.from(this));
        super.onCreate(savedInstanceState);
        screenController.onCreate();
        screenView = screenController.createView(null);
        changeView(screenView.getView(), null);
        screenController.bindView(screenView);
        disableScreenReading();
    }

    private void disableScreenReading() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            if (window != null && window.getDecorView() != null) {
                window.getDecorView().setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS);
            }
        }
    }

    protected abstract SegmentInfo provideDefaultScreenInfo();

    protected SegmentInfo changeScreen(SegmentInfo segmentInfo) {
        ScreenController newController = provideController(segmentInfo);
        newController.attach(this, LayoutInflater.from(this));
        final ScreenController oldController = this.screenController;
        final BaseScreen newScreen = newController.createView(null);
        switch (oldController.currentState) {
            case ScreenController.STATE_CREATE:
                newController.onCreate();
                newController.bindView(newScreen);
                break;
            case ScreenController.STATE_START:
                newController.onCreate();
                newController.bindView(newScreen);
                newController.onStart();
                break;
            case ScreenController.STATE_RESUME:
                oldController.onPause();
                newController.onCreate();
                newController.bindView(newScreen);
                newController.onStart();
                newController.onResume();
                break;
            case ScreenController.STATE_PAUSE:
                newController.onCreate();
                newController.bindView(newScreen);
                newController.onStart();
                break;
            case ScreenController.STATE_STOP:
                newController.onCreate();
                newController.bindView(newScreen);
                break;
            case ScreenController.STATE_DESTROY:
                return segmentInfo;

        }
        changeView(newScreen.getView(), new Runnable() {

            @Override
            public void run() {
                switch (oldController.currentState) {
                    case ScreenController.STATE_CREATE:
                        oldController.unBindView();
                        oldController.onDestroy();
                        break;
                    case ScreenController.STATE_PAUSE:
                    case ScreenController.STATE_RESUME:
                    case ScreenController.STATE_START:
                        oldController.onStop();
                        oldController.unBindView();
                        oldController.onDestroy();
                        break;
                    case ScreenController.STATE_STOP:
                        oldController.unBindView();
                        oldController.onDestroy();
                        break;
                    case ScreenController.STATE_DESTROY:
                        return;
                }
            }
        });
        this.screenController = newController;
        this.screenView = newScreen;
        return oldController.getSegmentInfo();
    }

    protected void changeView(View newView, Runnable onCompleHandler) {
        setContentView(newView);
        if (onCompleHandler != null) {
            mHandler.post(onCompleHandler);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        screenController.onStart();
    }

    @Override
    public void onResume() {
        screenController.onResume();
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ActivityPermissionResult permissionResult = ActivityPermissionResult.builder()
                .setPermissions(permissions)
                .setRequestCode(requestCode)
                .setGrantResult(grantResults).build();

        activityInteractor.publisPermissionResult(permissionResult);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ActivityResult activityResult = ActivityResult.builder().setRequestCode(requestCode).setResultCode(resultCode).setData(data).build();
        activityInteractor.publishActivityResult(activityResult);
    }

    @Override
    public void onPause() {
        screenController.onPause();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("SEGMENT_INFO",screenController.getSegmentInfo());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        screenController.onStop();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (screenController == null || !(screenController.handleBackPressed() || getScreenNavigation().popBackStack())) {
            try {
                super.onBackPressed();
            }
            catch (Exception e) {
                finish();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        screenController.onDestroy();
        this.screenController.unBindView();
        this.screenView = null;
        super.onDestroy();
    }

    protected abstract ScreenController<? extends BaseViewModel> provideController(SegmentInfo segmentInfo);

    protected abstract ScreenNavigation getScreenNavigation();

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }
}
