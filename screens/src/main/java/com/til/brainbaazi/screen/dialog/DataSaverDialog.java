package com.til.brainbaazi.screen.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import com.brainbaazi.component.Analytics;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.gamePlay.LiveGamePlayScreen.StreamModeCallback;
import com.til.slikeplayer.slikestream.StreamingMode;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class DataSaverDialog extends BaseDialog {
    private final Analytics analytics;
    private final float posX;
    private final StreamModeCallback streamModeCallback;
    private StreamingMode streamingMode, recommandedStreamingMode;

    private View llNormalMode;
    private CustomFontTextView tvNormalMode;

    private View llDataSaverMode;
    private ImageView ivSaverBig;
    private CustomFontTextView tvDataSaverMode;

    private View llTextMode;
    private ImageView ivTextBig;
    private CustomFontTextView tvTextMode;

    private View llMore;
    private CustomFontTextView tvLearnMore;
    private CustomFontTextView tvRecommended;

    public DataSaverDialog(@NonNull Context context, Analytics analytics, View anchorView, StreamModeCallback streamModeCallback) {
        super(context);
        this.analytics = analytics;
        this.posX = anchorView.getX();
        this.streamModeCallback = streamModeCallback;
    }

    public void setStreamingMode(StreamingMode streamingMode, StreamingMode recommandedStreamingMode) {
        this.streamingMode = streamingMode;
        this.recommandedStreamingMode = recommandedStreamingMode;
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_data_saver;
    }

    @Override
    public void inflateDialogView() {
        final View ivArrow = findViewById(R.id.ivArrow);
        ivArrow.post(new Runnable() {
            @Override
            public void run() {
                ivArrow.setX(posX);
            }
        });

        View ivClose = findViewById(R.id.ivClose);
        ivClose.setOnClickListener(this);

        llNormalMode = findViewById(R.id.llNormalMode);
        tvNormalMode = findViewById(R.id.tvNormalMode);

        llDataSaverMode = findViewById(R.id.llDataSaverMode);
        ivSaverBig = findViewById(R.id.ivSaverBig);
        tvDataSaverMode = findViewById(R.id.tvDataSaverMode);

        llTextMode = findViewById(R.id.llTextMode);
        ivTextBig = findViewById(R.id.ivTextBig);
        tvTextMode = findViewById(R.id.tvTextMode);

        llMore = findViewById(R.id.llMore);
        tvLearnMore = findViewById(R.id.tvLearnMore);
        tvRecommended = findViewById(R.id.tvRecommended);

        llMore.setVisibility(View.GONE);
        tvLearnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvLearnMore.setVisibility(View.GONE);
                llMore.setVisibility(View.VISIBLE);
            }
        });

        llNormalMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedMode(StreamingMode.DEFAULT_MODE, true);
            }
        });

        llDataSaverMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedMode(StreamingMode.DATA_SAVER_MODE, true);
            }
        });

        llTextMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedMode(StreamingMode.DIGI_ONLY, true);
            }
        });

        setCustomTextViews();
        setLearnMoreImageDrawables();
        setSelectedMode(streamingMode, false);
        setRecommendedMode();

        if (getWindow() != null) {
            getWindow().setGravity(Gravity.TOP);
        }
        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    private void setCustomTextViews() {
        GameplayStrings gameplayStrings = getBrainBaaziStrings().gameplayStrings();

        CustomFontTextView tv_normalMode = findViewById(R.id.tv_normalMode);
        CustomFontTextView tv_normalModeDesc = findViewById(R.id.tv_normalModeDesc);

        CustomFontTextView tv_dataSaverMode = findViewById(R.id.tv_dataSaverMode);
        CustomFontTextView tv_dataSaverModeDesc = findViewById(R.id.tv_dataSaverModeDesc);

        CustomFontTextView tv_textMode = findViewById(R.id.tv_textMode);
        CustomFontTextView tv_textModeDesc = findViewById(R.id.tv_textModeDesc);

        CustomFontTextView tv_streaming_mode = findViewById(R.id.tv_streaming_mode);

        tvNormalMode.setText(gameplayStrings.streamNormalText());
        tv_normalMode.setText(gameplayStrings.streamNormalModeText());
        tv_normalModeDesc.setText(gameplayStrings.streamNormalModeDescText());

        tvDataSaverMode.setText(gameplayStrings.streamDataSaveText());
        tv_dataSaverMode.setText(gameplayStrings.streamDataSaveModeText());
        tv_dataSaverModeDesc.setText(gameplayStrings.streamDataSaveModeDescText());

        tvTextMode.setText(gameplayStrings.streamTextOnlyText());
        tv_textMode.setText(gameplayStrings.streamTextOnlyModeText());
        tv_textModeDesc.setText(gameplayStrings.streamTextOnlyModeDescText());

        tv_streaming_mode.setText(gameplayStrings.dialogStreamingModeText());
        tvLearnMore.setText(gameplayStrings.streamLearnMoreText());
        tvRecommended.setText(gameplayStrings.streamRecommandedText());
    }

    private void setRecommendedMode() {
        tvRecommended.setVisibility(View.GONE);
        if (streamingMode != recommandedStreamingMode) {
            switch (recommandedStreamingMode) {
                case DIGI_ONLY:
                    tvRecommended.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    private void setSelectedMode(final StreamingMode tmpStreamingMode, boolean isUserSelected) {
        switch (tmpStreamingMode) {
            case DEFAULT_MODE:
                setNormalMode();
                break;
            case DATA_SAVER_MODE:
                setDataSaverMode();
                break;
            case DIGI_ONLY:
                setTextMode();
                break;
        }
        if (isUserSelected) {
            handleStreamModeSelection(tmpStreamingMode);
            //TODO event
            //CleverTapHelper.cleverTapStreamModeChangeEvent(mUser, tmpStreamingMode.toString(), false, false);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            }, 500);
        }
    }

    private void setNormalMode() {
        llNormalMode.setSelected(true);
        llDataSaverMode.setSelected(false);
        llTextMode.setSelected(false);

        ivSaverBig.setImageResource(R.drawable.saver_mode_big);
        ivTextBig.setImageResource(R.drawable.text_mode_big);

        tvNormalMode.setTextColor(Color.WHITE);
        tvDataSaverMode.setTextColor(Color.BLACK);
        tvTextMode.setTextColor(Color.BLACK);
    }

    private void setDataSaverMode() {
        llNormalMode.setSelected(false);
        llDataSaverMode.setSelected(true);
        llTextMode.setSelected(false);

        ivSaverBig.setImageResource(R.drawable.saver_mode_big_selected);
        ivTextBig.setImageResource(R.drawable.text_mode_big);

        tvNormalMode.setTextColor(Color.BLACK);
        tvDataSaverMode.setTextColor(Color.WHITE);
        tvTextMode.setTextColor(Color.BLACK);
    }

    private void setTextMode() {
        llNormalMode.setSelected(false);
        llDataSaverMode.setSelected(false);
        llTextMode.setSelected(true);

        ivSaverBig.setImageResource(R.drawable.saver_mode_big);
        ivTextBig.setImageResource(R.drawable.text_mode_big_selected);

        tvNormalMode.setTextColor(Color.BLACK);
        tvDataSaverMode.setTextColor(Color.BLACK);
        tvTextMode.setTextColor(Color.WHITE);
    }

    private void handleStreamModeSelection(StreamingMode streamingMode) {
        if (streamModeCallback != null) {
            streamModeCallback.onStreamStateChange(streamingMode);
        }
    }

    private void setLearnMoreImageDrawables() {
        int color = ContextCompat.getColor(getContext(), R.color.bbcolor_8a000000);

        Drawable normalIcon = ContextCompat.getDrawable(getContext(), R.drawable.normal_mode).mutate();
        normalIcon.setColorFilter(color, PorterDuff.Mode.MULTIPLY);

        Drawable dataSaverIcon = ContextCompat.getDrawable(getContext(), R.drawable.saver_mode).mutate();
        dataSaverIcon.setColorFilter(color, PorterDuff.Mode.MULTIPLY);

        Drawable textOnlyIcon = ContextCompat.getDrawable(getContext(), R.drawable.text_mode).mutate();
        textOnlyIcon.setColorFilter(color, PorterDuff.Mode.MULTIPLY);

        ((ImageView) findViewById(R.id.ivNormalMode)).setImageDrawable(normalIcon);
        ((ImageView) findViewById(R.id.ivDataSaverMode)).setImageDrawable(dataSaverIcon);
        ((ImageView) findViewById(R.id.ivTextMode)).setImageDrawable(textOnlyIcon);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivClose) {
            dismiss();
        }
    }
}
