package com.til.brainbaazi.screen.other;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.viewmodel.other.WebViewModel;

import butterknife.BindView;
import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 2/21/18.
 */

@AutoFactory(implementing = ScreenFactory.class)
public class WebViewScreen extends BaseScreen<WebViewModel> {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.webView)
    WebView webView;

    public WebViewScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_webview, viewGroup, false);
    }

    @Override
    protected void onBind(WebViewModel viewModel) {
        updateUI(viewModel.getWebData());
    }

    private void updateUI(WebData webData) {
        setNavigationIcon(toolbar, R.drawable.ic_action_cancel);
        customzeWebViewSettings();
        loadWebData(webData.getTitle(), webData.getWebUrl());
    }

    private void loadWebData(String title, String url) {
        toolbar.setTitle(title);
        webView.loadUrl(url);
    }

    private void customzeWebViewSettings() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return handleUrl(url) || super.shouldOverrideUrlLoading(view, url);
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                final Uri uri = request.getUrl();
                return handleUrl(uri.toString()) || super.shouldOverrideUrlLoading(view, request);
            }
        });
    }

    private boolean handleUrl(String url) {
        if (getContext().getString(R.string.url_how_play).equalsIgnoreCase(url)) {
            loadWebData(getString(R.string.how_to_play), url);
            return true;
        } else if (getContext().getString(R.string.url_faq).equalsIgnoreCase(url)) {
            loadWebData(getString(R.string.faqs), url);
            return true;
        } else if (getContext().getString(R.string.url_terms).equalsIgnoreCase(url)) {
            loadWebData(getString(R.string.terms_of_use), url);
            return true;
        } else if (getContext().getString(R.string.url_privacy).equalsIgnoreCase(url)) {
            loadWebData(getString(R.string.privacy), url);
            return true;
        } else if (getContext().getString(R.string.url_rules).equalsIgnoreCase(url)) {
            loadWebData(getString(R.string.rules), url);
            return true;
        }
        return false;
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getActivityInteractor().performBackPress();
    }

    @Override
    protected void onUnBind() {

    }
}
