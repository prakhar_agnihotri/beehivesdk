package com.til.brainbaazi.screen.customViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.brainbaazi.component.network.ImageViewInteractor;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CircleImageView;
import com.til.brainbaazi.screen.utils.Utils;

import java.util.ArrayList;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class WinCountFrameLayout extends FrameLayout {
    private View centerImageView;

    private ArrayList<ImageView> surroundingViews;
    private int viewCount = 11;
    private final int CENTER_IMAGE_DIMEN = 80;
    private final int SURROUNDING_IMAGE_DIMEN = 20;
    private final int VIEW_MARGIN = 5;

    public WinCountFrameLayout(@NonNull Context context) {
        super(context);
    }

    public WinCountFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WinCountFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        surroundingViews = new ArrayList<>();

        centerImageView = View.inflate(getContext(), R.layout.widget_network_circleimageview, null);
        if (centerImageView instanceof ImageView) {
            ((ImageView) centerImageView).setImageResource(R.drawable.ic_user_icon);
        }

        FrameLayout.LayoutParams lp = new LayoutParams(Utils.dpToPx(CENTER_IMAGE_DIMEN), Utils.dpToPx(CENTER_IMAGE_DIMEN));
        centerImageView.setLayoutParams(lp);

        addView(centerImageView);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        int leftPoint, topPoint, left, top;

        double eachAngle = getEachArcAngleInDegrees();

        centerImageView.setX(getWidth() / 2 - Utils.dpToPx(CENTER_IMAGE_DIMEN) / 2);
        centerImageView.setY(getHeight() / 2 - Utils.dpToPx(CENTER_IMAGE_DIMEN) / 2);

        for (int i = 0; i < surroundingViews.size(); i++) {
            double totalAngleForChild = eachAngle * (i);
            leftPoint = (int) ((3 * centerImageView.getWidth()) / 4 * Math.cos(Math.toRadians(totalAngleForChild)));
            topPoint = (int) ((3 * centerImageView.getWidth()) / 4 * Math.sin(Math.toRadians(totalAngleForChild)));

            left = getWidth() / 2 - leftPoint - Utils.dpToPx(SURROUNDING_IMAGE_DIMEN) / 2;
            top = getHeight() / 2 - topPoint - Utils.dpToPx(SURROUNDING_IMAGE_DIMEN) / 2;

            surroundingViews.get(i).layout(left, top, left + Utils.dpToPx(SURROUNDING_IMAGE_DIMEN), top + Utils.dpToPx(SURROUNDING_IMAGE_DIMEN));
        }
    }

    public void setWinCount(int winCount) {
        for (int i = 0; i < winCount; i++) {
            surroundingViews.get(i + 1).setBackgroundResource(R.drawable.tick_selected);
            surroundingViews.get(i + 1).setImageResource(R.drawable.check_white);
        }
    }

    public void setViewCount(int count) {
        //TODO live image views
        viewCount = count;

        for (int i = 0; i <= viewCount; i++) {
            CircleImageView view = new CircleImageView(getContext());
            view.setBackgroundResource(R.drawable.tick_unselected);

            FrameLayout.LayoutParams viewLp = new LayoutParams(Utils.dpToPx(SURROUNDING_IMAGE_DIMEN), Utils.dpToPx(SURROUNDING_IMAGE_DIMEN));
            viewLp.setMargins(Utils.dpToPx(VIEW_MARGIN), Utils.dpToPx(VIEW_MARGIN), Utils.dpToPx(VIEW_MARGIN), Utils.dpToPx(VIEW_MARGIN));
            view.setLayoutParams(viewLp);
            view.setPadding(Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2));

            addView(view);

            surroundingViews.add(view);
        }
    }

    public void setUserImage(String url) {
        if (centerImageView instanceof ImageViewInteractor) {
            ((ImageViewInteractor) centerImageView).setDefault(R.drawable.ic_user_icon);
            ((ImageViewInteractor) centerImageView).setImageUrl(url);
        } else if (centerImageView instanceof ImageView) {
            ((ImageView) centerImageView).setImageResource(R.drawable.ic_user_icon);
        }
    }

    private double getEachArcAngleInDegrees() {
        return 360 / ((double) viewCount);
    }
}
