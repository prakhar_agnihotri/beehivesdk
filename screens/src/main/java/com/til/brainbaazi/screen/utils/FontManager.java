package com.til.brainbaazi.screen.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.text.TextUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saurabh.garg on 2/15/18.
 */

public class FontManager {
    private static FontManager instance;
    private File fontsDir;
    private AssetManager assetManager;
    private Map<String, Typeface> typefaceMap;

    public static final String TYPEFACE_Folder = "font";
    public static final String TYPEFACE_EXTENSION = ".otf";

    private FontManager(Context context) {
        this.typefaceMap = new HashMap<>();
        this.fontsDir = context.getDir(TYPEFACE_Folder, Context.MODE_PRIVATE);
        this.assetManager = context.getAssets();
    }

    public static FontManager getInstance(Context context) {
        if (instance == null) {
            synchronized (FontManager.class) {
                if (instance == null) {
                    instance = new FontManager(context);
                }
            }
        }
        return instance;
    }

    public Typeface getTypeFace(String name, Typeface def) {
        Typeface typeface;
        if (TextUtils.isEmpty(name)) {
            return def;
        }
        if (this.typefaceMap.containsKey(name)) {
            typeface = typefaceMap.get(name);
        } else {
            typeface = getTypeFaceInternalStorage(name);
            if (typeface == null) {
                typeface = getTypeFaceAssets(name);
            }
            typefaceMap.put(name, typeface);
        }
        if (typeface == null) {
            typeface = Typeface.DEFAULT;
        }
        return typeface;
    }

    private Typeface getTypeFaceInternalStorage(String name) {
        Typeface typeface = null;
        File fontTile = new File(this.fontsDir, name + ".ttf");
        if (fontTile.exists()) {
            typeface = Typeface.createFromFile(fontTile);
        }
        return typeface;
    }

    private Typeface getTypeFaceAssets(String name) {
        try {
            return Typeface.createFromAsset(this.assetManager, TYPEFACE_Folder + "/" + name + TYPEFACE_EXTENSION);
        } catch (Exception e) {
            return null;
        }
    }
}
