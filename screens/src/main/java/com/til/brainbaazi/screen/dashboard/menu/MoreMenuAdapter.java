package com.til.brainbaazi.screen.dashboard.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.til.brainbaazi.entity.strings.MenuStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saurabh.garg on 08/03/18.
 */
public class MoreMenuAdapter extends BaseAdapter {

    public static final int MENU_ADD_PICTURE = 1;
    public static final int MENU_CHANGE_PICTURE = 2;
    public static final int MENU_REFERRAL_CODE = 3;
    public static final int MENU_HOW_TO_PLAY = 4;
    public static final int MENU_FAQ = 5;
    public static final int MENU_TERMS = 6;
    public static final int MENU_PRIVACY = 7;
    public static final int MENU_RULES = 8;
    public static final int MENU_RATE_US = 9;
    public static final int MENU_SHARE_APP = 10;
    public static final int MENU_SIGN_OUT = 11;
    public static final int MENU_MORE_OPTIONS = 12;

    private final int layout;
    private final int textViewId;
    private final int imageViewId;
    private LayoutInflater inflater;
    private final MenuStrings menuStrings;

    private List<MoreMenuItem> menuItems = new ArrayList<>();

    public MoreMenuAdapter(Context context, MenuStrings menuStrings) {
        this.inflater = LayoutInflater.from(context);
        this.layout = R.layout.dialog_menu_item;
        this.imageViewId = android.R.id.icon;
        this.textViewId = android.R.id.title;
        this.menuStrings = menuStrings;
    }

    public void add(MoreMenuItem moreMenuItem) {
        this.menuItems.add(moreMenuItem);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }
        MoreMenuItem item = getItem(position);
        ImageView imageView = convertView.findViewById(imageViewId);
        if (item.icon > 0) {
            imageView.setImageResource(item.icon);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.INVISIBLE);
        }
        ((CustomFontTextView) convertView.findViewById(textViewId)).setText(getTitle(item.menuId));
        return convertView;
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }

    @Override
    public MoreMenuItem getItem(int position) {
        return menuItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return menuItems.get(position).menuId;
    }

    public static class MoreMenuItem {
        private int menuId;
        private int icon;

        public MoreMenuItem(int menuId, int icon) {
            this.menuId = menuId;
            this.icon = icon;
        }
    }

    private String getTitle(int menuItem) {
        String title = null;
        switch (menuItem) {
            case MENU_ADD_PICTURE:
                title = menuStrings.addPictureText();
                break;
            case MENU_CHANGE_PICTURE:
                title = menuStrings.changePictureText();
                break;
            case MENU_REFERRAL_CODE:
                title = menuStrings.referralText();
                break;
            case MENU_HOW_TO_PLAY:
                title = menuStrings.howToPlayText();
                break;
            case MENU_FAQ:
                title = menuStrings.fAQText();
                break;
            case MENU_TERMS:
                title = menuStrings.termsText();
                break;
            case MENU_PRIVACY:
                title = menuStrings.privacyText();
                break;
            case MENU_RULES:
                title = menuStrings.rulesText();
                break;
            case MENU_RATE_US:
                title = menuStrings.rateUsText();
                break;
            case MENU_SHARE_APP:
                title = menuStrings.shareAppText();
                break;
            case MENU_SIGN_OUT:
                title = menuStrings.signOutText();
                break;
            case MENU_MORE_OPTIONS:
                title = menuStrings.moreText();
                break;

        }
        return title;
    }
}
