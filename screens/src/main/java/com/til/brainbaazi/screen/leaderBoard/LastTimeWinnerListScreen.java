package com.til.brainbaazi.screen.leaderBoard;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.leaderBoard.LastGameWinnerResponse;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.leaderBoard.adapters.LastGameWinnerAdapter;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.leaderBoard.LastWinnerListViewModel;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by saurabh.garg on 3/04/18.
 */

@AutoFactory(implementing = ScreenFactory.class)
public class LastTimeWinnerListScreen extends BaseScreen<LastWinnerListViewModel> {

    @BindView(R2.id.bbprogressBar)
    ProgressBar bbprogressBar;

    @BindView(R2.id.timeTV)
    CustomFontTextView timeTV;
    @BindView(R2.id.prizeTV)
    CustomFontTextView prizeTV;
    @BindView(R2.id.numberCount)
    CustomFontTextView numberCount;

    @BindView(R2.id.list)
    RecyclerView list;

    private LastGameWinnerAdapter lastGameWinnerAdapter;
    private long PAGE_SIZE = 0;
    private String prevUrl = "";
    private String nextUrl = "";
    private String lastNextUrl = "";
    private String winnerText, prizeMoneyText;

    public LastTimeWinnerListScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_last_game_winner, viewGroup, false);
    }

    @Override
    protected void onBind(LastWinnerListViewModel viewModel) {
        bindUIViews();
        observeGameViewState(viewModel);
    }

    private void bindUIViews() {
        winnerText = getContext().getString(R.string.winners_label);
        prizeMoneyText = getContext().getString(R.string.prize_money_text);

        bbprogressBar.setVisibility(View.VISIBLE);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        list.setLayoutManager(mLayoutManager);
        lastGameWinnerAdapter = new LastGameWinnerAdapter();
        list.setAdapter(lastGameWinnerAdapter);
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount <= PAGE_SIZE) {
                    if (!TextUtils.isEmpty(nextUrl) && !lastNextUrl.equals(nextUrl)) {
                        lastNextUrl = nextUrl;
                        loadMoreItems(nextUrl);
                    }
                }
            }
        });
    }

    private void observeGameViewState(LastWinnerListViewModel viewModel) {
        DisposableObserver<Response<LastGameWinnerResponse>> listModelDisposableObserver = new DisposableOnNextObserver<Response<LastGameWinnerResponse>>() {
            @Override
            public void onNext(Response<LastGameWinnerResponse> lastGameWinnerResponse) {
                hideProgressBar();
                if (lastGameWinnerResponse != null && lastGameWinnerResponse.success()) {
                    LastGameWinnerResponse response = lastGameWinnerResponse.value();
                    populateOtherFields(response);
                    updateLeaderBoardData(response);
                }
            }

        };
        addDisposable(listModelDisposableObserver);
        viewModel.observableLastGameData().observeOn(AndroidSchedulers.mainThread()).subscribe(listModelDisposableObserver);
    }

    private void loadMoreItems(String url) {
        getViewModel().loadLastGameWinnerData(url);
    }

    private void updateLeaderBoardData(LastGameWinnerResponse lastGameWinnerResponse) {
        bbprogressBar.setVisibility(View.GONE);
        lastGameWinnerAdapter.setAmountPerUser(lastGameWinnerResponse.getAmountPerUser());
        lastGameWinnerAdapter.addValues(lastGameWinnerResponse.getLastGameWinner(), lastGameWinnerResponse.isFirstListResponse());
    }

    private void populateOtherFields(LastGameWinnerResponse lastGameWinnerResponse) {
        PAGE_SIZE = Long.parseLong(lastGameWinnerResponse.getTotalWinner());
        if (!prevUrl.equals(lastGameWinnerResponse.getPreviousUrl())) {
            prevUrl = lastGameWinnerResponse.getPreviousUrl();
        }
        if (!nextUrl.equals(lastGameWinnerResponse.getNextUrl())) {
            nextUrl = lastGameWinnerResponse.getNextUrl();
        }
        if (TextUtils.isEmpty(prevUrl)) {
            String totalWinners = lastGameWinnerResponse.getTotalWinner();
            String date = AppUtils.Epoch2DateString(lastGameWinnerResponse.getDateTime(), "hh:mm a dd MMM yyyy");
            String prizeMoney = lastGameWinnerResponse.getPrizeMoney();

            if (!TextUtils.isEmpty(totalWinners)) {
                numberCount.setText(totalWinners + " " + winnerText);
            }
            if (!TextUtils.isEmpty(date)) {
                timeTV.setText(date);
            }
            if (!TextUtils.isEmpty(prizeMoney)) {
                prizeTV.setText(prizeMoneyText + " : " + getContext().getString(R.string.currencySymbol) + " " + prizeMoney);
            }
        }
    }

    private void hideProgressBar() {
        bbprogressBar.setVisibility(View.GONE);
    }

    @Override
    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        this.winnerText = brainBaaziStrings.leaderboardStrings().winnersText();
        this.prizeMoneyText = brainBaaziStrings.leaderboardStrings().prizeMoneyText();
    }

    @Override
    protected void onUnBind() {

    }
}
