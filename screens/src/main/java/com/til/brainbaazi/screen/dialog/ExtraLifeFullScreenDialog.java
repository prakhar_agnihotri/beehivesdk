package com.til.brainbaazi.screen.dialog;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainbaazi.component.Analytics;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.DashboardStrings;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;

import static android.content.Context.CLIPBOARD_SERVICE;


/**
 * Created by saurabh.garg on 2/21/18.
 */

public class ExtraLifeFullScreenDialog extends Dialog implements View.OnClickListener {

    private User mUser;
    private long lastClickMillisResult;
    private long THRESHOLD_MILLIS = 1500L;
    private Analytics analytics;
    private BrainbaaziStrings brainbaaziStrings;

    public ExtraLifeFullScreenDialog(@NonNull Context context, @NonNull User user, @NonNull Analytics analytics, BrainbaaziStrings brainbaaziStrings) {
        super(context);
        this.mUser = user;
        this.analytics = analytics;
        this.brainbaaziStrings = brainbaaziStrings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.extra_live_full_screen);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        inflateDialogView();
    }

    private void inflateDialogView() {
        findViewById(R.id.copyText).setOnClickListener(this);
        ImageView closeButtonExtraLife = findViewById(R.id.cancel_action);
        TextView youWonHead = findViewById(R.id.title);
        CustomFontTextView tv_message = findViewById(R.id.tv_message);
        CustomFontTextView tv_referral = findViewById(R.id.tv_referral);
        TextView refferalEditText = findViewById(R.id.refferalEditText);

        CustomFontTextView shareButton = findViewById(R.id.shareButton);
        CustomFontTextView lifeCountTV = findViewById(R.id.lifeCountTV);

        if (mUser != null) {
            refferalEditText.setText(mUser.getUserName());
            String life = AppUtils.formatWrap(mUser.getLives());
            lifeCountTV.setText(life);
        }

        if (brainbaaziStrings != null) {
            DashboardStrings strings = brainbaaziStrings.dashboardStrings();
            youWonHead.setText(strings.dialogExtraLivesText());
            tv_message.setText(strings.dialogEarnExtraLifeMessage());
            tv_referral.setText(strings.yourReferralCodeText());
            shareButton.setText(brainbaaziStrings.commonStrings().share());
        }

        closeButtonExtraLife.setOnClickListener(this);
        shareButton.setOnClickListener(this);

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                sendAnalyticsEvent("Extra Lives Closed", "Closed");
            }
        });
    }

    private void sendAnalyticsEvent(String mainEvent, String label) {
        if (mUser == null) {
            return;
        }
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent(mainEvent)
                .setCategory("Dashboard")
                .setAction("Extra Lives")
                .setLabel(label)
                .setUserName(mUser.getUserName())
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        analytics.logFireBaseEvent(gaEventModel);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.cancel_action) {
            dismiss();
        } else if (id == R.id.shareButton) {
            long now = SystemClock.elapsedRealtime();
            if (now - lastClickMillisResult > THRESHOLD_MILLIS) {
                sendAnalyticsEvent("Extra Lives Shared", "Shared");
                Utils.inviteUser(view.getContext(), mUser, "game_screen");
            }
            lastClickMillisResult = now;
        }else if (id == R.id.copyText){
            try {
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Referral Code", mUser.getUserName());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getContext(), "Referral code copied", Toast.LENGTH_SHORT).show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
