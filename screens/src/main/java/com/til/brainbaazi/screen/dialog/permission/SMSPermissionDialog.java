package com.til.brainbaazi.screen.dialog.permission;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.til.brainbaazi.entity.strings.OtpStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.dialog.BaseDialog;
import com.til.brainbaazi.screen.utils.PermissionUtils.OnPermissionListener;

/**
 * Created by saurabh.garg on 11/16/16.
 */

public class SMSPermissionDialog extends BaseDialog implements View.OnClickListener {

    private OnPermissionListener permissionListener;

    public SMSPermissionDialog(@NonNull Context context, OnPermissionListener permissionListener) {
        super(context);
        this.permissionListener = permissionListener;
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_permission;
    }

    @Override
    public void inflateDialogView() {
        CustomFontTextView titleText = findViewById(R.id.sureText);
        CustomFontTextView descriptionText = findViewById(R.id.confirmEmailDescTV);
        CustomFontTextView confirmEmailTV = findViewById(R.id.confirmEmailTV);

        Button confirmBtn = findViewById(R.id.confirmBtn);
        Button cancelBtn = findViewById(R.id.cancelBtn);

        confirmEmailTV.setVisibility(View.GONE);
        OtpStrings brainBaaziStrings = getBrainBaaziStrings().otpStrings();

        titleText.setText(brainBaaziStrings.readSMSTitleText());
        descriptionText.setText(brainBaaziStrings.readSMSMessageText());
        confirmBtn.setText(getBrainBaaziStrings().commonStrings().okayText());
        cancelBtn.setText(getBrainBaaziStrings().commonStrings().cancelText());

        confirmBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.confirmBtn) {
            permissionListener.dialogAllowed();
            dismiss();
        } else if (id == R.id.cancelBtn) {
            permissionListener.dialogDenied();
            dismiss();
        }
    }
}
