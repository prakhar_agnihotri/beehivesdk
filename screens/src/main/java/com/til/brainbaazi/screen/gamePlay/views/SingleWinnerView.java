package com.til.brainbaazi.screen.gamePlay.views;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.brainbaazi.component.network.ImageViewInteractor;
import com.til.brainbaazi.entity.game.event.WinUser;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;

/**
 * Created by saurabh.garg on 2/20/18.
 */

public class SingleWinnerView extends RelativeLayout {

    public SingleWinnerView(Context context, Winner winner, BrainbaaziStrings brainbaaziStrings) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.view_single_winner, this);
        inflateData(winner.getWinUserList().get(0), winner.getPrizeAmount(), brainbaaziStrings);
    }

    private void inflateData(WinUser winUser, long prizeAmount, BrainbaaziStrings brainbaaziStrings) {
        ImageView ivUserImage = findViewById(R.id.winnerProfilePic);
        CustomFontTextView tv_winner = findViewById(R.id.tv_winner);
        CustomFontTextView tvUserName = findViewById(R.id.profileName);
        CustomFontTextView tvPrizeMoney = findViewById(R.id.winnerPrizeMoney);

        tv_winner.setText("1 " + brainbaaziStrings.leaderboardStrings().winnerText());
        tvPrizeMoney.setText(getContext().getApplicationContext().getString(R.string.currencySymbol) + prizeAmount);
        tvUserName.setText(winUser.getName());
        updateUserImageUrl(ivUserImage, winUser.getImgUrl());
    }

    protected void updateUserImageUrl(ImageView ivUserImage, String imageUrl) {
        if (ivUserImage instanceof ImageViewInteractor) {
            ivUserImage.setImageResource(R.drawable.ic_profile);
            ((ImageViewInteractor) ivUserImage).setDefault(R.drawable.ic_user_icon);
            ((ImageViewInteractor) ivUserImage).setImageUrl(imageUrl);
        } else {
            ivUserImage.setImageResource(R.drawable.ic_profile);
        }
    }
}
