package com.til.brainbaazi.screen.dialog.coachmarks;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;

import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.dialog.BaseDialog;

/**
 * Created by saurabh.garg on 2/21/18.
 */

public class DataSaverInfoDialog extends BaseDialog {

    public DataSaverInfoDialog(@NonNull Context context) {
        super(context, android.R.style.Theme_NoTitleBar_Fullscreen);
        setDelayDismiss(10000);
    }

    @Override
    public int getDialogLayout() {
        return R.layout.dialog_data_saver_info;
    }

    @Override
    public void inflateDialogView() {
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.HSVToColor(100, new float[]{0f, 0f, 0f})));

        CustomFontTextView gotIt = findViewById(R.id.gotIt);
        CustomFontTextView tv_extra = findViewById(R.id.tv_extra);

        GameplayStrings strings = getBrainBaaziStrings().gameplayStrings();

        gotIt.setText(strings.gotItText());
        tv_extra.setText(strings.dataSaverCoachmarkText());
        findViewById(R.id.gotIt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
