package com.til.brainbaazi.screen.splash;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brainbaazi.component.Analytics;
import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.config.LanguageOption;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.viewmodel.splash.SplashViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by prashant.rathore on 13/02/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class SplashScreen extends BaseScreen<SplashViewModel> {

    @BindView(R2.id.rlConnection)
    View rlConnection;
    @BindView(R2.id.tvRetry)
    CustomFontTextView tvRetry;

    @BindView(R2.id.tagLine)
    CustomFontTextView tagLine;
    @BindView(R2.id.terms_text)
    CustomFontTextView terms_text;

    @BindView(R2.id.ll_lang_selection)
    View ll_lang_selection;
    @BindView(R2.id.rv_languages)
    RecyclerView rv_languages;

    @BindView(R2.id.get_started)
    CustomFontTextView buttonGetStarted;

    @OnClick(R2.id.get_started)
    void openRegistration() {
        getViewModel().proceedForRegistration();
    }

    @OnClick(R2.id.tvRetry)
    void retryNetworkConnection() {
        getViewModel().retryNetworkConnection();
    }

    private LanguageAdapter languageAdapter;

    public SplashScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_splash, viewGroup, false);
    }

    @Override
    protected void onBind(SplashViewModel viewModel) {
        bindUIData();
        observerSplashViewState(viewModel);
        observerAppConfigState(viewModel);
    }

    private void bindUIData() {
        ll_lang_selection.setVisibility(View.INVISIBLE);
        buttonGetStarted.setEnabled(false);

        languageAdapter = new LanguageAdapter(new LanguageAdapter.OnLanguageSelection() {
            @Override
            public void onLanguageSelection(LanguageOption languageOption) {
                modifySelectedLanguageText(languageOption);
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return languageAdapter.getSpanSize(position);
            }
        });
        rv_languages.setLayoutManager(gridLayoutManager);
        rv_languages.setAdapter(languageAdapter);

        String terms = "Terms of use,";
        String privacy = "Privacy Policy";
        String rules = "Rules";
        customizeTextView("By signing up you agree to the\n Terms of use, Privacy Policy and Rules", terms, privacy, rules);
    }

    private void observerSplashViewState(SplashViewModel viewModel) {
        Disposable subscribe = viewModel.observeViewState().distinctUntilChanged().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                switch (integer) {
                    case SplashViewModel.STATE_GET_STARTED:
                        showAppStartView();
                        break;

                    case SplashViewModel.STATE_LOADING:
                        showLoadingUI();
                        break;

                    case SplashViewModel.STATE_SERVER_ERROR:
                    case SplashViewModel.STATE_NETWORK_ERROR:
                        showNetworkErrorView();
                        break;

                    case SplashViewModel.STATE_UPDATE:
                        break;
                    case SplashViewModel.STATE_PROCEED_TO_DASHBOARD:
                        break;
                }
            }
        });
        addDisposable(subscribe);
    }

    private void observerAppConfigState(SplashViewModel viewModel) {
        Disposable subscribe = viewModel.observeAppConfigState().distinctUntilChanged().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<AppConfig>() {
            @Override
            public void accept(AppConfig appConfig) throws Exception {
                showLanguageSelection(appConfig.getLanguageOptions());
            }
        });
        addDisposable(subscribe);
    }

    private void showLoadingUI() {
        rlConnection.setVisibility(View.INVISIBLE);

        tagLine.setVisibility(View.INVISIBLE);
        terms_text.setVisibility(View.INVISIBLE);
        buttonGetStarted.setVisibility(View.INVISIBLE);
    }

    private void showNetworkErrorView() {
        rlConnection.setVisibility(View.VISIBLE);

        tagLine.setVisibility(View.VISIBLE);
        terms_text.setVisibility(View.VISIBLE);
        buttonGetStarted.setVisibility(View.VISIBLE);
    }

    private void showAppStartView() {
        rlConnection.setVisibility(View.INVISIBLE);

        tagLine.setVisibility(View.VISIBLE);
        terms_text.setVisibility(View.VISIBLE);
        buttonGetStarted.setVisibility(View.VISIBLE);
    }

    private void showLanguageSelection(List<LanguageOption> languageOptions) {
        if (languageOptions == null || languageOptions.size() == 0) {
            buttonGetStarted.setEnabled(true);
            //languageOptions = new ArrayList<>();
            //languageOptions.add(LanguageOption.builder().setName("English").build());
            //languageOptions.add(LanguageOption.builder().setName("हिंदी").build());
            //languageOptions.add(LanguageOption.builder().setName("தமிழ்").build());
            //languageOptions.add(LanguageOption.builder().setName("ಕನ್ನಡ").build());
            return;
        }
        ll_lang_selection.setVisibility(View.VISIBLE);
        languageAdapter.addValues(languageOptions);
    }

    private void modifySelectedLanguageText(LanguageOption languageOption) {
        tagLine.setText(languageOption.tagLineText());
        buttonGetStarted.setText(languageOption.getStartedText());
        buttonGetStarted.setEnabled(true);
        customizeTextView(languageOption.termConditionText(), languageOption.termsText(), languageOption.privacyText(), languageOption.rulesText());
    }

    private void customizeTextView(String continueText, String terms, String privacy, String rules) {
        if (TextUtils.isEmpty(continueText)) {
            return;
        }
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(continueText);
        int startIndexTerms = AppUtils.getStartIndex(spanTxt.toString(), terms);
        int startIndexPrivacy = AppUtils.getStartIndex(spanTxt.toString(), privacy);
        int startIndexRules = AppUtils.getStartIndex(spanTxt.toString(), rules);

        if (startIndexTerms >= 0) {
            spanTxt.setSpan(new RelativeSizeSpan(1.04f), startIndexTerms, startIndexTerms + terms.length(), 0);
            spanTxt.setSpan(new StyleSpan(Typeface.BOLD), startIndexTerms, startIndexTerms + terms.length(), 0);
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    openWebPage(getString(R.string.terms_of_use), "terms_of_use", getString(R.string.url_terms));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            }, startIndexTerms, AppUtils.getEndIndex(spanTxt.toString(), terms), 0);
            spanTxt.setSpan(new ForegroundColorSpan(Color.WHITE), startIndexTerms, startIndexTerms + terms.length(), 0);
        }

        if (startIndexPrivacy >= 0) {
            spanTxt.setSpan(new RelativeSizeSpan(1.04f), startIndexPrivacy, startIndexPrivacy + privacy.length(), 0);
            spanTxt.setSpan(new StyleSpan(Typeface.BOLD), startIndexPrivacy, startIndexPrivacy + privacy.length(), 0);
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    openWebPage(getString(R.string.privacy), "privacy_policy", getString(R.string.url_privacy));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            }, startIndexPrivacy, AppUtils.getEndIndex(spanTxt.toString(), privacy), 0);
            spanTxt.setSpan(new ForegroundColorSpan(Color.WHITE), startIndexPrivacy, startIndexPrivacy + privacy.length(), 0);
        }

        if (startIndexRules >= 0) {
            spanTxt.setSpan(new RelativeSizeSpan(1.04f), startIndexRules, startIndexRules + rules.length(), 0);
            spanTxt.setSpan(new StyleSpan(Typeface.BOLD), startIndexRules, startIndexRules + rules.length(), 0);
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    openWebPage(getString(R.string.rules), "rules", getString(R.string.url_rules));
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            }, startIndexRules, AppUtils.getEndIndex(spanTxt.toString(), rules), 0);
            spanTxt.setSpan(new ForegroundColorSpan(Color.WHITE), startIndexRules, startIndexRules + rules.length(), 0);
        }
        terms_text.setMovementMethod(LinkMovementMethod.getInstance());
        terms_text.setHighlightColor(Color.TRANSPARENT);
        terms_text.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

    private void openWebPage(String title, String event, String url) {
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent(event).build();
        getViewModel().logFireBaseScreen(gaEventModel, null);
        getViewModel().cleverTapEvent(event, null);

        WebData webData = WebData.builder().setTitle(title).setWebUrl(url).build();
        getViewModel().openWebPages(webData);
    }

    @Override
    protected void onUnBind() {

    }

    @Override
    public void resume() {
        super.resume();
        sendLandingScreenEvent();
    }

    private void sendLandingScreenEvent() {
        String userName = getViewModel().getDataRepository().getUserName();
        String firstLaunch = (!TextUtils.isEmpty(userName) ? "no" : "yes");

        getViewModel().logFireBaseScreen(Analytics.SCREEN_SPLASH);
        getViewModel().cleverAppLaunchEvent(firstLaunch, Utils.getDeviceId(getContext()), null, null);

        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("app_launched").build();
        Bundle bundle = new Bundle();
        bundle.putString("first_launch", "");
        bundle.putString("device_id", Utils.getDeviceId(getContext()));
        getViewModel().logFireBaseScreen(gaEventModel, bundle);


        gaEventModel = GaEventModel.builder().setMainEvent("app_launched").setCategory("App Launch")
                .setAction(Utils.getDeviceId(getContext()))
                .setLabel("")
                .setUserName(userName)
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getViewModel().logFireBaseEvent(gaEventModel);
    }
}
