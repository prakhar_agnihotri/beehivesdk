package com.til.brainbaazi.screen.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.payment.PaymentFailureViewModel;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by prashant.rathore on 09/03/18.
 */
@AutoFactory(implementing = ScreenFactory.class)
public class PaymentFailureScreen extends BaseScreen<PaymentFailureViewModel> {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.error_stmt_1)
    TextView errorStmt1;
    @BindView(R2.id.error_stmt_2)
    TextView errorStmt2;
    @BindView(R2.id.gotohome)
    TextView gotoHomeButton;
    @BindView(R2.id.failure_message)
    TextView failure_message;
    @BindView(R2.id.reason)
    TextView reason;
    @BindView(R2.id.retrybutton)
    Button retryButton;

    private ProgressDialog progressDialog;

    @OnClick(R2.id.gotohome)
    void getToHome() {
        getViewModel().getPaymentNavigation().proceedToHome();
    }

    @OnClick(R2.id.retrybutton)
    void retryClicked() {
        retryPayment();
    }

    public PaymentFailureScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.cashout_payment_failed_screen, viewGroup, false);
    }

    @Override
    protected void onBind(PaymentFailureViewModel viewModel) {
        bindData();
        gotoHomeButton.setText(getBrainBaaziStrings().paymentStrings().goToHomeText());
        toolbar.setTitle(getBrainBaaziStrings().paymentStrings().cashoutText());
        setNavigationIcon(toolbar, R.drawable.arrow_back);
        observerViewState(viewModel);
        observePaymentResponse(viewModel);
    }

    @Override
    protected void onUnBind() {
        hideProgressDialog();
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getPaymentNavigation().proceedToHome();
    }

    private void bindData() {
        switch (getViewModel().getResultCode()) {
            case PaymentFailureViewModel.PAYMENT_FAILURE_MOBIWIKI_CREATE_USER:
                failure_message.setText(getString(R.string.cashout_payment_failed_no_user));
                errorStmt1.setVisibility(View.GONE);
                errorStmt2.setVisibility(View.GONE);
                reason.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(getViewModel().getPhoneNumber()))
                    reason.setText(getString(R.string.str_mobikwik_no_user_exist) + " " + getViewModel().getPhoneNumber());
                retryButton.setText(getString(R.string.cashout_create_new_mobikqwik_wallet_text));
                break;
            default:
                if (!TextUtils.isEmpty(getViewModel().getAmountToTransfer())) {

                    String transferFail = getBrainBaaziStrings().paymentStrings().payFailTransferMobileText();
                    transferFail = transferFail.replace("<amount>", getViewModel().getAmountToTransfer());
                    transferFail = transferFail.replace("<service>", getViewModel().getSource());

                    errorStmt1.setText(transferFail);
                } else {
                    errorStmt1.setText("");
                }
                errorStmt2.setText("");
        }
    }

    private void retryPayment() {
        switch (getViewModel().getResultCode()) {
            case PaymentFailureViewModel.PAYMENT_FAILURE_MOBIWIKI_CREATE_USER:
                createMobiWikiWallet();
                break;
            default:
                getViewModel().tryForRepayment(createRequestData());
        }
    }

    private void createMobiWikiWallet() {
        getViewModel().openMobikwikWalletScreen();
    }



    private void showProgressLoader() {
        try {
            String message = getBrainBaaziStrings().paymentStrings().processRequestText();
            progressDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(true); // disable dismiss by tapping outside of the dialog
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PaymentRequest createRequestData() {
        return PaymentRequest.builder().
                setAmt(getViewModel().getAmountToTransfer()).
                setMob(getViewModel().getPhoneNumber()).
                setIp(PaymentUtils.getIpAddress(getContext().getApplicationContext())).
                setSrc(getViewModel().getSource()).
                build();
    }

    private void observerViewState(PaymentFailureViewModel viewModel) {
        Disposable subscribe = viewModel.observeViewState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                switch (integer) {
                    case PaymentFailureViewModel.STATE_LOADING:
                        showProgressLoader();
                        break;
                    case PaymentFailureViewModel.STATE_REMOVE_LOADING:
                        hideProgressDialog();
                        break;
                    case PaymentFailureViewModel.STATE_ERROR_MESSAGE:
                        String message = getBrainBaaziStrings().paymentStrings().unableToProcess_requestText();
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        errorStmt2.setText(message);
                        break;
                }
            }
        });
        addDisposable(subscribe);
    }

    private void observePaymentResponse(PaymentFailureViewModel viewModel) {
        DisposableOnNextObserver<PaymentResponseMessage> disposableOnNextObserver = new DisposableOnNextObserver<PaymentResponseMessage>() {
            @Override
            public void onNext(PaymentResponseMessage paymentResponseMessage) {
                if (paymentResponseMessage.isRequestSuccess()) {
                    getViewModel().getPaymentNavigation().openPaymentSuccessScreen(paymentResponseMessage, getViewModel().getSource(), Integer.parseInt(getViewModel().getAmountToTransfer()));
                } else {
                    errorStmt2.setText(paymentResponseMessage.getStatusMessage());
                }
            }
        };
        addDisposable(disposableOnNextObserver);
        viewModel.observerPaymentResponse().subscribe(disposableOnNextObserver);
    }
}
