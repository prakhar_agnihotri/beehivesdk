package com.til.brainbaazi.screen.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.interactor.activity.ActivityPermissionResult;
import com.til.brainbaazi.interactor.activity.ActivityResult;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.viewmodel.BaseScreenModel;
import com.til.brainbaazi.viewmodel.BaseViewModel;

/**
 * Created by prashant.rathore on 14/02/18.
 */

public abstract class ControllerFragment<VM extends BaseScreenModel> extends Fragment {

    private ScreenController<VM> screenController;
    private BaseScreen<VM> screenView;
    private View.OnKeyListener backPressListener;

    private ActivityInteractorImpl activityInteractor = new ActivityInteractorImpl() {

        @Override
        public void requestPermission(String[] permissions, int code) {
            ControllerFragment.this.requestPermissions(permissions, code);
        }

        @Override
        public void startActivityForResult(Intent intent, int requestCode) {
            ControllerFragment.this.startActivityForResult(intent, requestCode);
        }

        @Override
        public void performBackPress() {
            ControllerFragment.this.getActivity().onBackPressed();
        }
    };


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.screenController = provideController();
        this.screenController.attach(context, LayoutInflater.from(context));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        screenController.onCreate();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        screenView = screenController.createView(null);
        return screenView.getView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return screenController.handleBackPressed();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        screenController.bindView(screenView);
    }

    @Override
    public void onStart() {
        super.onStart();
        screenController.onStart();
    }

    @Override
    public void onResume() {
        screenController.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        screenController.onPause();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        screenController.onStop();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        this.getView().setOnKeyListener(null);
        this.screenController.unBindView();
        this.screenView = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        screenController.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        this.screenController = null;
        super.onDetach();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ActivityPermissionResult permissionResult = ActivityPermissionResult.builder()
                .setPermissions(permissions)
                .setRequestCode(requestCode)
                .setGrantResult(grantResults).build();

        activityInteractor.publisPermissionResult(permissionResult);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            ActivityResult activityResult = ActivityResult.builder().setRequestCode(requestCode).setResultCode(resultCode).setData(data).build();
            activityInteractor.publishActivityResult(activityResult);
        }
    }


    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    protected abstract ScreenController<VM> provideController();
}
