package com.til.brainbaazi.screen;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseScreen<VM extends BaseScreenModel> {

    private CompositeDisposable compositeDisposable;

    private final Context context;
    private VM viewModel;
    private final LayoutInflater layoutInflater;
    private final View view;

    private final static String KEY_SAVE_STATE = "saveViewState";

    BrainbaaziStrings brainbaaziStrings;

    public BaseScreen(Context context, LayoutInflater layoutInflater, ViewGroup parentView) {
        this.context = context;
        this.layoutInflater = layoutInflater;
        this.view = createView(layoutInflater, parentView);
        ButterKnife.bind(this, view);
    }

    public Context getContext() {
        return context;
    }

    public final View getView() {
        return this.view;
    }

    public VM getViewModel() {
        return viewModel;
    }

    protected abstract View createView(LayoutInflater layoutInflater, ViewGroup viewGroup);

    public void bindViewModel(VM viewModel) {
        this.compositeDisposable = new CompositeDisposable();
        this.viewModel = viewModel;
        observeBrainBaaziStrings();
        onBind(viewModel);
    }

    private void observeBrainBaaziStrings() {
        DisposableObserver<BrainbaaziStrings> observer = new DisposableOnNextObserver<BrainbaaziStrings>() {
            @Override
            public void onNext(BrainbaaziStrings  bs) {
                brainbaaziStrings = bs;
                updateBrainBaaziTexts(bs);
            }
        };
        addDisposable(observer);
        viewModel.observeBrainbaaziStrings().observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    protected abstract void onBind(VM viewModel);

    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
    }

    protected BrainbaaziStrings getBrainBaaziStrings() {
        if(brainbaaziStrings == null) {
            this.brainbaaziStrings = viewModel.loadDefaultStrings();
        }
        return brainbaaziStrings;
    }

    public void restoreState(Bundle bundle) {
        if (bundle != null) {
            try {
                SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray(KEY_SAVE_STATE);
                view.restoreHierarchyState(sparseParcelableArray);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void willShow() {

    }

    public void resume() {

    }

    public void pause() {

    }

    public void willHide() {

    }



    public void saveState(Bundle bundle) {
        SparseArray<Parcelable> viewState = new SparseArray<>();
        view.saveHierarchyState(viewState);
        bundle.putSparseParcelableArray(KEY_SAVE_STATE, viewState);
    }

    public final void unBindViewModel() {
        onUnBind();
        this.viewModel = null;
        this.compositeDisposable.dispose();
        this.compositeDisposable = null;
    }

    protected void addDisposable(Disposable disposable) {
        this.compositeDisposable.add(disposable);
    }

    protected abstract void onUnBind();

    protected void setNavigationIcon(Toolbar toolbar, int drawable) {
        toolbar.setNavigationIcon(drawable);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void onBackPressed() {

    }

    protected String getString(int stringID) {
        return getContext().getString(stringID);
    }



}
