package com.til.brainbaazi.screen.leaderBoard;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ImageViewInteractor;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardListModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.LeaderboardStrings;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.leaderBoard.adapters.LeaderBoardPagerAdapter;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardContainerViewModel;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by saurabh.garg on 2/21/18.
 */

@AutoFactory(implementing = ScreenFactory.class)
public class LeaderBoardContainerScreen extends BaseScreen<LeaderBoardContainerViewModel> implements ViewPager.OnPageChangeListener {

    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.tabs)
    TabLayout tabLayout;

    @BindView(R2.id.container)
    ViewPager mViewPager;

    @BindView(R2.id.user_lb_layout)
    View user_lb_layout;
    @BindView(R2.id.user_rank)
    CustomFontTextView user_rank;
    @BindView(R2.id.profile_img_post)
    ImageView profile_img_post;
    @BindView(R2.id.user_name)
    CustomFontTextView user_name;
    @BindView(R2.id.user_prize)
    CustomFontTextView user_prize;

    private LeaderBoardPagerAdapter leaderBoardPagerAdapter;
    private long weeklyScore, allTimeScore;

    public LeaderBoardContainerScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView, @Provided LeaderBoardPagerAdapter pagerAdapter) {
        super(context, layoutInflater, parentView);
        this.leaderBoardPagerAdapter = pagerAdapter;
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_leader_board_container, viewGroup, false);
    }

    @Override
    protected void onBind(LeaderBoardContainerViewModel viewModel) {
        bindContainerData();
        observeUserInfo(viewModel);
        observerLeaderBoardData(viewModel);
    }

    private void observeUserInfo(LeaderBoardContainerViewModel viewModel) {
        DisposableObserver<User> balanceObserver = new DisposableOnNextObserver<User>() {
            @Override
            public void onNext(User user) {
                if (user != null)
                    updateUserData(user);
            }

        };
        viewModel.observeUserInfo().observeOn(AndroidSchedulers.mainThread()).subscribe(balanceObserver);
    }

    private void observerLeaderBoardData(LeaderBoardContainerViewModel viewModel) {
        DisposableObserver<LeaderBoardListModel> listModelDisposableObserver = new DisposableOnNextObserver<LeaderBoardListModel>() {
            @Override
            public void onNext(LeaderBoardListModel listModel) {
                updateLeaderBoardData(listModel);
            }
        };
        addDisposable(listModelDisposableObserver);
        viewModel.observeLeaderBoardModel().observeOn(AndroidSchedulers.mainThread()).subscribe(listModelDisposableObserver);
    }

    private void bindContainerData() {
        toolbar.setTitle(getContext().getString(R.string.leaderboard_title_text));
        setNavigationIcon(toolbar, R.drawable.arrow_back);

        int color = getContext().getResources().getColor(R.color.bbcolorWhite);
        user_name.setTextColor(color);
        user_rank.setTextColor(color);
        user_prize.setTextColor(color);

        mViewPager.addOnPageChangeListener(this);
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        mViewPager.setAdapter(leaderBoardPagerAdapter);
        user_lb_layout.setVisibility(View.INVISIBLE);
        getViewModel().cleverLeaderBoardScreenViewEvent("Dashboard");
    }

    private void updateUserData(User user) {
        user_name.setText(user.getUserName());
        if (user.getWeeklyRank() > 0) {
            user_rank.setText(String.valueOf(user.getWeeklyRank()));
        } else {
            user_rank.setText(" ");
        }
        user_prize.setText(String.valueOf(user.getUserBalance()));
        updateUserImageUrl(user.getUserImgUrl());
    }

    private void updateUserImageUrl(String imgURL) {
        if (profile_img_post instanceof ImageViewInteractor) {
            ((ImageViewInteractor) profile_img_post).setDefault(R.drawable.ic_user_icon);
            profile_img_post.setVisibility(View.VISIBLE);
            ((ImageViewInteractor) profile_img_post).setImageUrl(imgURL);
        } else {
            profile_img_post.setImageResource(R.drawable.ic_user_icon);
        }
    }

    private void updateLeaderBoardData(LeaderBoardListModel listModel) {

        this.weeklyScore = listModel.getWeeklyScore();
        this.allTimeScore = listModel.getAllTimeScore();

        this.leaderBoardPagerAdapter.set(listModel.getLeaderBoardWeeklyUsers(), listModel.getLeaderBoardAllTimeUsers());

        modifyUserPrize(mViewPager.getCurrentItem());
    }

    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        LeaderboardStrings leaderboardStrings = brainBaaziStrings.leaderboardStrings();
        toolbar.setTitle(leaderboardStrings.winnersText());
        tabLayout.getTabAt(0).setText(leaderboardStrings.winnersText());
        tabLayout.getTabAt(1).setText(leaderboardStrings.thisWeekText());
        tabLayout.getTabAt(2).setText(leaderboardStrings.allTimeText());
    }

    @Override
    protected void onUnBind() {
        mViewPager.setAdapter(null);
    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            getViewModel().cleverLeaderBoardScreenViewEvent("Winners");
            user_lb_layout.setVisibility(View.INVISIBLE);
        } else {
            String text = (position == 1 ? "Weekly" : "All Time");
            getViewModel().cleverLeaderBoardScreenViewEvent(text);
            user_lb_layout.setVisibility(View.VISIBLE);
        }

        TabLayout.Tab tab = tabLayout.getTabAt(position);
        if (tab != null) {
            tab.select();
        }
        modifyUserPrize(position);
    }

    private void modifyUserPrize(int position) {
        if (position > 0) {
            long score = (position == 1 ? weeklyScore : allTimeScore);
            String userPrize = getContext().getResources().getString(R.string.currencySymbol) + " " + String.valueOf(score);
            user_prize.setText(userPrize);

            String label = (position == 1 ? "Switched to Weekly" : "All Time");
            getViewModel().sendAnalyticsEvent(label);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        getViewModel().getActivityInteractor().performBackPress();
    }

    @Override
    public void resume() {
        super.resume();
        getViewModel().logFireBaseScreen(Analytics.SCREEN_LEADER_BOARD_SCREEN);
    }

    private void sendAnalytics(String label) {

    }
}
