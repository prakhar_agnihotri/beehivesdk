package com.til.brainbaazi.screen.other;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainbaazi.component.Analytics;
import com.google.auto.factory.AutoFactory;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.LaunchStrings;
import com.til.brainbaazi.screen.BaseScreen;
import com.til.brainbaazi.screen.R;
import com.til.brainbaazi.screen.R2;
import com.til.brainbaazi.screen.ScreenFactory;
import com.til.brainbaazi.screen.customViews.base.CustomFontTextView;
import com.til.brainbaazi.screen.utils.Utils;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 2/22/18.
 */

@AutoFactory(implementing = ScreenFactory.class)
public class ForceUpdateScreen extends BaseScreen<BaseScreenModel> {

    @BindView(R2.id.weAreBetterText)
    CustomFontTextView weAreBetterText;

    @BindView(R2.id.lightUpdateText)
    CustomFontTextView lightUpdateText;

    @BindView(R2.id.tvUpdate)
    CustomFontTextView tvUpdate;

    @OnClick(R2.id.tvUpdate)
    void openAppUpdate() {
        Utils.openAppPlayStore(getContext());
    }

    public ForceUpdateScreen(Context context, LayoutInflater layoutInflater, @Nullable ViewGroup parentView) {
        super(context, layoutInflater, parentView);
    }

    @Override
    protected View createView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R.layout.screen_force_update, viewGroup, false);
    }

    @Override
    protected void onBind(BaseScreenModel viewModel) {
        modifyShowTexts(getBrainBaaziStrings());
    }

    @Override
    protected void updateBrainBaaziTexts(BrainbaaziStrings brainBaaziStrings) {
        super.updateBrainBaaziTexts(brainBaaziStrings);
        modifyShowTexts(brainBaaziStrings);
    }

    private void modifyShowTexts(BrainbaaziStrings brainBaaziStrings) {
        LaunchStrings launchStrings = brainBaaziStrings.launchStrings();
        weAreBetterText.setText(launchStrings.latestVersionText());
        lightUpdateText.setText(launchStrings.appUpdateText());
        tvUpdate.setText(launchStrings.updateNowText());
    }

    @Override
    protected void onUnBind() {

    }

    @Override
    public void resume() {
        super.resume();
        getViewModel().logFireBaseScreen(Analytics.SCREEN_FORCE_UPDATE);
    }
}
