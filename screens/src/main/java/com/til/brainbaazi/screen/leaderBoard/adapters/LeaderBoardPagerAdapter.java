package com.til.brainbaazi.screen.leaderBoard.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.google.common.collect.ImmutableList;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardUser;
import com.til.brainbaazi.screen.controller.ControllerPagerAdapter;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.leaderBoard.LastTimeWinnerListScreenFactory;
import com.til.brainbaazi.screen.leaderBoard.LeaderBoardListScreenFactory;
import com.til.brainbaazi.viewmodel.leaderBoard.LastWinnerListViewModel;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardListViewModel;

import java.util.ArrayList;

/**
 * Created by saurabh.garg on 2/22/18.
 */
@AutoFactory
public class LeaderBoardPagerAdapter extends ControllerPagerAdapter {

    private final ConnectionManager connectionManager;
    private final DataRepository dataRepository;
    private final Analytics analytics;
    private SegmentInfo[] segmentInfo;

    public LeaderBoardPagerAdapter(@Provided Activity activity, @Provided LayoutInflater layoutInflater, @Provided ConnectionManager connectionManager, @Provided DataRepository dataRepository, @Provided Analytics analytics) {
        super(activity, layoutInflater);
        this.connectionManager = connectionManager;
        this.dataRepository = dataRepository;
        this.analytics = analytics;
    }

    @Override
    protected ScreenController instantiateScreen(int position) {
        SegmentInfo si = segmentInfo[position];
        if (position == 0) {
            LastTimeWinnerListScreenFactory lastTimeWinnerListScreenFactory = new LastTimeWinnerListScreenFactory();
            LastWinnerListViewModel viewModel = new LastWinnerListViewModel(connectionManager, dataRepository, analytics);
            return new ScreenController(si, viewModel, lastTimeWinnerListScreenFactory);
        } else {
            LeaderBoardListScreenFactory leaderBoardListScreenFactory = new LeaderBoardListScreenFactory();
            LeaderBoardListViewModel viewModel = new LeaderBoardListViewModel(connectionManager, dataRepository, analytics);
            return new ScreenController(si, viewModel, leaderBoardListScreenFactory);
        }
    }

    @Override
    public int getCount() {
        return segmentInfo == null ? 0 : segmentInfo.length;
    }

    public void set(ImmutableList<LeaderBoardUser> weeklyScore, ImmutableList<LeaderBoardUser> allTimeScore) {
        segmentInfo = new SegmentInfo[3];

        Bundle param = new Bundle();
        segmentInfo[0] = new SegmentInfo(0, param);

        param = new Bundle();
        param.putParcelableArrayList(LeaderBoardListViewModel.PARAM_DATA, new ArrayList<LeaderBoardUser>(weeklyScore));
        segmentInfo[1] = new SegmentInfo(0, param);

        param = new Bundle();
        param.putParcelableArrayList(LeaderBoardListViewModel.PARAM_DATA, new ArrayList<LeaderBoardUser>(allTimeScore));
        segmentInfo[2] = new SegmentInfo(0, param);

        notifyDataSetChanged();
    }

}
