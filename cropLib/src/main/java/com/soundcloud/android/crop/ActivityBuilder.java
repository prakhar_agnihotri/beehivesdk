package com.soundcloud.android.crop;

import android.content.Intent;

/**
 * Created by saurabh.garg on 2/23/18.
 */

public class ActivityBuilder {
    private Intent intent;

    private int requestCode;

    public Intent getIntent() {
        return intent;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public ActivityBuilder setIntent(Intent intent) {
        this.intent = intent;
        return this;
    }

    public ActivityBuilder setRequestCode(int requestCode) {
        this.requestCode = requestCode;
        return this;
    }
}
