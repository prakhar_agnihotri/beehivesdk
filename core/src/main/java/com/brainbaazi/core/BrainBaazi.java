package com.brainbaazi.core;

import android.app.Application;

import com.brainbaazi.core.di.BBAndroidInjector;
import com.brainbaazi.core.di.BrainBaaziComponent;
import com.brainbaazi.core.di.DaggerBrainBaaziComponent;
import com.cookingfox.android.app_lifecycle.api.manager.AppLifecycleManager;
import com.cookingfox.android.app_lifecycle.impl.AppLifecycleProvider;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;

import dagger.android.AndroidInjector;

import static dagger.internal.Preconditions.checkNotNull;

public class BrainBaazi extends BBAndroidInjector {

    private static BrainBaazi instance;
    private final BrainBaaziConfig config;
    private final AppLifecycleManager appLifeCycleManager;
    private Application application;
    private BrainBaaziComponent brainBaaziComponent;

    private BrainBaazi(Application application, BrainBaaziConfig config) {
        this.application = application;
        this.config = config;
        appLifeCycleManager = AppLifecycleProvider.initialize(application);
    }

    public static void init(Application application, BrainBaaziConfig brainBaaziConfig) {
        if(instance == null) {
            synchronized (BrainBaazi.class) {
                if(instance == null) {
                    instance = new BrainBaazi(application,brainBaaziConfig);
                    instance.onCreate();
                }
            }
        }
    }

    public static void changeStrings(BrainbaaziStrings brainBaaziStrings) {
        checkNotNull(brainBaaziStrings,"Brainbaazi Strings cannot be null.");
        checkNotNull(instance,"Call Init before changing the strings");
        instance.updateStrings(brainBaaziStrings);
    }

    private void updateStrings(BrainbaaziStrings brainBaaziStrings) {
        brainBaaziComponent.dataRepository().setBrainbaaziStrings(brainBaaziStrings);
    }

    static BrainBaaziComponent getComponent() {
        return instance.brainBaaziComponent;
    }


    @Override
    protected AndroidInjector<BrainBaazi> applicationInjector() {
        this.brainBaaziComponent = (BrainBaaziComponent) DaggerBrainBaaziComponent
                .builder()
                .application(application)
                .applicationLifeCycleManager(appLifeCycleManager)
                .config(config)
                .create(this);
        return brainBaaziComponent;
    }
}
