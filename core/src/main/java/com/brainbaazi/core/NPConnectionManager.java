package com.brainbaazi.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.brainbaazi.component.network.ConnectionManager;
import com.til.brainbaazi.entity.ConnectionInfo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 03/12/17.
 */

public class NPConnectionManager implements ConnectionManager {

    public static final int CLASS_UNKNOWN = -1;
    public static final int CLASS_OFFLINE = 0;
    public static final int CLASS_4G = 1;
    public static final int CLASS_3G = 2;
    public static final int CLASS_2G = 3;
    public static final int CLASS_WIFI = 4;
    public static final int CLASS_BLUETOOTH = 5;
    public static final int CLASS_VPN = 6;

    public static final String NETWORK_CLASS_UNKNOWN = "UNKNOWN";

    private long nextConnectionUpdate;

    private ExecutorService cachedPool;

    private ConnectivityManager connectivityManager;
    private TelephonyManager mTelephonyManager;
    private boolean connected;
    private Handler mHandler = new Handler();
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    //    private ExecutorService externalExecutorService;
    private int mobileNetworkType = -1;
    private int internetSourceType = -1;
    private int networkClass = CLASS_UNKNOWN;

    private BehaviorSubject<ConnectionInfo> networkInfoPublisher = BehaviorSubject.create();

    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, final Intent intent) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    NetworkInfoResponse response = getNetworkInfoResponse();
                    updateAndNotifyNetworkChange(response, false);
                }
            });
        }
    };

    private NetworkInfoResponse getNetworkInfoResponse() {
        NetworkInfoResponse networkInfoResponse = new NetworkInfoResponse();
        try {
            Future<?> submit = cachedPool.submit(networkInfoResponse);
            submit.get(2, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            networkInfoResponse.skip = true;
        }
        return networkInfoResponse;
    }


    private void updateAndNotifyNetworkChange(final NetworkInfoResponse response, boolean forced) {
        final boolean mConnected = response.success && !response.skip ? response.isConnected : true;
        if ((forced && mConnected)
                || this.networkClass != response.networkClass
                || this.connected != mConnected
                || this.internetSourceType != response.internetSourceType
                || this.mobileNetworkType != response.mobileNetworkType) {

            this.internetSourceType = response.internetSourceType;
            this.mobileNetworkType = response.mobileNetworkType;
            if (this.networkClass == CLASS_UNKNOWN && this.connected && mConnected) {
                this.networkClass = response.networkClass;
                return;
            }
            this.connected = mConnected;
            this.networkClass = response.networkClass;
            notifyNetworkChanged(response.networkInfo, response.networkClass, mConnected);
        }

    }


    public NPConnectionManager(Context context) {
        cachedPool = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                30L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>());
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        connected = true;
        context.registerReceiver(networkChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        ConnectionInfo connectionInfo = ConnectionInfo.builder().setConnectedToInternet(connected).setName(getActiveNetworkInfo()).setType(networkClass).build();
        networkInfoPublisher.onNext(connectionInfo);
        //notifyNetworkChanged(connectivityManager.getActiveNetworkInfo());
        forceNetworkNotification();
    }

    public void forceNetworkNotification() {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                NetworkInfoResponse response = getNetworkInfoResponse();
                updateAndNotifyNetworkChange(response, true);
            }
        });
    }


    public void onDestroy(Context context) {
        context.unregisterReceiver(networkChangeReceiver);
    }

    @Override
    public Observable<ConnectionInfo> observeNetworkChanges() {
        return this.networkInfoPublisher.observeOn(AndroidSchedulers.mainThread());
    }


    public boolean isConnected() {
        if (!this.connected && nextConnectionUpdate < System.currentTimeMillis()) {
            forceNetworkNotification();
            nextConnectionUpdate = System.currentTimeMillis() + 2000;
        }
        return this.connected;
    }

    private void notifyNetworkChanged(NetworkInfo networkInfo, int networkClass, boolean connected) {
        ConnectionInfo build = ConnectionInfo.builder().setType(networkClass).setConnectedToInternet(connected).setName(getActiveNetworkInfoInternalName(networkClass))
                .build();
        networkInfoPublisher.onNext(build);
    }

    public String getActiveNetworkInfo() {
        return getActiveNetworkInfoInternalName(this.networkClass);
    }

    private String getActiveNetworkInfoInternalName(int networkClass) {
        if (!connected) {
            return "OFFLINE";
        }
        switch (networkClass) {
            case CLASS_2G:
                return "2g";
            case CLASS_3G:
                return "3g";
            case CLASS_4G:
                return "4g";
            case CLASS_WIFI:
                return "wifi";
            case CLASS_BLUETOOTH:
                return "bluetooth";
            case CLASS_VPN:
                return "vpn";
            default:
                return "UNKNWON-" + networkClass;
        }
    }

    public int getNetworkClass() {
        int nclass = networkClass;
        switch (this.networkClass) {
            case CLASS_VPN:
            case CLASS_BLUETOOTH:
                nclass = CLASS_WIFI;
        }
        return nclass;
    }


    private int getNetworkClass(int internetSourceType, int mobileNetworkType) {
        int networkName;
        switch (internetSourceType) {
            case ConnectivityManager.TYPE_MOBILE:
            case ConnectivityManager.TYPE_MOBILE_DUN:
                networkName = getNetworkClassInternal(mobileNetworkType);
                break;
            case ConnectivityManager.TYPE_BLUETOOTH:
                networkName = CLASS_BLUETOOTH;
                break;
            case ConnectivityManager.TYPE_VPN:
                networkName = CLASS_VPN;
                break;
            case ConnectivityManager.TYPE_WIFI:
            case ConnectivityManager.TYPE_WIMAX:
                networkName = CLASS_WIFI;
                break;
            default:
                networkName = CLASS_UNKNOWN;
        }
        return networkName;
    }

    private int getNetworkClassInternal(int mobileNetworkType) {
        switch (mobileNetworkType) {
            case -1:
                return CLASS_UNKNOWN;
            case 0:
                return CLASS_OFFLINE;
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return CLASS_2G;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return CLASS_3G;
            case TelephonyManager.NETWORK_TYPE_LTE:
                return CLASS_4G;
            default:
                return CLASS_WIFI;
        }
    }


    public void onAppResume() {
        forceNetworkNotification();

    }

    public void onAppStop(boolean background) {
        if (!background)
            forceNetworkNotification();
    }


    private class NetworkInfoResponse implements Runnable {

        int internetSourceType = -1;
        boolean isConnected;
        int networkClass = getNetworkClass(-1, -1);
        int mobileNetworkType = -1;

        boolean success;

        private NetworkInfo networkInfo;

        private boolean skip;

        public void init(NetworkInfo networkInfo) {
            this.networkInfo = networkInfo;
            if (networkInfo != null) {
                internetSourceType = networkInfo.getType();
                mobileNetworkType = mTelephonyManager.getNetworkType();
                isConnected = networkInfo.isConnected();
            } else {
                internetSourceType = -1;
                mobileNetworkType = -1;
                isConnected = false;
            }
            networkClass = getNetworkClass(internetSourceType, mobileNetworkType);
        }

        @Override
        public void run() {
            try {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (!skip) {
                    init(activeNetworkInfo);
                    success = !skip;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
