package com.brainbaazi.core.screen.di;

import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.brainbaazi.core.AppNavigation;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.balance.BalanceScreenFactory;
import com.til.brainbaazi.screen.balance.ZeroBalanceScreenFactory;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.other.WebViewScreenFactory;
import com.til.brainbaazi.viewmodel.balance.BalanceViewModel;
import com.til.brainbaazi.viewmodel.balance.BalanceViewModelFactory;
import com.til.brainbaazi.viewmodel.balance.ZeroBalanceViewModel;
import com.til.brainbaazi.viewmodel.balance.ZeroBalanceViewModelFactory;
import com.til.brainbaazi.viewmodel.other.WebViewModel;
import com.til.brainbaazi.viewmodel.payment.PaymentNavigation;

import dagger.Module;
import dagger.Provides;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@Module
class BalanceModule {

    @Provides
    ScreenController<BalanceViewModel> screenController(SegmentInfo segmentInfo, BalanceViewModel viewModel, BalanceScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    BalanceViewModel viewModel(BalanceViewModelFactory factory, ActivityInteractor activityInteractor, PaymentNavigation navigation) {
        return factory.create(activityInteractor, navigation);
    }


    @Provides
    ScreenController<ZeroBalanceViewModel> zeroBalanceScreenController(SegmentInfo segmentInfo, ZeroBalanceViewModel viewModel, ZeroBalanceScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    ZeroBalanceViewModel zeroBalanceViewModel(ZeroBalanceViewModelFactory factory, ActivityInteractor activityInteractor) {
        return factory.create(activityInteractor);
    }

}
