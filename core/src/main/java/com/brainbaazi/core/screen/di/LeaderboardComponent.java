package com.brainbaazi.core.screen.di;

import android.app.Activity;
import android.content.Context;

import com.brainbaazi.core.di.BrainBaaziComponent;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardContainerViewModel;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@ScreenScope
@Component(modules = {LeaderBoardModule.class}, dependencies = {BrainBaaziComponent.class})
public interface LeaderboardComponent {

    ScreenController<LeaderBoardContainerViewModel> screenController();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder segmentInfo(SegmentInfo segmentInfo);

        @BindsInstance
        Builder activityInteractor(ActivityInteractor activityInteractor);

        @BindsInstance
        Builder activity(Activity activity);

        Builder module(LeaderBoardModule module);

        Builder brainBaaziComponent(BrainBaaziComponent brainBaaziComponent);

        LeaderboardComponent build();
    }

}
