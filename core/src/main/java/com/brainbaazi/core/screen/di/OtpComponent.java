package com.brainbaazi.core.screen.di;

import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.core.di.BrainBaaziComponent;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.viewmodel.otp.OtpGenerateViewModel;
import com.til.brainbaazi.viewmodel.otp.OtpNavigation;
import com.til.brainbaazi.viewmodel.otp.OtpVerifyViewModel;
import com.til.brainbaazi.viewmodel.otp.ProfileViewModel;
import com.til.brainbaazi.viewmodel.splash.SplashNavigation;
import com.til.brainbaazi.viewmodel.splash.SplashViewModel;

import javax.inject.Named;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by prashant.rathore on 20/02/18.
 */
@ScreenScope
@Component(modules = {OtpModule.class}, dependencies = {BrainBaaziComponent.class})
public interface OtpComponent {

    ScreenController<OtpGenerateViewModel> generateOtpController();
    ScreenController<OtpVerifyViewModel> verifyOtpController();
    ScreenController<ProfileViewModel> profileController();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder segmentInfo(SegmentInfo segmentInfo);
        @BindsInstance
        Builder navigation(OtpNavigation navigation);
        @BindsInstance
        Builder activityInteractor(ActivityInteractor activityInteractor);
        Builder module(OtpModule module);
        Builder brainBaaziComponent(BrainBaaziComponent brainBaaziComponent);
        OtpComponent build();
    }

}
