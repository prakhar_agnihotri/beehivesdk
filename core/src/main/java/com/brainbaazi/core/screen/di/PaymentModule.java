package com.brainbaazi.core.screen.di;

import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.payment.MobikwikWalletScreenFactory;
import com.til.brainbaazi.screen.payment.PaymentFailureScreenFactory;
import com.til.brainbaazi.screen.payment.SuccessScreenFactory;
import com.til.brainbaazi.screen.payment.WalletSuccessScreenFactory;
import com.til.brainbaazi.viewmodel.payment.MobikiwikWalletModel;
import com.til.brainbaazi.viewmodel.payment.MobikiwikWalletModelFactory;
import com.til.brainbaazi.viewmodel.payment.PaymentFailureViewModel;
import com.til.brainbaazi.viewmodel.payment.PaymentFailureViewModelFactory;
import com.til.brainbaazi.viewmodel.payment.PaymentNavigation;
import com.til.brainbaazi.viewmodel.payment.SuccessViewModel;
import com.til.brainbaazi.viewmodel.payment.SuccessViewModelFactory;
import com.til.brainbaazi.viewmodel.payment.WalletSuccessViewModel;
import com.til.brainbaazi.viewmodel.payment.WalletSuccessViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by prashant.rathore on 05/03/18.
 */
@Module
public class PaymentModule {

    @Provides
    ScreenController<SuccessViewModel> paymentSuccessScreenController(SegmentInfo segmentInfo, SuccessViewModel successViewModel, SuccessScreenFactory successScreenFactory) {
        return new ScreenController<>(segmentInfo, successViewModel, successScreenFactory);
    }

    @Provides
    SuccessViewModel paymentSuccessScreenModel(SuccessViewModelFactory factory, PaymentNavigation paymentNavigation){
        return factory.create(paymentNavigation);
    }

    @Provides
    ScreenController<PaymentFailureViewModel> paymentFailureScreenController(SegmentInfo segmentInfo, PaymentFailureViewModel paymentFailureViewModel, PaymentFailureScreenFactory paymentFailureScreenFactory) {
        return new ScreenController<>(segmentInfo, paymentFailureViewModel, paymentFailureScreenFactory);
    }

    @Provides
    PaymentFailureViewModel paymentFailureScreenModel(PaymentFailureViewModelFactory factory, PaymentNavigation paymentNavigation){
        return factory.create(paymentNavigation);
    }

    @Provides
    ScreenController<MobikiwikWalletModel> mobikiwikWalletModelScreenController(SegmentInfo segmentInfo, MobikiwikWalletModel mobikiwikWalletModel, MobikwikWalletScreenFactory mobikwikWalletScreenFactory) {
        return new ScreenController<>(segmentInfo, mobikiwikWalletModel, mobikwikWalletScreenFactory);
    }

    @Provides
    MobikiwikWalletModel mobikiwikWalletModel(MobikiwikWalletModelFactory factory, PaymentNavigation paymentNavigation){
        return factory.create(paymentNavigation);
    }



    @Provides
    ScreenController<WalletSuccessViewModel> walletSuccessViewModelScreenController(SegmentInfo segmentInfo, WalletSuccessViewModel walletSuccessViewModel, WalletSuccessScreenFactory walletSuccessScreenFactory) {
        return new ScreenController<>(segmentInfo, walletSuccessViewModel, walletSuccessScreenFactory);
    }

    @Provides
    WalletSuccessViewModel walletSuccessViewModel(WalletSuccessViewModelFactory factory, PaymentNavigation paymentNavigation){
        return factory.create(paymentNavigation);
    }

}
