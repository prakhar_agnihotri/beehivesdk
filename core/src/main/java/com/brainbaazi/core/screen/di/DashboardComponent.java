package com.brainbaazi.core.screen.di;

import com.brainbaazi.core.di.BrainBaaziComponent;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.viewmodel.dashboard.DashboardNavigation;
import com.til.brainbaazi.viewmodel.dashboard.DashboardViewModel;
import com.til.brainbaazi.viewmodel.splash.SplashNavigation;
import com.til.brainbaazi.viewmodel.splash.SplashViewModel;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@ScreenScope
@Component(modules = {DashboardModule.class}, dependencies = {BrainBaaziComponent.class})
public interface DashboardComponent {

    ScreenController<DashboardViewModel> screenController();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder segmentInfo(SegmentInfo segmentInfo);
        @BindsInstance
        Builder navigation(DashboardNavigation navigation);
        @BindsInstance
        Builder activityInteractor(ActivityInteractor activityInteractor);
        Builder module(DashboardModule module);
        Builder brainBaaziComponent(BrainBaaziComponent brainBaaziComponent);
        DashboardComponent build();
    }

}
