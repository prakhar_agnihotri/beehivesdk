package com.brainbaazi.core.screen.di;

import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.brainbaazi.component.repo.SocketStore;
import com.til.brainbaazi.interactor.GameEngine;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.gamePlay.LiveGamePlayScreenFactory;
import com.til.brainbaazi.viewmodel.gamePlay.LiveGamePlayViewModel;
import com.til.brainbaazi.viewmodel.gamePlay.LiveGamePlayViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@Module
public class GameModule {

    @Provides
    ScreenController<LiveGamePlayViewModel> screenController(SegmentInfo segmentInfo, LiveGamePlayViewModel viewModel, LiveGamePlayScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    LiveGamePlayViewModel viewModel(LiveGamePlayViewModelFactory factory, ActivityInteractor activityInteractor) {
        return factory.create(activityInteractor);
    }

}
