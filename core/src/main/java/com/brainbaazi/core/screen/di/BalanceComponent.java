package com.brainbaazi.core.screen.di;

import com.brainbaazi.core.di.BrainBaaziComponent;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.viewmodel.balance.BalanceViewModel;
import com.til.brainbaazi.viewmodel.balance.ZeroBalanceViewModel;
import com.til.brainbaazi.viewmodel.other.WebViewModel;
import com.til.brainbaazi.viewmodel.payment.PaymentNavigation;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by prashant.rathore on 20/02/18.
 */
@ScreenScope
@Component(modules = {BalanceModule.class}, dependencies = {BrainBaaziComponent.class})
public interface BalanceComponent {

    ScreenController<BalanceViewModel> balanceScreenController();
    ScreenController<ZeroBalanceViewModel> zeroBalanceScreenController();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder segmentInfo(SegmentInfo segmentInfo);
        @BindsInstance
        Builder activityInteractor(ActivityInteractor activityInteractor);
        @BindsInstance
        Builder navigation(PaymentNavigation navigation);
        Builder module(BalanceModule module);
        Builder brainBaaziComponent(BrainBaaziComponent brainBaaziComponent);
        BalanceComponent build();
    }

}
