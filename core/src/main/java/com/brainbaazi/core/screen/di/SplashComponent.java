package com.brainbaazi.core.screen.di;

import com.brainbaazi.core.di.BrainBaaziComponent;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.other.ForceUpdateScreen;
import com.til.brainbaazi.viewmodel.BaseScreenModel;
import com.til.brainbaazi.viewmodel.BaseViewModel;
import com.til.brainbaazi.viewmodel.splash.MaintenanceViewModel;
import com.til.brainbaazi.viewmodel.splash.SplashNavigation;
import com.til.brainbaazi.viewmodel.splash.SplashViewModel;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by prashant.rathore on 20/02/18.
 */
@ScreenScope
@Component(modules = {SplashModule.class}, dependencies = {BrainBaaziComponent.class})
public interface SplashComponent {

    ScreenController<SplashViewModel> splashController();
    ScreenController<BaseScreenModel> update();
    ScreenController<MaintenanceViewModel> maintenance();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder segmentInfo(SegmentInfo segmentInfo);
        @BindsInstance
        Builder navigation(SplashNavigation splashNavigation);
        Builder splashModule(SplashModule splashModule);
        Builder brainBaaziComponent(BrainBaaziComponent brainBaaziComponent);
        SplashComponent build();
    }

}
