package com.brainbaazi.core.screen.di;

import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.other.ForceUpdateScreenFactory;
import com.til.brainbaazi.screen.other.MaintenanceScreenFactory;
import com.til.brainbaazi.screen.splash.SplashScreenFactory;
import com.til.brainbaazi.viewmodel.BaseScreenModel;
import com.til.brainbaazi.viewmodel.BaseScreenModelFactory;
import com.til.brainbaazi.viewmodel.splash.MaintenanceViewModel;
import com.til.brainbaazi.viewmodel.splash.MaintenanceViewModelFactory;
import com.til.brainbaazi.viewmodel.splash.SplashNavigation;
import com.til.brainbaazi.viewmodel.splash.SplashViewModel;
import com.til.brainbaazi.viewmodel.splash.SplashViewModelFactory;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@Module
public class SplashModule {


    @Provides
    ScreenController<SplashViewModel> splashScreenController(SegmentInfo segmentInfo, SplashViewModel splashViewModel, SplashScreenFactory splashScreenFactory) {
        return new ScreenController<>(segmentInfo, splashViewModel, splashScreenFactory);
    }

    @Provides
    SplashViewModel splashViewModel(SplashViewModelFactory factory, ConnectionManager connectionManager, @MainThreadScheduler Scheduler mainThreadScheduler, DataRepository dataRepository, SplashNavigation navigation, AppHelper appHelper) {
        return factory.create(navigation);
    }

    @Provides
    ScreenController<BaseScreenModel> foreupdateScreenController(SegmentInfo segmentInfo, BaseScreenModel viewModel, ForceUpdateScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    ScreenController<MaintenanceViewModel> maintenance(SegmentInfo segmentInfo, MaintenanceViewModel viewModel, MaintenanceScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    MaintenanceViewModel maintenanceViewModel(MaintenanceViewModelFactory factory ) {
        return factory.create();
    }

    @Provides
    BaseScreenModel baseViewModel(BaseScreenModelFactory factory) {
        return factory.create();
    }

}
