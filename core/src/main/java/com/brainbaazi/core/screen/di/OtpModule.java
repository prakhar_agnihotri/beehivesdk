package com.brainbaazi.core.screen.di;

import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.otp.GenerateOtpScreenFactory;
import com.til.brainbaazi.screen.otp.ProfileScreenFactory;
import com.til.brainbaazi.screen.otp.VerifyOTPScreenFactory;
import com.til.brainbaazi.viewmodel.otp.OtpGenerateViewModel;
import com.til.brainbaazi.viewmodel.otp.OtpGenerateViewModelFactory;
import com.til.brainbaazi.viewmodel.otp.OtpNavigation;
import com.til.brainbaazi.viewmodel.otp.OtpVerifyViewModel;
import com.til.brainbaazi.viewmodel.otp.OtpVerifyViewModelFactory;
import com.til.brainbaazi.viewmodel.otp.ProfileViewModel;
import com.til.brainbaazi.viewmodel.otp.ProfileViewModelFactory;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@Module
public class OtpModule {

    @Provides
    ScreenController<OtpGenerateViewModel> generateOtpScreenController(SegmentInfo segmentInfo, OtpGenerateViewModel viewModel, GenerateOtpScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    OtpGenerateViewModel generateOtpViewModel(OtpGenerateViewModelFactory factory, ActivityInteractor activityInteractor, OtpNavigation otpNavigation) {
        return factory.create(activityInteractor,otpNavigation);
    }

    @Provides
    ScreenController<OtpVerifyViewModel> verifyOtpScreenController(SegmentInfo segmentInfo, OtpVerifyViewModel viewModel, VerifyOTPScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    OtpVerifyViewModel verifyOtpViewModel(OtpVerifyViewModelFactory factory, ActivityInteractor activityInteractor, OtpNavigation otpNavigation) {
        return factory.create(activityInteractor, otpNavigation);
    }

    @Provides
    ScreenController<ProfileViewModel> profileScreenController(SegmentInfo segmentInfo, ProfileViewModel viewModel, ProfileScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    ProfileViewModel profileViewModel(ProfileViewModelFactory factory, ActivityInteractor activityInteractor, OtpNavigation otpNavigation) {
        return factory.create(activityInteractor, otpNavigation);
    }

}
