package com.brainbaazi.core.screen.di;

import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.dashboard.DashboardScreenFactory;
import com.til.brainbaazi.screen.other.WebViewScreen;
import com.til.brainbaazi.screen.other.WebViewScreenFactory;
import com.til.brainbaazi.viewmodel.dashboard.DashboardNavigation;
import com.til.brainbaazi.viewmodel.dashboard.DashboardViewModel;
import com.til.brainbaazi.viewmodel.other.WebViewModel;
import com.til.brainbaazi.viewmodel.other.WebViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@Module
public class WebViewModule {

    @Provides
    ScreenController<WebViewModel> screenController(SegmentInfo segmentInfo, WebViewModel viewModel, WebViewScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    WebViewModel viewModel(WebViewModelFactory factory, ActivityInteractor activityInteractor) {
        return factory.create(activityInteractor);
    }

}
