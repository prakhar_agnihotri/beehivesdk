package com.brainbaazi.core.screen.di;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;

import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.leaderBoard.LeaderBoardContainerScreenFactory;
import com.til.brainbaazi.screen.leaderBoard.LeaderBoardListScreenFactory;
import com.til.brainbaazi.screen.leaderBoard.adapters.LeaderBoardPagerAdapter;
import com.til.brainbaazi.screen.leaderBoard.adapters.LeaderBoardPagerAdapterFactory;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardContainerViewModel;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardContainerViewModelFactory;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardListViewModel;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardListViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@Module
public class LeaderBoardModule {

    @Provides
    ScreenController<LeaderBoardListViewModel> screenController(SegmentInfo segmentInfo, LeaderBoardListViewModel viewModel, LeaderBoardListScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    LeaderBoardListViewModel viewModel(LeaderBoardListViewModelFactory factory) {
        return factory.create();
    }

    @Provides
    LeaderBoardPagerAdapter pagerAdapter(LeaderBoardPagerAdapterFactory factory) {
        return factory.create();
    }

    @Provides
    ScreenController<LeaderBoardContainerViewModel> leaderboardScreenController(SegmentInfo segmentInfo, LeaderBoardContainerViewModel viewModel, LeaderBoardContainerScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    LeaderBoardContainerViewModel leaderboardViewModel(LeaderBoardContainerViewModelFactory factory, ActivityInteractor activityInteractor) {
        return factory.create(activityInteractor);
    }

    @Provides
    LayoutInflater layoutInflater(Activity activity) {
        return LayoutInflater.from(activity);
    }

}
