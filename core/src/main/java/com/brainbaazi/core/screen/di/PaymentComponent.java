package com.brainbaazi.core.screen.di;

import com.brainbaazi.core.di.BrainBaaziComponent;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.viewmodel.payment.MobikiwikWalletModel;
import com.til.brainbaazi.viewmodel.payment.PaymentFailureViewModel;
import com.til.brainbaazi.viewmodel.payment.PaymentNavigation;
import com.til.brainbaazi.viewmodel.payment.SuccessViewModel;
import com.til.brainbaazi.viewmodel.payment.WalletSuccessViewModel;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by prashant.rathore on 20/02/18.
 */
@ScreenScope
@Component(modules = {PaymentModule.class}, dependencies = {BrainBaaziComponent.class})
public interface PaymentComponent {

    ScreenController<SuccessViewModel> paymentSuccessScreenController();

    ScreenController<PaymentFailureViewModel> paymentFailureScreenController();

    ScreenController<MobikiwikWalletModel> mobikiwikWalletController();

    ScreenController<WalletSuccessViewModel> walletSuccesController();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder segmentInfo(SegmentInfo segmentInfo);
        @BindsInstance
        Builder navigation(PaymentNavigation navigation);
        @BindsInstance
        Builder activityInteractor(ActivityInteractor activityInteractor);
        Builder module(PaymentModule module);
        Builder brainBaaziComponent(BrainBaaziComponent brainBaaziComponent);
        PaymentComponent build();
    }

}
