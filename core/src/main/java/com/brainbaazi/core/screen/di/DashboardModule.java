package com.brainbaazi.core.screen.di;

import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.screen.dashboard.DashboardScreenFactory;
import com.til.brainbaazi.viewmodel.dashboard.DashboardNavigation;
import com.til.brainbaazi.viewmodel.dashboard.DashboardViewModel;
import com.til.brainbaazi.viewmodel.dashboard.DashboardViewModelFactory;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@Module
public class DashboardModule {

    @Provides
    ScreenController<DashboardViewModel> screenController(SegmentInfo segmentInfo, DashboardViewModel viewModel, DashboardScreenFactory screenFactory) {
        return new ScreenController<>(segmentInfo, viewModel, screenFactory);
    }

    @Provides
    DashboardViewModel viewModel(DashboardViewModelFactory factory, ActivityInteractor activityInteractor, DashboardNavigation navigation) {
        return factory.create(activityInteractor, navigation);
    }

}
