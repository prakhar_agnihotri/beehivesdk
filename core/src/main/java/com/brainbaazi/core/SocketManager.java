package com.brainbaazi.core;

import android.content.Context;
import android.text.TextUtils;

import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.SocketStore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameEventUtils;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.SubmitAnswer;
import com.til.brainbaazi.entity.game.SubmitPayload;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.entity.game.chat.ChatPayLoad;
import com.til.brainbaazi.entity.game.chat.ChatSendObject;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.LiveGameInfo;
import com.til.brainbaazi.network.socket.NVSocketClient;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by arpit.toshniwal on 25/02/18.
 */

public class SocketManager implements SocketStore {

    private NVSocketClient nvSocketClient;

    private Gson gson;
    private PublishSubject<InputEvent> eventsPublisher = PublishSubject.create();

    public SocketManager(Context context, ConnectionManager connectionManager) {
        gson = new Gson();
        String url = context.getString(R.string.url_socket);
        nvSocketClient = new NVSocketClient(context, url, connectionManager);
        nvSocketClient.observeSocketEvents().subscribe(new Consumer<InputEvent>() {
            @Override
            public void accept(InputEvent inputEvent) throws Exception {
                eventsPublisher.onNext(inputEvent);
            }
        });
    }

    @Override
    public void openSocket(String authToken, User user, GameInfo gameInfo) {
        nvSocketClient.connect(authToken,gameInfo,user);
    }

    @Override
    public void disconnectSocket() {
        disconnect();
    }

    @Override
    public Observable<ChatMsg> observeChatMessages(GameInfo gameInfo) {
        return eventsPublisher.filter(new Predicate<InputEvent>() {
            @Override
            public boolean test(InputEvent inputEvent) throws Exception {
                return inputEvent.type() == InputEvent.TYPE_CHAT;
            }
        }).map(new Function<InputEvent, ChatMsg>() {
            @Override
            public ChatMsg apply(InputEvent inputEvent) throws Exception {
                return (ChatMsg) inputEvent.gameEvent();
            }
        });
    }

    @Override
    public Observable<InputEvent> observeGameEvents() {
        return eventsPublisher.filter(new Predicate<InputEvent>() {
            @Override
            public boolean test(InputEvent inputEvent) throws Exception {
                return inputEvent.type() != InputEvent.TYPE_CHAT;
            }
        });
    }

    @Override
    public void sendMessage(String message) {
        if (nvSocketClient != null)
            nvSocketClient.sendUpStreamMessage(message);
    }

    @Override
    public void sendMessage(SubmitAnswer submitAnswer) {
        Gson gson = new GsonBuilder().registerTypeAdapter(SubmitPayload.class, SubmitPayload.typeAdapter(this.gson)).create();
        TypeAdapter<SubmitAnswer> submitAnswerRequestModelTypeAdapter = SubmitAnswer.typeAdapter(gson);
        String requestBody = submitAnswerRequestModelTypeAdapter.toJson(submitAnswer);
        nvSocketClient.sendUpStreamMessage(requestBody);
    }

    @Override
    public void sendChatMessage(ChatSendObject chatSendObject) {
        Gson gson = new GsonBuilder().registerTypeAdapter(ChatPayLoad.class, ChatPayLoad.typeAdapter(this.gson)).create();
        TypeAdapter<ChatSendObject> chatSendObjectTypeAdapter = ChatSendObject.typeAdapter(gson);
        String requestBody = chatSendObjectTypeAdapter.toJson(chatSendObject);
        nvSocketClient.sendUpStreamMessage(requestBody);
    }

    @Override
    public boolean getSocketStatus() {
        return nvSocketClient.isConnectionOpen();
    }

    private void disconnect() {
        if(nvSocketClient != null) {
            nvSocketClient.disconnect();
        }
    }
}
