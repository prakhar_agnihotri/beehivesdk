package com.brainbaazi.core.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.AnalyticsDevImpl;
import com.brainbaazi.component.di.ComputationThreadScheduler;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.brainbaazi.component.repo.NetworkStore;
import com.brainbaazi.component.repo.SocketStore;
import com.brainbaazi.core.NPConnectionManager;
import com.brainbaazi.core.R;
import com.brainbaazi.core.SocketManager;
import com.brainbaazi.core.BrainBaaziConfig;
import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.analytics.AnalyticsImpl;
import com.til.brainbaazi.interactor.GameEngine;
import com.til.brainbaazi.interactor.repo.DataRepositoryImpl;
import com.til.brainbaazi.network.rest.NetworkStoreImpl;
import com.til.brainbaazi.network.rest.PaymentRequestImpl;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by prashant.rathore on 20/02/18.
 */
@Module
public class BrainBaaziModule {

    @AppScope
    @Provides
    Context applicationContext(Application application) {
        return application;
    }

    @AppScope
    @Provides
    ConnectionManager connectionManager(Context context) {
        return new NPConnectionManager(context);
    }

    @AppScope
    @Provides
    DataRepository dataRepository(@ComputationThreadScheduler Scheduler backgroundScheduler, AppHelper appHelper, NetworkStore networkStore, SocketStore socketStore, SharedPreferences sharedPreferences, GameEngine gameEngine, PaymentRequests paymentRequests) {
        return new DataRepositoryImpl(appHelper, backgroundScheduler,networkStore,socketStore, sharedPreferences,gameEngine, paymentRequests);
    }

    @AppScope
    @Provides
    NetworkStore networkStore(NetworkStoreImpl networkStore) {
        return networkStore;
    }

    @AppScope
    @Provides
    NetworkStoreImpl networkStoreImpl(Context context) {
        return NetworkStoreImpl.getInstance(context);
    }


    @AppScope
    @Provides
    SocketStore socketStore(Context context, ConnectionManager connectionManager) {
        return new SocketManager(context, connectionManager);
    }

    @MainThreadScheduler
    @AppScope
    @Provides
    Scheduler mainThreadScheduler() {
        return AndroidSchedulers.mainThread();
    }


    @ComputationThreadScheduler
    @Provides
    Scheduler computationThreadScheduler() {
        return Schedulers.newThread();
    }

    @AppScope
    @Provides
    Analytics analytics(Context context, BrainBaaziConfig config) {
        return AnalyticsImpl.getInstance(context,config.getAnalyticsListener());
    }

    @AppScope
    @Provides
    PaymentRequests paymentRequest(Context context, NetworkStoreImpl networkStore,AppHelper appHelper) {
        return new PaymentRequestImpl(networkStore,context.getString(R.string.url_payment),appHelper);
    }


}
