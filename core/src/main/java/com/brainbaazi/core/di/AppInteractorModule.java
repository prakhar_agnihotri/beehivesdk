package com.brainbaazi.core.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.auth.PhoneNumberAuthenticatorImpl;
import com.brainbaazi.component.cache.Cache;
import com.brainbaazi.component.cache.CacheImpl;
import com.brainbaazi.component.di.ComputationThreadScheduler;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.core.AppHelperImpl;
import com.brainbaazi.core.DiskCache;
import com.brainbaazi.core.R;
import com.til.brainbaazi.interactor.GameEngine;
import com.til.brainbaazi.interactor.game.BrainBaaziRuleBook;
import com.til.brainbaazi.interactor.game.RuleUtils;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by prashant.rathore on 20/02/18.
 */
@Module
public class AppInteractorModule {

    @AppScope
    @Provides
    PhoneNumberAuthenticator phoneNumberAuthenticator(Context context, @ComputationThreadScheduler Scheduler backgroundScheduler, Cache cache, SharedPreferences sharedPreferences) {
        //return new PhoneNumberAuthenticatorDevImpl();
        return new PhoneNumberAuthenticatorImpl(context, backgroundScheduler, cache, sharedPreferences);
    }

    @AppScope
    @Provides
    Cache cache(Context context) {
        return new DiskCache(context);
    }

    @AppScope
    @Provides
    SharedPreferences sharedPreference(Context context) {
        return context.getSharedPreferences("brainbaazi", Context.MODE_PRIVATE);
    }

    @AppScope
    @Provides
    GameEngine gameEngine(Cache cache, @ComputationThreadScheduler Scheduler backgrounScheduler) {
        return new BrainBaaziRuleBook(cache, backgrounScheduler, RuleUtils.rules());
    }

    @AppScope
    @Provides
    AppHelper appHelper(Context context) {
        return new AppHelperImpl(context);
    }


}
