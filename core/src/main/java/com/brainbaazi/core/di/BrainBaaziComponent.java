package com.brainbaazi.core.di;

import android.app.Application;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.cache.Cache;
import com.brainbaazi.component.di.ComputationThreadScheduler;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.brainbaazi.component.repo.SocketStore;
import com.brainbaazi.core.BrainBaazi;
import com.brainbaazi.core.BrainBaaziConfig;
import com.cookingfox.android.app_lifecycle.api.manager.AppLifecycleManager;
import com.til.brainbaazi.interactor.GameEngine;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import io.reactivex.Scheduler;

/**
 * Created by prashant.rathore on 20/02/18.
 */

@AppScope
@Component(modules = {AndroidSupportInjectionModule.class,BrainBaaziModule.class, AppInteractorModule.class})
public interface BrainBaaziComponent extends AndroidInjector<BrainBaazi> {

    ConnectionManager connectionManager();
    DataRepository dataRepository();

    @MainThreadScheduler
    Scheduler mainThreadScheduler();

    @ComputationThreadScheduler
    Scheduler computationScheduler();

    PhoneNumberAuthenticator phoneNumberAuthenticator();
    GameEngine gameEngine();
    AppHelper appHelper();
    Analytics analytics();
    PaymentRequests paymentRequests();
    SocketStore socketStore();
    Cache cache();

    @dagger.Component.Builder
    abstract class Builder extends AndroidInjector.Builder<BrainBaazi> {
        @BindsInstance
        public abstract Builder application(Application application);

        @BindsInstance
        public abstract Builder applicationLifeCycleManager(AppLifecycleManager lifecycleManager);

        @BindsInstance
        public abstract Builder config(BrainBaaziConfig config);

        @Override
        public abstract BrainBaaziComponent build();
    }

}
