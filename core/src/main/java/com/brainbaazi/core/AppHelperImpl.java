package com.brainbaazi.core;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.Formatter;

import com.brainbaazi.component.repo.AppHelper;
import com.google.gson.Gson;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.utils.AppUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import static android.content.Context.WIFI_SERVICE;

/**
 * Created by arpit.toshniwal on 26/02/18.
 */

public class AppHelperImpl  implements AppHelper{
    private Context applicationContext;

    public AppHelperImpl(Context context){
        this.applicationContext = context.getApplicationContext();
    }

    @Override
    public int getAppVersion() {
        return 81;
    }

    @Override
    public String getDeviceId() {
        try {
            return Settings.Secure.getString(applicationContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String getIpAddress() {
        return getIpAddress(applicationContext);
    }

    @Override
    public BrainbaaziStrings getDefaultBrainBaaziStrings() {
        return BrainbaaziStringDefault.defaultStrings(applicationContext);
    }

    private String getIpAddress(Context applicationContext){
        String ip = getWifiIPAddress(applicationContext.getApplicationContext());
        if (TextUtils.isEmpty(ip) || ip.equalsIgnoreCase("0.0.0.0")) {
            ip = getMobileIPAddress();
        }
        return ip;
    }

    private  String getWifiIPAddress(Context applicationContext) {
        WifiManager wifiMgr = (WifiManager) applicationContext.getApplicationContext().getSystemService(WIFI_SERVICE);
        if(wifiMgr != null) {
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            return Formatter.formatIpAddress(ip);
        }
        return null;
    }

    public  String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return null;
    }
}
