package com.brainbaazi.core;

import android.content.Context;
import android.util.Log;

import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.CommonStrings;
import com.til.brainbaazi.entity.strings.DashboardStrings;
import com.til.brainbaazi.entity.strings.GameplayStrings;
import com.til.brainbaazi.entity.strings.LaunchStrings;
import com.til.brainbaazi.entity.strings.LeaderboardStrings;
import com.til.brainbaazi.entity.strings.MenuStrings;
import com.til.brainbaazi.entity.strings.OtpStrings;
import com.til.brainbaazi.entity.strings.PaymentStrings;
import com.til.brainbaazi.entity.strings.ProfileStrings;

/**
 * Created by prashant.rathore on 04/03/18.
 */

class BrainbaaziStringDefault {

    public static BrainbaaziStrings defaultStrings(Context context) {
        Log.d("BBDEFAULT", "Defaults loaded");
        return BrainbaaziStrings.builder()
                .setCommonStrings(commonStrings(context))
                .setDashboardStrings(dashboardStrings(context))
                .setGameplayStrings(gameplayStrings(context))
                .setLaunchStrings(launchStrings(context))
                .setLeaderboardStrings(leaderboardStrings(context))
                .setMenuStrings(menuStrings(context))
                .setOtpStrings(otpStrings(context))
                .setPaymentStrings(paymentStrings(context))
                .setProfileStrings(profileStrings(context))
                .build();
    }

    private static ProfileStrings profileStrings(Context context) {
        return ProfileStrings
                .builder()
                .setApiFailureMsg(context.getString(R.string.api_failure_msg))
                .setCheckUserName(context.getString(R.string.please_check_your_username))
                .setChoseGalleryText(context.getString(R.string.chose_from_gallery))
                .setEnterOTPText(context.getString(R.string.enter_otp))
                .setEnterPhoneText(context.getString(R.string.enter_phone_number))
                .setFailedUpdateProfilePicture(context.getString(R.string.failed_update_profile_picture))
                .setMaximumCharText(context.getString(R.string.enter_maximum_characters))
                .setMinimumCharText(context.getString(R.string.enter_minimum_characters))
                .setNoSpecialCharText(context.getString(R.string.special_characters_not_allowed))
                .setNoUpperCaseText(context.getString(R.string.uppercase_not_allowed))
                .setProfileText(context.getString(R.string.profile))
                .setReferralInValidText(context.getString(R.string.referral_code_invalid))
                .setReferralValidText(context.getString(R.string.referlCodeValid))
                .setRemoveAvatarText(context.getString(R.string.delete_avatar))
                .setTakeNewPhotoText(context.getString(R.string.take_new_photo))
                .setUploadedText(context.getString(R.string.uploaded_text))
                .setUploadingText(context.getString(R.string.upload_text))
                .setUserAvailableText(context.getString(R.string.userAvailable))
                .setUserNameDigitText(context.getString(R.string.username_can_not_be_all_digits))
                .setUserUnavailableText(context.getString(R.string.userUnavailable))
                .setSuggestionText(context.getString(R.string.profile_suggestion_text))
                .setReferralHintText(context.getString(R.string.refereal_code_optional))
                .setNotValidText(context.getString(R.string.not_valid))
                .setApplyText(context.getString(R.string.apply))
                .setUserNameText(context.getString(R.string.user_name))
                .setFinishText(context.getString(R.string.finish))
                .build();
    }

    private static PaymentStrings paymentStrings(Context context) {
        return PaymentStrings
                .builder()
                .setAmountToRedeemText(context.getString(R.string.amount_to_be_redeemed))
                .setCashOutDialogMessage(context.getString(R.string.cash_out_dialog_message))
                .setCashOutDialogTitle(context.getString(R.string.confirm_cashout_with))
                .setCashoutText(context.getString(R.string.cash_out))
                .setCashOutWithText(context.getString(R.string.cashoutWith))
                .setCouponsText(context.getString(R.string.coupon))
                .setCreateMobikwikText(context.getString(R.string.cashout_create_new_mobikqwik_wallet_text))
                .setCurrentBalanceText(context.getString(R.string.current_balance))
                .setEnterEmailText(context.getString(R.string.cashout_please_enter_your_email))
                .setGoToHomeText(context.getString(R.string.cashout_go_to_home_text))
                .setInsufficientBalanceText(context.getString(R.string.insufficient_balance))
                .setInvalidEmailText(context.getString(R.string.invalidEmail))
                .setMobikwikNoUserText(context.getString(R.string.str_mobikwik_no_user_exist))
                .setMobikwikSuccessText(context.getString(R.string.cashout_successfully_transferred_to_your_mobikwik_wallet))
                .setOrderIdText(context.getString(R.string.cashout_order_id))
                .setPayFailNoUserText(context.getString(R.string.cashout_payment_failed_no_user))
                .setPayFailTransferMobileText(context.getString(R.string.cashout_your_transfer_of_money_failed))//check from here
                .setPaymentErrorMobileText(context.getString(R.string.cashout_payment_error_mobile))
                .setPaymentInitiateText(context.getString(R.string.cashout_payment_request_initiated_text))
                .setProcessRequestText(context.getString(R.string.process_request))
                .setRetryText(context.getString(R.string.retry_text))
                .setSelectPaymentModeText(context.getString(R.string.select_a_payment_mode_to_cashout))
                .setTotalEarningText(context.getString(R.string.total_earnings))
                .setUnableToProcess_requestText(context.getString(R.string.unable_to_process_request))
                .setUpdateBalanceText(context.getString(R.string.cashout_updated_balance))
                .setWalletEmptyText(context.getString(R.string.your_wallet_is_empty))
                .setWinCashText(context.getString(R.string.zero_balance_des))
                .setZeroBalanceText(context.getString(R.string.zero_balance_des))
                .setTransactionInProgressText(context.getString(R.string.tip_text))
                .setTransactionAlreadyInProgressText(context.getString(R.string.tip_already_text))
                .setPaymentFailed(context.getString(R.string.payment_transfer_failed))
                .build();
    }

    private static OtpStrings otpStrings(Context context) {
        return OtpStrings
                .builder()
                .setCodeSentText(context.getString(R.string.code_sent_text))
                .setInvalidPhoneNumber(context.getString(R.string.invalid_phone_number))
                .setEnterSixDigitCodeText(context.getString(R.string.enter_six_digit_code))
                .setEnterPhoneText(context.getString(R.string.enter_phone_number))
                .setEnterOtpText(context.getString(R.string.enter_otp))
                .setInCorrectOTPText(context.getString(R.string.cashout_incorrect_otp))
                .setReadSMSMessageText(context.getString(R.string.read_permisson_sms_description))
                .setResendingOTPText(context.getString(R.string.resending_otp))
                .setReadSMSTitleText(context.getString(R.string.read_sms_permission))
                .setResendOTPInText(context.getString(R.string.resend_code_in_text))
                .setResentCodeText(context.getString(R.string.resend_code_text))
                .setCodeResentToText(context.getString(R.string.code_resent_to_text))
                .setSearchText(context.getString(R.string.search_text))
                .setCountryText(context.getString(R.string.bb_select_your_country))
                .setVerifyingText(context.getString(R.string.verify_text))
                .build();
    }

    private static MenuStrings menuStrings(Context context) {
        return MenuStrings
                .builder()
                .setFAQText(context.getString(R.string.faqs))
                .setHowToPlayText(context.getString(R.string.how_to_play))
                .setPrivacyText(context.getString(R.string.privacy))
                .setRateUsText(context.getString(R.string.rate_us))
                .setRulesText(context.getString(R.string.rules))
                .setShareAppText(context.getString(R.string.bb_share_app))
                .setSignOutText(context.getString(R.string.sign_out))
                .setTermsText(context.getString(R.string.terms))
                .setReferralText(context.getString(R.string.add_referral_code))
                .setAddPictureText(context.getString(R.string.add_profile_picture))
                .setChangePictureText(context.getString(R.string.change_profile_picture))
                .setMoreText(context.getString(R.string.menu_more))
                .setPrivacyPolicyText(context.getString(R.string.privacy_policy))
                .setTermsOfUseText(context.getString(R.string.terms_of_use))
                .build();
    }


    private static LeaderboardStrings leaderboardStrings(Context context) {
        return LeaderboardStrings
                .builder()
                .setAllTimeText(context.getString(R.string.all_time))
                .setThisWeekText(context.getString(R.string.this_week))
                .setWinnerText(context.getString(R.string.winner_label))
                .setWinnersText(context.getString(R.string.winners_label))
                .setPrizeMoneyText(context.getString(R.string.prize_money_text))
                .build();
    }

    private static LaunchStrings launchStrings(Context context) {
        return LaunchStrings
                .builder()
                .setAppUpdateText(context.getString(R.string.the_current_version_of_the_app_is_no_longer_supported_please_update_the_new_version))
                .setUpdateNowText(context.getString(R.string.update_now))
                .setLatestVersionText(context.getString(R.string.get_latest_version))
                .build();
    }

    private static GameplayStrings gameplayStrings(Context context) {
        return GameplayStrings
                .builder()
                .setQaAnswerLockedText(context.getString(R.string.answer_locked))
                .setQaAnswerSubmittedText(context.getString(R.string.answer_submitted_text))
                .setDialogCongratsExtraLife(context.getString(R.string.congrats_you_just_win_text))
                .setCongratsShareFriendsText(context.getString(R.string.congrats_share_friends))
                .setContinuePlayToEarnText(context.getString(R.string.contine_play_earn))
                .setContinueText(context.getString(R.string.continue_text))
                .setContinueWatching(context.getString(R.string.continue_watching_camel_text))
                .setQaCorrectText(context.getString(R.string.correct_popup_text))
                .setLateEliminatedGetExtraLife(context.getString(R.string.eliminatedButYoucanGetText))
                .setQaEliminatedText(context.getString(R.string.eliminated_pop_text))
                .setLateEliminateExtraLifeWinChanceText(context.getString(R.string.extraLivesIncreaseYourChancesText))
                .setDialogGetExtraLifeText(context.getString(R.string.get_extra_life))
                .setGotItText(context.getString(R.string.got_it))
                .setYouHaveExtraLifeText(context.getString(R.string.have_extra_life))
                .setQaInCorrectText(context.getString(R.string.incorrect_popup_text))
                .setQaUserLifeUsedSingleCountText(context.getString(R.string.life_used_single_text))
                .setQaUserLifeUsedMultipleCountText(context.getString(R.string.life_used_multiple_text))
                .setDialogThanksExtraLifeText(context.getString(R.string.thanks_for_staying_with_us_text))
                .setQaTimeUpText(context.getString(R.string.time_up))
                .setYouAreEliminatedText(context.getString(R.string.you_are_eliminated))
                .setYouAreLateTitleMsg(context.getString(R.string.lateText))
                .setYouAreLateTitle(context.getString(R.string.you_are_late_text))
                .setYouWonText(context.getString(R.string.you_won))
                .setTextOnlyModeText(context.getString(R.string.text_only_mode))
                .setWaitQuestionText(context.getString(R.string.wait_question))
                .setWaitAnswerText(context.getString(R.string.wait_answer))
                .setGenerateResultsText(context.getString(R.string.generate_results))
                .setSeeNextGameText(context.getString(R.string.see_next_game))
                .setPreviousQuestionStatsText(context.getString(R.string.prev_ques_stats))
                .setCorrectAnswerText(context.getString(R.string.correct_answer))
                .setIncorrectAnswerText(context.getString(R.string.incorrect_answer))
                .setUsedExtraLifeText(context.getString(R.string.used_extra_life))
                .setStreamNormalText(context.getString(R.string.stream_normal))
                .setStreamDataSaveText(context.getString(R.string.stream_data_saver))
                .setStreamTextOnlyText(context.getString(R.string.stream_text_only))
                .setStreamNormalModeText(context.getString(R.string.stream_normal_mode))
                .setStreamDataSaveModeText(context.getString(R.string.stream_data_saver_mode))
                .setStreamTextOnlyModeText(context.getString(R.string.stream_text_only_mode))
                .setStreamNormalModeDescText(context.getString(R.string.normal_mode_text))
                .setStreamDataSaveModeDescText(context.getString(R.string.data_saver_mode_text))
                .setStreamTextOnlyModeDescText(context.getString(R.string.text_mode_text))
                .setStreamLearnMoreText(context.getString(R.string.learn_more))
                .setStreamRecommandedText(context.getString(R.string.recommended))
                .setDialogStreamingModeText(context.getString(R.string.dialog_streaming_modes))
                .setPersonText(context.getString(R.string.person_text))
                .setPeopleText(context.getString(R.string.people_text))
                .setDataSaverCoachmarkText(context.getString(R.string.data_saver_coach_mark_desc))
                .setSwitchTextOnlyModeText(context.getString(R.string.switch_text_only_mode))
                .setSwitchDataSaverModeText(context.getString(R.string.switch_data_saver_mode))
                .setSetNetworkTimedOutText(context.getString(R.string.network_timed_out))
                .setBackInTheGameText(context.getString(R.string.back_in_the_game))
                .setSwitchDataSaverModeBodyText(context.getString(R.string.switch_data_saver_mode_body))
                .setNoWinnerMessageText(context.getString(R.string.noWinnersScreenMsg))
                .setPrizeMoneyRollMessageText(context.getString(R.string.prizeMoneyRollMsg))
                .setQuestionIndexText(context.getString(R.string.question_q_text))
                .setSwipeLeftToCommentText(context.getString(R.string.swipeToComment))
                .setNoWinnerTitleText(context.getString(R.string.no_winners_text))
                .setSwitchTextOnlyModeBodyText(context.getString(R.string.switch_text_only_mode_body))
                .build();
    }

    private static DashboardStrings dashboardStrings(Context context) {
        return DashboardStrings
                .builder()
                .setBalanceText(context.getString(R.string.your_balance))
                .setExtraLivesText(context.getString(R.string.extra_lives_text))
                .setInviteText(context.getString(R.string.invite))
                .setLeaderBoardText(context.getString(R.string.leaderboard_title_text))
                .setNextGameText(context.getString(R.string.next_game))
                .setPrizeText(context.getString(R.string.prize))
                .setReferralCodeAppliedText(context.getString(R.string.referral_code_applied))
                .setReferralGotLifeText(context.getString(R.string.referral_got_life))
                .setDialogEarnExtraLifeMessage(context.getString(R.string.earn_extra_live_message))
                .setDialogExtraLivesText(context.getString(R.string.extra_lives_text))
                .setDoneText(context.getString(R.string.done))
                .setYourReferralCodeText(context.getString(R.string.yourReferralText))
                .setAddFriendReferralText(context.getString(R.string.add_your_friends_referral_code))
                .setReferralCodeText(context.getString(R.string.referral_code))
                .setAddText(context.getString(R.string.add))
                .setGameLiveNowText(context.getString(R.string.game_live_now))
                .setTypeReferralCodeText(context.getString(R.string.type_referral_code))
                .build();
    }

    private static CommonStrings commonStrings(Context context) {
        return CommonStrings
                .builder()
                .setCancelText(context.getString(R.string.cancel))
                .setInternetNotConnected(context.getString(R.string.internet_not_connected))
                .setOkayText(context.getString(R.string.okay))
                .setShare(context.getString(R.string.share))
                .setFeedBackText(context.getString(R.string.feedback_text))
                .setConfirmText(context.getString(R.string.confirm))
                .setExitDialogGameBeginText(context.getString(R.string.exit_dialog_game_being))
                .setExitDialogGameStartedText(context.getString(R.string.exit_dialog_game))
                .setQuitText(context.getString(R.string.quit))
                .setUpdateText(context.getString(R.string.the_current_version_of_the_app_is_no_longer_supported_please_update_the_new_version))
                .setGetStartedText(context.getString(R.string.get_started))
                .setComeBackAWhileText(context.getString(R.string.come_back_while))
                .build();
    }


}
