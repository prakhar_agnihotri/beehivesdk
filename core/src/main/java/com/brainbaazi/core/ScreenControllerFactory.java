package com.brainbaazi.core;

import android.app.Activity;

import com.brainbaazi.core.screen.di.DaggerBalanceComponent;
import com.brainbaazi.core.screen.di.DaggerDashboardComponent;
import com.brainbaazi.core.screen.di.DaggerGameComponent;
import com.brainbaazi.core.screen.di.DaggerLeaderboardComponent;
import com.brainbaazi.core.screen.di.DaggerOtpComponent;
import com.brainbaazi.core.screen.di.DaggerPaymentComponent;
import com.brainbaazi.core.screen.di.DaggerSplashComponent;
import com.brainbaazi.core.screen.di.DaggerWebViewComponent;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.viewmodel.BaseScreenModel;
import com.til.brainbaazi.viewmodel.dashboard.DashboardNavigation;
import com.til.brainbaazi.viewmodel.dashboard.DashboardViewModel;
import com.til.brainbaazi.viewmodel.gamePlay.LiveGamePlayViewModel;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardContainerViewModel;
import com.til.brainbaazi.viewmodel.other.WebViewModel;
import com.til.brainbaazi.viewmodel.otp.OtpGenerateViewModel;
import com.til.brainbaazi.viewmodel.otp.OtpNavigation;
import com.til.brainbaazi.viewmodel.otp.OtpVerifyViewModel;
import com.til.brainbaazi.viewmodel.otp.ProfileViewModel;
import com.til.brainbaazi.viewmodel.payment.MobikiwikWalletModel;
import com.til.brainbaazi.viewmodel.payment.PaymentFailureViewModel;
import com.til.brainbaazi.viewmodel.payment.PaymentNavigation;
import com.til.brainbaazi.viewmodel.payment.SuccessViewModel;
import com.til.brainbaazi.viewmodel.payment.WalletSuccessViewModel;
import com.til.brainbaazi.viewmodel.splash.MaintenanceViewModel;
import com.til.brainbaazi.viewmodel.splash.SplashNavigation;
import com.til.brainbaazi.viewmodel.splash.SplashViewModel;

/**
 * Created by prashant.rathore on 20/02/18.
 */

public class ScreenControllerFactory {

    public static final int SCREEN_SPLASH = 1;
    public static final int SCREEN_GENERATE_OTP = 2;
    public static final int SCREEN_VERIFY_OTP = 3;
    public static final int SCREEN_DASHBOARD = 4;
    public static final int SCREEN_GAME_PLAY = 5;
    public static final int SCREEN_WEB_VIEW = 6;
    public static final int SCREEN_PROFILE = 7;
    public static final int SCREEN_UPDATE = 8;
    public static final int SCREEN_LEADERBOARD = 9;
    public static final int SCREEN_BALANCE = 10;
    public static final int SCREEN_ZERO_BALANCE = 11;
    public static final int SCREEN_MAINTENANCE = 12;
    public static final int SCREEN_PAYMENT_SUCCESS = 13;
    public static final int SCREEN_PAYMENT_FAILURE = 14;
    public static final int SCREEN_MOBIKIWIK_WALLET = 15;
    public static final int SCREEN_MOBIKIWIK_SUCCESS_WALLET = 16;


    public static ScreenController<SplashViewModel> splash(SegmentInfo segmentInfo, SplashNavigation navigation) {
        return DaggerSplashComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .navigation(navigation)
                .build()
                .splashController();
    }

    public static ScreenController<OtpGenerateViewModel> generateOtp(SegmentInfo segmentInfo, OtpNavigation navigation, ActivityInteractor activityInteractor) {
        return DaggerOtpComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .activityInteractor(activityInteractor)
                .navigation(navigation)
                .build().generateOtpController();
    }

    public static ScreenController<OtpVerifyViewModel> verifyOtp(SegmentInfo segmentInfo, OtpNavigation navigation, ActivityInteractor activityInteractor) {
        return DaggerOtpComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .activityInteractor(activityInteractor)
                .navigation(navigation)
                .build().verifyOtpController();
    }

    public static ScreenController<ProfileViewModel> profile(SegmentInfo segmentInfo, OtpNavigation navigation, ActivityInteractor activityInteractor) {
        return DaggerOtpComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .activityInteractor(activityInteractor)
                .navigation(navigation)
                .build().profileController();
    }

    public static ScreenController<DashboardViewModel> dashboard(SegmentInfo segmentInfo, DashboardNavigation navigation, ActivityInteractor activityInteractor) {
        return DaggerDashboardComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .activityInteractor(activityInteractor)
                .navigation(navigation)
                .build().screenController();
    }

    public static ScreenController<LiveGamePlayViewModel> game(SegmentInfo segmentInfo, ActivityInteractor activityInteractor) {
        return DaggerGameComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .activityInteractor(activityInteractor)
                .build().screenController();
    }

    public static ScreenController<WebViewModel> webView(SegmentInfo segmentInfo, ActivityInteractor activityInteractor) {
        return DaggerWebViewComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .activityInteractor(activityInteractor)
                .build().screenController();
    }

    public static ScreenController<BaseScreenModel> update(SegmentInfo segmentInfo, SplashNavigation navigation) {
        return DaggerSplashComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .navigation(navigation)
                .build()
                .update();
    }

    public static ScreenController<MaintenanceViewModel> maintenance(SegmentInfo segmentInfo, SplashNavigation navigation) {
        return DaggerSplashComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .navigation(navigation)
                .build()
                .maintenance();
    }


    public static ScreenController<LeaderBoardContainerViewModel> leaderBoard(Activity activity, SegmentInfo segmentInfo, ActivityInteractor activityInteractor) {
        return DaggerLeaderboardComponent.builder()
                .segmentInfo(segmentInfo)
                .activity(activity)
                .activityInteractor(activityInteractor)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .build()
                .screenController();
    }


    public static ScreenController zeroBalance(SegmentInfo segmentInfo, ActivityInteractor activityInteractor, PaymentNavigation navigation) {
        return DaggerBalanceComponent.builder()
                .segmentInfo(segmentInfo)
                .navigation(navigation)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .activityInteractor(activityInteractor)
                .build().zeroBalanceScreenController();
    }

    public static ScreenController balance(SegmentInfo segmentInfo, ActivityInteractor activityInteractor, PaymentNavigation navigation) {
        return DaggerBalanceComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .navigation(navigation)
                .activityInteractor(activityInteractor)
                .build().balanceScreenController();
    }

    public static ScreenController<SuccessViewModel> paymentSuccess(SegmentInfo segmentInfo, PaymentNavigation navigation, ActivityInteractor activityInteractor) {
        return DaggerPaymentComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .navigation(navigation)
                .activityInteractor(activityInteractor)
                .build().paymentSuccessScreenController();
    }

    public static ScreenController<PaymentFailureViewModel> paymentFaliure(SegmentInfo segmentInfo, PaymentNavigation navigation, ActivityInteractor activityInteractor) {
        return DaggerPaymentComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .navigation(navigation)
                .activityInteractor(activityInteractor)
                .build().paymentFailureScreenController();
    }

    public static ScreenController<MobikiwikWalletModel> mobikiwikWallet(SegmentInfo segmentInfo, PaymentNavigation navigation, ActivityInteractor activityInteractor) {
        return DaggerPaymentComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .navigation(navigation)
                .activityInteractor(activityInteractor)
                .build().mobikiwikWalletController();
    }

    public static ScreenController<WalletSuccessViewModel> mobikiwikWalletSuccess(SegmentInfo segmentInfo, PaymentNavigation navigation, ActivityInteractor activityInteractor) {
        return DaggerPaymentComponent.builder()
                .segmentInfo(segmentInfo)
                .brainBaaziComponent(BrainBaazi.getComponent())
                .navigation(navigation)
                .activityInteractor(activityInteractor)
                .build().walletSuccesController();
    }
}
