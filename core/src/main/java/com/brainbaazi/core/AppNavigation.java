package com.brainbaazi.core;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.screen.controller.ScreenNavigation;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.balance.BalanceViewModel;
import com.til.brainbaazi.viewmodel.balance.ZeroBalanceViewModel;
import com.til.brainbaazi.viewmodel.dashboard.DashboardNavigation;
import com.til.brainbaazi.viewmodel.gamePlay.LiveGamePlayViewModel;
import com.til.brainbaazi.viewmodel.leaderBoard.LeaderBoardContainerViewModel;
import com.til.brainbaazi.viewmodel.other.WebViewModel;
import com.til.brainbaazi.viewmodel.payment.MobikiwikWalletModel;
import com.til.brainbaazi.viewmodel.payment.PaymentFailureViewModel;
import com.til.brainbaazi.viewmodel.payment.PaymentNavigation;
import com.til.brainbaazi.viewmodel.payment.SuccessViewModel;
import com.til.brainbaazi.viewmodel.payment.WalletSuccessViewModel;

/**
 * Created by prashant.rathore on 23/02/18.
 */

public abstract class AppNavigation extends ScreenNavigation implements DashboardNavigation, PaymentNavigation {

    private final Activity activity;


    public AppNavigation(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void openUserBalanceScreen(User user) {
        Bundle bundle = new Bundle();
        bundle.putByteArray(BalanceViewModel.PARAM_USER, ParcelableUtil.marshall(user));
        startActivity(new SegmentInfo(ScreenControllerFactory.SCREEN_BALANCE, bundle));
    }

    @Override
    public void openZeroBalanceScreen(User user) {
        Bundle bundle = new Bundle();
        bundle.putByteArray(ZeroBalanceViewModel.PARAM_USER, ParcelableUtil.marshall(user));
        startActivity(new SegmentInfo(ScreenControllerFactory.SCREEN_ZERO_BALANCE, bundle));
    }

    @Override
    public void openPlayGameScreen(DashboardInfo dashboardInfo) {
        Bundle param = new Bundle();
        param.putByteArray(LiveGamePlayViewModel.PARAM_DASHBOARD_INFO, ParcelableUtil.marshall(dashboardInfo));
        startActivity(new SegmentInfo(ScreenControllerFactory.SCREEN_GAME_PLAY, param));
    }

    @Override
    public void openLeaderBoard(User user) {
        Bundle param = new Bundle();
        param.putByteArray(LeaderBoardContainerViewModel.PARAM_USER, ParcelableUtil.marshall(user));
        startActivity(new SegmentInfo(ScreenControllerFactory.SCREEN_LEADERBOARD, param));
    }

    @Override
    public void openWebScreen(WebData webData) {
        Bundle param = new Bundle();
        param.putByteArray(WebViewModel.PARAM_WEB_DATA, ParcelableUtil.marshall(webData));
        startActivity(new SegmentInfo(ScreenControllerFactory.SCREEN_WEB_VIEW, param));
    }

    @Override
    public void proceedToSplash() {

    }

    private void startActivity(SegmentInfo segmentInfo) {
        Intent intent = new Intent(activity, BrainBaaziActivity.class);
        intent.putExtra("SEGMENT_INFO", segmentInfo);
        activity.startActivity(intent);
    }

    @Override
    public void openPaymentSuccessScreen(PaymentResponseMessage paymentResponseMessage, String source, int amount) {
        Bundle param = new Bundle();
        param.putString(SuccessViewModel.PARAM_AMOUNT, String.valueOf(amount));
        param.putString(SuccessViewModel.PARAM_SOURCE, source);
        param.putString(SuccessViewModel.PARAM_MESSAGE, paymentResponseMessage.getStatusMessage());
        param.putString(SuccessViewModel.PARAM_ORDER_ID, paymentResponseMessage.getOrderId());
        navigateToScreen(new SegmentInfo(ScreenControllerFactory.SCREEN_PAYMENT_SUCCESS, param));
        clearStack();
    }

    @Override
    public void proceedToHome() {
        activity.finish();
    }



    @Override
    public void openPaymentFailureScreen(PaymentResponseMessage paymentResponseMessage, String source,  User user, String authToken) {
        Bundle param = new Bundle();
        if("159".equalsIgnoreCase(paymentResponseMessage.getStatusCode())){
            param.putInt(PaymentFailureViewModel.PAYMENT_RESULT_CODE, PaymentFailureViewModel.PAYMENT_FAILURE_MOBIWIKI_CREATE_USER);
        }else
            param.putInt(PaymentFailureViewModel.PAYMENT_RESULT_CODE, PaymentFailureViewModel.PAYMENT_FAILURE);
        param.putString(PaymentFailureViewModel.PARAM_SOURCE, source);
        param.putString(PaymentFailureViewModel.AUTH_TOKEN, authToken);
        param.putByteArray(PaymentFailureViewModel.PARAM_USER, ParcelableUtil.marshall(user));
        clearStack();
        navigateToScreen(new SegmentInfo(ScreenControllerFactory.SCREEN_PAYMENT_FAILURE, param));
    }

    @Override
    public void openMobikwikWalletScreen(User user, String authToken) {
        Bundle param = new Bundle();
        param.putByteArray(MobikiwikWalletModel.PARAM_USER, ParcelableUtil.marshall(user));
        param.putString(MobikiwikWalletModel.AUTH_TOKEN, authToken);
        addToBackStack(new SegmentInfo(ScreenControllerFactory.SCREEN_MOBIKIWIK_WALLET, param));
    }

    @Override
    public void openMobikwikWalletSuccessScreen(User user, String authToken) {
        Bundle param = new Bundle();
        param.putString(WalletSuccessViewModel.AUTH_TOKEN, authToken);
        param.putByteArray(WalletSuccessViewModel.PARAM_USER, ParcelableUtil.marshall(user));
        navigateToScreen(new SegmentInfo(ScreenControllerFactory.SCREEN_MOBIKIWIK_SUCCESS_WALLET, param));
        clearStack();
    }

}
