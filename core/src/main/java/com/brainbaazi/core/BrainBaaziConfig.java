package com.brainbaazi.core;

import com.brainbaazi.component.AnalyticsListener;
import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 04/03/18.
 */
@AutoValue
public abstract class BrainBaaziConfig {

    @Nullable
    public abstract AnalyticsListener getAnalyticsListener();

    public abstract String getAppKey();

    public static Builder builder() {
        return new AutoValue_BrainBaaziConfig.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setAnalyticsListener(AnalyticsListener analyticsListener);

        public abstract Builder setAppKey(String appKey);

        public abstract BrainBaaziConfig build();

    }

}
