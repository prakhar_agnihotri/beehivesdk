package com.brainbaazi.core;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.ScreenControllerActivity;
import com.til.brainbaazi.screen.controller.ScreenNavigation;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.viewmodel.BaseViewModel;

/**
 * Created by prashant.rathore on 20/02/18.
 */

public class BrainBaaziActivity extends ScreenControllerActivity {

    private AppNavigation appNavigation = new AppNavigation(this) {

        @Override
        public SegmentInfo navigateToScreen(SegmentInfo segmentInfo) {
            return changeScreen(segmentInfo);
        }
    };


    @Override
    protected SegmentInfo provideDefaultScreenInfo() {
        return null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected ScreenController<? extends BaseViewModel> provideController(SegmentInfo segmentInfo) {
        ScreenController screenController = null;
        switch (segmentInfo.getId()) {
            case ScreenControllerFactory.SCREEN_DASHBOARD:
                screenController = ScreenControllerFactory.dashboard(segmentInfo, appNavigation, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_GAME_PLAY:
                screenController = ScreenControllerFactory.game(segmentInfo, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_WEB_VIEW:
                screenController = ScreenControllerFactory.webView(segmentInfo, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_LEADERBOARD:
                screenController = ScreenControllerFactory.leaderBoard(this,segmentInfo,getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_BALANCE:
                screenController = ScreenControllerFactory.balance(segmentInfo,getActivityInteractor(), appNavigation);
                break;
            case ScreenControllerFactory.SCREEN_ZERO_BALANCE:
                screenController = ScreenControllerFactory.zeroBalance(segmentInfo,getActivityInteractor(), appNavigation);
                break;
            case ScreenControllerFactory.SCREEN_PAYMENT_SUCCESS:
                screenController = ScreenControllerFactory.paymentSuccess(segmentInfo, appNavigation, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_PAYMENT_FAILURE:
                screenController = ScreenControllerFactory.paymentFaliure(segmentInfo, appNavigation, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_MOBIKIWIK_WALLET:
                screenController = ScreenControllerFactory.mobikiwikWallet(segmentInfo, appNavigation, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_MOBIKIWIK_SUCCESS_WALLET:
                screenController = ScreenControllerFactory.mobikiwikWalletSuccess(segmentInfo, appNavigation, getActivityInteractor());
                break;
        }
        return screenController;
    }

    @Override
    protected ScreenNavigation getScreenNavigation() {
        return appNavigation;
    }
}
