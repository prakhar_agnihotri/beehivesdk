package com.brainbaazi.core;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.brainbaazi.component.cache.Cache;
import com.til.brainbaazi.utils.ParcelableUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by prashant.rathore on 01/03/18.
 */

public class DiskCache implements Cache {

    private Context context;
    private File cacheDir;

    public DiskCache(Context context) {
        this.context = context;
        this.cacheDir = context.getCacheDir();
    }

    @Override
    public void saveParcelable(String key, Parcelable parcelable) {
        FileOutputStream fw = null;
        try {
            File file = new File(cacheDir, key);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            byte[] marshall = ParcelableUtil.marshall(parcelable);
            fw = new FileOutputStream(file);
            fw.write(marshall);
            fw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public <T extends Parcelable> T loadParcelable(String key, Parcelable.Creator<T> creator) {
        try {
            File file = new File(cacheDir, key);
            if (file.exists() && file.length() > 0) {
                FileInputStream fis = new FileInputStream(file);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int read;
                byte[] b = new byte[1024];
                while ((read = fis.read(b)) != -1) {
                    baos.write(b,0,read);
                }
                return ParcelableUtil.unmarshall(baos.toByteArray(),creator);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
