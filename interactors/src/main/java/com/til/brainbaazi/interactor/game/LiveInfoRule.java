package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.LiveGameInfo;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class LiveInfoRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        return inputEvent.type() == InputEvent.TYPE_LIVE_INFO && ((LiveGameInfo) inputEvent.gameEvent()).getConcurrentUserCount() > 0;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {
        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;
        GameState updatedState = gameState.toBuilder().setConcurrentUserCount(((LiveGameInfo) inputEvent.gameEvent()).getConcurrentUserCount()).build();
        GameResponse gameResponse = GameResponse.builder()
                .setGameState(updatedState)
                .setEventType((previousState | GameResponse.TYPE_USER_STATE)).build();
        return gameResponse;
    }
}
