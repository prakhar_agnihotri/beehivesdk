package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.GameLiveEvent;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class GameLiveRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        return inputEvent.type() == InputEvent.TYPE_LIVE_TRIGGER;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {
        previousState = previousState & GameResponse.CLEAR_USER_INFO_BITS;
        GameState updatedState = gameState.toBuilder().setGameLive(((GameLiveEvent) inputEvent.gameEvent()).isGameLive()).build();
        GameResponse gameResponse = GameResponse.builder()
                .setGameState(updatedState)
                .setEventType((previousState | GameResponse.TYPE_USER_STATE)).build();
        return gameResponse;
    }
}
