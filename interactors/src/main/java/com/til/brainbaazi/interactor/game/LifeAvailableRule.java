package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class LifeAvailableRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        boolean condition = false;
        if (gameState.getUserPlayState() != GameState.PLAY_STATE_PLAYING) {
            int lives = inputEvent.user().getLives();
            condition = lives > 0 && gameState.getLifeAvailable() != 1;
        }
        return condition;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {

        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;

        GameState newGameState = gameState.toBuilder().setLifeAvailable(1).build();
        GameResponse gameResponse = GameResponse.builder()
                .setEventType((previousState | GameResponse.TYPE_USER_STATE))
                .setGameState(newGameState).build();
        return gameResponse;
    }
}
