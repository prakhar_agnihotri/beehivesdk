package com.til.brainbaazi.interactor.game;

import com.brainbaazi.component.cache.Cache;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;
import com.til.brainbaazi.interactor.GameEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by prashant.rathore on 19/02/18.
 */

public class BrainBaaziRuleBook implements GameEngine {

    private final List<Rule<GameState, InputEvent, GameResponse<?>>> ruleList;
    private final Cache cache;

    private Map<Long, GameItem> gameItemMap = new HashMap<>();

    public BrainBaaziRuleBook(Cache cache, Scheduler ioScheduler, List<Rule<GameState, InputEvent, GameResponse<?>>> ruleList) {
        this.ruleList = ruleList;
        this.cache = cache;
    }

    private void fire(InputEvent inputEvent) {
        GameItem gameItem = getGameItem(inputEvent.gameInfo(), inputEvent.user());
        gameItem.gameProcessEvents.onNext(inputEvent);
    }

    private GameItem getGameItem(GameInfo gameInfo, User user) {
        GameItem gameItem = gameItemMap.get(gameInfo.getCurrentGameID());
        if (gameItem == null) {
            gameItem = new GameItem(gameInfo, user);
            gameItemMap.put(gameInfo.getCurrentGameID(), gameItem);
        }
        return gameItem;
    }

    @Override
    public GameState getGameState(GameInfo gameInfo, User user) {
        GameItem gameItem = getGameItem(gameInfo, user);
        if (gameItem.gameState == null) {
            gameItem.initDefaultGameState();
        }
        return gameItem.gameState;
    }

    @Override
    public boolean hasUserWon(GameInfo gameInfo, User user) {
        GameState gameState = getGameState(gameInfo, user);
        return gameState.getUserPlayState() == GameState.PLAY_STATE_PLAYING;
    }

    @Override
    public boolean hasGameFinished(GameInfo gameInfo, User user) {
        GameState gameState = getGameState(gameInfo, user);
        return !gameState.isGameLive();
    }

    @Override
    public Observable<GameResponse<?>> observeGameEvents(GameInfo gameInfo, User user) {
        GameItem gameItem = getGameItem(gameInfo, user);
        return gameItem.gameResponsePublishSubject;
    }

    @Override
    public void processEvent(InputEvent inputEvent) {
        fire(inputEvent);
    }


    class GameItem {

        final GameInfo gameInfo;
        final User user;
        final String gameCacheKey;

        GameState gameState;
        PublishSubject<InputEvent> gameProcessEvents = PublishSubject.create();
        PublishSubject<GameResponse<?>> gameResponsePublishSubject = PublishSubject.create();

        GameItem(final GameInfo gameInfo, User user) {
            this.gameInfo = gameInfo;
            this.user = user;
            this.gameCacheKey = "Game-" + gameInfo.getCurrentGameID();
            gameProcessEvents.observeOn(Schedulers.newThread()).subscribe(new Consumer<InputEvent>() {
                @Override
                public void accept(InputEvent inputEvent) throws Exception {
                    if (gameState == null) {
                        initDefaultGameState();
                    }
                    processEvent(inputEvent);
                }
            });
        }

        private void initDefaultGameState() {
            gameState = cache.loadParcelable(gameCacheKey, GameState.CREATOR());
            if (gameState == null) {
                gameState = GameState.builderDefault()
                        .setUserPlayState(GameState.PLAY_STATE_PLAYING)
                        .setGameLive(true)
                        .setLifeUsed(0)
                        .setLifeAvailable(user.getLives() > 0 ? 1 : 0)
                        .build();
            }
        }

        void processEvent(InputEvent inputEvent) {
            GameState gameState = this.gameState;
            GameResponse<?> gameResponse = null;
            int previousResponse = 0;
            for (Rule<GameState, InputEvent, GameResponse<?>> rule : ruleList) {
                if (rule.when(gameState, inputEvent)) {
                    gameResponse = rule.then(previousResponse, gameState, inputEvent);
                    gameState = gameResponse.gameState();
                    previousResponse = gameResponse.eventType();
                }
            }

            if (gameResponse != null) {
                this.gameState = gameResponse.gameState();
                gameResponsePublishSubject.onNext(gameResponse);
                cache.saveParcelable(gameCacheKey, this.gameState);
            }
        }
    }
}
