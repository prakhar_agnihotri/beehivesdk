package com.til.brainbaazi.interactor.repo;

import android.content.SharedPreferences;

import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.brainbaazi.component.repo.MemoryStore;
import com.brainbaazi.component.repo.NetworkStore;
import com.brainbaazi.component.repo.SocketStore;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.ReferralResponse;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.UserStatus;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.SubmitAnswerRequestModel;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.SocketDisconnected;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.AmIWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LastGameWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardListModel;
import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.user.UpdateUserImageModel;
import com.til.brainbaazi.entity.user.UserLoginResponse;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;
import com.til.brainbaazi.interactor.GameEngine;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 23/02/18.
 */

public class DataRepositoryImpl implements DataRepository {

    private final Scheduler backgroundScheduler;
    private final AppHelper appHelper;
    private final SharedPreferences sharedPreferences;
    private final GameEngine gameEngine;
    private final PaymentRequests paymentsRequest;
    private NetworkStore networkStore;
    private SocketStore socketStore;
    private MemoryStore memoryStore;

    private BehaviorSubject<UserStatus> userStatusPublisher;
    private BehaviorSubject<DashboardInfo> dashboardInfoPublisher = BehaviorSubject.create();
    private BehaviorSubject<BrainbaaziStrings> brainBaaziStringsPublisher = BehaviorSubject.create();

    static {
        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                throwable.printStackTrace();
            }
        });
    }


    public DataRepositoryImpl(AppHelper appHelper, Scheduler backgrounScheduler, NetworkStore networkStore, SocketStore socketStore, SharedPreferences sharedPreferences, GameEngine gameEngine, PaymentRequests paymentRequests) {
        this.networkStore = networkStore;
        this.socketStore = socketStore;
        this.backgroundScheduler = backgrounScheduler;
        this.appHelper = appHelper;
        this.sharedPreferences = sharedPreferences;
        this.gameEngine = gameEngine;
        this.paymentsRequest = paymentRequests;
        initDefaults();
    }


    private int userStatus() {
        String authToken = getAuthToken();
        String gtoken = sharedPreferences.getString(KEY_GTOKEN, null);
        if (authToken != null && !authToken.isEmpty()) {
            return UserStatus.STATUS_LOGGED_IN;
        } else if (gtoken != null && !gtoken.isEmpty()) {
            return UserStatus.STATUS_UNREGISTERED;
        } else {
            return UserStatus.STATUS_FRESH;
        }
    }

    private void initDefaults() {
        initDefaultBrainBaaziStrings();
        UserStatus defaultUserStatus = UserStatus.builder().setStatus(userStatus()).build();
        userStatusPublisher = BehaviorSubject.createDefault(defaultUserStatus);
        dashboardInfoPublisher.subscribe(new Observer<DashboardInfo>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(DashboardInfo dashboardInfo) {
                GameInfo gameInfo = dashboardInfo.getCurrentGameInfo() != null ? dashboardInfo.getCurrentGameInfo() : dashboardInfo.getNextGameInfo();
                socketStore.openSocket(getAuthToken(), dashboardInfo.getUser(), gameInfo);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
            }
        });

        socketStore.observeGameEvents().subscribe(new Observer<InputEvent>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(InputEvent inputEvent) {
                if (inputEvent.type() == InputEvent.TYPE_SOCKET_DISCONNECTED) {
                    if (((SocketDisconnected) inputEvent.gameEvent()).getErrorCode() == 4001) {
                        logOutUser();
                    }
                } else if (dashboardInfoPublisher.hasValue() && dashboardInfoPublisher.getValue().getCurrentGameInfo() != null) {
                    gameEngine.processEvent(inputEvent);
                }
//                else {
//                    loadDashboardInfo().subscribe();
//                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }

        });
    }

    private void initDefaultBrainBaaziStrings() {
        brainBaaziStringsPublisher.subscribe(new Consumer<BrainbaaziStrings>() {
            @Override
            public void accept(BrainbaaziStrings brainbaaziStrings) throws Exception {
                paymentsRequest.updateBrainbaaziStrings(brainbaaziStrings);
            }
        });
        backgroundScheduler.scheduleDirect(new Runnable() {
            @Override
            public void run() {
                BrainbaaziStrings brainBaaziStrings = appHelper.getDefaultBrainBaaziStrings();
                if (brainBaaziStrings != null) {
                    if (!brainBaaziStringsPublisher.hasValue()) {
                        brainBaaziStringsPublisher.onNext(brainBaaziStrings);
                    }
                }
            }
        });

    }


    @Override
    public void logOutUser() {
        sharedPreferences.edit().remove(KEY_AUTH_TOKEN).apply();
        sharedPreferences.edit().remove(KEY_GTOKEN).apply();
        UserStatus userStatus = UserStatus.builder()
                .setStatus(UserStatus.STATUS_LOGGED_OUT).
                        build();
        userStatusPublisher.onNext(userStatus);
        socketStore.disconnectSocket();
    }

    @Override
    public Observable<Response<User>> loadUserInfo() {
        Observable<User> userObservable = networkStore.loadUserInfo(null);
        return null;
    }

    @Override
    public String getAuthToken() {
        return sharedPreferences.getString(KEY_AUTH_TOKEN, null);
    }

    @Override
    public String getGToken() {
        return sharedPreferences.getString(KEY_GTOKEN, null);
    }

    @Override
    public String getUserName() {
        return null;
    }

    @Override
    public int getChatFrequency() {
        return sharedPreferences.getInt(KEY_CHAT_FREQUENCY, -1);
    }

    @Override
    public String getChatAbusiveText() {
        return sharedPreferences.getString(KEY_CHAT_ABUSIVE_TEXT, null);
    }

    @Override
    public String getSharedPrefString(String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }

    @Override
    public void setSharedPrefString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    @Override
    public Observable<Response<DashboardInfo>> loadDashboardInfo() {
        return networkStore.loadDashboardInfo(sharedPreferences.getString(KEY_AUTH_TOKEN, null))
                .map(new Function<DashboardInfo, Response<DashboardInfo>>() {
                    @Override
                    public Response<DashboardInfo> apply(DashboardInfo dashboardInfoResponse) throws Exception {
                        UserStatus userStatus = UserStatus.builder().setUser(dashboardInfoResponse.getUser()).setStatus(UserStatus.STATUS_LOGGED_IN).build();
                        userStatusPublisher.onNext(userStatus);
                        dashboardInfoPublisher.onNext(dashboardInfoResponse);
                        return Response.<DashboardInfo>builder().setValue(dashboardInfoResponse).setSuccess(true).build();
                    }
                })
                .onErrorReturn(new Function<Throwable, Response<DashboardInfo>>() {
                    @Override
                    public Response<DashboardInfo> apply(Throwable throwable) throws Exception {
                        return Response.builder().setSuccess(false).build();
                    }
                });
    }

    @Override
    public Observable<Response<ReferralResponse>> addReferral(String referralId) {
        return networkStore.addReferral(sharedPreferences.getString(KEY_AUTH_TOKEN, null), referralId)
                .map(new Function<ReferralResponse, Response<ReferralResponse>>() {
                    @Override
                    public Response<ReferralResponse> apply(ReferralResponse referralResponse) throws Exception {
                        return Response.<DashboardInfo>builder().setValue(referralResponse).setSuccess(true).build();
                    }
                })
                .onErrorReturn(new Function<Throwable, Response<ReferralResponse>>() {
                    @Override
                    public Response<ReferralResponse> apply(Throwable throwable) throws Exception {
                        return Response.builder().setSuccess(false).build();
                    }
                });
    }

    @Override
    public Observable<UserStatus> observeUserStatus() {
        return userStatusPublisher;
    }

   /* @Override
    public Observable<Response<UserStatus>> loginUser(String token, UserRequestModel userRequestModel) {
        User user = User.builder()
                .setWeeklyRank(5)
                .setUserName("testUserName")
                .setUserBalance(567)
                .setLives(2)
                .setPhoneNumber(userRequestModel.getMobileNumber())
                .setUserLifeTimeBalance(1231)
                .setTransactionInProgress(false)
                .build();

        sharedPreferences.edit().putString(KEY_AUTH_TOKEN, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiIxYzc4M2RhNDY5ZTlmODIyIiwiaWQiOiIxYzc4M2RhNDY5ZTlmODIyIiwibW9iIjoiKzkxOTk5OTA1NzUzMCIsImxmZSI6MCwidW5tIjoiYnJhaW5iYWF6aTIiLCJpYXQiOjE1MTk1NzUwNDAsImV4cCI6MTUyMjE2NzA0MH0.rX4dV6Yg0SYPu851cl2sUqJomKxHLuPwpBLcHleDs6E")
                .apply();
        UserStatus userStatus = UserStatus.builder().setStatus(UserStatus.STATUS_LOGGED_IN)
                .setUser(user)
                .build();
        Response<UserStatus> build = Response.builder().setSuccess(true).setValue(userStatus).build();
        userStatusPublisher.onNext(userStatus);
        return Observable.just(build).delay(2, TimeUnit.SECONDS);
    }*/


    @Override
    public Observable<Response<UserStatus>> loginUser(String token, UserRequestModel userRequestModel) {
        return networkStore.loginUser(token, userRequestModel).map(new Function<UserLoginResponse, Response<UserStatus>>() {
            @Override
            public Response<UserStatus> apply(UserLoginResponse userLoginResponse) throws Exception {
                boolean userRegistered = userLoginResponse.getRegistered() != null ? userLoginResponse.getRegistered() : true;
                UserStatus userStatus;
                if (userRegistered) {
                    sharedPreferences.edit().putString(KEY_AUTH_TOKEN, userLoginResponse.getAuthToken()).apply();
                    userStatus = UserStatus.builder()
                            .setStatus(UserStatus.STATUS_LOGGED_IN)
                            .build();
                } else {
                    userStatus = UserStatus.builder().setStatus(UserStatus.STATUS_UNREGISTERED)
                            .build();
                }
                userStatusPublisher.onNext(userStatus);
                return Response.builder().setSuccess(true).setValue(userStatus).build();

            }
        }).onErrorReturn(new Function<Throwable, Response<UserStatus>>() {
            @Override
            public Response<UserStatus> apply(Throwable throwable) throws Exception {
                logOutUser();
                return Response.builder().setSuccess(false).setException((Exception) throwable).build();
            }
        });
    }

    @Override
    public Observable<Response<UserStatus>> registerUser(UserRequestModel userRequestModel) {
        String gtoken = sharedPreferences.getString(KEY_GTOKEN, null);
        return networkStore.registerUser(gtoken, userRequestModel).map(new Function<UserLoginResponse, Response<UserStatus>>() {
            @Override
            public Response<UserStatus> apply(UserLoginResponse userLoginResponse) throws Exception {
                boolean userRegistered = userLoginResponse.getRegistered() != null ? userLoginResponse.getRegistered() : true;
                UserStatus userStatus;
                if (userRegistered && userLoginResponse.getAuthToken() != null && !userLoginResponse.getAuthToken().isEmpty()) {
                    sharedPreferences.edit().putString(KEY_AUTH_TOKEN, userLoginResponse.getAuthToken()).apply();
                    userStatus = UserStatus.builder()
                            .setStatus(UserStatus.STATUS_LOGGED_IN)
                            .build();
                    //userStatusPublisher.onNext(userStatus);
                } else {
                    userStatus = UserStatus.builder().setStatus(UserStatus.STATUS_UNREGISTERED)
                            .build();
                }
                return Response.builder().setSuccess(true).setValue(userStatus).build();

            }
        }).onErrorReturn(new Function<Throwable, Response<UserStatus>>() {
            @Override
            public Response<UserStatus> apply(Throwable throwable) throws Exception {
                return Response.builder().setSuccess(false).setException((Exception) throwable).build();
            }
        });
    }

    @Override
    public Observable<ChatMsg> observerChatMessages(GameInfo gameInfo) {
        return socketStore.observeChatMessages(gameInfo);
    }

    @Override
    public Observable<InputEvent> observerGameEvents() {
        return socketStore.observeGameEvents();
    }

    @Override
    public Observable<Response<LastGameWinnerResponse>> loadLastGameWinnerData(String appendUrl) {
        return networkStore.loadLastGameWinnerData(appendUrl, getAuthToken()).map(new Function<LastGameWinnerResponse, Response<LastGameWinnerResponse>>() {
            @Override
            public Response<LastGameWinnerResponse> apply(LastGameWinnerResponse value) throws Exception {
                return Response.<LastGameWinnerResponse>builder().setValue(value).setSuccess(true).build();
            }
        }).onErrorReturn(new Function<Throwable, Response<LastGameWinnerResponse>>() {
            @Override
            public Response<LastGameWinnerResponse> apply(Throwable throwable) throws Exception {
                return Response.builder().setSuccess(false).setException((Exception) (throwable)).build();
            }
        });
    }

    @Override
    public Observable<Response<LeaderBoardListModel>> loadLeaderBoardData() {
        return networkStore.loadLeaderBoardData(getAuthToken()).map(new Function<LeaderBoardListModel, Response<LeaderBoardListModel>>() {
            @Override
            public Response<LeaderBoardListModel> apply(LeaderBoardListModel value) throws Exception {
                return Response.<LeaderBoardListModel>builder().setValue(value).setSuccess(true).build();
            }
        }).onErrorReturn(new Function<Throwable, Response<LeaderBoardListModel>>() {
            @Override
            public Response<LeaderBoardListModel> apply(Throwable throwable) throws Exception {
                return Response.builder().setSuccess(false).setException((Exception) (throwable)).build();
            }
        });
    }

    @Override
    public Observable<Response<CountryListModel>> loadCountryListModel() {
        return networkStore.loadCountryListModel(null).map(new Function<CountryListModel, Response<CountryListModel>>() {
            @Override
            public Response<CountryListModel> apply(CountryListModel value) throws Exception {
                return Response.<CountryListModel>builder().setValue(value).setSuccess(true).build();
            }
        }).onErrorReturn(new Function<Throwable, Response<CountryListModel>>() {
            @Override
            public Response<CountryListModel> apply(Throwable throwable) throws Exception {
                return Response.builder().setSuccess(false).setException((Exception) (throwable)).build();
            }
        });
    }

    @Override
    public Observable<UsernameAvailableResponse> loadUsernameAvailableResponse(String username) {
        String gtoken = sharedPreferences.getString(KEY_GTOKEN, null);
        return networkStore.loadUsernameAvailableResponse(gtoken, username);
    }

    @Override
    public Observable<Response<User>> loadUpdateUserImageResponse(UpdateUserImageModel imageModel) {
        return networkStore.loadUpdateUserImageResponse(getAuthToken(), imageModel).map(new Function<User, Response<User>>() {
            @Override
            public Response<User> apply(User value) throws Exception {
                return Response.<User>builder().setValue(value).setSuccess(true).build();
            }
        }).onErrorReturn(new Function<Throwable, Response<User>>() {
            @Override
            public Response<User> apply(Throwable throwable) throws Exception {
                return Response.builder().setSuccess(false).setException((Exception) (throwable)).build();
            }
        });
    }

    @Override
    public Observable<Response<AppConfig>> loadAppConfig() {
        return networkStore.loadAppConfig().map(new Function<AppConfig, Response<AppConfig>>() {
            @Override
            public Response<AppConfig> apply(AppConfig value) throws Exception {
                if (value != null) {
                    sharedPreferences.edit().putInt(KEY_CHAT_FREQUENCY, value.getChatFrequency()).apply();
                }
                String authToken = sharedPreferences.getString(KEY_AUTH_TOKEN, null);
                if (authToken != null && !authToken.isEmpty()) {
                    value = value.toBuilder().setRegistered(true).build();
                }
                return Response.<AppConfig>builder().setValue(value).setSuccess(true).build();
            }
        }).onErrorReturn(new Function<Throwable, Response<AppConfig>>() {
            @Override
            public Response<AppConfig> apply(Throwable throwable) throws Exception {
                return Response.builder().setSuccess(false).setException((Exception) (throwable)).build();
            }
        });
    }

    @Override
    public Observable<String> submitUserAnswer(SubmitAnswerRequestModel answerRequestModel) {
        return networkStore.submitUserAnswer(getAuthToken(), answerRequestModel);
    }

    @Override
    public Observable<Response<AmIWinnerResponse>> sendWinnerRequest() {
        String gtoken = getAuthToken();
        return networkStore.sendWinnerRequest(gtoken).map(new Function<AmIWinnerResponse, Response<AmIWinnerResponse>>() {
            @Override
            public Response<AmIWinnerResponse> apply(AmIWinnerResponse value) throws Exception {
                return Response.<AmIWinnerResponse>builder().setValue(value).setSuccess(true).build();
            }
        }).onErrorReturn(new Function<Throwable, Response<AmIWinnerResponse>>() {
            @Override
            public Response<AmIWinnerResponse> apply(Throwable throwable) throws Exception {
                return Response.builder().setSuccess(false).setException((Exception) (throwable)).build();
            }
        });
    }

    @Override
    public Observable<Response<Winner>> loadGameWinners() {
        String gtoken = getAuthToken();
        return networkStore.loadGameWinners(gtoken).map(new Function<Winner, Response<Winner>>() {
            @Override
            public Response<Winner> apply(Winner value) throws Exception {
                return Response.<Winner>builder().setValue(value).setSuccess(true).build();
            }
        }).onErrorReturn(new Function<Throwable, Response<Winner>>() {
            @Override
            public Response<Winner> apply(Throwable throwable) throws Exception {
                return Response.builder().setSuccess(false).setException((Exception) (throwable)).build();
            }
        });
    }

    @Override
    public Observable<BrainbaaziStrings> observeBrainBaaziStrings() {
        return brainBaaziStringsPublisher;
    }

    @Override
    public BrainbaaziStrings loadDefaultStrings() {
        return brainBaaziStringsPublisher.hasValue() ? brainBaaziStringsPublisher.getValue() : appHelper.getDefaultBrainBaaziStrings();
    }

    @Override
    public void setBrainbaaziStrings(BrainbaaziStrings brainBaaziStrings) {
        brainBaaziStringsPublisher.onNext(brainBaaziStrings);
    }

    @Override
    public Observable<Boolean> checkChatMessageValid(final String message) {
        return loadAppConfig().flatMap(new Function<Response<AppConfig>, ObservableSource<String>>() {
            @Override
            public ObservableSource<String> apply(Response<AppConfig> appConfigResponse) throws Exception {
                return networkStore.loadAbusiveChatData(appConfigResponse.value());
            }
        }).map(new Function<String, Boolean>() {
            @Override
            public Boolean apply(String matcherString) throws Exception {
                boolean valid = false;
                if (!isEmpty(matcherString) && !isEmpty(message)) {
                    Pattern p = Pattern.compile("(?<=\\|)(" + message + ")(?=\\|)", Pattern.CASE_INSENSITIVE);
                    Matcher matcher = p.matcher(matcherString);
                    valid = !matcher.find();
                }
                return valid;
            }
        }).onErrorReturn(new Function<Throwable, Boolean>() {
            @Override
            public Boolean apply(Throwable throwable) throws Exception {
                return false;
            }
        });
    }

    private boolean isEmpty(String empty) {
        return (empty == null || empty.isEmpty());
    }
}
