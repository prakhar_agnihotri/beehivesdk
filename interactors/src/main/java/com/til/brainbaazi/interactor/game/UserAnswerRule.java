package com.til.brainbaazi.interactor.game;

import com.google.common.collect.ImmutableMap;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.UserAnswer;
import com.til.brainbaazi.entity.game.response.GameResponse;

import java.util.HashMap;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class UserAnswerRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_USER_ANSWERED) {
            UserAnswer answer = (UserAnswer) inputEvent.gameEvent();
            QuestionInfo questionInfo = gameState.getQuestions().get(answer.getQuestionId());
            return questionInfo != null && questionInfo.userAnswered() == -1;
        }
        return false;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {
        UserAnswer answer = (UserAnswer) inputEvent.gameEvent();
        QuestionInfo questionInfo = gameState.getQuestions().get(answer.getQuestionId());

        GameState.Builder gameStateBuilder = gameState.toBuilder();

        QuestionInfo questionInfoUpdated = questionInfo.toBuilder().setUserAnswered(answer.getUserAnswerOption()).build();

        ImmutableMap<Long, QuestionInfo> questions = gameState.getQuestions();
        HashMap<Long, QuestionInfo> map = new HashMap<>(questions);
        map.put(answer.getQuestionId(), questionInfoUpdated);
        ImmutableMap<Long, QuestionInfo> questionsUpdate = ImmutableMap.<Long, QuestionInfo>builder()
                .putAll(map)
                .build();

        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;
        gameStateBuilder.setQuestions(questionsUpdate);
        return GameResponse.builder()
                .setGameState(gameStateBuilder.build())
                .setEventType((previousState | GameResponse.TYPE_USER_STATE)).build();
    }
}
