package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class LateRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        boolean condition = false;
        if (gameState.getUserPlayState() == GameState.PLAY_STATE_PLAYING && inputEvent.trigger()) {
            if (inputEvent.type() == InputEvent.TYPE_QUESTION) {
                Question question = (Question) inputEvent.gameEvent();
                if(question.getQuestionNumber() > gameState.getQuestions().size() + 1) {
                    condition = true;
                }
                else if (gameState.getLastQuestionEvent() == null) {
                    condition = false;
                } else {
                    Question lastQuestion = (Question) gameState.getLastQuestionEvent().gameEvent();
                    Question currentQuestion = (Question) inputEvent.gameEvent();
                    if(currentQuestion.getQuestionNumber() > gameState.getQuestions().size() + 1) {
                        condition = true;
                    }
                    else if (lastQuestion.getQuestionNumber() + 1 < currentQuestion.getQuestionNumber()) {
                        condition = true;
                    }
                }
            } else if (inputEvent.type() == InputEvent.TYPE_ANSWER) {
                if (gameState.getLastQuestionEvent() == null) {
                    condition = true;
                } else {
                    Answer currentAnswer = (Answer) inputEvent.gameEvent();
                    QuestionInfo questionForThisAnswer = gameState.getQuestions().get(currentAnswer.getQuestionId());
                    if (questionForThisAnswer != null && questionForThisAnswer.userAnswered() != -1) {
                        condition = false;
                    } else {
                        condition = true;
                    }
                }
            }
        }
        return condition;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {

        previousState = previousState & GameResponse.CLEAR_USER_INFO_BITS;

        GameState newGameState = gameState.toBuilder().setLate(true).setUserPlayState(GameState.PLAY_STATE_LATE).build();
        GameResponse gameResponse = GameResponse.builder()
                .setGameState(newGameState)
                .setEventType((previousState | GameResponse.TYPE_USER_INFO_LATE)).build();
        return gameResponse;
    }
}
