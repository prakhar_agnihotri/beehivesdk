package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class ShowWinnerRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_WINNER && inputEvent.trigger()) {
            Winner winner = (Winner) inputEvent.gameEvent();
            return (winner != null);
            //return (winner != null && !gameState.isWinnerDisplayed());
        }
        return false;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {
        GameState newGamestate = gameState.toBuilder().setWinnerDisplayed(true).build();
        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;
        return GameResponse.builder()
                .setGameState(newGamestate)
                .setValue(inputEvent.gameEvent())
                .setEventType((previousState | GameResponse.TYPE_SHOW_WINNER))
                .build();
    }
}
