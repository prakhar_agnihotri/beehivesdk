package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.GameLiveEvent;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class GameEndRules extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        return inputEvent.type() == InputEvent.TYPE_GAME_END;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {
        previousState = GameResponse.CLEAR_SHOW_BITS & previousState;
        GameState updatedState = gameState.toBuilder().setGameLive(((GameLiveEvent) inputEvent.gameEvent()).isGameLive()).build();
        GameResponse gameResponse = GameResponse.builder()
                .setGameState(updatedState)
                .setEventType((previousState | GameResponse.TYPE_SHOW_GAME_END)).build();
        return gameResponse;
    }
}
