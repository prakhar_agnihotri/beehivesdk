package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by prashant.rathore on 22/02/18.
 */

public class RuleUtils {

    public static List<Rule<GameState, InputEvent, GameResponse<?>>> rules() {
        List<Rule<GameState, InputEvent, GameResponse<?>>> ruleList = new LinkedList<>();
//        ruleList.add(new UserStateRule());
        ruleList.add(new GameLiveRule());
        ruleList.add(new LiveInfoRule());
        ruleList.add(new LateRule());
        ruleList.add(new LifeAvailableRule());
        ruleList.add(new QuestionRule());
        ruleList.add(new UserAnswerRule());
        ruleList.add(new AnswerRule());
        ruleList.add(new WrongAnswerRule());
        ruleList.add(new ShowAnswerRule());
        ruleList.add(new ShowQuestionRule());
        ruleList.add(new IncludeWinnerRule());
        ruleList.add(new ShowWinnerRule());
        ruleList.add(new GameEndRules());
        return ruleList;
    }
}
