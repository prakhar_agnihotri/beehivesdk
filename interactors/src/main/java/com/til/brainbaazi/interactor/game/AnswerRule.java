package com.til.brainbaazi.interactor.game;

import com.google.common.collect.ImmutableMap;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

import java.util.HashMap;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class AnswerRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_ANSWER) {
            Answer answer = (Answer) inputEvent.gameEvent();
            QuestionInfo questionInfo = gameState.getQuestions().get(answer.getQuestionId());
            return questionInfo != null && questionInfo.answer() == null;
        }
        return false;
    }

    @Override
    public GameResponse<?> then(int previousResponse, GameState gameState, InputEvent inputEvent) {
        Answer answer = (Answer) inputEvent.gameEvent();
        QuestionInfo questionInfo = gameState.getQuestions().get(answer.getQuestionId());

        int newUserState = gameState.getUserPlayState();
        GameState.Builder gameStateBuilder = gameState.toBuilder();

        QuestionInfo questionInfoUpdated = questionInfo.toBuilder().setAnswer(answer).setNewUserState(newUserState).build();

        ImmutableMap<Long, QuestionInfo> questions = gameState.getQuestions();
        HashMap<Long, QuestionInfo> map = new HashMap<>(questions);
        map.put(answer.getQuestionId(), questionInfoUpdated);
        ImmutableMap<Long, QuestionInfo> questionsUpdate = ImmutableMap.<Long, QuestionInfo>builder()
                .putAll(map)
                .build();

        gameStateBuilder.setQuestions(questionsUpdate);

        return GameResponse.builder()
                .setGameState(gameStateBuilder.build())
                .setEventType(previousResponse).build();
    }
}
