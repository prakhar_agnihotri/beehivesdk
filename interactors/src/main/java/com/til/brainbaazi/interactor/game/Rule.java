package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.response.GameResponse;
import com.til.brainbaazi.interactor.GameEngine;

/**
 * Created by prashant.rathore on 19/02/18.
 */

public abstract class Rule<State,Event,Output> {

    public abstract boolean when( State input, Event event);
    public abstract Output then(int previousResponse, State input, Event event);

}
