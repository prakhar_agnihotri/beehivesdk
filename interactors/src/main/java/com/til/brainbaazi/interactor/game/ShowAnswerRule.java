package com.til.brainbaazi.interactor.game;

import com.google.common.collect.ImmutableMap;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

import java.util.HashMap;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class ShowAnswerRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_ANSWER && inputEvent.trigger()) {
            Answer answer = (Answer) inputEvent.gameEvent();
            QuestionInfo questionInfo = gameState.getQuestions().get(answer.getQuestionId());
            return (questionInfo != null && !questionInfo.answerDisplayed());
        }
        return false;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {

        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;

        long id = ((Answer) inputEvent.gameEvent()).getQuestionId();
        QuestionInfo questionInfo = gameState.getQuestions().get(id);
        QuestionInfo questionInfoUpdated = questionInfo.toBuilder().setAnswerDisplayed(true).build();

        ImmutableMap<Long, QuestionInfo> questions = gameState.getQuestions();
        HashMap<Long, QuestionInfo> map = new HashMap<>(questions);
        map.put(id, questionInfoUpdated);
        ImmutableMap<Long, QuestionInfo> questionsUpdate = ImmutableMap.<Long, QuestionInfo>builder()
                .putAll(map)
                .build();

        GameState newGamestate = gameState.toBuilder().setQuestions(questionsUpdate).build();

        return GameResponse.builder()
                .setEventType((previousState | GameResponse.TYPE_SHOW_ANSWER))
                .setValue(newGamestate.getQuestions().get(id))
                .setGameState(newGamestate).build();

    }
}
