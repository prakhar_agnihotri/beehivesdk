package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by saurabh.garg on 27/02/18.
 */

class IncludeWinnerRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_WINNER) {
            Winner winner = (Winner) inputEvent.gameEvent();
            Winner tmpWinner = gameState.getWinner();
            return (tmpWinner == null && winner != null && !winner.isEmpty());
        }
        return false;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {
        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;
        Winner winner = (Winner) inputEvent.gameEvent();
        GameState newGamestate = gameState.toBuilder().setWinner(winner).build();

        return GameResponse.builder()
                .setGameState(newGamestate)
                .setValue(inputEvent.gameEvent())
                .setEventType((previousState | GameResponse.TYPE_USER_STATE))
                .build();
    }
}
