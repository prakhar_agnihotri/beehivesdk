package com.til.brainbaazi.interactor.game;

import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class UserStateRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        return true;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState gameState, InputEvent inputEvent) {
        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;
        GameResponse gameResponse = GameResponse.builder()
                .setGameState(gameState)
                .setEventType((previousState | GameResponse.TYPE_USER_STATE)).build();
        return gameResponse;
    }
}
