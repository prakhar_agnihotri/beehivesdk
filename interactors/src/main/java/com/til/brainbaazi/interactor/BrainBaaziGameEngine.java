package com.til.brainbaazi.interactor;

import com.til.brainbaazi.entity.game.GameData;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.SocketData;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by prashant.rathore on 17/02/18.
 */

public class BrainBaaziGameEngine {

//    private final Scheduler eventProcessorScheduler;
//    private final SocketService socketService;
//    private Map<Long, GameItem> gameEntry = new HashMap<>();
//    private Cache cache;
//
//    public BrainBaaziGameEngine(Scheduler eventProcessorScheduler, SocketService socketService , Cache cache) {
//        this.cache = cache;
//        this.eventProcessorScheduler = eventProcessorScheduler;
//        this.socketService = socketService;
//        observeSocketData();
//    }

//    private void observeSocketData() {
//        socketService.observeSocketData().subscribe(new Observer<SocketData>() {
//            @Override
//            public void onSubscribe(Disposable d) {
//
//            }
//
//            @Override
//            public void onNext(SocketData socketData) {
//                processEvent(socketData.getGameInfo(),socketData.getInputEvent());
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });
//    }
//
//    public Observable<GameResponse<?>> observeGameEvents(GameInfo gameInfo) {
//        return getGameEntry(gameInfo).gameResponseEventsPublisher;
//    }
//
//    public void processEvent(GameInfo gameInfo, InputEvent inputEvent) {
//        getGameEntry(gameInfo).gameInputEventsPublisher.onNext(inputEvent);
//    }
//
//
//    private GameItem getGameEntry(GameInfo gameInfo) {
//        GameItem gameItem = gameEntry.get(gameInfo.getCurrentGameID());
//        if (gameItem == null) {
//            gameItem = new GameItem(gameInfo);
//            this.gameEntry.put(gameInfo.getCurrentGameID(), gameItem);
//        }
//
//        return gameItem;
//    }
//
//    class GameItem {
//        final GameInfo gameInfo;
//        final PublishSubject<GameResponse<?>> gameResponseEventsPublisher;
//        final PublishSubject<InputEvent> gameInputEventsPublisher;
//
//        GameData gameData;
//
//        public GameItem(GameInfo gameInfo) {
//            this.gameInfo = gameInfo;
//            this.gameInputEventsPublisher = PublishSubject.create();
//            this.gameResponseEventsPublisher = PublishSubject.create();
//            setupProcessor();
//        }
//
//        private void initGameData() {
//            if (gameData == null) {
//                GameData cached = (GameData) cache.load("GameData" + gameInfo.getCurrentGameID());
//                if (cached == null) {
//                    cached = new GameData();
//                }
//                gameData = cached;
//            }
//        }
//
//        public void setupProcessor() {
//            this.gameInputEventsPublisher.subscribeOn(eventProcessorScheduler).subscribe(new Observer<InputEvent>() {
//
//                @Override
//                public void onSubscribe(Disposable d) {
//
//                }
//
//                @Override
//                public void onNext(InputEvent inputEvent) {
//                    initGameData();
//                    GameResponse<?> response = gameData.process(inputEvent);
//                    gameResponseEventsPublisher.onNext(response);
//                    if (gameData.isStateModified()) {
//                        GameData clone = gameData.clone();
//                        cache.save(clone);
//                    }
//                }
//
//                @Override
//                public void onError(Throwable e) {
//
//                }
//
//                @Override
//                public void onComplete() {
//
//                }
//            });
//        }
//
//
//    }


}
