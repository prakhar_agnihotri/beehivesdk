package com.til.brainbaazi.interactor.game;

import com.google.common.collect.ImmutableMap;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.response.GameResponse;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class QuestionRule extends Rule<GameState, InputEvent, GameResponse<?>> {
    @Override
    public boolean when(GameState state, InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_QUESTION) {
            Question gameEvent = (Question) inputEvent.gameEvent();
            return state.getQuestions() == null || state.getQuestions().get(gameEvent.getId()) == null;
        }
        return false;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState input, InputEvent inputEvent) {
        ImmutableMap.Builder<Long, QuestionInfo> put = ImmutableMap.builder();
        if (input.getQuestions() != null) {
            put.putAll(input.getQuestions());
        }
        Question question = (Question) inputEvent.gameEvent();
        QuestionInfo questionInfo = QuestionInfo.builder()
                .setQuestion(question)
                .setNewUserState(input.getUserPlayState()).build();
        put.put(question.getId(), questionInfo);


        GameState state = input.toBuilder().setLastQuestionEvent(inputEvent).setQuestions(put.build()).build();

        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;

        GameResponse gameResponse = GameResponse.builder()
                .setGameState(state)
                .setEventType((previousState | GameResponse.TYPE_USER_STATE)).build();
        return gameResponse;
    }
}
