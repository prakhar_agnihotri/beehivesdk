package com.til.brainbaazi.interactor.game;

import com.google.common.collect.ImmutableMap;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.response.GameResponse;

import java.util.HashMap;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class ShowQuestionRule extends Rule<GameState, InputEvent, GameResponse<?>> {
    @Override
    public boolean when(GameState input, InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_QUESTION && inputEvent.trigger()) {
            Question question = (Question) inputEvent.gameEvent();
            return !input.getQuestions().get(question.getId()).questionDisplayed();
        }
        return false;
    }

    @Override
    public GameResponse<?> then(int previousState, GameState input, InputEvent inputEvent) {
        long id = ((Question) inputEvent.gameEvent()).getId();
        QuestionInfo questionInfo = input.getQuestions().get(id);
        QuestionInfo questionInfoUpdated = questionInfo.toBuilder().setQuestionDisplayed(true).build();

        ImmutableMap<Long, QuestionInfo> questions = input.getQuestions();
        HashMap<Long, QuestionInfo> map = new HashMap<>(questions);
        map.put(id, questionInfoUpdated);
        ImmutableMap<Long, QuestionInfo> questionsUpdate = ImmutableMap.<Long, QuestionInfo>builder()
                .putAll(map)
                .build();

        GameState newGamestate = input.toBuilder().setQuestions(questionsUpdate).build();

        previousState = previousState & GameResponse.CLEAR_SHOW_BITS;

        return GameResponse.builder()
                .setGameState(newGamestate)
                .setValue(inputEvent.gameEvent())
                .setEventType((previousState | GameResponse.TYPE_SHOW_QUESTION))
                .build();
    }
}
