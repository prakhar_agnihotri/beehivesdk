package com.til.brainbaazi.interactor.activity;


import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;

/**
 * Created by prashant.rathore on 23/02/18.
 */
@AutoValue
public abstract class ActivityPermissionResult {

//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    public abstract int getRequestCode();
    public abstract ImmutableList<String> getPermissions();
    @SuppressWarnings("mutable")
    public abstract int[] getGrantResult();

    public static Builder builder() {
        return new AutoValue_ActivityPermissionResult.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setRequestCode(int code);
        public abstract Builder setPermissions(String[] permissions);
        public abstract Builder setGrantResult(int[] value);
        public abstract ActivityPermissionResult build();
    }
}
