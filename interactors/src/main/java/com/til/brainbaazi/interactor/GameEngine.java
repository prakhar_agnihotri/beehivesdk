package com.til.brainbaazi.interactor;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

import io.reactivex.Observable;

/**
 * Created by prashant.rathore on 19/02/18.
 */

public interface GameEngine {

    public Observable<GameResponse<?>> observeGameEvents(GameInfo gameInfo, User user);

    public void processEvent(InputEvent inputEvent);

    public GameState getGameState(GameInfo gameInfo, User user);

    public boolean hasUserWon(GameInfo gameInfo, User user);

    public boolean hasGameFinished(GameInfo gameInfo, User user);

}
