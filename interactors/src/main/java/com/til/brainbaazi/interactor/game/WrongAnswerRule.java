package com.til.brainbaazi.interactor.game;

import com.google.common.collect.ImmutableMap;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.response.GameResponse;

import java.util.HashMap;

/**
 * Created by prashant.rathore on 19/02/18.
 */

class WrongAnswerRule extends Rule<GameState, InputEvent, GameResponse<?>> {

    @Override
    public boolean when(GameState gameState, InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_ANSWER
                && inputEvent.trigger()
                && gameState.getUserPlayState() == GameState.PLAY_STATE_PLAYING
                ) {
            Answer answer = (Answer) inputEvent.gameEvent();
            QuestionInfo questionInfo = gameState.getQuestions().get(answer.getQuestionId());
            return questionInfo != null
                    && questionInfo.answer() != null
                    && questionInfo.userAnswered() != answer.getCorrectAnswerId();
        }
        return false;
    }

    @Override
    public GameResponse<?> then(int previousResponse, GameState gameState, InputEvent inputEvent) {
        Answer answer = (Answer) inputEvent.gameEvent();
        QuestionInfo questionInfo = gameState.getQuestions().get(answer.getQuestionId());

        int newUserState = gameState.getUserPlayState();
        GameState.Builder gameStateBuilder = gameState.toBuilder();
        previousResponse = (previousResponse & GameResponse.CLEAR_USER_INFO_BITS);
        if (gameState.getLifeUsed() + 1 > gameState.getLifeAvailable()) {
            gameStateBuilder.setEliminated(true);
            newUserState = GameState.PLAY_STATE_ELIMINATED;
            gameStateBuilder.setUserPlayState(newUserState);
            previousResponse = previousResponse | GameResponse.TYPE_USER_INFO_ELIMINATED;
        } else {
            previousResponse = previousResponse | GameResponse.TYPE_USER_INFO_LIFE_USED;
            gameStateBuilder.setLifeUsed(gameState.getLifeUsed() + 1);
        }

        QuestionInfo questionInfoUpdated = questionInfo.toBuilder().setNewUserState(newUserState).build();
        ImmutableMap<Long, QuestionInfo> questions = gameState.getQuestions();
        HashMap<Long, QuestionInfo> map = new HashMap<>(questions);
        map.put(answer.getQuestionId(), questionInfoUpdated);

        ImmutableMap<Long, QuestionInfo> questionsUpdate = ImmutableMap.<Long, QuestionInfo>builder()
                .putAll(map)
                .build();

        gameStateBuilder.setQuestions(questionsUpdate);

        return GameResponse.builder()
                .setGameState(gameStateBuilder.build())
                .setEventType(previousResponse).build();
    }
}
