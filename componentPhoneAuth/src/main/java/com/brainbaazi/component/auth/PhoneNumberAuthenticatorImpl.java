package com.brainbaazi.component.auth;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.brainbaazi.component.cache.Cache;
import com.brainbaazi.component.repo.DataRepository;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.otp.PhoneNumber;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by prashant.rathore on 21/02/18.
 */

public class PhoneNumberAuthenticatorImpl implements PhoneNumberAuthenticator {

    private final Scheduler operatingSchedueler;
    private final Cache cache;
    private FirebaseAuth firebaseAuth;
    private PublishSubject<OTPResponse> otpResponseBehaviorSubject = PublishSubject.create();
    private PublishSubject<String> smsPublisher = PublishSubject.create();
    private Flowable<OTPResponse> otpResponseObservable;
    private Flowable<String> smsObservable;
    private Scheduler.Worker worker;
    private SharedPreferences sharedPreferences;

    private String CACHE_KEY_FORCE_TOKEN = "FirebaseForceToken";
    private String PREFERENCE_KEY_VALIDATION_ID = "FirebaseValidationID";
    private String PREFERENCE_PHONE_NUMBER = "FirebasePhoneNumber";
    private String LOGGED_IN_USER_IMAGE = "LOGGED_IN_USER_IMAGE";

    //private SMSContentObserver smsContentObserver;
    private Context context;

    public PhoneNumberAuthenticatorImpl(Context context, Scheduler operatingSchedueler, Cache cache, SharedPreferences sharedPreferences) {
        FirebaseApp.initializeApp(context);
        this.context = context;
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.operatingSchedueler = operatingSchedueler;
        this.sharedPreferences = sharedPreferences;
        worker = operatingSchedueler.createWorker();
        this.cache = cache;
        otpResponseObservable = otpResponseBehaviorSubject.subscribeOn(operatingSchedueler).toFlowable(BackpressureStrategy.BUFFER);
        smsObservable = smsPublisher.toFlowable(BackpressureStrategy.BUFFER);
    }


    @Override
    public void verifyOTP(final String otp, final PhoneNumber phoneNumber) {
        worker.schedule(new Runnable() {
            @Override
            public void run() {
                if (firebaseAuth == null) {
                    firebaseAuth = FirebaseAuth.getInstance();
                }
                String verificationID = sharedPreferences.getString(PREFERENCE_KEY_VALIDATION_ID, "");
                PhoneAuthCredential credential1 = PhoneAuthProvider.getCredential(verificationID, otp);
                signInWithCredentials(credential1, phoneNumber);
            }
        });
    }

    @Override
    public Observable<String> observeSMSCodeCallbacks() {
        return smsObservable.toObservable();
    }

    @Override
    public void generateOtp(final PhoneNumber phoneNumber, final boolean isCodeResend) {
        worker.schedule(new Runnable() {
            @Override
            public void run() {
                if (firebaseAuth == null) {
                    firebaseAuth = FirebaseAuth.getInstance();
                }
                registerSmsObserver();
                PhoneAuthProvider.ForceResendingToken forceResendingToken = cache.<PhoneAuthProvider.ForceResendingToken>loadParcelable(CACHE_KEY_FORCE_TOKEN, PhoneAuthProvider.ForceResendingToken.CREATOR);

                Executor executor = Executors.newSingleThreadExecutor();
                PhoneAuthProvider.getInstance()
                        .verifyPhoneNumber(phoneNumber.getPhoneNumber()
                                , 60
                                , TimeUnit.SECONDS
                                , executor
                                , new PhoneAuthCallbacks(phoneNumber, isCodeResend)
                                , forceResendingToken);

                sharedPreferences.edit().putString(PREFERENCE_PHONE_NUMBER, phoneNumber.getPhoneNumber()).apply();

            }
        });
    }

    @Override
    public Observable<OTPResponse> observeOtpCallbacks() {
        return otpResponseObservable.toObservable();
    }

    @Override
    public void uploadUserImage(final Uri filePath) {
        worker.schedule(new Runnable() {
            @Override
            public void run() {
                if (firebaseAuth == null) {
                    firebaseAuth = FirebaseAuth.getInstance();
                }
                StorageReference riversRef = null;
                try {
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageReference = storage.getReference();
                    String userId = firebaseAuth.getCurrentUser().getPhoneNumber();
                    riversRef = storageReference.child(userId + "/images/profilePic.jpg");
                } catch (Exception ignored) {
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageReference = storage.getReference();
                    riversRef = storageReference.child("TMP/images/profilePic.jpg");
                }
                if (riversRef != null) {
                    otpResponseBehaviorSubject.onNext(OTPResponse.uploadStart());
                    riversRef.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            String imageUrl = taskSnapshot.getDownloadUrl().toString();
                            sharedPreferences.edit().putString(LOGGED_IN_USER_IMAGE, imageUrl).apply();
                            otpResponseBehaviorSubject.onNext(OTPResponse.uploadComplete(taskSnapshot.getDownloadUrl().toString(), null));

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            otpResponseBehaviorSubject.onNext(OTPResponse.uploadComplete(null, exception));
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            int progress = (int) ((100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                            otpResponseBehaviorSubject.onNext(OTPResponse.uploadProgress(String.valueOf(progress)));
                        }
                    });
                }
            }
        });
    }

    class PhoneAuthCallbacks extends PhoneAuthProvider.OnVerificationStateChangedCallbacks {

        private final PhoneNumber phoneNumber;
        private final boolean isCodeResend;

        public PhoneAuthCallbacks(PhoneNumber phoneNumber, boolean isCodeResend) {
            this.phoneNumber = phoneNumber;
            this.isCodeResend = isCodeResend;
        }

        @Override
        public void onCodeSent(String verificationId, final PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            otpResponseBehaviorSubject.onNext(OTPResponse.requestSentSuccess(phoneNumber));
            sharedPreferences.edit().putString(PREFERENCE_KEY_VALIDATION_ID, verificationId).apply();
            worker.schedule(new Runnable() {
                @Override
                public void run() {
                    cache.saveParcelable(CACHE_KEY_FORCE_TOKEN, forceResendingToken);
                }
            });
        }

        @Override
        public void onCodeAutoRetrievalTimeOut(String s) {
            super.onCodeAutoRetrievalTimeOut(s);
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            destroySmsObserver();
            if (!TextUtils.isEmpty(phoneAuthCredential.getSmsCode())) {
                smsPublisher.onNext(phoneAuthCredential.getSmsCode());
            }
            otpResponseBehaviorSubject.onNext(OTPResponse.requestSentSuccess(phoneNumber));
            signInWithCredentials(phoneAuthCredential, phoneNumber);
//            otpResponseBehaviorSubject.onNext(OTPResponse.verificationSuccess());
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            e.printStackTrace();
            otpResponseBehaviorSubject.onNext(OTPResponse.verificationFailed(e));
        }
    }

    private void registerSmsObserver() {
        /*if (smsContentObserver == null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || context.checkSelfPermission(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                smsContentObserver = new SMSContentObserver(context);
                Uri uri = Uri.parse("content://sms");
                context.getContentResolver().registerContentObserver(uri, true, smsContentObserver);
            }
        }*/
    }

    private void destroySmsObserver() {
        /*if(smsContentObserver != null) {
            context.getContentResolver().unregisterContentObserver(smsContentObserver);
            smsContentObserver = null;
        }*/
    }

    private void signInWithCredentials(PhoneAuthCredential phoneAuthCredential, PhoneNumber phoneNumber) {
        firebaseAuth.signInWithCredential(phoneAuthCredential)
                .addOnFailureListener(new OnFailuerListenerImpl(phoneNumber))
                .addOnSuccessListener(new OnAuthResultSuccess(phoneNumber));
    }

    class OnAuthResultSuccess implements OnSuccessListener<AuthResult> {

        private final PhoneNumber phoneNumber;

        public OnAuthResultSuccess(PhoneNumber phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        @Override
        public void onSuccess(AuthResult authResult) {
            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
            currentUser.getIdToken(true).addOnSuccessListener(new OnGetTokenSuccessListener(phoneNumber))
                    .addOnFailureListener(new OnFailuerListenerImpl(phoneNumber));
        }
    }

    class OnGetTokenSuccessListener implements OnSuccessListener<GetTokenResult> {

        public OnGetTokenSuccessListener(PhoneNumber phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        private final PhoneNumber phoneNumber;

        @Override
        public void onSuccess(GetTokenResult getTokenResult) {
            sharedPreferences.edit().putString(DataRepository.KEY_GTOKEN, getTokenResult.getToken()).apply();
            otpResponseBehaviorSubject.onNext(OTPResponse.verificationSuccess(getTokenResult.getToken(), phoneNumber));
        }
    }

    class OnFailuerListenerImpl implements OnFailureListener {

        private final PhoneNumber phoneNumber;

        public OnFailuerListenerImpl(PhoneNumber phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        @Override
        public void onFailure(@NonNull Exception e) {
            e.printStackTrace();
            otpResponseBehaviorSubject.onNext(OTPResponse.verificationFailed(e));
        }
    }


    class SMSContentObserver extends ContentObserver {

        private Context mContext;

        public SMSContentObserver(Context context) {
            super(new Handler(Looper.getMainLooper()));
            this.mContext = context;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public void onChange(boolean selfChange, Uri uri) {
            String message = null;
            try {
                Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        int bodyIndex = cursor.getColumnIndex(Telephony.Sms.BODY);
                        message = cursor.getString(bodyIndex);
                        smsPublisher.onNext(message);
                    }
                    cursor.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onChange(selfChange, uri);
        }

    }
}
