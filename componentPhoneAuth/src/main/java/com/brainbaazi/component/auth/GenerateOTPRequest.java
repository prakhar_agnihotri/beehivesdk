package com.brainbaazi.component.auth;

import android.app.Activity;

import com.google.auto.value.AutoValue;
import com.til.brainbaazi.entity.otp.OTPRequest;

/**
 * Created by prashant.rathore on 21/02/18.
 */

@AutoValue
public abstract class GenerateOTPRequest implements OTPRequest {

    public abstract Activity getActivity();

    public abstract String getPhoneNumber();

    public static Builder builder() {
        return new AutoValue_GenerateOTPRequest.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setActivity(Activity activity);

        public abstract Builder setPhoneNumber(String phoneNumber);

        public abstract GenerateOTPRequest build();
    }
}
