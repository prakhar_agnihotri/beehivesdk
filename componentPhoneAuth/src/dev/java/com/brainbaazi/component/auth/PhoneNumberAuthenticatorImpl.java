package com.brainbaazi.component.auth;

import com.til.brainbaazi.entity.game.response.GameResponse;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.otp.PhoneNumber;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;

/**
 * Created by prashant.rathore on 21/02/18.
 */

public class PhoneNumberAuthenticatorImpl implements PhoneNumberAuthenticator {


    @Override
    public void verifyOTP(String otp) {

    }

    @Override
    public void generateOtp(PhoneNumber phoneNumber) {

    }

    @Override
    public Observable<OTPResponse> observeOtpCallbacks() {
        Observable<Long> interval = Observable.interval(2, TimeUnit.SECONDS);
        BiFunction<OTPResponse, Long, OTPResponse> biFunction = new BiFunction<OTPResponse, Long, OTPResponse>() {
            @Override
            public OTPResponse apply(OTPResponse gameResponse, Long aLong) throws Exception {
                return gameResponse;
            }
        };
        return Observable.just(OTPResponse.verificationSuccess("temp token"))
                .zipWith(interval, biFunction);
    }
}
