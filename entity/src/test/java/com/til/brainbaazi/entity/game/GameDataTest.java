package com.til.brainbaazi.entity.game;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.event.QuestionOptions;
import com.til.brainbaazi.entity.game.response.GameResponse;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by prashant.rathore on 18/02/18.
 */
public class GameDataTest {

    private GameData gameData;

    @org.junit.Before
    public void setUp() throws Exception {
        gameData = new GameData();
    }

    @Test
    public void testUserWithZeroLivesOnStart() throws Exception {
        User build = User.builder().setLives(0).setUserBalance("0").setUserLifeTimeBalance("0").setUserImgUrl("").setUserName("User with zero life").setPhoneNumber("123456789").setWeeklyRank("1").build();
        GameResponse<?> userState = gameData.getLiveGameInfo(build);
        assertEquals(GameData.PLAY_STATE_PLAYING, userState.userPlayState());
        assertFalse(userState.lifeAvailable());
    }

    @Test
    public void testUserWithMoreThan1LivesOnStart() throws Exception {
        User build = User.builder().setLives(1).setUserBalance("0").setUserLifeTimeBalance("0").setUserImgUrl("").setUserName("User with zero life").setPhoneNumber("123456789").setWeeklyRank("2").build();
        GameResponse<?> userState = gameData.getLiveGameInfo(build);
        assertEquals(GameData.PLAY_STATE_PLAYING, userState.userPlayState());
        assertTrue(userState.lifeAvailable());
    }

    @Test
    public void testQuestionEventFromSocket() throws Exception {
        User user = User.builder().setLives(0).setUserBalance("0").setUserLifeTimeBalance("0").setUserImgUrl("").setUserName("User with zero life").setPhoneNumber("123456789").setWeeklyRank("3").build();

        Question question = Question.builder().setId(123).setQuestionOptions(generateQuestionOptions()).setText("Question").build();
        InputEvent inputEvent = InputEvent.builder().setUser(user).setTrigger(false).setType(InputEvent.TYPE_QUESTION).setGameEvent(question).build();
        GameResponse<?> userState = gameData.process(inputEvent);
        assertEquals(InputEvent.TYPE_USER_STATE, userState.eventType());
        assertEquals(GameData.PLAY_STATE_PLAYING, userState.userPlayState());
    }


    @Test
    public void testQuestionEventFromVideoStream() throws Exception {
        User user = User.builder().setLives(0).setUserBalance("0").setUserLifeTimeBalance("0").setUserImgUrl("").setUserName("User with zero life").setPhoneNumber("123456789").setWeeklyRank("4").build();

        Question question = Question.builder().setId(123).setQuestionOptions(generateQuestionOptions()).setText("Question").build();

        InputEvent inputEvent = InputEvent.builder().setUser(user).setTrigger(true).setType(InputEvent.TYPE_QUESTION).setGameEvent(question).build();
        GameResponse<?> userState = gameData.process(inputEvent);

        assertEquals(InputEvent.TYPE_QUESTION, userState.eventType());
        assertEquals(GameData.PLAY_STATE_PLAYING, userState.userPlayState());
    }

    private QuestionOptions[] generateQuestionOptions() {
        QuestionOptions opt1 = QuestionOptions.builder().setOptionId(1).setPosition(1).setText("Option 1").build();
        QuestionOptions opt2 = QuestionOptions.builder().setOptionId(2).setPosition(2).setText("Option 2").build();
        QuestionOptions opt3 = QuestionOptions.builder().setOptionId(3).setPosition(3).setText("Option 3").build();
        return new QuestionOptions[]{opt1, opt2, opt3};
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

}