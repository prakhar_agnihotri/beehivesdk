package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class LaunchStrings implements Parcelable {

    public abstract String latestVersionText();

    public abstract String appUpdateText();

    public abstract String updateNowText();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_LaunchStrings.Builder();
    }

    public static TypeAdapter<LaunchStrings> typeAdapter(Gson gson) {
        return new AutoValue_LaunchStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setLatestVersionText(String value);

        public abstract Builder setAppUpdateText(String value);

        public abstract Builder setUpdateNowText(String value);

        public abstract LaunchStrings build();

    }

}
