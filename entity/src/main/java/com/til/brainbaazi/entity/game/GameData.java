package com.til.brainbaazi.entity.game;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.response.GameResponse;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by prashant.rathore on 17/02/18.
 */

public class GameData implements Serializable {

//    public static final int PLAY_STATE_PLAYING = 0;
//    public static final int PLAY_STATE_LATE = 1;
//    public static final int PLAY_STATE_ELIMINATED = 2;
//    public static final int PLAY_STATE_PLAYING_WITH_LIFE = 3;
//
//    private InputEvent lastQuestionEvent;
//    private Map<Long, QuestionInfo> questions;
//
//    private boolean late;
//    private boolean lifeUsed;
//    private boolean eliminated;
//
//    private int userPlayState = PLAY_STATE_PLAYING;
//
//    private String concurrentUserCount;
//    private boolean isLive = true;
//
//    private boolean stateModified;
//
//    public GameResponse<?> process(InputEvent inputEvent) {
//        switch (inputEvent.type()) {
//            case InputEvent.TYPE_QUESTION: {
//                return handleQuestionEvent(inputEvent);
//            }
//            case InputEvent.TYPE_ANSWER: {
//                return handleAnswerEvent(inputEvent);
//            }
//            case InputEvent.TYPE_WINNER: {
//                return handleAnswerEvent(inputEvent);
//            }
//            case InputEvent.TYPE_LIVE_DATA: {
//                return handleAnswerEvent(inputEvent);
//            }
//        }
//        return getLiveGameInfo(inputEvent.user());
//    }
//
//    public GameResponse<?> getLiveGameInfo(User user) {
//        return this.generateResponse(user).setValue(null).setEventType(InputEvent.TYPE_USER_STATE).build();
//    }
//
//    private <T> GameResponse.Builder<T> generateResponse(User user) {
//        return GameResponse.<T>builder().setConcurrentUserCount(concurrentUserCount)
//                .setUserPlayState(userPlayState)
//                .setConcurrentUserCount(this.concurrentUserCount)
//                .setLifeAvailable(isLifeAvailable(user))
//                .setGameLive(isLive);
//    }
//
//    private boolean isLifeAvailable(User user) {
//        return !lifeUsed && user.getLives() > 0 && isUserInPlayMode();
//    }
//
//    private GameResponse<?> handleAnswerEvent(InputEvent inputEvent) {
//        Answer answer = (Answer) inputEvent.gameEvent();
//        Question question = (Question) lastQuestionEvent.gameEvent();
//        if (answer.getQuestionId() == ((Question) lastQuestionEvent.gameEvent()).getId()) {
//            this.lastQuestionEvent = null;
//            QuestionInfo questionInfo = questions.get(question.getId());
//            if (questionInfo != null) {
//                QuestionInfo.Builder builder = questionInfo.toBuilder();
//                if (questionInfo.userAnswered() != answer.getQuestionId()) {
//                    if (inputEvent.user().getLives() == 0 || lifeUsed) {
//                        this.eliminated = true;
//                    } else {
//                        lifeUsed = true;
//                    }
//                }
//                updateUserPlayState();
//                builder.setNewUserState(userPlayState);
//                this.questions.put(question.getId(), builder.setAnswer(answer).build());
//            } else {
//                // Case if user did not answer the question and auto submission failed;
//                this.late = true;
//                updateUserPlayState();
//
//                questionInfo = QuestionInfo.builder()
//                        .setAnswer(answer)
//                        .setQuestion((Question) lastQuestionEvent.gameEvent())
//                        .setUserAnswered(-2)
//                        .setNewUserState(userPlayState)
//                        .build();
//                this.questions.put(question.getId(), questionInfo);
//
//            }
//            return this.<QuestionInfo>generateResponse(inputEvent.user()).setEventType(InputEvent.TYPE_ANSWER).setValue(questionInfo).build();
//        }
//        return getLiveGameInfo(inputEvent.user());
//    }
//
//    private GameResponse<?> handleQuestionEvent(InputEvent inputEvent) {
//        if (lastQuestionEvent == null) {
//            lastQuestionEvent = inputEvent;
//            stateModified = true;
//        } else {
//            Question questionEvent = (Question) inputEvent.gameEvent();
//            if (!questionEvent.equals(lastQuestionEvent.gameEvent())) {
//                // This should never occur. If this happens it means user never answered to question.
//                // and auto answer failed too.
//                QuestionInfo build = QuestionInfo.builder()
//                        .setAnswer(null)
//                        .setQuestion((Question) lastQuestionEvent.gameEvent())
//                        .setUserAnswered(-1)
//                        .build();
//                if (late) {
//                    late = true;
//                }
//                this.questions.put(questionEvent.getId(), build);
//                this.lastQuestionEvent = null;
//                updateUserPlayState();
//                stateModified = true;
//            }
//        }
//
//        if (inputEvent.trigger() && lastQuestionEvent != null) {
//            return this.<Question>generateResponse(inputEvent.user()).setEventType(lastQuestionEvent.type()).setValue((Question) lastQuestionEvent.gameEvent()).build();
//        } else {
//            return getLiveGameInfo(inputEvent.user());
//        }
//    }
//
//    private boolean isUserInPlayMode() {
//        return this.userPlayState == PLAY_STATE_PLAYING
//                || this.userPlayState != PLAY_STATE_PLAYING_WITH_LIFE;
//    }
//
//    private void updateUserPlayState() {
//        if (isUserInPlayMode()) {
//            if (this.eliminated) {
//                userPlayState = PLAY_STATE_ELIMINATED;
//            } else if (this.late) {
//                userPlayState = PLAY_STATE_LATE;
//            } else if (lifeUsed) {
//                userPlayState = PLAY_STATE_PLAYING_WITH_LIFE;
//            } else {
//                userPlayState = PLAY_STATE_PLAYING;
//            }
//        }
//    }
//
//    @Override
//    public GameData clone() {
//        GameData gameData = new GameData();
//        gameData.lastQuestionEvent = lastQuestionEvent;
//        gameData.questions.putAll(gameData.questions);
//        gameData.late = late;
//        gameData.lifeUsed = lifeUsed;
//        gameData.eliminated = eliminated;
//        gameData.userPlayState = userPlayState;
//        gameData.concurrentUserCount = concurrentUserCount;
//        gameData.isLive = isLive;
//        return gameData;
//    }
//
//    public Map<Long, QuestionInfo> getQuestions() {
//        return questions;
//    }
//
//    public boolean isStateModified() {
//        return stateModified;
//    }
}
