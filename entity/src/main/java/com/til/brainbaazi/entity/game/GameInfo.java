package com.til.brainbaazi.entity.game;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by prashant.rathore on 15/02/18.
 */

@AutoValue
public abstract class GameInfo implements Parcelable {

    @SerializedName("id")
    public abstract long getCurrentGameID();

    @SerializedName("tm")
    public abstract long getTimeStamp();

    @SerializedName("pmt")
    public abstract int getGamePrize();

    @SerializedName("surl")
    public abstract String getStreamingUrl();

    public static Builder builder() {
        return new AutoValue_GameInfo.Builder().setGamePrize(0);
    }

    public static TypeAdapter<GameInfo> typeAdapter(Gson gson) {
        return new AutoValue_GameInfo.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setCurrentGameID(long currentGameID);

        public abstract Builder setTimeStamp(long timeStamp);

        public abstract Builder setGamePrize(int gamePrize);

        public abstract Builder setStreamingUrl(String url);

        public abstract GameInfo build();
    }


}
