package com.til.brainbaazi.entity.game.event;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class AnswerOptions implements Parcelable {

    public abstract boolean correctOption();
    public abstract double count();
    public abstract String optionId();
    public abstract String text();
    public abstract int position();


    public static Builder builder() {
        return new AutoValue_AnswerOptions.Builder().setCorrectOption(false);
    }

    public static final Parcelable.Creator<AutoValue_AnswerOptions> CREATOR() {
        return AutoValue_AnswerOptions.CREATOR;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setCorrectOption(boolean correctOption);
        public abstract Builder setCount(double count);
        public abstract Builder setOptionId(String optionId);
        public abstract Builder setText(String text);
        public abstract Builder setPosition(int  position);

        public abstract AnswerOptions build();

    }

}
