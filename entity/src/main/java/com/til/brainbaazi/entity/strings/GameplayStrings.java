package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class GameplayStrings implements Parcelable {

    public abstract String continueWatching();

    public abstract String youAreLateTitle();

    public abstract String youAreLateTitleMsg();

    public abstract String continueText();

    public abstract String dialogCongratsExtraLife();

    public abstract String dialogThanksExtraLifeText();

    public abstract String youAreEliminatedText();

    public abstract String continuePlayToEarnText();

    public abstract String dialogGetExtraLifeText();

    public abstract String qaUserLifeUsedSingleCountText();

    public abstract String qaUserLifeUsedMultipleCountText();

    public abstract String qaAnswerLockedText();

    public abstract String qaTimeUpText();

    public abstract String qaAnswerSubmittedText();

    public abstract String qaEliminatedText();

    public abstract String qaCorrectText();

    public abstract String qaInCorrectText();

    public abstract String gotItText();

    public abstract String youHaveExtraLifeText();

    public abstract String lateEliminatedGetExtraLife();

    public abstract String lateEliminateExtraLifeWinChanceText();

    public abstract String youWonText();

    public abstract String congratsShareFriendsText();

    public abstract String textOnlyModeText();

    public abstract String waitQuestionText();

    public abstract String waitAnswerText();

    public abstract String generateResultsText();

    public abstract String seeNextGameText();

    public abstract String previousQuestionStatsText();

    public abstract String correctAnswerText();

    public abstract String incorrectAnswerText();

    public abstract String usedExtraLifeText();

    public abstract String streamNormalText();

    public abstract String streamDataSaveText();

    public abstract String streamTextOnlyText();

    public abstract String streamNormalModeText();

    public abstract String streamDataSaveModeText();

    public abstract String streamTextOnlyModeText();

    public abstract String streamNormalModeDescText();

    public abstract String streamDataSaveModeDescText();

    public abstract String streamTextOnlyModeDescText();

    public abstract String streamLearnMoreText();

    public abstract String streamRecommandedText();

    public abstract String dialogStreamingModeText();

    public abstract String personText();

    public abstract String peopleText();

    public abstract String dataSaverCoachmarkText();

    public abstract String switchTextOnlyModeText();

    public abstract String switchDataSaverModeText();

    public abstract String setNetworkTimedOutText();

    public abstract String backInTheGameText();

    public abstract String switchDataSaverModeBodyText();

    public abstract String switchTextOnlyModeBodyText();

    public abstract String prizeMoneyRollMessageText();

    public abstract String noWinnerMessageText();

    public abstract String noWinnerTitleText();

    public abstract String questionIndexText();

    public abstract String swipeLeftToCommentText();

    //public abstract String Text();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_GameplayStrings.Builder();
    }

    public static TypeAdapter<GameplayStrings> typeAdapter(Gson gson) {
        return new AutoValue_GameplayStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setContinueWatching(String value);

        public abstract Builder setYouAreLateTitle(String value);

        public abstract Builder setYouAreLateTitleMsg(String value);

        public abstract Builder setContinueText(String value);

        public abstract Builder setDialogCongratsExtraLife(String value);

        public abstract Builder setDialogThanksExtraLifeText(String value);

        public abstract Builder setYouAreEliminatedText(String value);

        public abstract Builder setContinuePlayToEarnText(String value);

        public abstract Builder setDialogGetExtraLifeText(String value);

        public abstract Builder setQaUserLifeUsedSingleCountText(String value);

        public abstract Builder setQaUserLifeUsedMultipleCountText(String value);

        public abstract Builder setQaAnswerLockedText(String value);

        public abstract Builder setQaTimeUpText(String value);

        public abstract Builder setQaAnswerSubmittedText(String value);

        public abstract Builder setQaEliminatedText(String value);

        public abstract Builder setQaCorrectText(String value);

        public abstract Builder setQaInCorrectText(String value);

        public abstract Builder setGotItText(String value);

        public abstract Builder setYouHaveExtraLifeText(String value);

        public abstract Builder setLateEliminatedGetExtraLife(String value);

        public abstract Builder setLateEliminateExtraLifeWinChanceText(String value);

        public abstract Builder setYouWonText(String value);

        public abstract Builder setCongratsShareFriendsText(String value);

        public abstract Builder setTextOnlyModeText(String value);

        public abstract Builder setWaitQuestionText(String value);

        public abstract Builder setWaitAnswerText(String value);

        public abstract Builder setGenerateResultsText(String value);

        public abstract Builder setSeeNextGameText(String value);

        public abstract Builder setPreviousQuestionStatsText(String value);

        public abstract Builder setCorrectAnswerText(String value);

        public abstract Builder setIncorrectAnswerText(String value);

        public abstract Builder setUsedExtraLifeText(String value);

        public abstract Builder setStreamNormalText(String value);

        public abstract Builder setStreamDataSaveText(String value);

        public abstract Builder setStreamTextOnlyText(String value);

        public abstract Builder setStreamNormalModeText(String value);

        public abstract Builder setStreamDataSaveModeText(String value);

        public abstract Builder setStreamTextOnlyModeText(String value);

        public abstract Builder setStreamNormalModeDescText(String value);

        public abstract Builder setStreamDataSaveModeDescText(String value);

        public abstract Builder setStreamTextOnlyModeDescText(String value);

        public abstract Builder setStreamLearnMoreText(String value);

        public abstract Builder setStreamRecommandedText(String value);

        public abstract Builder setDialogStreamingModeText(String value);

        public abstract Builder setPersonText(String value);

        public abstract Builder setPeopleText(String value);

        public abstract Builder setDataSaverCoachmarkText(String value);

        public abstract Builder setSwitchTextOnlyModeText(String value);

        public abstract Builder setSwitchDataSaverModeText(String value);

        public abstract Builder setSwitchTextOnlyModeBodyText(String value);

        public abstract Builder setSetNetworkTimedOutText(String value);

        public abstract Builder setBackInTheGameText(String value);

        public abstract Builder setSwitchDataSaverModeBodyText(String value);

        public abstract Builder setPrizeMoneyRollMessageText(String value);

        public abstract Builder setNoWinnerMessageText(String value);

        public abstract Builder setNoWinnerTitleText(String value);

        public abstract Builder setQuestionIndexText(String value);

        public abstract Builder setSwipeLeftToCommentText(String value);

        public abstract GameplayStrings build();

    }

}
