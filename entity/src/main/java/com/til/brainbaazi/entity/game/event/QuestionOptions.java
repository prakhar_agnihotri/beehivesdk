package com.til.brainbaazi.entity.game.event;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class QuestionOptions implements Parcelable {

    public abstract String optionId();
    public abstract String text();
    public abstract int position();

    public static Builder builder() {
        return new AutoValue_QuestionOptions.Builder();
    }

    public static final Parcelable.Creator<AutoValue_QuestionOptions> CREATOR() {
        return AutoValue_QuestionOptions.CREATOR;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setOptionId(String optionId);
        public abstract Builder setText(String text);
        public abstract Builder setPosition(int  position);

        public abstract QuestionOptions build();
    }

}
