package com.til.brainbaazi.entity.game.event;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class KickedEvent implements GameEvent {

    public abstract String getKickedList();

    public static Builder builder() {
        return new AutoValue_KickedEvent.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setKickedList(String kickedList);
        
        public abstract KickedEvent build();
    }
}
