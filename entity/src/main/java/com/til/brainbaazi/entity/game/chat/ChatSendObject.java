package com.til.brainbaazi.entity.game.chat;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by saurabh.garg on 2/27/18.
 */

@AutoValue
public abstract class ChatSendObject implements Serializable {

    @SerializedName("a")
    public abstract String getAction();

    @SerializedName("e")
    public abstract String getEvent();

    @SerializedName("p")
    public abstract ChatPayLoad getPayLoad();

    public static Builder builder() {
        return new AutoValue_ChatSendObject.Builder();
    }

    public static TypeAdapter<ChatSendObject> typeAdapter(Gson gson) {
        return new AutoValue_ChatSendObject.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setAction(String action);

        public abstract Builder setEvent(String event);

        public abstract Builder setPayLoad(ChatPayLoad chatPayLoad);

        public abstract ChatSendObject build();
    }
}
