package com.til.brainbaazi.entity.game;

import com.google.auto.value.AutoValue;
import com.til.brainbaazi.entity.game.event.InputEvent;

/**
 * Created by prashant.rathore on 18/02/18.
 */

@AutoValue
public abstract class SocketData {

    public abstract GameInfo getGameInfo();
    public abstract InputEvent getInputEvent();

    public static Builder builder() {
        return new AutoValue_SocketData.Builder();
    }


    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setGameInfo(GameInfo gameInfo);
        public abstract Builder setInputEvent(InputEvent inputEvent);

        public abstract SocketData build();
    }

}
