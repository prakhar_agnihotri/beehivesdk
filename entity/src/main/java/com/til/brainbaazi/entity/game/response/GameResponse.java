package com.til.brainbaazi.entity.game.response;

import com.google.auto.value.AutoValue;
import com.til.brainbaazi.entity.game.GameState;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 18/02/18.
 */

@AutoValue
public abstract class GameResponse<T> {

    public static final int CLEAR_SHOW_BITS      = 0x000111;
    public static final int CLEAR_USER_INFO_BITS = 0x111000;

    public static final int MASK_SHOW_BITS      = 0x111000;
    public static final int MASK_USER_INFO_BITS = 0x000111;

    public static final int TYPE_USER_STATE             = 0x000000;
    public static final int TYPE_SHOW_QUESTION          = 0x010000;
    public static final int TYPE_SHOW_ANSWER            = 0x001000;
    public static final int TYPE_SHOW_WINNER            = 0x011000;
    public static final int TYPE_SHOW_GAME_END          = 0x111000;
    public static final int TYPE_USER_INFO_KICKED_OUT   = 0x000100;
    public static final int TYPE_USER_INFO_LATE         = 0x000010;
    public static final int TYPE_USER_INFO_LIFE_USED    = 0x000001;
    public static final int TYPE_USER_INFO_ELIMINATED   = 0x000011;

    public abstract int eventType();

    @Nullable
    public abstract T value();

    public abstract GameState gameState();

    public static <T> Builder<T> builder() {
        return new AutoValue_GameResponse.Builder<T>();
    }

    @AutoValue.Builder
    public abstract static class Builder<T> {
        public abstract Builder<T> setEventType(int type);
        public abstract Builder<T> setValue(T value);
        public abstract Builder<T> setGameState(GameState gameState);

        public abstract GameResponse<T> build();
    }

}
