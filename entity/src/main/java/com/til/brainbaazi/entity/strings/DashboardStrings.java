package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class DashboardStrings implements Parcelable {

    public abstract String nextGameText();

    public abstract String leaderBoardText();

    public abstract String inviteText();

    public abstract String balanceText();

    public abstract String extraLivesText();

    public abstract String prizeText();

    public abstract String referralCodeAppliedText();

    public abstract String referralGotLifeText();

    public abstract String doneText();

    public abstract String dialogExtraLivesText();

    public abstract String dialogEarnExtraLifeMessage();

    public abstract String yourReferralCodeText();

    public abstract String addFriendReferralText();

    public abstract String referralCodeText();

    public abstract String addText();

    public abstract String gameLiveNowText();

    public abstract String typeReferralCodeText();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_DashboardStrings.Builder();
    }

    public static TypeAdapter<DashboardStrings> typeAdapter(Gson gson) {
        return new AutoValue_DashboardStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setExtraLivesText(String value);

        public abstract Builder setNextGameText(String value);

        public abstract Builder setLeaderBoardText(String value);

        public abstract Builder setInviteText(String value);

        public abstract Builder setBalanceText(String value);

        public abstract Builder setPrizeText(String value);

        public abstract Builder setReferralCodeAppliedText(String value);

        public abstract Builder setReferralGotLifeText(String value);

        public abstract Builder setDoneText(String value);

        public abstract Builder setDialogExtraLivesText(String value);

        public abstract Builder setDialogEarnExtraLifeMessage(String value);

        public abstract Builder setYourReferralCodeText(String value);

        public abstract Builder setAddFriendReferralText(String value);

        public abstract Builder setReferralCodeText(String value);

        public abstract Builder setAddText(String value);

        public abstract Builder setGameLiveNowText(String value);

        public abstract Builder setTypeReferralCodeText(String value);

        public abstract DashboardStrings build();

    }

}
