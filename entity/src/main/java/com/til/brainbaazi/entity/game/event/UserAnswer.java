package com.til.brainbaazi.entity.game.event;

import com.google.auto.value.AutoValue;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class UserAnswer implements GameEvent {

    public abstract long getQuestionId();

    public abstract int getUserAnswerOption();

    public static Builder builder() {
        return new AutoValue_UserAnswer.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setUserAnswerOption(int userAnswerOption);

        public abstract Builder setQuestionId(long questionId);

        public abstract UserAnswer build();

    }


}
