package com.til.brainbaazi.entity.game;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.Question;

import java.io.Serializable;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class QuestionInfo implements Parcelable {

    public abstract int userAnswered();

    @Nullable
    public abstract Answer answer();

    public abstract Question question();

    public abstract int newUserState();

    public abstract boolean questionDisplayed();

    public abstract boolean answerDisplayed();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_QuestionInfo.Builder().setUserAnswered(-1).setQuestionDisplayed(false).setAnswerDisplayed(false);
    }

    public static final Parcelable.Creator<AutoValue_QuestionInfo> CREATOR() {
        return AutoValue_QuestionInfo.CREATOR;
    }


    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setUserAnswered(int value);

        public abstract Builder setAnswer(Answer answer);

        public abstract Builder setQuestion(Question value);

        public abstract Builder setNewUserState(int log);

        public abstract Builder setQuestionDisplayed(boolean questionDisplayed);

        public abstract Builder setAnswerDisplayed(boolean answerDisplayed);

        public abstract QuestionInfo build();
    }
}
