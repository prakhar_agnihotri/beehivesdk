package com.til.brainbaazi.entity.typeadapter;

import android.os.Parcel;

import com.google.common.collect.ImmutableList;
import com.ryanharter.auto.value.parcel.TypeAdapter;
import com.til.brainbaazi.entity.game.event.QuestionOptions;
import com.til.brainbaazi.entity.game.event.WinUser;

/**
 * Created by prashant.rathore on 01/03/18.
 */

public class WinnersListAdapter implements TypeAdapter<ImmutableList<WinUser>> {
    @Override
    public ImmutableList<WinUser> fromParcel(Parcel in) {
        WinUser[] typedArray = in.createTypedArray(WinUser.CREATOR());
        return ImmutableList.<WinUser>builder().add(typedArray).build();
    }

    @Override
    public void toParcel(ImmutableList<WinUser> value, Parcel dest) {
        dest.writeTypedArray(value.toArray(QuestionOptions.CREATOR().newArray(value.size())),0);
    }

}
