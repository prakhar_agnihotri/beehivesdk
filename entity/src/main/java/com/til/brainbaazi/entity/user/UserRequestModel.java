package com.til.brainbaazi.entity.user;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.BufferedWriter;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 2/24/18.
 */
@AutoValue
public abstract class UserRequestModel {

    @Nullable
    @SerializedName("unm")
    public abstract String getUserName();

    @SerializedName("dtp")
    public abstract String getDeviceType();

    @SerializedName("did")
    public abstract String getDeviceId();

    @SerializedName("cid")
    public abstract String getCountryCode();

    @Nullable
    @SerializedName("uim")
    public abstract String getProfileImageUrl();

    @Nullable
    @SerializedName("aqs")
    public abstract String getAcquistionSource();

    @Nullable
    @SerializedName("rid")
    public abstract String getReferralId();

    @SerializedName("mob")
    public abstract String getMobileNumber();

    public static Builder builder() {
        return new AutoValue_UserRequestModel.Builder();
    }

    public static TypeAdapter<UserRequestModel> typeAdapter(Gson gson) {
        return new AutoValue_UserRequestModel.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setUserName(String userName);

        public abstract Builder setDeviceType(String deviceType);

        public abstract Builder setDeviceId(String deviceId);

        public abstract Builder setProfileImageUrl(String profileImageUrl);

        public abstract Builder setAcquistionSource(String acquistion_source);

        public abstract Builder setReferralId(String referralId);

        public abstract Builder setMobileNumber(String mobileNumber);

        public abstract Builder setCountryCode(String countryCode);

        public abstract UserRequestModel build();
    }
}
