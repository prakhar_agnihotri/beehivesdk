package com.til.brainbaazi.entity.user;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.AnswerOptions;

import javax.annotation.Nullable;

/**
 * Created by saurabh.garg on 2/24/18.
 */

@AutoValue
public abstract class UsernameAvailableResponse {

    public static final int CHECK_USERNAME = 1;
    public static final int CHECK_REFERRAL = 3;

    public abstract boolean isStatus();

    public abstract int getCheckType();

    @Nullable
    public abstract ImmutableList<String> getSuggestions();

    public static Builder builder() {
        return new AutoValue_UsernameAvailableResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setStatus(boolean status);

        public abstract Builder setCheckType(int checkType);

        public abstract Builder setSuggestions(String... suggestions);

        public abstract UsernameAvailableResponse build();
    }
}
