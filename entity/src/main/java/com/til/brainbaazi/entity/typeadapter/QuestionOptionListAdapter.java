package com.til.brainbaazi.entity.typeadapter;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.collect.ImmutableList;
import com.ryanharter.auto.value.parcel.TypeAdapter;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.QuestionOptions;
import com.til.brainbaazi.entity.otp.PhoneNumber;

/**
 * Created by prashant.rathore on 01/03/18.
 */

public class QuestionOptionListAdapter implements TypeAdapter<ImmutableList<QuestionOptions>> {
    @Override
    public ImmutableList<QuestionOptions> fromParcel(Parcel in) {
        QuestionOptions[] typedArray = in.createTypedArray(QuestionOptions.CREATOR());
        return ImmutableList.<QuestionOptions>builder().add(typedArray).build();
    }

    @Override
    public void toParcel(ImmutableList<QuestionOptions> value, Parcel dest) {
        dest.writeTypedArray(value.toArray(QuestionOptions.CREATOR().newArray(value.size())),0);
    }

}
