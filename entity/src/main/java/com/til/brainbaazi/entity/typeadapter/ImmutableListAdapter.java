package com.til.brainbaazi.entity.typeadapter;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.collect.ImmutableList;
import com.ryanharter.auto.value.parcel.TypeAdapter;

/**
 * Created by prashant.rathore on 01/03/18.
 */

public abstract class ImmutableListAdapter<T extends Parcelable> implements TypeAdapter<ImmutableList<T>> {


    protected abstract Parcelable.Creator<T> getCreator();

    @Override
    public ImmutableList<T> fromParcel(Parcel in) {
        T[] typedArray = in.createTypedArray(getCreator());
        return ImmutableList.<T>builder().add(typedArray).build();
    }

    @Override
    public void toParcel(ImmutableList<T> value, Parcel dest) {
        dest.writeTypedArray(getCreator().newArray(value.size()),0);
    }

}
