package com.til.brainbaazi.entity;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 21/02/18.
 */

@AutoValue
public abstract class LoginResponse {

    public abstract UserStatus getUserStatus();

    public abstract boolean isSuccess();

    @Nullable
    public abstract Exception getException();

    public static Builder builder() {
        return new AutoValue_LoginResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setUserStatus(UserStatus userStatus);

        public abstract Builder setSuccess(boolean success);

        public abstract Builder setException(Exception exception);

        public abstract LoginResponse build();
    }


}
