package com.til.brainbaazi.entity.user;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 2/27/18.
 */

@AutoValue
public abstract class UpdateUserImageModel {

    @Nullable
    @SerializedName("uim")
    public abstract String getUim();

    public static Builder builder() {
        return new AutoValue_UpdateUserImageModel.Builder();
    }

    public static TypeAdapter<UpdateUserImageModel> typeAdapter(Gson gson) {
        return new AutoValue_UpdateUserImageModel.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setUim(String uim);

        public abstract UpdateUserImageModel build();
    }
}
