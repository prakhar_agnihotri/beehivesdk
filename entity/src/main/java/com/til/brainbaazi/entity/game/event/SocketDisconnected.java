package com.til.brainbaazi.entity.game.event;

import com.google.auto.value.AutoValue;

/**
 * Created by saurabh.garg on 2/16/18.
 */

@AutoValue
public abstract class SocketDisconnected implements GameEvent {

    public abstract int getErrorCode();

    public static Builder builder() {
        return new AutoValue_SocketDisconnected.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setErrorCode(int errorCode);

        public abstract SocketDisconnected build();

    }
}
