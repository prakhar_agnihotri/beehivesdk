package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class OtpStrings implements Parcelable {

    public abstract String countryText();

    public abstract String enterSixDigitCodeText();

    public abstract String invalidPhoneNumber();

    public abstract String resendOTPInText();

    public abstract String resentCodeText();

    public abstract String codeResentToText();

    public abstract String codeSentText();

    public abstract String readSMSTitleText();

    public abstract String readSMSMessageText();

    public abstract String verifyingText();

    public abstract String resendingOTPText();

    public abstract String inCorrectOTPText();

    public abstract String searchText();

    public abstract String enterPhoneText();

    public abstract String enterOtpText();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_OtpStrings.Builder();
    }

    public static TypeAdapter<OtpStrings> typeAdapter(Gson gson) {
        return new AutoValue_OtpStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setCountryText(String value);

        public abstract Builder setEnterSixDigitCodeText(String value);

        public abstract Builder setInvalidPhoneNumber(String value);

        public abstract Builder setResendOTPInText(String value);

        public abstract Builder setResentCodeText(String value);

        public abstract Builder setCodeResentToText(String value);

        public abstract Builder setCodeSentText(String value);

        public abstract Builder setVerifyingText(String value);

        public abstract Builder setResendingOTPText(String value);

        public abstract Builder setInCorrectOTPText(String value);

        public abstract Builder setReadSMSTitleText(String value);

        public abstract Builder setReadSMSMessageText(String value);

        public abstract Builder setSearchText(String value);

        public abstract Builder setEnterPhoneText(String value);

        public abstract Builder setEnterOtpText(String value);

        public abstract OtpStrings build();
    }

}
