package com.til.brainbaazi.entity.game.chat;

import com.google.auto.value.AutoValue;
import com.til.brainbaazi.entity.game.event.GameEvent;

import java.io.Serializable;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class ChatMsg implements GameEvent {

    public abstract String getMessage();

    public abstract String getName();

    public abstract String getImgUrl();

    public static Builder builder() {
        return new AutoValue_ChatMsg.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setMessage(String message);

        public abstract Builder setName(String name);

        public abstract Builder setImgUrl(String imgUrl);

        public abstract ChatMsg build();
    }
}
