package com.til.brainbaazi.entity.otp;

/**
 * Created by prashant.rathore on 15/02/18.
 */

public class OTPResponse {

    public static final int EVENT_SEND = 1;
    public static final int EVENT_VERIFY = 2;

    public static final int EVENT_UPLOAD_START = 3;
    public static final int EVENT_UPLOAD_PROGRESS = 4;
    public static final int EVENT_UPLOAD_COMPLETE = 5;

    public final int event;
    public final boolean success;
    public final Exception exception;
    public final String token;
    public final PhoneNumber phoneNumber;


    private OTPResponse(int event, boolean success, Exception exception, String token, PhoneNumber phoneNumber) {
        this.event = event;
        this.success = success;
        this.exception = exception;
        this.token = token;
        this.phoneNumber = phoneNumber;
    }

    public static OTPResponse verificationSuccess(String token,PhoneNumber phoneNumber) {
        return new OTPResponse(EVENT_VERIFY,true,null,token, phoneNumber);
    }

    public static OTPResponse verificationFailed(Exception exception) {
        return new OTPResponse(EVENT_VERIFY,false,exception,null, null);
    }

    public static OTPResponse requestSentSuccess(PhoneNumber phoneNumber) {
        return new OTPResponse(EVENT_SEND,true,null,null, phoneNumber);
    }

    public static OTPResponse requestSentFailed(Exception exception) {
        return new OTPResponse(EVENT_SEND,false,exception,null, null);
    }

    public static OTPResponse uploadStart() {
        return new OTPResponse(EVENT_UPLOAD_START, true, null, null , null);
    }

    public static OTPResponse uploadProgress(String progress) {
        return new OTPResponse(EVENT_UPLOAD_PROGRESS, true, null, progress, null);
    }

    public static OTPResponse uploadComplete(String token, Exception ex) {
        return new OTPResponse(EVENT_UPLOAD_COMPLETE, (token != null), ex, token , null);
    }
}
