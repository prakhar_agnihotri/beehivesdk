package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class ProfileStrings implements Parcelable {

    public abstract String userNameDigitText();

    public abstract String noUpperCaseText();

    public abstract String noSpecialCharText();

    public abstract String checkUserName();

    public abstract String minimumCharText();

    public abstract String maximumCharText();

    public abstract String takeNewPhotoText();

    public abstract String choseGalleryText();

    public abstract String removeAvatarText();

    public abstract String profileText();

    public abstract String enterPhoneText();

    public abstract String enterOTPText();

    public abstract String apiFailureMsg();

    public abstract String failedUpdateProfilePicture();

    public abstract String uploadingText();

    public abstract String uploadedText();

    public abstract String userAvailableText();

    public abstract String userUnavailableText();

    public abstract String referralValidText();

    public abstract String referralInValidText();

    public abstract String suggestionText();

    public abstract String referralHintText();

    public abstract String notValidText();

    public abstract String applyText();

    public abstract String userNameText();

    public abstract String finishText();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_ProfileStrings.Builder();
    }

    public static TypeAdapter<ProfileStrings> typeAdapter(Gson gson) {
        return new AutoValue_ProfileStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setUserNameDigitText(String value);

        public abstract Builder setNoUpperCaseText(String value);

        public abstract Builder setNoSpecialCharText(String value);

        public abstract Builder setCheckUserName(String value);

        public abstract Builder setMinimumCharText(String value);

        public abstract Builder setMaximumCharText(String value);

        public abstract Builder setTakeNewPhotoText(String value);

        public abstract Builder setChoseGalleryText(String value);

        public abstract Builder setRemoveAvatarText(String value);

        public abstract Builder setProfileText(String value);

        public abstract Builder setEnterPhoneText(String value);

        public abstract Builder setEnterOTPText(String value);

        public abstract Builder setApiFailureMsg(String value);

        public abstract Builder setFailedUpdateProfilePicture(String value);

        public abstract Builder setUploadingText(String value);

        public abstract Builder setUploadedText(String value);

        public abstract Builder setUserAvailableText(String value);

        public abstract Builder setUserUnavailableText(String value);

        public abstract Builder setReferralValidText(String value);

        public abstract Builder setReferralInValidText(String value);

        public abstract Builder setSuggestionText(String value);

        public abstract Builder setReferralHintText(String value);

        public abstract Builder setNotValidText(String value);

        public abstract Builder setApplyText(String value);

        public abstract Builder setUserNameText(String value);

        public abstract Builder setFinishText(String value);

        public abstract ProfileStrings build();

    }

}
