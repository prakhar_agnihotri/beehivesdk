package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class PaymentStrings implements Parcelable {

    public abstract String amountToRedeemText();

    public abstract String selectPaymentModeText();

    public abstract String cashOutWithText();

    public abstract String couponsText();

    public abstract String walletEmptyText();

    public abstract String winCashText();

    public abstract String currentBalanceText();

    public abstract String totalEarningText();

    public abstract String insufficientBalanceText();

    public abstract String cashOutDialogTitle();

    public abstract String cashOutDialogMessage();

    public abstract String paymentInitiateText();

    public abstract String updateBalanceText();

    public abstract String goToHomeText();

    public abstract String paymentErrorMobileText();

    public abstract String orderIdText();

    public abstract String payFailTransferMobileText();

    public abstract String payFailNoUserText();

    public abstract String createMobikwikText();

    public abstract String enterEmailText();

    public abstract String mobikwikSuccessText();

    public abstract String mobikwikNoUserText();

    public abstract String unableToProcess_requestText();

    public abstract String processRequestText();

    public abstract String retryText();

    public abstract String zeroBalanceText();

    public abstract String invalidEmailText();

    public abstract String cashoutText();

    public abstract String transactionInProgressText();

    public abstract String transactionAlreadyInProgressText();

    public abstract String paymentFailed();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_PaymentStrings.Builder();
    }


    public static TypeAdapter<PaymentStrings> typeAdapter(Gson gson) {
        return new AutoValue_PaymentStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setAmountToRedeemText(String value);

        public abstract Builder setSelectPaymentModeText(String value);

        public abstract Builder setCashOutWithText(String value);

        public abstract Builder setCouponsText(String value);

        public abstract Builder setWalletEmptyText(String value);

        public abstract Builder setWinCashText(String value);

        public abstract Builder setCurrentBalanceText(String value);

        public abstract Builder setTotalEarningText(String value);

        public abstract Builder setInsufficientBalanceText(String value);

        public abstract Builder setCashOutDialogTitle(String value);

        public abstract Builder setCashOutDialogMessage(String value);

        public abstract Builder setPaymentInitiateText(String value);

        public abstract Builder setUpdateBalanceText(String value);

        public abstract Builder setGoToHomeText(String value);

        public abstract Builder setPaymentErrorMobileText(String value);

        public abstract Builder setOrderIdText(String value);

        public abstract Builder setPayFailTransferMobileText(String value);

        public abstract Builder setPayFailNoUserText(String value);

        public abstract Builder setCreateMobikwikText(String value);

        public abstract Builder setEnterEmailText(String value);

        public abstract Builder setMobikwikSuccessText(String value);

        public abstract Builder setMobikwikNoUserText(String value);

        public abstract Builder setUnableToProcess_requestText(String value);

        public abstract Builder setProcessRequestText(String value);

        public abstract Builder setRetryText(String value);

        public abstract Builder setZeroBalanceText(String value);

        public abstract Builder setInvalidEmailText(String value);

        public abstract Builder setCashoutText(String value);

        public abstract Builder setTransactionInProgressText(String value);

        public abstract Builder setTransactionAlreadyInProgressText(String value);

        public abstract Builder setPaymentFailed(String value);

        public abstract PaymentStrings build();

    }

}
