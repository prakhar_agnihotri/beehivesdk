package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class MenuStrings implements Parcelable {

    public abstract String howToPlayText();

    public abstract String fAQText();

    public abstract String rateUsText();

    public abstract String shareAppText();

    public abstract String signOutText();

    public abstract String termsText();

    public abstract String privacyText();

    public abstract String rulesText();

    public abstract String referralText();

    public abstract String addPictureText();

    public abstract String changePictureText();

    public abstract String moreText();

    public abstract String privacyPolicyText();

    public abstract String termsOfUseText();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_MenuStrings.Builder();
    }

    public static TypeAdapter<MenuStrings> typeAdapter(Gson gson) {
        return new AutoValue_MenuStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setHowToPlayText(String value);

        public abstract Builder setFAQText(String value);

        public abstract Builder setRateUsText(String value);

        public abstract Builder setShareAppText(String value);

        public abstract Builder setSignOutText(String value);

        public abstract Builder setTermsText(String value);

        public abstract Builder setPrivacyText(String value);

        public abstract Builder setRulesText(String value);

        public abstract Builder setReferralText(String value);

        public abstract Builder setAddPictureText(String value);

        public abstract Builder setChangePictureText(String value);

        public abstract Builder setMoreText(String value);

        public abstract Builder setPrivacyPolicyText(String value);

        public abstract Builder setTermsOfUseText(String value);

        public abstract MenuStrings build();

    }

}
