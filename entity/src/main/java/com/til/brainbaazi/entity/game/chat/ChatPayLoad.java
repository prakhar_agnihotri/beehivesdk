package com.til.brainbaazi.entity.game.chat;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by saurabh.garg on 27/02/18.
 */

@AutoValue
public abstract class ChatPayLoad implements Serializable {

    @SerializedName("m")
    public abstract String getMessage();

    @SerializedName("a")
    public abstract String getAction();

    @SerializedName("u")
    public abstract String getUid();

    @SerializedName("i")
    public abstract String getUserImg();

    public static Builder builder() {
        return new AutoValue_ChatPayLoad.Builder();
    }

    public static TypeAdapter<ChatPayLoad> typeAdapter(Gson gson) {
        return new AutoValue_ChatPayLoad.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setMessage(String message);

        public abstract Builder setAction(String action);

        public abstract Builder setUid(String uid);

        public abstract Builder setUserImg(String userImg);

        public abstract ChatPayLoad build();
    }
}