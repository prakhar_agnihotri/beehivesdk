package com.til.brainbaazi.entity.otp;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 22/02/18.
 */

@AutoValue
public abstract class CountryListModel {

    @Nullable
    public abstract ImmutableList<CountryModel> getCountryModels();

    public static Builder builder() {
        return new AutoValue_CountryListModel.Builder();
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setCountryModels(CountryModel... countryModels);

        public abstract CountryListModel build();

    }

}
