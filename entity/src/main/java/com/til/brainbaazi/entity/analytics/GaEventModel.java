package com.til.brainbaazi.entity.analytics;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 3/3/18.
 */

@AutoValue
public abstract class GaEventModel {

    @Nullable
    public abstract String getMainEvent();

    @Nullable
    public abstract String getCategory();

    @Nullable
    public abstract String getAction();

    @Nullable
    public abstract String getLabel();

    @Nullable
    public abstract String getUserName();

    @Nullable
    public abstract String getTimeStamp();

    public static Builder builder() {
        return new AutoValue_GaEventModel.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setMainEvent(String mainEvent);

        public abstract Builder setCategory(String category);

        public abstract Builder setAction(String action);

        public abstract Builder setLabel(String label);

        public abstract Builder setUserName(String userName);

        public abstract Builder setTimeStamp(String timeStamp);

        public abstract GaEventModel build();
    }
}
