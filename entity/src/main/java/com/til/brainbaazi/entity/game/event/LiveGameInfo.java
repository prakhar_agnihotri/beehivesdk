package com.til.brainbaazi.entity.game.event;

import com.google.auto.value.AutoValue;

/**
 * Created by saurabh.garg on 2/16/18.
 */

@AutoValue
public abstract class LiveGameInfo implements GameEvent {

    public abstract int getConcurrentUserCount();

    public static Builder builder() {
        return new AutoValue_LiveGameInfo.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setConcurrentUserCount(int userCount);

        public abstract LiveGameInfo build();

    }
}
