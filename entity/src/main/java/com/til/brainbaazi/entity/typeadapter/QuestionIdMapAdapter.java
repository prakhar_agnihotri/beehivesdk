package com.til.brainbaazi.entity.typeadapter;

import android.os.Parcel;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.ryanharter.auto.value.parcel.TypeAdapter;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.QuestionOptions;
import com.til.brainbaazi.entity.game.event.WinUser;

/**
 * Created by prashant.rathore on 01/03/18.
 */

public class QuestionIdMapAdapter implements TypeAdapter<ImmutableMap<Long,QuestionInfo>> {
    @Override
    public ImmutableMap<Long,QuestionInfo> fromParcel(Parcel in) {
        QuestionInfo[] typedArray = in.createTypedArray(QuestionInfo.CREATOR());
        ImmutableMap.Builder<Long, QuestionInfo> builder = ImmutableMap.<Long, QuestionInfo>builder();
        for(QuestionInfo questionInfo: typedArray) {
            builder.put(questionInfo.question().getId(),questionInfo);
        }
        return builder.build();
    }

    @Override
    public void toParcel(ImmutableMap<Long,QuestionInfo> value, Parcel dest) {
        ImmutableCollection<QuestionInfo> values = value.values();
        dest.writeTypedArray(values.toArray(QuestionInfo.CREATOR().newArray(values.size())),0);
    }

}
