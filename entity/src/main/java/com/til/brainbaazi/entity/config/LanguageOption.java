package com.til.brainbaazi.entity.config;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 10/03/18.
 */

@AutoValue
public abstract class LanguageOption implements Parcelable {

    @SerializedName("lc")
    @Nullable
    public abstract String languageCode();

    @SerializedName("v")
    public abstract int version();

    @SerializedName("nm")
    @Nullable
    public abstract String name();

    @SerializedName("tl")
    @Nullable
    public abstract String translation();

    @SerializedName("gdtl")
    @Nullable
    public abstract String gameDataTranslation();

    @SerializedName("tgs")
    @Nullable
    public abstract String getStartedText();

    @SerializedName("ttl")
    @Nullable
    public abstract String tagLineText();

    @SerializedName("ttnc")
    @Nullable
    public abstract String termConditionText();

    @SerializedName("terms")
    @Nullable
    public abstract String termsText();

    @SerializedName("privacy")
    @Nullable
    public abstract String privacyText();

    @SerializedName("rules")
    @Nullable
    public abstract String rulesText();

    @SerializedName("tsl")
    @Nullable
    public abstract String selectLanguageText();

    public static Builder builder() {
        return new AutoValue_LanguageOption.Builder().setVersion(1)
                .setGetStartedText("Get Started")
                .setSelectLanguageText("Select your language")
                .setTagLineText("Jeetoon main, Jeete India");
    }

    public static final Creator<AutoValue_LanguageOption> CREATOR() {
        return AutoValue_LanguageOption.CREATOR;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setLanguageCode(String value);

        public abstract Builder setVersion(int value);

        public abstract Builder setName(String value);

        public abstract Builder setTranslation(String value);

        public abstract Builder setGameDataTranslation(String value);

        public abstract Builder setGetStartedText(String value);

        public abstract Builder setTagLineText(String value);

        public abstract Builder setTermConditionText(String value);

        public abstract Builder setTermsText(String value);

        public abstract Builder setPrivacyText(String value);

        public abstract Builder setRulesText(String value);

        public abstract Builder setSelectLanguageText(String value);

        public abstract LanguageOption build();
    }

}
