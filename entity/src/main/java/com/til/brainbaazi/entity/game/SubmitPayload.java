package com.til.brainbaazi.entity.game;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by saurabh.garg on 26/02/18.
 */

@AutoValue
public abstract class SubmitPayload implements Serializable {

    @SerializedName("g")
    public abstract String getGameID();

    @SerializedName("q")
    public abstract String getQuestionID();

    @SerializedName("t")
    public abstract String getTime();

    @SerializedName("o")
    public abstract String getOptionID();

    public static Builder builder() {
        return new AutoValue_SubmitPayload.Builder();
    }

    public static TypeAdapter<SubmitPayload> typeAdapter(Gson gson) {
        return new AutoValue_SubmitPayload.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setGameID(String g);

        public abstract Builder setQuestionID(String q);

        public abstract Builder setTime(String t);

        public abstract Builder setOptionID(String o);

        public abstract SubmitPayload build();
    }

}
