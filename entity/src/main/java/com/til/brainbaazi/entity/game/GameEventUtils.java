package com.til.brainbaazi.entity.game;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.AnswerOptions;
import com.til.brainbaazi.entity.game.event.GameEvent;
import com.til.brainbaazi.entity.game.event.GameLiveEvent;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.KickedEvent;
import com.til.brainbaazi.entity.game.event.LiveGameInfo;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.event.QuestionOptions;
import com.til.brainbaazi.entity.game.event.WinUser;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.otp.CountryModel;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by saurabh.garg on 2/26/18.
 */

public class GameEventUtils {
    public static InputEvent extractPayloadData(GameInfo gameInfo, String cueText, int inputType, User user) {
        InputEvent inputEvent = null;
        JSONObject payloadObj = null;
        try {
            String keys[] = cueText.split("\\|");
            JSONArray jsonArray = new JSONArray(keys[2]);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            payloadObj = jsonObject.optJSONObject("p");
        } catch (Exception e) {
            e.printStackTrace();
        }
        switch (inputType) {
            case InputEvent.TYPE_QUESTION:
                inputEvent = parseQuestion(payloadObj, user, gameInfo, false, true);
                break;
            case InputEvent.TYPE_ANSWER:
                inputEvent = parseAnswerPayload(payloadObj, user, gameInfo, false, true);
                break;
            case InputEvent.TYPE_WINNER:
                inputEvent = parseWinner(payloadObj, user, gameInfo, false, true);
                break;
            case InputEvent.TYPE_GAME_END:
                inputEvent = parseGameEnd(payloadObj, user, gameInfo, false, true);
                break;
        }
        return inputEvent;
    }

    public static InputEvent parseLiveGameInfo(JSONObject jsonObject, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        LiveGameInfo liveGameInfo = LiveGameInfo.builder().
                setConcurrentUserCount(jsonObject.optInt("c")).
                build();
        return getInputEvent(InputEvent.TYPE_LIVE_INFO, liveGameInfo, user, gameInfo, isFromSocket, toTrigger);
    }

    public static InputEvent parseUserKickedEvent(JSONObject payload, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        String userList = payload.optString("unm", "");
        KickedEvent kicked = KickedEvent.builder().
                setKickedList(userList).
                build();
        return getInputEvent(InputEvent.TYPE_KICKED_OUT, kicked, user, gameInfo, isFromSocket, toTrigger);
    }

    public static InputEvent parseWinner(JSONObject payload, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        Winner winner = getWinnerData(payload);
        return getInputEvent(InputEvent.TYPE_WINNER, winner, user, gameInfo, isFromSocket, toTrigger);
    }

    public static Winner getWinnerData(JSONObject payload) {
        WinUser[] winUsers = null;
        long prize = 0;
        long count = 0;
        String extraLifeList = null;
        boolean empty = true;
        if (payload != null) {
            prize = payload.optLong("pmt");
            count = payload.optLong("wc");
            extraLifeList = payload.optString("l");
            JSONArray jsonArrayWinner = payload.optJSONArray("w");
            if (jsonArrayWinner != null && jsonArrayWinner.length() > 0) {
                int length = jsonArrayWinner.length();
                winUsers = new WinUser[length];
                for (int index = 0; index < length; index++) {
                    JSONObject ansOpt = jsonArrayWinner.optJSONObject(index);
                    winUsers[index] = WinUser.builder().
                            setImgUrl(ansOpt.optString("uim")).
                            setName(ansOpt.optString("unm")).
                            build();
                }
            }
            empty = false;
        }
        return Winner.builder().
                setWinUserList(winUsers).
                setEarnExtraLifeList(extraLifeList).
                setPrizeAmount(prize).
                setWinnerCount(count).
                setEmpty(empty).
                build();
    }

    public static InputEvent parseGameOnOff(JSONObject payload, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        boolean isGameLive = false;
        if (0 == payload.optInt("a")) {
            isGameLive = false;
        } else if (1 == payload.optInt("a")) {
            isGameLive = true;
        }
        GameLiveEvent gameLiveEvent = GameLiveEvent.builder().
                setGameLive(isGameLive).
                build();
        if (isGameLive) {
            return getInputEvent(InputEvent.TYPE_LIVE_TRIGGER, gameLiveEvent, user, gameInfo, isFromSocket, toTrigger);
        } else {
            return getInputEvent(InputEvent.TYPE_GAME_END, gameLiveEvent, user, gameInfo, isFromSocket, toTrigger);
        }
    }

    public static InputEvent parseAnswerPayload(JSONObject payload, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        JSONArray jsonArrayAnswerOptions = payload.optJSONArray("opts");
        AnswerOptions[] answerOptions = new AnswerOptions[jsonArrayAnswerOptions.length()];
        double count = 0;
        for (int index = 0; index < jsonArrayAnswerOptions.length(); index++) {
            JSONObject ansOpt = jsonArrayAnswerOptions.optJSONObject(index);
            double optionCount = ansOpt.getDouble("cnt");
            count = count + optionCount;
            answerOptions[index] = AnswerOptions.builder().
                    setOptionId(ansOpt.optString("oid")).
                    setPosition(ansOpt.optInt("pos")).
                    setText(ansOpt.optString("val")).
                    setCount(optionCount).
                    build();
        }
        int lifeCount = getParsedValued(payload.optString("luc"));
        Answer answer = Answer.builder().
                setAnswerOptions(answerOptions).
                setCorrectAnswerId(payload.optLong("crt")).
                setQuestionId(payload.optLong("qid")).
                setMaxUserCount((int) count).
                setLifeUsedCount(lifeCount).
                build();
        return getInputEvent(InputEvent.TYPE_ANSWER, answer, user, gameInfo, isFromSocket, toTrigger);
    }

    public static InputEvent parseQuestion(JSONObject payload, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        JSONArray jsonArrayQuestionOptions = payload.optJSONArray("ans");
        QuestionOptions[] options = new QuestionOptions[jsonArrayQuestionOptions.length()];
        for (int index = 0; index < jsonArrayQuestionOptions.length(); index++) {
            JSONObject qusOpt = jsonArrayQuestionOptions.optJSONObject(index);

            options[index] = QuestionOptions.builder().
                    setOptionId(qusOpt.optString("oid")).
                    setPosition(qusOpt.optInt("pos")).
                    setText(qusOpt.optString("val")).build();
        }
        Question question = Question.builder().
                setText(payload.optString("qt")).
                setId(payload.optLong("qid")).
                setQuestionNumber(payload.optInt("sqn")).
                setLastQuestion(payload.optBoolean("lst")).
                setQuestionOptions(options).build();
        return getInputEvent(InputEvent.TYPE_QUESTION, question, user, gameInfo, isFromSocket, toTrigger);
    }

    public static InputEvent parseChatMessage(JSONObject payload, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        ChatMsg chatMsg = ChatMsg.builder().
                setImgUrl(payload.optString("i")).
                setMessage(payload.optString("m")).
                setName(payload.optString("u")).
                build();
        return getInputEvent(InputEvent.TYPE_CHAT, chatMsg, user, gameInfo, isFromSocket, toTrigger);
    }

    public static InputEvent parseGameEnd(JSONObject payload, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        GameLiveEvent gameLiveEvent = GameLiveEvent.builder().setGameLive(false).build();
        return getInputEvent(InputEvent.TYPE_GAME_END, gameLiveEvent, user, gameInfo, isFromSocket, toTrigger);
    }

    public static InputEvent getInputEvent(int type, GameEvent gameEvent, User user, GameInfo gameInfo, boolean isFromSocket, boolean toTrigger) {
        return InputEvent.builder().
                setUser(user).
                setType(type).
                setTrigger(toTrigger).
                setGameInfo(gameInfo).
                setSource(isFromSocket ? InputEvent.SOURCE_SOCKET : InputEvent.SOURCE_CUE).
                setGameEvent(gameEvent).
                build();
    }

    public static CountryListModel parseCountryListModel(String response) {
        CountryModel[] countryModels = null;
        if (response != null && response.length() > 0) {
            JSONArray jsonArrayWinner = new JSONArray(response);
            if (jsonArrayWinner.length() > 0) {
                int length = jsonArrayWinner.length();
                countryModels = new CountryModel[jsonArrayWinner.length()];
                for (int index = 0; index < length; index++) {
                    JSONObject ansOpt = jsonArrayWinner.optJSONObject(index);
                    countryModels[index] = CountryModel.builder().
                            setName(ansOpt.optString("name")).
                            setDialCode(ansOpt.optString("dial_code")).
                            setCode(ansOpt.optString("code")).
                            setImgUrl("").
                            build();
                }
            }
        }
        return CountryListModel.builder().
                setCountryModels(countryModels).
                build();
    }

    public static int getParsedValued(String value) {
        int count = -1;
        try {
            count = Integer.parseInt(value);
        } catch (Exception ex) {
            count = -1;
        }
        return count;
    }
}
