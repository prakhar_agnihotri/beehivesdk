package com.til.brainbaazi.entity.otp;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

/**
 * Created by prashant.rathore on 22/02/18.
 */

@AutoValue
public abstract class PhoneNumber implements Parcelable {

    public abstract String getPhoneNumber();

    public abstract CountryModel getCountryModel();

    public static Builder builder() {
        return new AutoValue_PhoneNumber.Builder();
    }

    public static Creator<AutoValue_PhoneNumber> creator() {
        return AutoValue_PhoneNumber.CREATOR;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setPhoneNumber(String phoneNumber);

        public abstract Builder setCountryModel(CountryModel countryInfo);

        public abstract PhoneNumber build();
    }

}
