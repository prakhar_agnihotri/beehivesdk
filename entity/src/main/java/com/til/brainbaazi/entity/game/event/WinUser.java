package com.til.brainbaazi.entity.game.event;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class WinUser implements GameEvent , Parcelable {

    public abstract String getName();

    public abstract String getImgUrl();

    public static Builder builder() {
        return new AutoValue_WinUser.Builder();
    }

    public static final Parcelable.Creator<AutoValue_WinUser> CREATOR() {
        return AutoValue_WinUser.CREATOR;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setName(String name);

        public abstract Builder setImgUrl(String imgUrl);

        public abstract WinUser build();
    }
}
