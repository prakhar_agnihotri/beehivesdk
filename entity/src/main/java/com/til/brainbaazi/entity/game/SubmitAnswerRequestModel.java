package com.til.brainbaazi.entity.game;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.event.InputEvent;

/**
 * Created by saurabh.garg on 2/26/18.
 */

@AutoValue
public abstract class SubmitAnswerRequestModel {

    @SerializedName("uid")
    public abstract String getUid();

    @SerializedName("uqid")
    public abstract String getUqid();

    @SerializedName("gameId")
    public abstract String getGameId();

    @SerializedName("quesId")
    public abstract String getQuesId();

    @SerializedName("timeTaken")
    public abstract String getTimeTaken();

    @SerializedName("optId")
    public abstract String getOptId();

    public static Builder builder() {
        return new AutoValue_SubmitAnswerRequestModel.Builder();
    }

    public static TypeAdapter<SubmitAnswerRequestModel> typeAdapter(Gson gson) {
        return new AutoValue_SubmitAnswerRequestModel.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setUid(String uid);

        public abstract Builder setUqid(String uqid);

        public abstract Builder setGameId(String gameId);

        public abstract Builder setQuesId(String quesId);

        public abstract Builder setTimeTaken(String timeTaken);

        public abstract Builder setOptId(String optId);


        public abstract SubmitAnswerRequestModel build();
    }

}
