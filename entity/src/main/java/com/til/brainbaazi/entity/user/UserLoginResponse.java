package com.til.brainbaazi.entity.user;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 25/02/18.
 */

@AutoValue
public abstract class UserLoginResponse {

    @SerializedName("status")
    @Nullable
    public abstract Boolean getRegistered();

    @SerializedName("auth_token")
    @Nullable
    public abstract String getAuthToken();

    public static TypeAdapter<UserLoginResponse> typeAdapter(Gson gson) {
        return new AutoValue_UserLoginResponse.GsonTypeAdapter(gson);
    }

    public static Builder builder() {
        return new AutoValue_UserLoginResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setAuthToken(String authToken);
        public abstract Builder setRegistered(Boolean loggedIn);

        public abstract UserLoginResponse build();

    }

}
