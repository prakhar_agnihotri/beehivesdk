package com.til.brainbaazi.entity.game.event;

import com.google.auto.value.AutoValue;

/**
 * Created by saurabh.garg on 2/16/18.
 */

@AutoValue
public abstract class SocketConnectError implements GameEvent {

    public abstract Exception getWebSocketException();

    public static Builder builder() {
        return new AutoValue_SocketConnectError.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setWebSocketException(Exception webSocketException);

        public abstract SocketConnectError build();

    }
}
