package com.til.brainbaazi.entity.game.event;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.ryanharter.auto.value.parcel.ParcelAdapter;
import com.til.brainbaazi.entity.typeadapter.QuestionOptionListAdapter;

/**
 * Created by prashant.rathore on 17/02/18.
 */
@AutoValue
public abstract class Question implements GameEvent {

    public abstract long getId();

    public abstract String getText();

    public abstract int getQuestionNumber();

    public abstract boolean isLastQuestion();

    @ParcelAdapter(QuestionOptionListAdapter.class)
    public abstract ImmutableList<QuestionOptions> getQuestionOptions();

    public static Builder builder() {
        return new AutoValue_Question.Builder();
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setId(long id);

        public abstract Builder setText(String text);

        public abstract Builder setQuestionNumber(int questionNumber);

        public abstract Builder setQuestionOptions(QuestionOptions... questionOptions);

        public abstract Builder setLastQuestion(boolean lastQuestion);

        public abstract Question build();

    }

}
