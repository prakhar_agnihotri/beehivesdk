package com.til.brainbaazi.entity.payment;

import com.google.auto.value.AutoValue;

/**
 * Created by arpit.toshniwal on 23/02/18.
 */

@AutoValue
public abstract class PaymentResponse {

    public abstract boolean isError();

    public abstract boolean isStatus();

    public abstract PaymentResponseMessage getPaymentResponseMessage();

    public static Builder builder(){
        return new AutoValue_PaymentResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder{
     public abstract Builder setStatus(boolean status);

     public abstract Builder setError(boolean error);

     public abstract Builder setPaymentResponseMessage(PaymentResponseMessage paymentResponseMessage);

     public abstract PaymentResponse build();
    }
}
