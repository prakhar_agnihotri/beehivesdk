package com.til.brainbaazi.entity.otp;

import com.google.auto.value.AutoValue;

/**
 * Created by prashant.rathore on 21/02/18.
 */

public interface OTPRequest {

    public String getPhoneNumber();

}
