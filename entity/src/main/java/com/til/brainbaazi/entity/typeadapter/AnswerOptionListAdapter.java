package com.til.brainbaazi.entity.typeadapter;

import android.os.Parcel;

import com.google.common.collect.ImmutableList;
import com.ryanharter.auto.value.parcel.TypeAdapter;
import com.til.brainbaazi.entity.game.event.AnswerOptions;
import com.til.brainbaazi.entity.game.event.QuestionOptions;

/**
 * Created by prashant.rathore on 01/03/18.
 */

public class AnswerOptionListAdapter implements TypeAdapter<ImmutableList<AnswerOptions>> {
    @Override
    public ImmutableList<AnswerOptions> fromParcel(Parcel in) {
        AnswerOptions[] typedArray = in.createTypedArray(AnswerOptions.CREATOR());
        return ImmutableList.<AnswerOptions>builder().add(typedArray).build();
    }

    @Override
    public void toParcel(ImmutableList<AnswerOptions> value, Parcel dest) {
        dest.writeTypedArray(value.toArray(AnswerOptions.CREATOR().newArray(value.size())),0);
    }

}
