package com.til.brainbaazi.entity.user;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by saurabh.garg on 2/24/18.
 */

@AutoValue
public abstract class UpdateUserImageResponse {

    public abstract boolean isStatus();

    public abstract String getError();

    public static TypeAdapter<UpdateUserImageResponse> typeAdapter(Gson gson) {
        return new AutoValue_UpdateUserImageResponse.GsonTypeAdapter(gson);
    }

    public static Builder builder() {
        return new AutoValue_UpdateUserImageResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setStatus(boolean status);

        public abstract Builder setError(String error);

        public abstract UpdateUserImageResponse build();
    }
}
