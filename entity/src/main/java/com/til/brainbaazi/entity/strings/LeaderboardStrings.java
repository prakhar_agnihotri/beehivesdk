package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class LeaderboardStrings implements Parcelable {

    public abstract String thisWeekText();

    public abstract String allTimeText();

    public abstract String winnerText();

    public abstract String winnersText();

    public abstract String prizeMoneyText();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_LeaderboardStrings.Builder();
    }

    public static TypeAdapter<LeaderboardStrings> typeAdapter(Gson gson) {
        return new AutoValue_LeaderboardStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setThisWeekText(String value);

        public abstract Builder setAllTimeText(String value);

        public abstract Builder setWinnerText(String value);

        public abstract Builder setWinnersText(String value);

        public abstract Builder setPrizeMoneyText(String value);

        public abstract LeaderboardStrings build();

    }

}
