package com.til.brainbaazi.entity;

import com.google.auto.value.AutoValue;

/**
 * Created by prashant.rathore on 21/02/18.
 */

@AutoValue
public abstract class LoginRequest {

    public abstract String getMobileNumber();

    public abstract String getUserName();

    public abstract String getDeviceType();

    public abstract String getDeviceId();

    public abstract String getCountryCode();

    public abstract String getProfileImageUrl();

    public abstract String getAcquistionSource();

    public abstract String getReferralId();

    public abstract String getAuthToken();

    public static Builder builder() {
        return new AutoValue_LoginRequest.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder setMobileNumber(String value);

        public abstract Builder setUserName(String value);

        public abstract Builder setDeviceType(String value);

        public abstract Builder setDeviceId(String value);

        public abstract Builder setCountryCode(String value);

        public abstract Builder setProfileImageUrl(String value);

        public abstract Builder setAcquistionSource(String value);

        public abstract Builder setReferralId(String value);

        public abstract Builder setAuthToken(String authToken);

        public abstract LoginRequest build();
    }

}
