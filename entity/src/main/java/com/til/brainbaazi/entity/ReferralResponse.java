package com.til.brainbaazi.entity;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by arpit.toshniwal on 04/03/18.
 */

@AutoValue
public abstract class ReferralResponse implements Parcelable {

    @Nullable
    public abstract String getError();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_ReferralResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setError(String error);

        public abstract ReferralResponse build();
    }
}

