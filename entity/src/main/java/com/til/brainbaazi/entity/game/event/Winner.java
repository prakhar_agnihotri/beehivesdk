package com.til.brainbaazi.entity.game.event;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.ryanharter.auto.value.parcel.ParcelAdapter;
import com.til.brainbaazi.entity.typeadapter.WinnersListAdapter;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class Winner implements GameEvent, Parcelable {

    public abstract long getPrizeAmount();

    public abstract long getWinnerCount();

    @ParcelAdapter(WinnersListAdapter.class)
    @Nullable
    public abstract ImmutableList<WinUser> getWinUserList();

    @Nullable
    public abstract String getEarnExtraLifeList();

    public abstract boolean isEmpty();

    public static Builder builder() {
        return new AutoValue_Winner.Builder();
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setPrizeAmount(long prizeAmount);

        public abstract Builder setWinnerCount(long winnerCount);

        public abstract Builder setWinUserList(WinUser... winUsers);

        public abstract Builder setEarnExtraLifeList(String earnExtraLifeList);

        public abstract Builder setEmpty(boolean empty);

        public abstract Winner build();
    }
}
