package com.til.brainbaazi.entity;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 15/02/18.
 */

@AutoValue
public abstract class User implements Parcelable {

    @SerializedName("uim")
    @Nullable
    public abstract String getUserImgUrl();

    @SerializedName("unm")
    public abstract String getUserName();

    @SerializedName("amt")
    public abstract int getUserBalance();

    @SerializedName("tamt")
    public abstract int getUserLifeTimeBalance();

    @SerializedName("mob")
    public abstract String getPhoneNumber();

    @Nullable
    @SerializedName("rid")
    public abstract String getReferralID();

    @SerializedName("lfe")
    public abstract int getLives();

    @SerializedName("wrk")
    public abstract long getWeeklyRank();

    @SerializedName("tip")
    public abstract boolean isTransactionInProgress();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_User.Builder()
                .setWeeklyRank(0);
    }

    public static TypeAdapter<User> typeAdapter(Gson gson) {
        return new AutoValue_User.GsonTypeAdapter(gson);
    }

    public static Creator<AutoValue_User> creator() {
        return AutoValue_User.CREATOR;
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setUserImgUrl(String imageUrl);

        public abstract Builder setUserName(String userName);

        public abstract Builder setUserBalance(int balance);

        public abstract Builder setUserLifeTimeBalance(int lifeTimeBalance);

        public abstract Builder setPhoneNumber(String phoneNumber);

        public abstract Builder setLives(int lives);

        public abstract Builder setWeeklyRank(long weeklyRank);

        public abstract Builder setTransactionInProgress(boolean transactionInProgress);

        public abstract Builder setReferralID(String rid);

        public abstract User build();
    }


}
