package com.til.brainbaazi.entity.leaderBoard;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 2/21/18.
 */

@AutoValue
public abstract class LeaderBoardUser implements Parcelable {

    @SerializedName("rank")
    public abstract long getUserRank();

    @SerializedName("prize")
    public abstract long getPrize();

    @SerializedName("unm")
    @Nullable
    public abstract String getUserName();

    @Nullable
    @SerializedName("uim")
    public abstract String getImgUrl();

    public static Builder builder() {
        return new AutoValue_LeaderBoardUser.Builder()
                .setPrize(0).setUserRank(0);
    }

    public static TypeAdapter<LeaderBoardUser> typeAdapter(Gson gson) {
        return new AutoValue_LeaderBoardUser.GsonTypeAdapter(gson);
    }


    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setUserRank(long userRank);

        public abstract Builder setPrize(long prize);

        public abstract Builder setUserName(String userName);

        public abstract Builder setImgUrl(String imgUrl);

        public abstract LeaderBoardUser build();
    }

}
