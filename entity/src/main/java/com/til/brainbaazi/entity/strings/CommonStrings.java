package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class CommonStrings implements Parcelable {

    public abstract String share();

    public abstract String okayText();

    public abstract String cancelText();

    public abstract String internetNotConnected();

    public abstract String feedBackText();

    public abstract String confirmText();

    public abstract String exitDialogGameBeginText();

    public abstract String exitDialogGameStartedText();

    public abstract String quitText();

    public abstract String updateText();

    public abstract String getStartedText();

    public abstract String comeBackAWhileText();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_CommonStrings.Builder();
    }

    public static TypeAdapter<CommonStrings> typeAdapter(Gson gson) {
        return new AutoValue_CommonStrings.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setShare(String share);

        public abstract Builder setOkayText(String value);

        public abstract Builder setCancelText(String value);

        public abstract Builder setInternetNotConnected(String value);

        public abstract Builder setFeedBackText(String value);

        public abstract Builder setConfirmText(String value);

        public abstract Builder setExitDialogGameBeginText(String value);

        public abstract Builder setExitDialogGameStartedText(String value);

        public abstract Builder setQuitText(String value);

        public abstract Builder setUpdateText(String value);

        public abstract Builder setGetStartedText(String value);

        public abstract Builder setComeBackAWhileText(String value);

        public abstract CommonStrings build();

    }

}
