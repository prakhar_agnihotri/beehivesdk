package com.til.brainbaazi.entity.payment;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by arpit.toshniwal on 23/02/18.
 */

@AutoValue
public abstract  class PaymentResponseMessage {

    public abstract boolean isRequestSuccess();

    public abstract Integer getRequestType();

    @Nullable
    public abstract String getMessageCode();
    @Nullable
    public abstract String getStatus();
    @Nullable
    public abstract String getStatusCode();
    @Nullable
    public abstract String getStatusDescription();
    @Nullable
    public abstract String getChecksum();
    @Nullable
    public abstract String getEmailAddress();
    @Nullable
    public abstract String getNonZeroFlag();
    @Nullable
    public abstract String getOrderId();
    @Nullable
    public abstract String getStatusMessage();
    @Nullable
    public abstract String getMetaData();
    @Nullable
    public abstract String getTxnId();

    public abstract Builder toBuilder();

    public static Builder builder(){
        return new AutoValue_PaymentResponseMessage.Builder().setRequestType(0);
    }

    @AutoValue.Builder
    public static abstract class Builder{
        public abstract Builder setRequestSuccess(boolean isRequestSuccess);
        public abstract Builder setMessageCode(String messageCode);
        public abstract Builder setRequestType(Integer requestType);
        public abstract Builder setStatus(String status);
        public abstract Builder setStatusCode(String statusCode);
        public abstract Builder setStatusDescription(String statusDescription);
        public abstract Builder setChecksum(String checksum);
        public abstract Builder setEmailAddress(String emailAddress);
        public abstract Builder setNonZeroFlag(String nonZeroFlag);
        public abstract Builder setOrderId(String orderId);
        public abstract Builder setStatusMessage(String statusMessage);
        public abstract Builder setMetaData(String metaData);
        public abstract Builder setTxnId(String txnId);

        public abstract PaymentResponseMessage build();
    }
}
