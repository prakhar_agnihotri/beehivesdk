package com.til.brainbaazi.entity;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 16/02/18.
 */

@AutoValue
public abstract class Response<T> {

    @Nullable
    public abstract T value();

    @Nullable
    public abstract Exception exception();

    public abstract boolean success();

    public abstract long time();

    public static <T> Builder builder() {
        return new AutoValue_Response.Builder<T>().setTime(System.currentTimeMillis());
    }

    @AutoValue.Builder
    public static abstract class Builder<T> {
        public abstract Builder<T> setValue(T value);

        public abstract Builder<T> setException(Exception exception);

        public abstract Builder<T> setSuccess(boolean success);

        public abstract Builder<T> setTime(long time);

        public abstract Response<T> build();

    }
}
