package com.til.brainbaazi.entity;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 15/02/18.
 */

@AutoValue
public abstract class WebData implements Parcelable {

    @SerializedName("title")
    @Nullable
    public abstract String getTitle();

    @SerializedName("url")
    @Nullable
    public abstract String getWebUrl();

    public static Builder builder() {
        return new AutoValue_WebData.Builder();
    }

    public static Creator<AutoValue_WebData> creator() {
        return AutoValue_WebData.CREATOR;
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setTitle(String title);

        public abstract Builder setWebUrl(String webUrl);

        public abstract WebData build();
    }


}
