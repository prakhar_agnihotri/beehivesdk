package com.til.brainbaazi.entity.game.event;

import com.google.auto.value.AutoValue;

/**
 * Created by saurabh.garg on 2/16/18.
 */

@AutoValue
public abstract class GameLiveEvent implements GameEvent {

    public abstract boolean isGameLive();

    public static Builder builder() {
        return new AutoValue_GameLiveEvent.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setGameLive(boolean isGameLive);

        public abstract GameLiveEvent build();
    }
}
