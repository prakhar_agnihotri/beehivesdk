package com.til.brainbaazi.entity.leaderBoard;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 3/4/18.
 */

@AutoValue
public abstract class LastGameWinnerResponse {

    @Nullable
    @SerializedName("pu")
    public abstract String getPreviousUrl();

    @Nullable
    @SerializedName("nu")
    public abstract String getNextUrl();

    @SerializedName("dt")
    public abstract long getDateTime();

    @Nullable
    @SerializedName("tw")
    public abstract String getTotalWinner();

    @Nullable
    @SerializedName("pm")
    public abstract String getPrizeMoney();

    @Nullable
    @SerializedName("amountPerUser")
    public abstract Integer getAmountPerUser();

    @SerializedName("users")
    public abstract ImmutableList<LeaderBoardUser> getLastGameWinner();

    public abstract boolean isFirstListResponse();

    public static Builder builder() {
        return new AutoValue_LastGameWinnerResponse.Builder();
    }

    public static TypeAdapter<LastGameWinnerResponse> typeAdapter(Gson gson) {
        return new AutoValue_LastGameWinnerResponse.GsonTypeAdapter(gson);
    }


    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setPreviousUrl(String previousUrl);

        public abstract Builder setNextUrl(String nextUrl);

        public abstract Builder setDateTime(long dateTime);

        public abstract Builder setTotalWinner(String totalWinner);

        public abstract Builder setPrizeMoney(String prizeMoney);

        public abstract Builder setAmountPerUser(Integer amountPerUser);

        public abstract Builder setLastGameWinner(ImmutableList<LeaderBoardUser> lastGameWinner);

        public abstract Builder setFirstListResponse(boolean firstListResponse);

        public abstract LastGameWinnerResponse build();
    }
}
