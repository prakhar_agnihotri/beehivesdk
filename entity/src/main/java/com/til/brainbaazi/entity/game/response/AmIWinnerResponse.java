package com.til.brainbaazi.entity.game.response;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saurabh.garg on 2/27/18.
 */

@AutoValue
public abstract class AmIWinnerResponse {

    @SerializedName("w")
    public abstract boolean isWin();

    public static Builder builder() {
        return new AutoValue_AmIWinnerResponse.Builder();
    }

    public static TypeAdapter<AmIWinnerResponse> typeAdapter(Gson gson) {
        return new AutoValue_AmIWinnerResponse.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setWin(boolean win);

        public abstract AmIWinnerResponse build();
    }
}
