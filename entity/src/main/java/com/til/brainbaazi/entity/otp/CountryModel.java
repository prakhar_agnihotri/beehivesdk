package com.til.brainbaazi.entity.otp;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by saurabh.garg on 22/02/18.
 */

@AutoValue
public abstract class CountryModel implements Parcelable {

    public abstract String getName();

    public abstract String getDialCode();

    public abstract String getCode();

    @Nullable
    public abstract String getImgUrl();

    public static Builder builder() {
        return new AutoValue_CountryModel.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setName(String name);

        public abstract Builder setDialCode(String dialCode);

        public abstract Builder setCode(String code);

        public abstract Builder setImgUrl(String imgUrl);

        public abstract CountryModel build();
    }


}
