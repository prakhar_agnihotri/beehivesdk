package com.til.brainbaazi.entity.game.event;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.chat.ChatMsg;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 17/02/18.
 */
@AutoValue
public abstract class InputEvent implements Parcelable {

    public static final int TYPE_QUESTION = 1;
    public static final int TYPE_ANSWER = 2;
    public static final int TYPE_WINNER = 3;
    public static final int TYPE_LIVE_TRIGGER = 4;
    public static final int TYPE_GAME_END = 5;
    public static final int TYPE_CHAT = 6;
    public static final int TYPE_KICKED_OUT = 7;
    public static final int TYPE_USER_STATE = 8;
    public static final int TYPE_USER_ANSWERED = 9;
    public static final int TYPE_SOCKET_CONNECT_ERROR = 10;
    public static final int TYPE_SOCKET_DISCONNECTED = 11;
    public static final int TYPE_GAME_LIVE_INFO = 12;
    public static final int TYPE_LIVE_INFO = 13;

    public static final int SOURCE_SOCKET = 1;
    public static final int SOURCE_CUE = 2;

    public abstract int source();

    public abstract boolean trigger();

    public abstract int type();

    public abstract GameEvent gameEvent();

    public abstract User user();

    public abstract GameInfo gameInfo();

    public static Builder builder() {
        return new AutoValue_InputEvent.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setTrigger(boolean trigger);

        public abstract Builder setType(int type);

        public abstract Builder setGameEvent(GameEvent gameEvent);

        public abstract Builder setUser(User user);

        public abstract Builder setSource(int source);

        public abstract Builder setGameInfo(GameInfo gameInfo);

        public abstract InputEvent build();

    }


}
