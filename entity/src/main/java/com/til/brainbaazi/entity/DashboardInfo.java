package com.til.brainbaazi.entity;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.til.brainbaazi.entity.game.GameInfo;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 25/02/18.
 */

@AutoValue
public abstract class DashboardInfo implements Parcelable {

    @Nullable
    @SerializedName("user")
    public abstract User getUser();

    @Nullable
    @SerializedName("curg")
    public abstract GameInfo getCurrentGameInfo();

    @Nullable
    @SerializedName("nxtg")
    public abstract GameInfo getNextGameInfo();

    @Nullable
    @SerializedName("isActive")
    public abstract Boolean getActive();

    @Nullable
    @SerializedName("active_info")
    public abstract String getActiveInfo();

    @Nullable
    @SerializedName("stm")
    public abstract Long getServerTime();

    @Nullable
    public abstract Boolean getRegistered();

    public static Builder builder() {
        return new AutoValue_DashboardInfo.Builder();
    }

    public static TypeAdapter<DashboardInfo> typeAdapter(Gson gson) {
        return new AutoValue_DashboardInfo.GsonTypeAdapter(gson);
    }

    public static Parcelable.Creator<AutoValue_DashboardInfo> creator() {
        return AutoValue_DashboardInfo.CREATOR;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setUser(User user);

        public abstract Builder setCurrentGameInfo(GameInfo gameInfo);

        public abstract Builder setNextGameInfo(GameInfo gameInfo);

        public abstract Builder setActive(Boolean active);

        public abstract Builder setActiveInfo(String activeInfo);

        public abstract Builder setServerTime(Long time);

        public abstract Builder setRegistered(Boolean registered);

        public abstract DashboardInfo build();

    }


}
