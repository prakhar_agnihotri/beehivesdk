package com.til.brainbaazi.entity.strings;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by prashant.rathore on 04/03/18.
 */

@AutoValue
public abstract class BrainbaaziStrings implements Parcelable {

    public abstract CommonStrings commonStrings();

    public abstract DashboardStrings dashboardStrings();

    public abstract GameplayStrings gameplayStrings();

    public abstract LaunchStrings launchStrings();

    public abstract MenuStrings menuStrings();

    public abstract OtpStrings otpStrings();

    public abstract PaymentStrings paymentStrings();

    public abstract ProfileStrings profileStrings();

    public abstract LeaderboardStrings leaderboardStrings();

    public static Builder builder() {
        return new AutoValue_BrainbaaziStrings.Builder();
    }

    public static TypeAdapter<BrainbaaziStrings> typeAdapter(Gson gson) {
        return new AutoValue_BrainbaaziStrings.GsonTypeAdapter(gson);
    }


    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder setCommonStrings(CommonStrings value);

        public abstract Builder setDashboardStrings(DashboardStrings value);

        public abstract Builder setGameplayStrings(GameplayStrings value);

        public abstract Builder setLaunchStrings(LaunchStrings value);

        public abstract Builder setMenuStrings(MenuStrings value);

        public abstract Builder setOtpStrings(OtpStrings value);

        public abstract Builder setPaymentStrings(PaymentStrings value);

        public abstract Builder setProfileStrings(ProfileStrings value);

        public abstract Builder setLeaderboardStrings(LeaderboardStrings value);

        public abstract BrainbaaziStrings build();

    }
}
