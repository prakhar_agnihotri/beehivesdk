package com.til.brainbaazi.entity;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 21/02/18.
 */

@AutoValue
public abstract class UserStatus {

    public static final int STATUS_FRESH = 0;
    public static final int STATUS_LOGGED_OUT = 1;
    public static final int STATUS_LOGGED_IN = 2;
    public static final int STATUS_UNREGISTERED = 3;

    public abstract int getStatus();

    @Nullable
    public abstract User getUser();

    public static Builder builder() {
        return new AutoValue_UserStatus.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setStatus(int status);

        public abstract Builder setUser(User user);

        public abstract UserStatus build();
    }

}
