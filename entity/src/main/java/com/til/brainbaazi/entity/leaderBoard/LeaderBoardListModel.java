package com.til.brainbaazi.entity.leaderBoard;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by prashant.rathore on 17/02/18.
 */
@AutoValue
public abstract class LeaderBoardListModel {

    @SerializedName("weeklyScore")
    public abstract long getWeeklyScore();

    @SerializedName("alltimeScore")
    public abstract long getAllTimeScore();

    @SerializedName("weekly")
    public abstract ImmutableList<LeaderBoardUser> getLeaderBoardWeeklyUsers();

    @SerializedName("alltime")
    public abstract ImmutableList<LeaderBoardUser> getLeaderBoardAllTimeUsers();

    public static Builder builder() {
        return new AutoValue_LeaderBoardListModel.Builder()
                .setAllTimeScore(0)
                .setWeeklyScore(0);
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setWeeklyScore(long weeklyScore);

        public abstract Builder setAllTimeScore(long allTimeScore);

        public abstract Builder setLeaderBoardWeeklyUsers(ImmutableList<LeaderBoardUser> leaderBoardWeeklyUsers);

        public abstract Builder setLeaderBoardAllTimeUsers(ImmutableList<LeaderBoardUser> leaderBoardAllTimeUsers);

        public abstract LeaderBoardListModel build();

    }

}
