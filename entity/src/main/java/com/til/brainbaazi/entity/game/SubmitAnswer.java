package com.til.brainbaazi.entity.game;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by saurabh.garg on 26/02/18.
 */

@AutoValue
public abstract class SubmitAnswer implements Serializable {

    @SerializedName("a")
    public abstract String getA();

    @SerializedName("p")
    public abstract SubmitPayload getP();

    public static Builder builder() {
        return new AutoValue_SubmitAnswer.Builder();
    }

    public static TypeAdapter<SubmitAnswer> typeAdapter(Gson gson) {
        return new AutoValue_SubmitAnswer.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setA(String a);

        public abstract Builder setP(SubmitPayload p);

        public abstract SubmitAnswer build();
    }

}
