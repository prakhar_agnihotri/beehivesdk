package com.til.brainbaazi.entity;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 13/02/18.
 */

@AutoValue
public abstract class ConnectionInfo {

    public abstract boolean connectedToInternet();

    public abstract int type();

    @Nullable
    public abstract String name();

    public static Builder builder() {
        return new AutoValue_ConnectionInfo.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setConnectedToInternet(boolean value);

        public abstract Builder setType(int type);

        public abstract Builder setName(String name);

        public abstract ConnectionInfo build();
    }
}
