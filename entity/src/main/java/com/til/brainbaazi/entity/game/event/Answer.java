package com.til.brainbaazi.entity.game.event;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.ryanharter.auto.value.parcel.ParcelAdapter;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.typeadapter.AnswerOptionListAdapter;

/**
 * Created by prashant.rathore on 17/02/18.
 */

@AutoValue
public abstract class Answer implements GameEvent {

    @ParcelAdapter(AnswerOptionListAdapter.class)
    public abstract ImmutableList<AnswerOptions> getAnswerOptions();

    public abstract long getCorrectAnswerId();

    public abstract long getQuestionId();

    public abstract int getMaxUserCount();

    public abstract int getLifeUsedCount();

    public static Builder builder() {
        return new AutoValue_Answer.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setAnswerOptions(AnswerOptions... answerOptions);

        public abstract Builder setCorrectAnswerId(long correctAnswerId);

        public abstract Builder setQuestionId(long questionId);

        public abstract Builder setMaxUserCount(int maxUserCount);

        public abstract Builder setLifeUsedCount(int lifeUsedCount);

        public abstract Answer build();

    }


}
