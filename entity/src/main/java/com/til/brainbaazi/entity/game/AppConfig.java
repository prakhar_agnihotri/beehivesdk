package com.til.brainbaazi.entity.game;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.til.brainbaazi.entity.config.LanguageOption;

import javax.annotation.Nullable;

/**
 * Created by prashant.rathore on 24/02/18.
 */
@AutoValue
public abstract class AppConfig {

    public abstract int getVersion();
    public abstract int getChatFrequency();
    public abstract boolean isInMaintenance();
    public abstract String getAbusiveChatReferencePath();
    public abstract boolean isRegistered();

    @Nullable
    public abstract ImmutableList<LanguageOption> getLanguageOptions();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_AppConfig.Builder().setRegistered(false);
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setVersion(int version);
        public abstract Builder setChatFrequency(int frequency);
        public abstract Builder setInMaintenance(boolean inMaintenance);
        public abstract Builder setAbusiveChatReferencePath(String path);
        public abstract Builder setRegistered(boolean value);
        public abstract Builder setLanguageOptions(LanguageOption... languageOptions);

        public abstract AppConfig build();
    }

}
