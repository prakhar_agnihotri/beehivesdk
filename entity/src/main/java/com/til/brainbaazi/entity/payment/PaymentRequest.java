package com.til.brainbaazi.entity.payment;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by arpit.toshniwal on 23/02/18.
 */

@AutoValue
public abstract class PaymentRequest {

    public abstract String getSrc();

    @Nullable
    public abstract String getEmail();

    public abstract String getMob();

    public abstract String getIp();

    public abstract String getAmt();

    public static PaymentRequest.Builder builder(){
        return new AutoValue_PaymentRequest.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder{
        public abstract Builder setSrc(String src);

        public abstract Builder setEmail(String email);

        public abstract Builder setMob(String mob);

        public abstract Builder setIp(String ip);

        public abstract Builder setAmt(String amt);

        public abstract PaymentRequest build();
    }
}
