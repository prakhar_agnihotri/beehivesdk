package com.til.brainbaazi.entity.game;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableMap;
import com.ryanharter.auto.value.parcel.ParcelAdapter;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.typeadapter.QuestionIdMapAdapter;

import java.io.Serializable;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 19/02/18.
 */

@AutoValue
public abstract class GameState implements Parcelable {

    public static final int PLAY_STATE_PLAYING = 0;
    public static final int PLAY_STATE_LATE = 1;
    public static final int PLAY_STATE_ELIMINATED = 2;
    public static final int PLAY_STATE_ELIMINATED_ON_QUIT = 3;

    @Nullable
    public abstract InputEvent getLastQuestionEvent();

    @ParcelAdapter(QuestionIdMapAdapter.class)
    @Nullable
    public abstract ImmutableMap<Long, QuestionInfo> getQuestions();

    @Nullable
    public abstract Winner getWinner();

    public abstract boolean isLate();

    public abstract int getLifeUsed();

    public abstract boolean isEliminated();

    public abstract int getLifeAvailable();

    public abstract int getUserPlayState();

    public abstract int getConcurrentUserCount();

    public abstract boolean isGameLive();

    public abstract boolean isWinnerDisplayed();

    public static Builder builderDefault() {
        GameState.Builder builder = builder()
                .setEliminated(false)
                .setGameLive(false)
                .setLate(false)
                .setLifeUsed(0)
                .setUserPlayState(PLAY_STATE_PLAYING)
                .setLifeAvailable(0)
                .setEliminated(false)
                .setConcurrentUserCount(0)
                .setWinnerDisplayed(false)
                .setWinner(null)
                .setQuestions(ImmutableMap.<Long, QuestionInfo>builder().build());
        return builder;
    }

    public static final Parcelable.Creator<AutoValue_GameState> CREATOR() {
        return AutoValue_GameState.CREATOR;
    }


    public static Builder builder() {
        return new AutoValue_GameState.Builder();
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setLastQuestionEvent(InputEvent inputEvent);

        public abstract Builder setQuestions(ImmutableMap<Long, QuestionInfo> questionInfoMap);

        public abstract Builder setLate(boolean late);

        public abstract Builder setLifeUsed(int lifeUsed);

        public abstract Builder setEliminated(boolean eliminated);

        public abstract Builder setUserPlayState(int userPlayState);

        public abstract Builder setConcurrentUserCount(int concurrentUsers);

        public abstract Builder setGameLive(boolean gameLive);

        public abstract Builder setLifeAvailable(int lifeAvailable);

        public abstract Builder setWinnerDisplayed(boolean winnerDisplayed);

        public abstract Builder setWinner(Winner winner);

        public abstract GameState build();
    }

}
