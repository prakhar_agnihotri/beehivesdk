package com.brainbaazi.component.repo;

import com.google.common.collect.ImmutableList;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.ReferralResponse;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.UserStatus;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.SubmitAnswerRequestModel;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.WinUser;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.AmIWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LastGameWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardListModel;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardUser;
import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.otp.CountryModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.user.UpdateUserImageModel;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 22/02/18.
 */

public class DataRepositoryDevImpl implements DataRepository {

    private BehaviorSubject<User> userPublisher;

    @Override
    public Observable<Response<User>> loadUserInfo() {
        User user = User.builder()
                .setLives(1)
                .setUserBalance(450)
                .setUserLifeTimeBalance(10000)
                .setPhoneNumber("123456789")
                .setUserImgUrl("testImageUrl")
                .setUserName("Test User Name")
                .setWeeklyRank(2)
                .build();
        Response<User> build = Response.<User>builder().setValue(user).setSuccess(true).build();
        return Observable.just(build);
    }

    @Override
    public void logOutUser() {

    }

    @Override
    public Observable<Response<DashboardInfo>> loadDashboardInfo() {
        GameInfo gameInfo = GameInfo.builder()
                .setCurrentGameID(1L)
                .setGamePrize(1000)
                .setTimeStamp(1L)
                .setStreamingUrl("http://timeslive.live-s.cdn.bitgravity.com/cdn-live/_definst_/timeslive/live/timesnow.smil/playlist.m3u8")
                .build();

        User user = User.builder()
                .setLives(1)
                .setUserBalance(450)
                .setUserLifeTimeBalance(10000)
                .setPhoneNumber("123456789")
                .setUserImgUrl("testImageUrl")
                .setUserName("Test User Name")
                .setWeeklyRank(2)
                .build();

        DashboardInfo build1 = DashboardInfo.builder()
                .setCurrentGameInfo(gameInfo)
                .setNextGameInfo(gameInfo)
                .setActive(true)
                .setActiveInfo("")
                .setServerTime(System.currentTimeMillis())
                .setUser(user).build();
        Response<DashboardInfo> build = Response.<GameInfo>builder().setValue(build1).setSuccess(true).build();
        return Observable.just(build).delay(2, TimeUnit.SECONDS);
    }

    @Override
    public Observable<UserStatus> observeUserStatus() {
        User user = User.builder()
                .setLives(1)
                .setUserBalance(450)
                .setUserLifeTimeBalance(10000)
                .setPhoneNumber("123456789")
                .setUserImgUrl("testImageUrl")
                .setUserName("Test User Name")
                .setWeeklyRank(2)
                .build();

        UserStatus userStatus = UserStatus.builder()
                .setStatus(UserStatus.STATUS_UNREGISTERED)
                .setUser(user)
                .build();
        return Observable.just(userStatus);
    }

    @Override
    public Observable<Response<ReferralResponse>> addReferral(String referralId) {
        return null;
    }

    @Override
    public Observable<Response<UserStatus>> loginUser(String token, UserRequestModel userRequestModel) {
        return null;
    }

    @Override
    public Observable<Response<UserStatus>> registerUser(UserRequestModel userRequestModel) {
        return null;
    }

    @Override
    public Observable<ChatMsg> observerChatMessages(GameInfo gameInfo) {
        ChatMsg[] chatResponses = new ChatMsg[20];

        for (int i = 0; i < 20; i++) {
            String index = String.valueOf(i + 1);
            ChatMsg chatMsg = ChatMsg.builder().setImgUrl("").setMessage("Message " + index).setName("User " + index).build();
            chatResponses[i] = chatMsg;
        }

        Observable<Long> interval = Observable.interval(1, TimeUnit.SECONDS);
        BiFunction<ChatMsg, Long, ChatMsg> biFunction = new BiFunction<ChatMsg, Long, ChatMsg>() {
            @Override
            public ChatMsg apply(ChatMsg chatMsgResponse, Long aLong) throws Exception {
                return chatMsgResponse;
            }
        };
        return Observable.fromArray(chatResponses)
                .zipWith(interval, biFunction);
    }

    @Override
    public Observable<InputEvent> observerGameEvents() {
        return null;
    }

    @Override
    public Observable<Response<LastGameWinnerResponse>> loadLastGameWinnerData(String appendUrl) {
        return null;
    }

    @Override
    public Observable<Response<LeaderBoardListModel>> loadLeaderBoardData() {
        ImmutableList.Builder<LeaderBoardUser> leaderBoardUsers = ImmutableList.<LeaderBoardUser>builder();

        for (int i = 0; i < 20; i++) {
            int index = i + 1;
            leaderBoardUsers.add(LeaderBoardUser.builder().setUserName("User " + index).setUserRank(index).setImgUrl("").setPrize(100L).build());
        }

        LeaderBoardListModel leaderBoardListModel = LeaderBoardListModel.builder()
                .setLeaderBoardWeeklyUsers(leaderBoardUsers.build())
                .setLeaderBoardAllTimeUsers(leaderBoardUsers.build())
                .setWeeklyScore(123)
                .setAllTimeScore(4312)
                .build();

        return Observable.<Response<LeaderBoardListModel>>just(Response.builder().setSuccess(true).setValue(leaderBoardListModel).build());
    }

    @Override
    public Observable<Response<CountryListModel>> loadCountryListModel() {
        CountryModel[] countryModels = new CountryModel[20];

        countryModels[0] = CountryModel.builder()
                .setCode("IN")
                .setName("India")
                .setDialCode("+91")
                .setImgUrl("")
                .build();
        for (int i = 1; i < 20; i++) {
            String index = String.valueOf(i + 1);
            countryModels[i] = CountryModel.builder().setName("Country " + index).setCode(index).setDialCode("+" + index).setImgUrl("").build();
        }

        CountryListModel listModel = CountryListModel.builder()
                .setCountryModels(countryModels)
                .build();
        return Observable.<Response<CountryListModel>>just(Response.builder().setSuccess(true).setValue(listModel).build());
    }

    @Override
    public Observable<Response<User>> loadUpdateUserImageResponse(UpdateUserImageModel imageModel) {
        User user = User.builder()
                .setLives(1)
                .setUserBalance(450)
                .setUserLifeTimeBalance(10000)
                .setPhoneNumber("123456789")
                .setUserImgUrl("testImageUrl")
                .setUserName("Test User Name")
                .setWeeklyRank(2)
                .build();
        Response<User> build = Response.<User>builder().setValue(user).setSuccess(true).build();
        return Observable.just(build);
    }

    @Override
    public Observable<Response<AppConfig>> loadAppConfig() {
        AppConfig appConfig = AppConfig.builder().setAbusiveChatReferencePath("")
                .setVersion(1)
                .setInMaintenance(false)
                .setChatFrequency(4)
                .build();
        Response<AppConfig> build = Response.<AppConfig>builder().setValue(appConfig).setSuccess(true).build();
        return Observable.just(build);
    }

    @Override
    public Observable<String> submitUserAnswer(SubmitAnswerRequestModel answerRequestModel) {
        return null;
    }

    @Override
    public Observable<Response<AmIWinnerResponse>> sendWinnerRequest() {
        AmIWinnerResponse winnerResponse = AmIWinnerResponse.builder().setWin(true)
                .build();
        Response<AmIWinnerResponse> build = Response.<AmIWinnerResponse>builder().setValue(winnerResponse).setSuccess(true).build();
        return Observable.just(build);
    }

    @Override
    public Observable<Response<Winner>> loadGameWinners() {
        Winner.Builder builder = Winner.builder();
        WinUser[] winUsers = new WinUser[20];
        for (int i = 0; i < winUsers.length; i++) {
            winUsers[i] = WinUser.builder()
                    .setImgUrl("")
                    .setName("Name " + (i + 1))
                    .build();
        }
        builder.setWinUserList(winUsers);
        builder.setPrizeAmount(100).setEmpty(false);
        Response<Winner> build = Response.<Winner>builder().setValue(builder.build()).setSuccess(true).build();
        return Observable.just(build);
    }

    @Override
    public String getAuthToken() {
        return null;
    }

    @Override
    public String getGToken() {
        return null;
    }

    @Override
    public String getUserName() {
        return null;
    }

    @Override
    public int getChatFrequency() {
        return 0;
    }

    @Override
    public String getChatAbusiveText() {
        return null;
    }

    @Override
    public String getSharedPrefString(String key, String defValue) {
        return null;
    }

    @Override
    public void setSharedPrefString(String key, String value) {

    }

    @Override
    public Observable<BrainbaaziStrings> observeBrainBaaziStrings() {
        return null;
    }

    @Override
    public BrainbaaziStrings loadDefaultStrings() {
        return null;
    }

    @Override
    public void setBrainbaaziStrings(BrainbaaziStrings brainBaaziStrings) {

    }

    @Override
    public Observable<Boolean> checkChatMessageValid(String message) {
        return null;
    }

    /*@Override
    public Observable<Response<User>> loadUserStatus(UserRequestModel requestModel) {
        User user = User.builder()
                .setLives(1)
                .setUserBalance(450)
                .setUserLifeTimeBalance(10000)
                .setPhoneNumber("123456789")
                .setUserImgUrl("testImageUrl")
                .setUserName("Test User Name")
                .setWeeklyRank(2)
                .build();
        Response<User> build = Response.<User>builder().setValue(user).setSuccess(true).build();
        return Observable.just(build);
    }*/

    @Override
    public Observable<UsernameAvailableResponse> loadUsernameAvailableResponse(String userName) {
        UsernameAvailableResponse appConfig = UsernameAvailableResponse.builder().
                setStatus(true).
                setCheckType(UsernameAvailableResponse.CHECK_USERNAME).
                build();
        return Observable.just(appConfig);
    }
}
