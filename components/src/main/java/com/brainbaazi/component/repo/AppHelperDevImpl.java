package com.brainbaazi.component.repo;

import com.til.brainbaazi.entity.strings.BrainbaaziStrings;

/**
 * Created by prashant.rathore on 24/02/18.
 */

public class AppHelperDevImpl implements AppHelper {


    @Override
    public int getAppVersion() {
        return 73;
    }

    @Override
    public String getDeviceId() {
        return "sampletoken";
    }

    @Override
    public String getIpAddress() {
        return "1:1:1:1";
    }

    @Override
    public BrainbaaziStrings getDefaultBrainBaaziStrings() {
        return null;
    }
}
