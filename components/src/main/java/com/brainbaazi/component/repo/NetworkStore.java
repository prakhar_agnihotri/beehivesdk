package com.brainbaazi.component.repo;

import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.ReferralResponse;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.entity.game.SubmitAnswerRequestModel;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.AmIWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LastGameWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardListModel;
import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.user.UpdateUserImageModel;
import com.til.brainbaazi.entity.user.UpdateUserImageResponse;
import com.til.brainbaazi.entity.user.UserLoginResponse;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;

import io.reactivex.Observable;

/**
 * Created by prashant.rathore on 23/02/18.
 */

public interface NetworkStore {

    public Observable<User> loadUserInfo(String authToken);

    public Observable<UserLoginResponse> loginUser(final String authTokeh, final UserRequestModel userRequestModel);

    public Observable<UserLoginResponse> registerUser(String g_token, final UserRequestModel userRequestModel);

    public Observable<LastGameWinnerResponse> loadLastGameWinnerData(String appendUrl, String authToken);

    public Observable<LeaderBoardListModel> loadLeaderBoardData(String authToken);

    public Observable<CountryListModel> loadCountryListModel(String authToken);

    public Observable<AppConfig> loadAppConfig();

    public Observable<String> loadAbusiveChatData(final AppConfig appConfig);

    public Observable<UsernameAvailableResponse> loadUsernameAvailableResponse(String gtoken, String username);

    public Observable<User> loadUpdateUserImageResponse(String authToken, UpdateUserImageModel imageModel);

    Observable<DashboardInfo> loadDashboardInfo(String authToken);

    Observable<ReferralResponse> addReferral(String authToken, String referralId);

    public Observable<AmIWinnerResponse> sendWinnerRequest(String authToken);

    public Observable<Winner> loadGameWinners(String authToken);

    public Observable<String> submitUserAnswer(final String authTokem, final SubmitAnswerRequestModel answerRequestModel);

}
