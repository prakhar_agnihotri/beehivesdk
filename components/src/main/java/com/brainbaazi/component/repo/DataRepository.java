package com.brainbaazi.component.repo;

import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.ReferralResponse;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.UserStatus;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.SubmitAnswerRequestModel;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.AmIWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LastGameWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardListModel;
import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.user.UpdateUserImageModel;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;

import io.reactivex.Observable;

/**
 * Created by prashant.rathore on 15/02/18.
 */

public interface DataRepository {
    public static final String KEY_GTOKEN = "gtoken";
    public static final String KEY_AUTH_TOKEN = "userAuthToken";
    public static final String KEY_CHAT_FREQUENCY = "chat_frequency";
    public static final String KEY_CHAT_ABUSIVE_TEXT = "chat_abusive_text";

    public Observable<Response<User>> loadUserInfo();

    public void logOutUser();

    public Observable<Response<DashboardInfo>> loadDashboardInfo();

    public Observable<UserStatus> observeUserStatus();

    public Observable<Response<ReferralResponse>> addReferral(String referralId);

    public Observable<Response<UserStatus>> loginUser(String token, UserRequestModel userRequestModel);

    public Observable<Response<UserStatus>> registerUser(UserRequestModel userRequestModel);

    public Observable<ChatMsg> observerChatMessages(GameInfo gameInfo);

    public Observable<InputEvent> observerGameEvents();

    public Observable<Response<LastGameWinnerResponse>> loadLastGameWinnerData(String appendUrl);

    public Observable<Response<LeaderBoardListModel>> loadLeaderBoardData();

    public Observable<Response<CountryListModel>> loadCountryListModel();

    public Observable<UsernameAvailableResponse> loadUsernameAvailableResponse(String userName);

    public Observable<Response<User>> loadUpdateUserImageResponse(UpdateUserImageModel imageModel);

    public Observable<Response<AppConfig>> loadAppConfig();

    public Observable<String> submitUserAnswer(SubmitAnswerRequestModel answerRequestModel);

    public Observable<Response<AmIWinnerResponse>> sendWinnerRequest();

    public Observable<Response<Winner>> loadGameWinners();

    String getAuthToken();

    String getGToken();

    public String getUserName();

    public int getChatFrequency();

    public String getChatAbusiveText();

    public String getSharedPrefString(String key, String defValue);

    public void setSharedPrefString(String key, String value);

    public Observable<BrainbaaziStrings> observeBrainBaaziStrings();

    public BrainbaaziStrings loadDefaultStrings();

    void setBrainbaaziStrings(BrainbaaziStrings brainBaaziStrings);

    Observable<Boolean> checkChatMessageValid(String message);
}


