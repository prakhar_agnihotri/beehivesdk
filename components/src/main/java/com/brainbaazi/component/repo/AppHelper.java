package com.brainbaazi.component.repo;

import com.til.brainbaazi.entity.strings.BrainbaaziStrings;

/**
 * Created by prashant.rathore on 24/02/18.
 */

public interface AppHelper {
    int getAppVersion();

    String getDeviceId();

    String getIpAddress();

    BrainbaaziStrings getDefaultBrainBaaziStrings();
}
