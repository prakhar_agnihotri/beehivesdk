package com.brainbaazi.component.repo;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;

/**
 * Created by prashant.rathore on 25/02/18.
 */

public interface MemoryStore {

    public GameInfo getCurrentGameInfo();
    public GameInfo getNextGameInfo();

}
