package com.brainbaazi.component.repo;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.SubmitAnswer;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.entity.game.chat.ChatSendObject;
import com.til.brainbaazi.entity.game.event.InputEvent;

import io.reactivex.Observable;


/**
 * Created by prashant.rathore on 23/02/18.
 */

public interface SocketStore {

    public void openSocket(String authToken, User user, GameInfo gameInfo);

    void disconnectSocket();

    Observable<ChatMsg> observeChatMessages(GameInfo gameInfo);

    Observable<InputEvent> observeGameEvents();

    void sendMessage(String message);

    void sendMessage(SubmitAnswer submitAnswer);

    void sendChatMessage(ChatSendObject chatSendObject);

    boolean getSocketStatus();

}
