package com.brainbaazi.component.network;

import com.til.brainbaazi.entity.ConnectionInfo;

import io.reactivex.Observable;

/**
 * Created by prashant.rathore on 13/02/18.
 */

public interface ConnectionManager {

    public Observable<ConnectionInfo> observeNetworkChanges();

    public int TYPE_UNKNWON = -1;
    public int TYPE_OFFLINE = 0;
    public int TYPE_WIFI = 1;
    public int TYPE_MOBILE = 2;
}
