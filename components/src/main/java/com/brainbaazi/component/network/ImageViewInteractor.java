package com.brainbaazi.component.network;

/**
 * Created by prashant.rathore on 24/02/18.
 */

public interface ImageViewInteractor {
    public void setImageUrl(String imageUrl);
    public void setDefault(int res);
}
