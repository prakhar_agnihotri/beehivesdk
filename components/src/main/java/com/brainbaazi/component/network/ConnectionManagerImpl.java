package com.brainbaazi.component.network;

import com.til.brainbaazi.entity.ConnectionInfo;

import io.reactivex.Observable;

/**
 * Created by prashant.rathore on 22/02/18.
 */

public class ConnectionManagerImpl implements ConnectionManager {
    @Override
    public Observable<ConnectionInfo> observeNetworkChanges() {
        return Observable.just(ConnectionInfo.builder().
                setConnectedToInternet(true)
                .setName("Wifi")
                .setType(0).build());
    }
}
