package com.brainbaazi.component;

import android.os.Bundle;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;

import java.util.Map;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 26/02/18.
 */

public interface Analytics {

    public int SCREEN_SPLASH = 1;
    public int SCREEN_OTP_GENERATE = 2;
    public int SCREEN_OTP_VERIFY = 3;
    public int SCREEN_OTP_PROFILE = 4;
    public int SCREEN_REGISTER_USER = 5;
    public int SCREEN_DASHBOARD = 6;
    public int SCREEN_GAMEPLAY = 7;
    public int SCREEN_LEADERBOARD_ALL_TIME = 8;
    public int SCREEN_LEADERBOARD_WEEKLY = 9;
    public int SCREEN_MENU = 10;
    public int SCREEN_FORCE_UPDATE = 11;
    public int SCREEN_ISD_SELECTION = 12;
    public int SCREEN_CHAT_SWIPE = 13;
    public int SCREEN_ZERO_BALANCE_SCREEN = 14;
    public int SCREEN_BALANCE_SCREEN = 15;
    public int SCREEN_LEADER_BOARD_SCREEN = 16;
    public int SCREEN_PAYMENT_MOBIKWICK = 17;
    public int SCREEN_PAYMENT_SUCCESS = 18;
    public int SCREEN_PAYMENT_FAILURE = 19;
    public int SCREEN_MAINTENANCE = 20;
    public int EVENT_QUESTION_SHOWN = 21;
    public int EVENT_ANSWER_SHOWN = 22;
    public int EVENT_ANSWER_SUBMIT = 23;


    public void logFireBaseEvent(GaEventModel eventModel);

    public void logFireBaseScreen(GaEventModel eventModel, @Nullable Bundle bundle);

    public void logFireBaseScreen(int screenType);

    public void setFireBaseUser(User user);

    public void cleverTapScreenEvent(User user, String eventName);

    public void cleverTapEvent(String eventName, Map<String, Object> map);

    public void cleverAppLaunchEvent(String firstLaunch, String deviceId, String city, String installSource);

    public void cleverWalletCashoutEvent(User user, String cashoutAmount, String walletName, String transactionStatus);

    public void cleverMobiqwikCreationEvent(User user, String status);

    public void cleverBalanceEvent(User user, String eventName, String source);

    public String getTimeStampInHHMMSSIST();

    public void logScreenView(int screenId);
}
