package com.brainbaazi.component.payment;

import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;

import io.reactivex.Observable;

/**
 * Created by arpit.toshniwal on 23/02/18.
 */

public interface PaymentRequests {

    Observable<PaymentResponseMessage> transferAmount(String authenticationToken, PaymentRequest paymentRequest);

    Observable<PaymentResponseMessage> generateMobikwikOTP(String authenticationToken);

    Observable<PaymentResponseMessage> verifyMobikwikOTP(String email, String otp, String authenticationToken);

    Observable<BrainbaaziStrings> loadBrainBaziStrings();
    BrainbaaziStrings loadDefaultBrainBaziStrings();

    void updateBrainbaaziStrings(BrainbaaziStrings brainbaaziStrings);
}
