package com.brainbaazi.component.payment;

import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

/**
 * Created by arpit.toshniwal on 23/02/18.
 */

public class PaymentRequestsDevImpl implements PaymentRequests {

    @Override
    public Observable<PaymentResponseMessage> transferAmount(String authenticationToken, PaymentRequest paymentRequest) {
        PaymentResponseMessage paymentResponseMessage = PaymentResponseMessage.builder().
                setStatusDescription("Wahhh Mazza Agaya bhai").
                setStatusMessage("Payment is successful").
                setOrderId("78yu65ghy").
                setRequestSuccess(true).
                build();
        return Observable.just(paymentResponseMessage).delay(5, TimeUnit.SECONDS);
    }

    @Override
    public Observable<PaymentResponseMessage> generateMobikwikOTP(String authenticationToken) {
        PaymentResponseMessage paymentResponseMessage = PaymentResponseMessage.builder().
                setStatusCode("0").
                setRequestSuccess(true).
                build();
        return Observable.just(paymentResponseMessage).delay(5, TimeUnit.SECONDS);
    }

    @Override
    public Observable<PaymentResponseMessage> verifyMobikwikOTP(String email, String otp, String authenticationToken) {
        PaymentResponseMessage paymentResponseMessage = PaymentResponseMessage.builder().
                setStatusCode("0").
                setStatusDescription("Mobiwiki Wallet Creation Failed").
                setOrderId("78yu65ghy").
                setRequestSuccess(true).
                build();
        return Observable.just(paymentResponseMessage).delay(5, TimeUnit.SECONDS);
    }

    @Override
    public Observable<BrainbaaziStrings> loadBrainBaziStrings() {
        return null;
    }

    @Override
    public BrainbaaziStrings loadDefaultBrainBaziStrings() {
        return null;
    }

    @Override
    public void updateBrainbaaziStrings(BrainbaaziStrings brainbaaziStrings) {

    }
}
