package com.brainbaazi.component;

import android.os.Bundle;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;

import java.util.Map;

/**
 * Created by prashant.rathore on 03/03/18.
 */

public class AnalyticsDevImpl implements Analytics {

    @Override
    public void logFireBaseEvent(GaEventModel eventModel) {

    }

    @Override
    public void logFireBaseScreen(GaEventModel eventModel, Bundle bundle) {

    }

    @Override
    public void logFireBaseScreen(int screenType) {

    }

    @Override
    public void setFireBaseUser(User user) {

    }

    @Override
    public void cleverTapScreenEvent(User user, String eventName) {

    }

    @Override
    public void cleverTapEvent(String eventName, Map<String, Object> map) {

    }

    @Override
    public void cleverAppLaunchEvent(String firstLaunch, String deviceId, String city, String installSource) {

    }

    @Override
    public void cleverWalletCashoutEvent(User user, String cashoutAmount, String walletName, String transactionStatus) {

    }

    @Override
    public void cleverMobiqwikCreationEvent(User user, String status) {

    }

    @Override
    public void cleverBalanceEvent(User user, String eventName, String source) {

    }

    @Override
    public String getTimeStampInHHMMSSIST() {
        return null;
    }

    @Override
    public void logScreenView(int screenId) {

    }
}
