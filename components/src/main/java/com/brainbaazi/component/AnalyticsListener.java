package com.brainbaazi.component;

/**
 * Created by prashant.rathore on 04/03/18.
 */

public interface AnalyticsListener {

    public void onScreenView(int screenId);

}
