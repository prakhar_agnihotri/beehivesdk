package com.brainbaazi.component.cache;

import android.os.Parcelable;

/**
 * Created by prashant.rathore on 18/02/18.
 */

public interface Cache {
    public String CACHE_KEY_PHONE_NUMBER = "UserPhoneNumber";

    public void saveParcelable(String key, Parcelable parcelable);

    public <T extends Parcelable> T loadParcelable(String key, Parcelable.Creator<T> creator);

}
