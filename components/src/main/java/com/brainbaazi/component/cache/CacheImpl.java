package com.brainbaazi.component.cache;

import android.os.Parcelable;

/**
 * Created by prashant.rathore on 22/02/18.
 */

public class CacheImpl implements Cache {

    @Override
    public void saveParcelable(String key, Parcelable parcelable) {

    }

    @Override
    public <T extends Parcelable> T loadParcelable(String key, Parcelable.Creator<T> creator) {
        return null;
    }
}
