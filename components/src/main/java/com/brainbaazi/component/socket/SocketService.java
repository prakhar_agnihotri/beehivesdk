package com.brainbaazi.component.socket;

import com.til.brainbaazi.entity.game.SocketData;
import com.til.brainbaazi.entity.game.event.InputEvent;

import io.reactivex.Observable;

/**
 * Created by prashant.rathore on 18/02/18.
 */

public interface SocketService {
    public Observable<SocketData> observeSocketData();
    public boolean sendData();
}
