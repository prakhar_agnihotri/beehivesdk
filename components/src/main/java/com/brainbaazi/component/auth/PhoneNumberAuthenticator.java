package com.brainbaazi.component.auth;

import android.net.Uri;

import com.til.brainbaazi.entity.otp.OTPRequest;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.otp.PhoneNumber;

import io.reactivex.Observable;

/**
 * Created by prashant.rathore on 20/02/18.
 */

public interface PhoneNumberAuthenticator {

    public void verifyOTP(String otp, PhoneNumber phoneNumber);

    public void generateOtp(PhoneNumber phoneNumber, boolean isCodeResend);

    public Observable<OTPResponse> observeOtpCallbacks();

    public Observable<String> observeSMSCodeCallbacks();

    public void uploadUserImage(Uri filePath);

}
