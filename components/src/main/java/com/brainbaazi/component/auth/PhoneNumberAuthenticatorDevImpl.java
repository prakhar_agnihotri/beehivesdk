package com.brainbaazi.component.auth;

import android.net.Uri;

import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.otp.PhoneNumber;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by prashant.rathore on 25/02/18.
 */

public class PhoneNumberAuthenticatorDevImpl implements PhoneNumberAuthenticator {

    private PublishSubject<OTPResponse> otpResponsePublisher = PublishSubject.create();


    @Override
    public void verifyOTP(String otp, PhoneNumber phoneNumber) {
        otpResponsePublisher.onNext(OTPResponse.verificationSuccess("sometoken", phoneNumber));
    }

    @Override
    public void generateOtp(PhoneNumber phoneNumber, boolean isCodeResend) {
        otpResponsePublisher.onNext(OTPResponse.requestSentSuccess(phoneNumber));
//        verifyOTP(null);
    }

    @Override
    public Observable<OTPResponse> observeOtpCallbacks() {
        return otpResponsePublisher.delay(2, TimeUnit.SECONDS);
    }

    @Override
    public Observable<String> observeSMSCodeCallbacks() {
        return null;
    }

    @Override
    public void uploadUserImage(Uri filePath) {

    }
}
