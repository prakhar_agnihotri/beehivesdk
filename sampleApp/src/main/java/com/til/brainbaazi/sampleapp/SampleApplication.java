package com.til.brainbaazi.sampleapp;

import android.support.multidex.MultiDexApplication;

import com.brainbaazi.core.BrainBaazi;
import com.brainbaazi.core.BrainBaaziConfig;
import com.crashlytics.android.Crashlytics;
import com.squareup.leakcanary.LeakCanary;
import com.til.brainbaazi.impl.BrainbaaziAnalyticsListener;

import io.fabric.sdk.android.Fabric;

/**
 * Created by prashant.rathore on 01/03/18.
 */

public class SampleApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        BrainBaaziConfig config = BrainBaaziConfig.builder().setAnalyticsListener(new BrainbaaziAnalyticsListener()).setAppKey("testappKey").build();
        BrainBaazi.init(this, config);
        LeakCanary.install(this);
        Fabric.with(this, new Crashlytics());
    }
}
