package com.til.brainbaazi.network.rest.request;

import com.android.volley.toolbox.RequestFuture;
import com.til.brainbaazi.entity.game.GameEventUtils;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import org.json.JSONObject;

/**
 * Created by saurabh.garg on 2/27/18.
 */

public class LoadWinnerRequest extends GzipJsonRequest<Winner> {

    public LoadWinnerRequest(String url, com.android.volley.Response.Listener listener, com.android.volley.Response.ErrorListener errorListener) {
        super(Method.POST, url, null, listener, errorListener);
    }


    @Override
    protected Winner parse(byte[] data) throws Exception {
        JSONObject jsonObject = new JSONObject(new String(data));
        return GameEventUtils.getWinnerData(jsonObject);
    }


    public static LoadWinnerRequest createRequest(String url, RequestFuture<Winner> requestFuture, String authTokeh) {
        LoadWinnerRequest request = new LoadWinnerRequest(url, requestFuture, requestFuture);
        request.addHeader(ApiConstants.AUTH_TOKEN, authTokeh);
        return request;
    }
}