package com.til.brainbaazi.network.rest;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.brainbaazi.component.repo.NetworkStore;
import com.google.gson.Gson;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.ReferralResponse;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.entity.game.GameEventUtils;
import com.til.brainbaazi.entity.game.SubmitAnswerRequestModel;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.AmIWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LastGameWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardListModel;
import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.user.UpdateUserImageModel;
import com.til.brainbaazi.entity.user.UserLoginResponse;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;
import com.til.brainbaazi.network.rest.request.AbusiveChatRequest;
import com.til.brainbaazi.network.rest.request.AddReferralCodeRequest;
import com.til.brainbaazi.network.rest.request.AppConfigRequest;
import com.til.brainbaazi.network.rest.request.DashboardRequest;
import com.til.brainbaazi.network.rest.request.LastGameWinnerRequest;
import com.til.brainbaazi.network.rest.request.LeaderboardRequest;
import com.til.brainbaazi.network.rest.request.LoadWinnerRequest;
import com.til.brainbaazi.network.rest.request.SendWinnerRequest;
import com.til.brainbaazi.network.rest.request.SubmitAnswerRequest;
import com.til.brainbaazi.network.rest.request.UserImageUpdateRequest;
import com.til.brainbaazi.network.rest.request.UserLoginRequest;
import com.til.brainbaazi.network.rest.request.UserNameExistsRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by prashant.rathore on 24/02/18.
 */

public class NetworkStoreImpl implements NetworkStore {

    private static NetworkStoreImpl instance;

    private Gson gson;
    private Context appContext;

    final RequestQueue volleyQueue;
    private final Uri baseUrl;
    private final Uri configUrlBase;

    private NetworkStoreImpl(Context context, String baseUrl, String configUrl) {
        this.appContext = context;
        this.baseUrl = Uri.parse(baseUrl);
        this.configUrlBase = Uri.parse(configUrl);
        volleyQueue = Volley.newRequestQueue(context,BBStack.stack(context));
        gson = new Gson();
    }

    public static NetworkStoreImpl getInstance(Context context) {
        if (instance == null) {
            synchronized (NetworkStoreImpl.class) {
                if (instance == null) {
                    String baseUrl = context.getString(R.string.url_base);
                    String configUrl = context.getString(R.string.url_config);
                    instance = new NetworkStoreImpl(context, baseUrl, configUrl);
                }
            }
        }
        return instance;
    }

    @Override
    public Observable<AppConfig> loadAppConfig() {
        return Observable.generate(new Consumer<Emitter<AppConfig>>() {
            @Override
            public void accept(Emitter<AppConfig> appConfigEmitter) throws Exception {
                Uri.Builder url = configUrlBase.buildUpon()
                        .appendEncodedPath(ApiConstants.BBCONFIG_PATH);
                RequestFuture<AppConfig> requestFuture = RequestFuture.newFuture();
                AppConfigRequest request = AppConfigRequest.createRequest(requestFuture, url);
                volleyQueue.add(request);
                AppConfig appConfig = requestFuture.get(60, TimeUnit.SECONDS);
                appConfigEmitter.onNext(appConfig);
                appConfigEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<String> loadAbusiveChatData(final AppConfig appConfig) {
        return Observable.generate(new Consumer<Emitter<String>>() {
            @Override
            public void accept(Emitter<String> emitter) throws Exception {
                Uri.Builder url = configUrlBase.buildUpon()
                        .appendEncodedPath(appConfig.getAbusiveChatReferencePath());
                RequestFuture<String> requestFuture = RequestFuture.newFuture();
                AbusiveChatRequest request = AbusiveChatRequest.createRequest(requestFuture, url);
                volleyQueue.add(request);
                String value = requestFuture.get();
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<User> loadUserInfo(String authToken) {
//        RequestFuture<Object> requestFuture = RequestFuture.newFuture();
//        return makeVolleyRequest(req);
        return null;
    }

    @Override
    public Observable<UserLoginResponse> loginUser(final String authTokeh, final UserRequestModel userRequestModel) {
        return Observable.generate(new Consumer<Emitter<UserLoginResponse>>() {
            @Override
            public void accept(Emitter<UserLoginResponse> emitter) throws Exception {
                Log.d("BBAPP", "User Login " + authTokeh + " " + UserRequestModel.typeAdapter(gson).toJson(userRequestModel).toString());
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.USER_LOGIN);
                RequestFuture<UserLoginResponse> requestFuture = RequestFuture.newFuture();
                UserLoginRequest request = UserLoginRequest.create(gson, url.build().toString(), requestFuture, userRequestModel, authTokeh, appContext.getResources().getString(R.string.bb_app_identifier));
                volleyQueue.add(request);
                UserLoginResponse value = requestFuture.get();
                Log.d("BBAPP", "User Login Success " + UserLoginResponse.typeAdapter(gson).toJson(value).toString());
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    public static void nuke() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                            return myTrustedAnchors;
                        }

                        @Override
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                        @Override
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Observable<UserLoginResponse> registerUser(final String g_token, final UserRequestModel userRequestModel) {
        return Observable.generate(new Consumer<Emitter<UserLoginResponse>>() {
            @Override
            public void accept(Emitter<UserLoginResponse> emitter) throws Exception {
                Log.d("BBAPP", "User Login " + g_token + " " + UserRequestModel.typeAdapter(gson).toJson(userRequestModel).toString());
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.REGISTER_USER_PATH);
                RequestFuture<UserLoginResponse> requestFuture = RequestFuture.newFuture();
                UserLoginRequest request = UserLoginRequest.create(gson, url.build().toString(), requestFuture, userRequestModel, g_token, appContext.getResources().getString(R.string.bb_app_identifier));
                volleyQueue.add(request);
                UserLoginResponse value = requestFuture.get();
                Log.d("BBAPP", "User Login Success " + UserLoginResponse.typeAdapter(gson).toJson(value).toString());
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<DashboardInfo> loadDashboardInfo(final String authToken) {
        return Observable.generate(new Consumer<Emitter<DashboardInfo>>() {
            @Override
            public void accept(Emitter<DashboardInfo> emitter) throws Exception {
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.DASHBOARD_PATH);
                RequestFuture<DashboardInfo> requestFuture = RequestFuture.newFuture();
                DashboardRequest request = DashboardRequest.create(gson, url.build().toString(), requestFuture, authToken);
                volleyQueue.add(request);
                DashboardInfo value = requestFuture.get(60, TimeUnit.SECONDS);
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ReferralResponse> addReferral(final String authToken, final String referralId) {
        return Observable.generate(new Consumer<Emitter<ReferralResponse>>() {
            @Override
            public void accept(Emitter<ReferralResponse> emitter) throws Exception {
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.ADD_REFERRAL_PATH);
                RequestFuture<ReferralResponse> requestFuture = RequestFuture.newFuture();
                AddReferralCodeRequest request = AddReferralCodeRequest.create(url.build().toString(), requestFuture, authToken, referralId);
                volleyQueue.add(request);
                ReferralResponse value = requestFuture.get(60, TimeUnit.SECONDS);
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<AmIWinnerResponse> sendWinnerRequest(final String authToken) {
        return Observable.generate(new Consumer<Emitter<AmIWinnerResponse>>() {
            @Override
            public void accept(Emitter<AmIWinnerResponse> emitter) throws Exception {
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.AM_I_WINNER_PATH);
                RequestFuture<AmIWinnerResponse> requestFuture = RequestFuture.newFuture();
                SendWinnerRequest request = SendWinnerRequest.createRequest(gson, url.build().toString(), requestFuture, authToken);
                volleyQueue.add(request);
                AmIWinnerResponse value = requestFuture.get(60, TimeUnit.SECONDS);
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Winner> loadGameWinners(final String authToken) {
        return Observable.generate(new Consumer<Emitter<Winner>>() {
            @Override
            public void accept(Emitter<Winner> emitter) throws Exception {
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.WINNER_LIST_PATH);
                RequestFuture<Winner> requestFuture = RequestFuture.newFuture();
                LoadWinnerRequest request = LoadWinnerRequest.createRequest(url.build().toString(), requestFuture, authToken);
                volleyQueue.add(request);
                Winner value = requestFuture.get(60, TimeUnit.SECONDS);
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<String> submitUserAnswer(final String authToken, final SubmitAnswerRequestModel answerRequestModel) {
        return Observable.generate(new Consumer<Emitter<String>>() {
            @Override
            public void accept(Emitter<String> emitter) throws Exception {

                Log.d("BBAPP", "Submitting ansmwer " + answerRequestModel.toString());
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.SUBMIT_ANSWER_PATH);
                RequestFuture<String> requestFuture = RequestFuture.newFuture();
                SubmitAnswerRequest request = SubmitAnswerRequest.create(gson, url.build().toString(), requestFuture, answerRequestModel, authToken);
                volleyQueue.add(request);
                String s = requestFuture.get();
                Log.d("BBAPP", "Answer submitted ");
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io()).onErrorReturn(new Function<Throwable, String>() {
            @Override
            public String apply(Throwable throwable) throws Exception {
                Log.d("BBAPP", "Answer Exception " + throwable.getMessage());
                throwable.printStackTrace();
                return "";
            }
        });
    }

    @Override
    public Observable<LastGameWinnerResponse> loadLastGameWinnerData(final String appendUrl, final String authToken) {
        return Observable.generate(new Consumer<Emitter<LastGameWinnerResponse>>() {
            @Override
            public void accept(Emitter<LastGameWinnerResponse> emitter) throws Exception {
                Uri.Builder url;
                if (TextUtils.isEmpty(appendUrl)) {
                    url = baseUrl.buildUpon()
                            .appendEncodedPath(ApiConstants.LAST_GAME_LEADERBOARD_PATH);
                } else {
                    url = Uri.parse(ApiConstants.LAST_GAME_MORE_LEADERBOARD_PATH).buildUpon().appendEncodedPath(appendUrl);
                }
                RequestFuture<LastGameWinnerResponse> requestFuture = RequestFuture.newFuture();
                LastGameWinnerRequest request = LastGameWinnerRequest.create(gson, url.build().toString(), requestFuture, authToken, TextUtils.isEmpty(appendUrl));
                volleyQueue.add(request);
                LastGameWinnerResponse value = requestFuture.get();
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<LeaderBoardListModel> loadLeaderBoardData(final String authToken) {
        return Observable.generate(new Consumer<Emitter<LeaderBoardListModel>>() {
            @Override
            public void accept(Emitter<LeaderBoardListModel> emitter) throws Exception {
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.LEADERBOARD_PATH);
                RequestFuture<LeaderBoardListModel> requestFuture = RequestFuture.newFuture();
                LeaderboardRequest request = LeaderboardRequest.create(gson, url.build().toString(), requestFuture, authToken);
                volleyQueue.add(request);
                LeaderBoardListModel value = requestFuture.get();
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<User> loadUpdateUserImageResponse(final String authToken, final UpdateUserImageModel imageModel) {
        return Observable.generate(new Consumer<Emitter<User>>() {
            @Override
            public void accept(Emitter<User> emitter) throws Exception {
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.UPDATE_IMAGE_PATH);
                RequestFuture<User> requestFuture = RequestFuture.newFuture();
                UserImageUpdateRequest request = UserImageUpdateRequest.create(gson, url.toString(), requestFuture, imageModel, authToken);
                volleyQueue.add(request);
                User value = requestFuture.get(60, TimeUnit.SECONDS);
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<UsernameAvailableResponse> loadUsernameAvailableResponse(final String g_token, final String userName) {
        return Observable.generate(new Consumer<Emitter<UsernameAvailableResponse>>() {
            @Override
            public void accept(Emitter<UsernameAvailableResponse> emitter) throws Exception {
                Uri.Builder url = baseUrl.buildUpon()
                        .appendEncodedPath(ApiConstants.CHECK_GAME_AVAILABLE_PATH);
                RequestFuture<UsernameAvailableResponse> requestFuture = RequestFuture.newFuture();
                UserNameExistsRequest request = UserNameExistsRequest.create(requestFuture, url.toString(), userName, g_token);
                volleyQueue.add(request);
                UsernameAvailableResponse value = requestFuture.get(60, TimeUnit.SECONDS);
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<CountryListModel> loadCountryListModel(String authToken) {
        return Observable.generate(new Consumer<Emitter<CountryListModel>>() {
            @Override
            public void accept(Emitter<CountryListModel> emitter) throws Exception {
                InputStream inputStream = appContext.getResources().openRawResource(R.raw.country_codes);
                Writer writer = new StringWriter();
                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } catch (Exception ignored) {
                } finally {
                    try {
                        inputStream.close();
                    } catch (IOException ignored) {
                    }
                }
                String jsonString = writer.toString();
                CountryListModel value = GameEventUtils.parseCountryListModel(jsonString);
                emitter.onNext(value);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }


    public RequestQueue requestQueue() {
        return this.volleyQueue;
    }
}
