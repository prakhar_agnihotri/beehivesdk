package com.til.brainbaazi.network.rest.request;

import android.net.Uri;
import android.util.JsonReader;
import android.util.JsonToken;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.RequestFuture;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by prashant.rathore on 24/02/18.
 */

public class AbusiveChatRequest extends GzipJsonRequest<String> {

    public AbusiveChatRequest(String url, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, null, listener, errorListener);
        setShouldCache(true);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        response.headers.put("Cache-Control","max-age="+System.currentTimeMillis() + 24 * 60 * 60 *1000);
        return super.parseNetworkResponse(response);
    }

    @Override
    protected String parse(byte[] data) throws Exception {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        JsonReader jsonReader = new JsonReader(new InputStreamReader(bis));
        String value = null;
        if(jsonReader.hasNext() && jsonReader.peek() == JsonToken.BEGIN_OBJECT) {
            jsonReader.beginObject();
            while(jsonReader.hasNext()) {
                String nodeName = jsonReader.nextName();
                if(jsonReader.peek() == JsonToken.STRING && "p".equals(nodeName)) {
                    value = jsonReader.nextString();
                }
                else {
                    jsonReader.skipValue();
                }
            }
        }
        if(value == null) {
            throw new NullPointerException("No Chat found");
        }
        return value;
    }


    public static AbusiveChatRequest createRequest(RequestFuture<String> requestFuture, Uri.Builder url) {
        String number;
        Date date = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm", Locale.ENGLISH);
        try {
            number = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            number = "";
        }
        url.appendQueryParameter("date", System.currentTimeMillis() + "_" + number)
                .build();
        return new AbusiveChatRequest(url.build().toString(),requestFuture,requestFuture);
    }
}
