package com.til.brainbaazi.network.rest;

import android.net.Uri;

import com.android.volley.Request;
import com.android.volley.toolbox.RequestFuture;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.network.rest.payment.CreateMobikwikWalletRequest;
import com.til.brainbaazi.network.rest.payment.GenerateMobikwikOtpRequest;
import com.til.brainbaazi.network.rest.payment.TransferAmountRequest;

import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by arpit.toshniwal on 24/02/18.
 */

public class PaymentRequestImpl implements PaymentRequests {

    private final NetworkStoreImpl networkStore;
    private final Uri paymentsUrlUri;
    private final AppHelper appHelper;
    private BehaviorSubject<BrainbaaziStrings> brainbaaziStringsPublisher = BehaviorSubject.create();

    public PaymentRequestImpl(NetworkStoreImpl networkStore, String paymentsUrl, AppHelper appHelper) {
        this.networkStore = networkStore;
        this.paymentsUrlUri = Uri.parse(paymentsUrl);
        this.appHelper = appHelper;
    }

    @Override
    public Observable<PaymentResponseMessage> transferAmount(final String authenticationToken, final PaymentRequest paymentRequest) {
        return Observable.generate(new Consumer<Emitter<PaymentResponseMessage>>() {
            @Override
            public void accept(Emitter<PaymentResponseMessage> appConfigEmitter) throws Exception {
                String url = paymentsUrlUri.
                        buildUpon().
                        appendEncodedPath(ApiConstants.CHECKOUT_PAYMENT_BASE_PATH).
                        appendEncodedPath(paymentRequest.getSrc().toLowerCase()).
                        build().toString();
                RequestFuture<PaymentResponseMessage> future = RequestFuture.newFuture();
                TransferAmountRequest request = TransferAmountRequest.create(future, url, authenticationToken);
                addRequest(request);
                PaymentResponseMessage paymentResponseMessage = future.get();
                appConfigEmitter.onNext(paymentResponseMessage);
                appConfigEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());

    }

    @Override
    public Observable<PaymentResponseMessage> generateMobikwikOTP(final String authenticationToken) {
        return Observable.generate(new Consumer<Emitter<PaymentResponseMessage>>() {
            @Override
            public void accept(Emitter<PaymentResponseMessage> appConfigEmitter) throws Exception {

                String url = paymentsUrlUri.
                        buildUpon().
                        appendEncodedPath(ApiConstants.SEND_MOBIKWIK_OTP_PATH).
                        build().toString();
                RequestFuture<PaymentResponseMessage> future = RequestFuture.newFuture();
                GenerateMobikwikOtpRequest request = GenerateMobikwikOtpRequest.create(future, url, authenticationToken);
                addRequest(request);
                PaymentResponseMessage paymentResponseMessage = future.get();
                appConfigEmitter.onNext(paymentResponseMessage);
                appConfigEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<PaymentResponseMessage> verifyMobikwikOTP(final String email, final String otp, final String authenticationToken) {

        return Observable.generate(new Consumer<Emitter<PaymentResponseMessage>>() {
            @Override
            public void accept(Emitter<PaymentResponseMessage> appConfigEmitter) throws Exception {
                String url = paymentsUrlUri.
                        buildUpon().
                        appendEncodedPath(ApiConstants.CREATE_MOBIKWIK_WALLET_PATH).
                        appendQueryParameter("email", email).
                        appendQueryParameter("otp", otp).
                        build().toString();
                RequestFuture<PaymentResponseMessage> future = RequestFuture.newFuture();
                CreateMobikwikWalletRequest request = CreateMobikwikWalletRequest.create(future, url, authenticationToken);
                addRequest(request);
                PaymentResponseMessage paymentResponseMessage = future.get();
                appConfigEmitter.onNext(paymentResponseMessage);
                appConfigEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<BrainbaaziStrings> loadBrainBaziStrings() {
        return brainbaaziStringsPublisher;
    }

    @Override
    public BrainbaaziStrings loadDefaultBrainBaziStrings() {
        return appHelper.getDefaultBrainBaaziStrings();
    }

    @Override
    public void updateBrainbaaziStrings(BrainbaaziStrings brainbaaziStrings) {
        brainbaaziStringsPublisher.onNext(brainbaaziStrings);
    }

    private void addRequest(Request<?> request) {
        networkStore.requestQueue().add(request);
    }
}
