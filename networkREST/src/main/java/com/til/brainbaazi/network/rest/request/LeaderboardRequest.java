package com.til.brainbaazi.network.rest.request;

import com.android.volley.toolbox.RequestFuture;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardListModel;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardUser;
import com.til.brainbaazi.entity.user.UserLoginResponse;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by prashant.rathore on 25/02/18.
 */

public class LeaderboardRequest extends GzipJsonRequest<LeaderBoardListModel> {

    private Gson gson;

    public LeaderboardRequest(Gson gson, String url, com.android.volley.Response.Listener listener, com.android.volley.Response.ErrorListener errorListener) {
        super(Method.POST, url, null, listener, errorListener);
        this.gson = gson;
    }

    @Override
    protected LeaderBoardListModel parse(byte[] data) throws Exception {
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(new ByteArrayInputStream(data));
            JsonReader jsonReader = new JsonReader(inputStreamReader);
            return parseValue(jsonReader);
        }
        finally {
            if(inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                }
                catch (Exception e) {}
            }
        }
    }


    public static LeaderboardRequest create(Gson gson, String url, RequestFuture<LeaderBoardListModel> requestFuture, String authTokeh) {
        LeaderboardRequest request = new LeaderboardRequest(gson,url, requestFuture, requestFuture);
        request.addHeader(ApiConstants.AUTH_TOKEN, authTokeh);
        return request;
    }


    private LeaderBoardListModel parseValue(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        }
        jsonReader.beginObject();
        ImmutableList.Builder<LeaderBoardUser> weeklyBuilder = ImmutableList.<LeaderBoardUser>builder();
        ImmutableList.Builder<LeaderBoardUser> allTimeBuilder = ImmutableList.<LeaderBoardUser>builder();
        LeaderBoardListModel.Builder builder = LeaderBoardListModel.builder();
        while (jsonReader.hasNext()) {
            String _name = jsonReader.nextName();
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                continue;
            }
            switch (_name) {
                case "weeklyScore": {
                    builder.setWeeklyScore(jsonReader.nextLong());
                    break;
                }
                case "alltimeScore": {
                    builder.setAllTimeScore(jsonReader.nextLong());
                    break;
                }
                case "weekly": {
                    if(jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                        jsonReader.beginArray();
                        while(jsonReader.hasNext()) {
                            weeklyBuilder.add(LeaderBoardUser.typeAdapter(gson).read(jsonReader));
                        }
                        builder.setLeaderBoardWeeklyUsers(weeklyBuilder.build());
                        jsonReader.endArray();
                    }
                    break;
                }
                case "alltime": {
                    if(jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                        jsonReader.beginArray();
                        while(jsonReader.hasNext()) {
                            allTimeBuilder.add(LeaderBoardUser.typeAdapter(gson).read(jsonReader));
                        }
                        builder.setLeaderBoardAllTimeUsers(allTimeBuilder.build());
                        jsonReader.endArray();
                    }
                    break;
                }
                default: {
                    jsonReader.skipValue();
                }
            }
        }
        jsonReader.endObject();
        return builder.build();
    }
}
