package com.til.brainbaazi.network.rest.payment;

import com.android.volley.Response;
import com.android.volley.toolbox.RequestFuture;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import org.json.JSONObject;

/**
 * Created by arpit.toshniwal on 24/02/18.
 */

public class CreateMobikwikWalletRequest extends GzipJsonRequest<PaymentResponseMessage> {
    public CreateMobikwikWalletRequest(String url, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, null, listener, errorListener);
    }

    @Override
    protected PaymentResponseMessage parse(byte[] data) throws Exception {
        JSONObject jsonObject = new JSONObject(new String(data));
        Boolean status = null;
        Boolean error = null;
        boolean isRequestSuccess = false;
        if(jsonObject.has("status")){
            status = jsonObject.optBoolean("status");
        }
        if(jsonObject.has("error")){
            error = jsonObject.optBoolean("error");
        }
        if ((status != null && status) || (error != null && !error)) {
            isRequestSuccess = true;
        }
        JSONObject message = jsonObject.optJSONObject("message");
        return PaymentResponseMessage.builder().
                setRequestSuccess(isRequestSuccess).
                setMessageCode(message.optString("messagecode")).
                setStatus(message.optString("status")).
                setStatusCode(message.optString("statuscode")).
                setStatusDescription(message.optString("statusdescription")).
                setChecksum(message.optString("checksum")).
                setEmailAddress(message.optString("emailaddress")).
                setNonZeroFlag(message.optString("nonzeroflag")).
                setOrderId(message.optString("orderId")).
                setStatusMessage(message.optString("statusMessage")).
                setMetaData(message.optString("metadata")).
                setTxnId(message.optString("txnId")).
                build();
    }

    public static CreateMobikwikWalletRequest create(RequestFuture<PaymentResponseMessage> future, String url, String authenticationToken) {
        CreateMobikwikWalletRequest request = new CreateMobikwikWalletRequest(url, future, future);
        request.addHeader(ApiConstants.AUTH_TOKEN, authenticationToken);
        return request;
    }
}
