package com.til.brainbaazi.network.rest.request;

import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.game.SubmitAnswerRequestModel;
import com.til.brainbaazi.entity.user.UserLoginResponse;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

/**
 * Created by prashant.rathore on 25/02/18.
 */

public class SubmitAnswerRequest extends GzipJsonRequest<String> {

    public SubmitAnswerRequest(Gson gson, String url, String requestBody, com.android.volley.Response.Listener listener, com.android.volley.Response.ErrorListener errorListener) {
        super(Method.POST, url, requestBody, listener, errorListener);
    }

    @Override
    protected String parse(byte[] data) throws Exception {
        return "";
    }


    public static SubmitAnswerRequest create(Gson gson, String url, RequestFuture<String> requestFuture, SubmitAnswerRequestModel userRequestModel, String authTokeh) {
        TypeAdapter<SubmitAnswerRequestModel> answerRequestModelTypeAdapter = SubmitAnswerRequestModel.typeAdapter(gson);
        String requestBody = answerRequestModelTypeAdapter.toJson(userRequestModel);
        SubmitAnswerRequest request = new SubmitAnswerRequest(gson, url, requestBody, requestFuture, requestFuture);
        request.addHeader(ApiConstants.AUTH_TOKEN, authTokeh);
        return request;
    }
}
