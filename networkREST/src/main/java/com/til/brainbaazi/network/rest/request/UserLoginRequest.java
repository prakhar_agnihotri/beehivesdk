package com.til.brainbaazi.network.rest.request;

import android.text.TextUtils;

import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.user.UserLoginResponse;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

/**
 * Created by prashant.rathore on 25/02/18.
 */

public class UserLoginRequest extends GzipJsonRequest<UserLoginResponse> {

    private Gson gson;

    public UserLoginRequest(Gson gson, String url, String requestBody, com.android.volley.Response.Listener listener, com.android.volley.Response.ErrorListener errorListener) {
        super(Method.POST, url, requestBody, listener, errorListener);
        this.gson = gson;
    }

    @Override
    protected UserLoginResponse parse(byte[] data) throws Exception {
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(new ByteArrayInputStream(data));
            return UserLoginResponse.typeAdapter(gson).fromJson(inputStreamReader);
        }
        finally {
            if(inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static UserLoginRequest create(Gson gson, String url, RequestFuture<UserLoginResponse> requestFuture, UserRequestModel userRequestModel, String authTokeh, String app_indentifier) {
        TypeAdapter<UserRequestModel> userRequestModelTypeAdapter = UserRequestModel.typeAdapter(gson);
        String requestBody = userRequestModelTypeAdapter.toJson(userRequestModel);
        UserLoginRequest request = new UserLoginRequest(gson,url, requestBody, requestFuture, requestFuture);
        request.addHeader(ApiConstants.GOOGLE_REGISTRATION_TOKEN, authTokeh);
        if(!TextUtils.isEmpty(app_indentifier)) {
            request.addHeader(ApiConstants.HEADER_APP_IDENTIFIER, app_indentifier);
        }
        return request;
    }
}
