package com.til.brainbaazi.network.rest.payment;

import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.toolbox.RequestFuture;
import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import org.json.JSONObject;

/**
 * Created by arpit.toshniwal on 24/02/18.
 */

public class TransferAmountRequest extends GzipJsonRequest<PaymentResponseMessage> {
    public TransferAmountRequest(String url, String requestBody, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, requestBody, listener, errorListener);
    }

    @Override
    protected PaymentResponseMessage parse(byte[] data) throws Exception {
        JSONObject jsonObject = new JSONObject(new String(data));
        Boolean status = null;
        Boolean error = null;
        boolean isRequestSuccess = false;
        if(jsonObject.has("status")){
            status = jsonObject.optBoolean("status");
        }
        if(jsonObject.has("error")){
            error = jsonObject.optBoolean("error");
        }
        if ((status != null && status) || (error != null && !error)) {
            isRequestSuccess = true;
        }
        JSONObject message = jsonObject.optJSONObject("message");
        return PaymentResponseMessage.builder().
                setRequestSuccess(isRequestSuccess).
                setMessageCode(message.optString("messagecode")).
                setStatus(message.optString("status")).
                setStatusCode(message.optString("statuscode")).
                setStatusDescription(message.optString("statusdescription")).
                setChecksum(message.optString("checksum")).
                setEmailAddress(message.optString("emailaddress")).
                setNonZeroFlag(message.optString("nonzeroflag")).
                setOrderId(message.optString("orderId")).
                setStatusMessage(message.optString("statusMessage")).
                setMetaData(message.optString("metadata")).
                setTxnId(message.optString("txnId")).
                build();
    }

    public static TransferAmountRequest create(RequestFuture<PaymentResponseMessage> future, String url, String authenticationToken) {
        /*JSONObject object = new JSONObject();//paymentsobject
        try {
            object.put("mob", paymentRequest.getMob());
            object.put("amt", paymentRequest.getAmt());
            object.put("ip", paymentRequest.getIp());
            if(!TextUtils.isEmpty(paymentRequest.getEmail()))
                object.put("email", paymentRequest.getEmail());
            if(!TextUtils.isEmpty(paymentRequest.getSrc()))
                object.put("src", paymentRequest.getSrc());
        }catch (Exception e){
            e.printStackTrace();
        }*/
        TransferAmountRequest request = new TransferAmountRequest(url, null, future, future);
        request.addHeader(ApiConstants.AUTH_TOKEN, authenticationToken);
        return request;
    }
}
