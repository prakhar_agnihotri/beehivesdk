package com.til.brainbaazi.network.rest;

import android.content.Context;
import android.util.AttributeSet;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.brainbaazi.component.network.ImageViewInteractor;


/**
 * Created by prashant.rathore on 24/02/18.
 */

public class BBImageView extends NetworkImageView implements ImageViewInteractor {

    private static ImageLoader imageLoader;


    public BBImageView(Context context) {
        super(context);
        init(context);
    }

    public BBImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public BBImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    private void init(Context context) {
        if (imageLoader == null) {
            imageLoader = new ImageLoader(NetworkStoreImpl.getInstance(context).requestQueue(), new ImageCacheManager(10 * 1024 * 1024));
        }
    }

    @Override
    public void setImageUrl(String imageUrl) {
        super.setImageUrl(imageUrl, imageLoader);
    }

    @Override
    public void setDefault(int res) {
        super.setDefaultImageResId(res);
    }
}
