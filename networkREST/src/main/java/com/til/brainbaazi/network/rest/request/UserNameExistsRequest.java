package com.til.brainbaazi.network.rest.request;

import com.android.volley.Response;
import com.android.volley.toolbox.RequestFuture;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by arpit.toshniwal on 24/02/18.
 */

public class UserNameExistsRequest extends GzipJsonRequest<UsernameAvailableResponse> {
    public UserNameExistsRequest(String url, String requestBody, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, requestBody, listener, errorListener);
    }

    @Override
    protected UsernameAvailableResponse parse(byte[] data) throws Exception {
        JSONObject jsonObject = new JSONObject(new String(data));
        return parseData(jsonObject);
    }


    private UsernameAvailableResponse parseData(JSONObject payload) {
        String[] suggestions = null;
        boolean status = true;
        if (payload != null) {
            status = payload.optBoolean("status");
            JSONArray jsonArrayText = payload.optJSONArray("sgts");
            if (jsonArrayText != null && jsonArrayText.length() > 0) {
                int length = jsonArrayText.length();
                suggestions = new String[length];
                for (int index = 0; index < length; index++) {
                    suggestions[index] = jsonArrayText.optString(index);
                }
            }
        }
        return UsernameAvailableResponse.builder().
                setCheckType(UsernameAvailableResponse.CHECK_USERNAME).
                setStatus(status).
                setSuggestions(suggestions).
                build();
    }

    public static UserNameExistsRequest create(RequestFuture<UsernameAvailableResponse> future, String url, String username, String g_token) {
        JSONObject object = new JSONObject();
        try {
            object.put("unm", username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        UserNameExistsRequest request = new UserNameExistsRequest(url, object.toString(), future, future);
        request.addHeader(ApiConstants.GOOGLE_REGISTRATION_TOKEN, g_token);
        return request;
    }
}
