package com.til.brainbaazi.network.rest;

public class ApiConstants {

    public static final String AUTH_TOKEN = "authtoken";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";

    //    public static final String GOOGLE_REGISTRATION_TOKEN = "GRT";
    public static final String GOOGLE_REGISTRATION_TOKEN = "g_token";

    public static final String HEADER_APP_IDENTIFIER = "app_identifier";
    public static final String HEADER_APP_IDENTIFIER_VALUE = "1003214213125";

    
    // PATHS
    public static final String SUBMIT_PATH = "v1/trivia/submit";
    public static final String CHECK_GAME_AVAILABLE_PATH = "v1/trivia/checkgname";

    public static final String CHECK_USERNAME_AVAILABLE_PATH = "v1/trivia/checkgname";


    public static final String FAQ_PATH = "v1/trivia/faq";

    public static final String ADD_REFERRAL_PATH = "v1/trivia/addref";
    public static final String CHECKOUT_PAYMENT_PATH = "v1/trivia/payment/mobikwik";
    public static final String CHECKOUT_PAYTM_PAYMENT_PATH = "v1/trivia/payment/paytm";

    public static final String CHECKOUT_PAYMENT_BASE_PATH = "v1/trivia/payment";
    public static final String SEND_MOBIKWIK_OTP_PATH = "v1/trivia/otp/mobikwik";
    public static final String CREATE_MOBIKWIK_WALLET_PATH = "v1/trivia/wallet/mobikwik";

    public static final String REGISTER_USER_PATH = "v1/trivia/auth/register";

    public static final String UPDATE_IMAGE_PATH = "v1/trivia/updateuser";


    public static final String USER_LOGIN = "v1/trivia/auth/dashboard"; // check user already profile exist already
    public static final String DASHBOARD_PATH = "v1/trivia/dashboard";
    public static final String LEADERBOARD_PATH = "v1/trivia/leaderboard";
    public static final String CONFIG_PATH = "v1/trivia/config.json";
    public static final String CONCURRENT_API_PATH = "v1/trivia/activeusers";
    public static final String SUBMIT_ANSWER_PATH = "v1/trivia/submit";
    public static final String WINNER_LIST_PATH = "v1/trivia/winners";
    public static final String AM_I_WINNER_PATH = "v1/trivia/amiwinner";
    public static final String SUBMIT_GAMESTATE_PATH = "v1/trivia/userState";

    public static final String BBCONFIG_PATH = "config.json";
    public static final String DEVICE_CONFIG_PATH = "v1/trivia/deviceConfig";

    public static final String LAST_GAME_LEADERBOARD_PATH = "v2/trivia/leaderboard";
    public static final String LAST_GAME_MORE_LEADERBOARD_PATH = "https://brainbaazi.akamaized.net/";


}