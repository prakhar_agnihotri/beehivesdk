package com.til.brainbaazi.network.rest;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created by prashant.rathore on 24/02/18.
 */

public abstract class GzipJsonRequest<T> extends JsonRequest<T> {

    private Map<String,String> headers = new HashMap<>();

    public GzipJsonRequest(int method, String url, String requestBody, Response.Listener listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        setShouldCache(false);
        addHeader("Accept-Encoding","gzip");
        addHeader("Content-Type", "application/json");
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String contentEncoding = response.headers.get("ContentEncoding");
        byte[] data = response.data;
        try {
            if (!TextUtils.isEmpty(contentEncoding) && contentEncoding.contains("gzip")) {
                GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(response.data));
                byte[] b = new byte[1024];
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int read;
                while((read = gis.read(b))!= -1) {
                    baos.write(b,0,read);
                }
                data = baos.toByteArray();
            }
            T parse = parse(data);
            if(parse == null) {
                throw new NullPointerException("Unknown Error");
            }
            return Response.success(parse, HttpHeaderParser.parseCacheHeaders(response));
        }
        catch (Exception e) {
            e.printStackTrace();
            return Response.error(new VolleyError(response));
        }
    }



    protected abstract T parse(byte[] data) throws Exception;

    public void addHeader(String key, String value) {
        this.headers.put(key,value);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers;
    }

}
