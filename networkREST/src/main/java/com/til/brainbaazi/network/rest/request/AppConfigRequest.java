package com.til.brainbaazi.network.rest.request;

import android.net.Uri;

import com.android.volley.Response;
import com.android.volley.toolbox.RequestFuture;
import com.til.brainbaazi.entity.config.LanguageOption;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by prashant.rathore on 24/02/18.
 */

public class AppConfigRequest extends GzipJsonRequest<AppConfig> {

    public AppConfigRequest(String url, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, null, listener, errorListener);
    }


    @Override
    protected AppConfig parse(byte[] data) throws Exception {
        JSONObject jsonObject = new JSONObject(new String(data));

        LanguageOption[] languageOptions = null;
        JSONArray jsonArray = jsonObject.optJSONArray("lc");
        if (jsonArray != null && jsonArray.length() > 0) {
            int length = jsonArray.length();
            languageOptions = new LanguageOption[length];
            for (int index = 0; index < length; index++) {
                JSONObject ansOpt = jsonArray.optJSONObject(index);
                languageOptions[index] = LanguageOption.builder().
                        setLanguageCode(ansOpt.optString("lc")).
                        setVersion(ansOpt.optInt("v")).
                        setName(ansOpt.optString("nm")).
                        setTranslation(ansOpt.optString("tl")).
                        setGameDataTranslation(ansOpt.optString("gdtl")).
                        setGetStartedText(ansOpt.optString("tgs")).
                        setTagLineText(ansOpt.optString("ttl")).
                        setTermConditionText(ansOpt.optString("ttnc")).
                        setTermsText(ansOpt.optString("tms")).
                        setPrivacyText(ansOpt.optString("prv")).
                        setRulesText(ansOpt.optString("rls")).
                        setSelectLanguageText(ansOpt.optString("tsl")).
                        build();
            }
        }
        return AppConfig.builder()
                .setChatFrequency(jsonObject.optInt("cm", 0))
                .setInMaintenance(jsonObject.optInt("m", 0) == 1)
                .setVersion(jsonObject.optInt("v", 0))
                .setAbusiveChatReferencePath(jsonObject.optString("cd"))
                .setLanguageOptions(languageOptions)
                .build();

    }


    public static AppConfigRequest createRequest(RequestFuture<AppConfig> requestFuture, Uri.Builder url) {
        String number;
        Date date = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm", Locale.ENGLISH);
        try {
            formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            number = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            number = "";
        }
        url.appendQueryParameter("date", System.currentTimeMillis() + "_" + number)
                .build();
        return new AppConfigRequest(url.build().toString(), requestFuture, requestFuture);
    }
}
