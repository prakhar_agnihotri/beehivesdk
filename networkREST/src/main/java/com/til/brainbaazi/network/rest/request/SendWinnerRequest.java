package com.til.brainbaazi.network.rest.request;

import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.til.brainbaazi.entity.game.response.AmIWinnerResponse;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import org.json.JSONObject;

/**
 * Created by saurabh.garg on 2/27/18.
 */

public class SendWinnerRequest extends GzipJsonRequest<AmIWinnerResponse> {

    private Gson gson;

    public SendWinnerRequest(Gson gson, String url, com.android.volley.Response.Listener listener, com.android.volley.Response.ErrorListener errorListener) {
        super(Method.POST, url, null, listener, errorListener);
        this.gson = gson;
    }


    @Override
    protected AmIWinnerResponse parse(byte[] data) throws Exception {
        JSONObject jsonObject = new JSONObject(new String(data));
        return AmIWinnerResponse.builder()
                .setWin(jsonObject.optBoolean("w", false))
                .build();
    }


    public static SendWinnerRequest createRequest(Gson gsn, String url, RequestFuture<AmIWinnerResponse> requestFuture, String authTokeh) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(AmIWinnerResponse.class, AmIWinnerResponse.typeAdapter(gsn))
                .create();
        SendWinnerRequest request = new SendWinnerRequest(gson, url, requestFuture, requestFuture);
        request.addHeader(ApiConstants.AUTH_TOKEN, authTokeh);
        return request;
    }
}