package com.til.brainbaazi.network.rest.request;

import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by prashant.rathore on 25/02/18.
 */

public class DashboardRequest extends GzipJsonRequest<DashboardInfo> {

    private Gson gson;

    public DashboardRequest(Gson gson, String url, com.android.volley.Response.Listener listener, com.android.volley.Response.ErrorListener errorListener) {
        super(Method.POST, url, null, listener, errorListener);
        this.gson = gson;
    }

    @Override
    protected DashboardInfo parse(byte[] data) throws Exception {
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(new ByteArrayInputStream(data));
            JsonReader jsonReader = new JsonReader(inputStreamReader);
            return parseValue(jsonReader);
        }
        finally {
            if(inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                }
                catch (Exception e) {}
            }
        }
    }


    public static DashboardRequest create(Gson gsn, String url, RequestFuture<DashboardInfo> requestFuture, String authTokeh) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(User.class, User.typeAdapter(gsn))
                .registerTypeAdapter(GameInfo.class, GameInfo.typeAdapter(gsn))
                .create();
        DashboardRequest request = new DashboardRequest(gson,url, requestFuture, requestFuture);
        request.addHeader(ApiConstants.AUTH_TOKEN, authTokeh);
        return request;
    }

    private  DashboardInfo parseValue(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        }
        jsonReader.beginObject();
        User user = null;
        GameInfo currentGameInfo = null;
        GameInfo nextGameInfo = null;
        Boolean active = null;
        String activeInfo = null;
        Long serverTime = null;
        Boolean registered = null;
        while (jsonReader.hasNext()) {
            String _name = jsonReader.nextName();
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                continue;
            }
            switch (_name) {
                case "user": {
                    user = User.typeAdapter(gson).read(jsonReader);
                    break;
                }
                case "curg": {
                    try {
                        currentGameInfo = GameInfo.typeAdapter(gson).read(jsonReader);
                    }catch (Exception e){
                        currentGameInfo = null;
                        e.printStackTrace();
                    }
                    break;
                }
                case "nxtg": {
                    nextGameInfo = GameInfo.typeAdapter(gson).read(jsonReader);
                    break;
                }
                case "isActive": {
                    active = jsonReader.nextBoolean();
                    break;
                }
                case "active_info": {
                    activeInfo = jsonReader.nextString();
                    break;
                }
                case "stm": {
                    serverTime = jsonReader.nextLong();
                    break;
                }
                case "registered": {
                    registered = jsonReader.nextBoolean();
                    break;
                }
                default: {
                    jsonReader.skipValue();
                }
            }
        }
        if(currentGameInfo == null){
            active = false;
        }
        jsonReader.endObject();
        return  DashboardInfo.builder().
                setActive(active).
                setActiveInfo(activeInfo).
                setCurrentGameInfo(currentGameInfo).
                setNextGameInfo(nextGameInfo).
                setUser(user).
                setServerTime(serverTime).
                setRegistered(registered).
                build();
    }
}
