package com.til.brainbaazi.network.rest.request;

import com.android.volley.Response;
import com.android.volley.toolbox.RequestFuture;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.til.brainbaazi.entity.leaderBoard.LastGameWinnerResponse;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardUser;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by prashant.rathore on 25/02/18.
 */

public class LastGameWinnerRequest extends GzipJsonRequest<LastGameWinnerResponse> {

    private Gson gson;
    private boolean isFirstGameResponse;

    public LastGameWinnerRequest(int method, Gson gson, String url, boolean isFirstGameResponse, Response.Listener listener, Response.ErrorListener errorListener) {
        super(method, url, null, listener, errorListener);
        this.gson = gson;
        this.isFirstGameResponse = isFirstGameResponse;
    }

    @Override
    protected LastGameWinnerResponse parse(byte[] data) throws Exception {
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(new ByteArrayInputStream(data));
            JsonReader jsonReader = new JsonReader(inputStreamReader);
            return parseValue(jsonReader);
        } finally {
            if (inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                } catch (Exception e) {
                }
            }
        }
    }


    public static LastGameWinnerRequest create(Gson gson, String url, RequestFuture<LastGameWinnerResponse> requestFuture, String authTokeh, boolean isFirstGameResponse) {
        int method = (isFirstGameResponse ? Method.POST : Method.GET);
        LastGameWinnerRequest request = new LastGameWinnerRequest(method, gson, url, isFirstGameResponse, requestFuture, requestFuture);
        if (isFirstGameResponse) {
            request.addHeader(ApiConstants.AUTH_TOKEN, authTokeh);
        }
        return request;
    }


    private LastGameWinnerResponse parseValue(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        }
        jsonReader.beginObject();
        ImmutableList.Builder<LeaderBoardUser> lastGameWinnerBuilder = ImmutableList.<LeaderBoardUser>builder();
        LastGameWinnerResponse.Builder builder = LastGameWinnerResponse.builder().setFirstListResponse(isFirstGameResponse);
        while (jsonReader.hasNext()) {
            String _name = jsonReader.nextName();
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                continue;
            }
            switch (_name) {
                case "pu": {
                    builder.setPreviousUrl(jsonReader.nextString());
                    break;
                }
                case "nu": {
                    builder.setNextUrl(jsonReader.nextString());
                    break;
                }
                case "dt": {
                    builder.setDateTime(jsonReader.nextLong());
                    break;
                }
                case "tw": {
                    builder.setTotalWinner(jsonReader.nextString());
                    break;
                }
                case "pm": {
                    builder.setPrizeMoney(jsonReader.nextString());
                    break;
                }
                case "amountPerUser": {
                    builder.setAmountPerUser(jsonReader.nextInt());
                    break;
                }
                case "users": {
                    if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                        jsonReader.beginArray();
                        while (jsonReader.hasNext()) {
                            lastGameWinnerBuilder.add(LeaderBoardUser.typeAdapter(gson).read(jsonReader));
                        }
                        builder.setLastGameWinner(lastGameWinnerBuilder.build());
                        jsonReader.endArray();
                    }
                    break;
                }
                default: {
                    jsonReader.skipValue();
                }
            }
        }
        jsonReader.endObject();
        return builder.build();
    }
}
