package com.til.brainbaazi.network.rest.request;

import com.android.volley.Response;
import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.user.UpdateUserImageModel;
import com.til.brainbaazi.entity.user.UpdateUserImageResponse;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

/**
 * Created by arpit.toshniwal on 24/02/18.
 */

public class UserImageUpdateRequest extends GzipJsonRequest<User> {
    private Gson gson;

    public UserImageUpdateRequest(Gson gson, String url, String requestBody, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, requestBody, listener, errorListener);
        this.gson = gson;
    }

    @Override
    protected User parse(byte[] data) throws Exception {
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(new ByteArrayInputStream(data));
            return User.typeAdapter(gson).fromJson(inputStreamReader);
        } finally {
            if (inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public static UserImageUpdateRequest create(Gson gson, String url, RequestFuture<User> requestFuture, UpdateUserImageModel userRequestModel, String authTokeh) {
        TypeAdapter<UpdateUserImageModel> userRequestModelTypeAdapter = UpdateUserImageModel.typeAdapter(gson);
        String requestBody = userRequestModelTypeAdapter.toJson(userRequestModel);
        UserImageUpdateRequest request = new UserImageUpdateRequest(gson, url, requestBody, requestFuture, requestFuture);
        request.addHeader(ApiConstants.AUTH_TOKEN, authTokeh);
        return request;
    }
}
