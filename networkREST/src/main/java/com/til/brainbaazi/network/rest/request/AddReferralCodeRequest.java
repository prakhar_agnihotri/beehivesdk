package com.til.brainbaazi.network.rest.request;

import com.android.volley.Response;
import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.ReferralResponse;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.network.rest.ApiConstants;
import com.til.brainbaazi.network.rest.GzipJsonRequest;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by prashant.rathore on 25/02/18.
 */

public class AddReferralCodeRequest extends GzipJsonRequest<ReferralResponse> {

    public AddReferralCodeRequest(String url, String requestBody, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, requestBody, listener, errorListener);
    }

    @Override
    protected ReferralResponse parse(byte[] data) throws Exception {
        JSONObject jsonObject = new JSONObject(new String(data));
        return ReferralResponse.builder().
                setError(jsonObject.optString("error", "")).
                build();
    }


    public static AddReferralCodeRequest create(String url, RequestFuture<ReferralResponse> requestFuture, String authTokeh, String referralId) {

        JSONObject object = new JSONObject();
        try {
            object.put("rid", referralId);
        }catch (Exception e){
            e.printStackTrace();
        }
        AddReferralCodeRequest request = new AddReferralCodeRequest(url, object.toString(), requestFuture, requestFuture);
        request.addHeader(ApiConstants.AUTH_TOKEN, authTokeh);
        return request;
    }
}
