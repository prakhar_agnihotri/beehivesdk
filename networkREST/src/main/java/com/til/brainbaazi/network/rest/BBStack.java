package com.til.brainbaazi.network.rest;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by saurabh.garg on 3/7/18.
 */

public class BBStack {

    public static HurlStack stack(Context context) {
        RequestQueue requestQueue;

        HurlStack stack;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {


            try {
                ProviderInstaller.installIfNeeded(context);
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            try {
                stack = new HurlStack(null, new TLSSocketFactory());
            } catch (KeyManagementException e) {
                e.printStackTrace();
                Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            }
        }
        else {
            stack = new HurlStack();
        }
        return stack;

//            requestQueue = Volley.newRequestQueue(context, stack);
//        } else {
//            requestQueue = Volley.newRequestQueue(context);
//        }
//        return new HurlStack(requestQueue);
    }


}
