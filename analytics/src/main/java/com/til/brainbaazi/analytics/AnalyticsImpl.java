package com.til.brainbaazi.analytics;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.AnalyticsListener;
import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by prashant.rathore on 26/02/18.
 */

public class AnalyticsImpl implements Analytics {

    private final AnalyticsListener listener;
    private final int CUSTOM_DIMEN_PARENT_APP;

    private static AnalyticsImpl instance;
    private Context context;
    private Tracker sTracker;
    private CleverTapAPI cleverTapAPI;
    //private FirebaseAnalytics firebaseAnalytics;

    public AnalyticsImpl(Context context, AnalyticsListener analyticsListener) {
        this.listener = analyticsListener;
        this.context = context.getApplicationContext();
        this.sTracker = getDefaultTracker(context.getString(R.string.bb_ga_id));
        this.cleverTapAPI = getCleverTapAPI(context.getString(R.string.bb_clevertap_acc_id), context.getString(R.string.bb_clevertap_acc_token));
        this.CUSTOM_DIMEN_PARENT_APP = context.getResources().getInteger(R.integer.bb_ga_parent_app_cd);
        //this.firebaseAnalytics = getFirebaseAnalytics();
    }

    public static AnalyticsImpl getInstance() {
        return instance;
    }

    public static AnalyticsImpl getInstance(Context context, AnalyticsListener analyticsListener) {
        if (instance == null) {
            synchronized (AnalyticsImpl.class) {
                if (instance == null) {
                    instance = new AnalyticsImpl(context, analyticsListener);
                }
            }
        }
        return instance;
    }

    @Override
    public void logFireBaseEvent(GaEventModel eventModel) {
        sendGaEvents(eventModel);
        try {
            Bundle bundle = new Bundle();
            String category = (!TextUtils.isEmpty(eventModel.getCategory()) ? eventModel.getCategory() : "NA");
            String action = (!TextUtils.isEmpty(eventModel.getAction()) ? eventModel.getAction() : "NA");
            String label = (!TextUtils.isEmpty(eventModel.getLabel()) ? eventModel.getLabel() : "NA");
            String userName = (!TextUtils.isEmpty(eventModel.getUserName()) ? eventModel.getUserName() : "NA");

            bundle.putString("category", category);
            bundle.putString("action", action);
            bundle.putString("label", label);
            bundle.putString("username", userName);
            bundle.putString("time_stamp", eventModel.getTimeStamp());
            sendFirebaseEvent(eventModel.getMainEvent(), bundle);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void logFireBaseScreen(GaEventModel eventModel, Bundle bundle) {
        sendFirebaseEvent(eventModel.getMainEvent(), bundle);
    }

    @Override
    public void logFireBaseScreen(int screenType) {
        String screenName = null;
        switch (screenType) {
            case SCREEN_SPLASH:
                screenName = "SplashScreen";
                break;
            case SCREEN_OTP_GENERATE:
                screenName = "LoginScreen";
                break;
            case SCREEN_OTP_VERIFY:
                screenName = "OTPScreen";
                break;
            case SCREEN_OTP_PROFILE:
                screenName = "OTPProfileScreen";
                break;
            case SCREEN_REGISTER_USER:
                screenName = "ProfileScreen";
                break;
            case SCREEN_DASHBOARD:
                screenName = "DashboardScreen";
                break;
            case SCREEN_GAMEPLAY:
                screenName = "LiveGameScreen";
                break;
            case SCREEN_LEADERBOARD_ALL_TIME:
                screenName = "AllTimeWinnerScreen";
                break;
            case SCREEN_LEADERBOARD_WEEKLY:
                screenName = "WeeklyWinnerScreen";
                break;
            case SCREEN_MENU:
                screenName = "MenuScreen";
                break;

            case SCREEN_FORCE_UPDATE:
                screenName = "ForceUpdateScreen";
                break;
            case SCREEN_ISD_SELECTION:
                screenName = "ISDSelectionScreen";
                break;
            case SCREEN_CHAT_SWIPE:
                screenName = "ChatSwipeScreen";
                break;
            case SCREEN_ZERO_BALANCE_SCREEN:
                screenName = "ZeroBalanceScreen";
                break;
            case SCREEN_BALANCE_SCREEN:
                screenName = "BalanceScreen";
                break;
            case SCREEN_LEADER_BOARD_SCREEN:
                screenName = "LeaderBoardScreen";
                break;
            case SCREEN_PAYMENT_MOBIKWICK:
                screenName = "PaymentMobikwickScreen";
                break;
            case SCREEN_PAYMENT_SUCCESS:
                screenName = "PaymentSuccessScreen";
                break;
            case SCREEN_PAYMENT_FAILURE:
                screenName = "PaymentFailureScreen";
                break;
            case SCREEN_MAINTENANCE:
                screenName = "MaintenanceScreen";
                break;
            case EVENT_QUESTION_SHOWN:
                screenName = "GameQuestionShown";
                break;
            case EVENT_ANSWER_SHOWN:
                screenName = "GameAnswerShown";
                break;
            case EVENT_ANSWER_SUBMIT:
                screenName = "GameAnswerSubmit";
                break;
        }
        if (!TextUtils.isEmpty(screenName)) {
            logScreenView(screenType);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", screenName);
            sendFirebaseEvent(screenName, bundle);
        }
    }

    @Override
    public void setFireBaseUser(User user) {
        try {
            setCleverTapUserEvent(user);
            //getFirebaseAnalytics().setUserId(user.getPhoneNumber());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setCleverTapUserEvent(User user) {
        try {
            Map<String, Object> data = new HashMap<>();
            data.put("Email", "");
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
            data.put("Identity", user.getPhoneNumber());
            data.put("Photo", user.getUserImgUrl());
            data.put("Wallet Balance", user.getUserBalance());
            data.put("current_lifes", user.getLives());
            data.put("Event Time", getTimeStampInHHMMSSIST());
            data.put("isBaaziNewspoint", true);
            data.put("isBaaziPrime", false);
            if (cleverTapAPI != null) {
                cleverTapAPI.onUserLogin(data);
                cleverTapAPI.profile.push(data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void cleverTapScreenEvent(User user, String eventName) {
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
            if (eventName.equals("balance_dialog_viewed")) {
                data.put("Wallet Balance", String.valueOf(user.getUserBalance()));
            }
        }
        data.put("Event Time", getTimeStampInHHMMSSIST());
        pushCleverTapEvent(eventName, data);
    }

    @Override
    public void cleverTapEvent(String eventName, Map<String, Object> map) {
        pushCleverTapEvent(eventName, map);
    }

    @Override
    public void cleverAppLaunchEvent(String firstLaunch, String deviceId, String city, String installSource) {
        Map<String, Object> data = new HashMap<>();
        data.put("first_launch", firstLaunch);
        data.put("android_id", deviceId);
        data.put("Device_Name", city);
        data.put("city", city);
        data.put("install_source", "");
        data.put("Device_Name", city);
        data.put("Event Time", getTimeStampInHHMMSSIST());
        pushCleverTapEvent("app_launched", data);
    }

    @Override
    public void cleverWalletCashoutEvent(User user, String cashoutAmount, String walletName, String transactionStatus) {
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("cashout_amount", cashoutAmount);
        data.put("cashout_wallet_name", walletName);
        data.put("transaction_status", transactionStatus);
        data.put("Event Time", getTimeStampInHHMMSSIST());
        pushCleverTapEvent("cashout", data);
    }

    @Override
    public void cleverMobiqwikCreationEvent(User user, String status) {
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("transaction_status", status);
        data.put("Event Time", getTimeStampInHHMMSSIST());
        pushCleverTapEvent("mobikwik_wallet_creation_Event", data);
    }

    @Override
    public void cleverBalanceEvent(User user, String eventName, String source) {
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
            data.put("amount", String.valueOf(user.getUserBalance()));
        }
        data.put("Event Time", getTimeStampInHHMMSSIST());
        pushCleverTapEvent(eventName, data);
    }

    /*private FirebaseAnalytics getFirebaseAnalytics() {
        if (firebaseAnalytics == null) {
            firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        }
        return firebaseAnalytics;
    }*/

    public Tracker getDefaultTracker(String gaID) {
        GoogleAnalytics sAnalytics = GoogleAnalytics.getInstance(context);
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(gaID);
            sTracker.enableAutoActivityTracking(true);
        }

        return sTracker;
    }

    public CleverTapAPI getCleverTapAPI(String accountID, String token) {
        if (cleverTapAPI == null) {
            CleverTapAPI.changeCredentials(accountID, token);
            CleverTapAPI.setDebugLevel(CleverTapAPI.LogLevel.OFF);
            ActivityLifecycleCallback.register((Application) context);

            try {
                cleverTapAPI = CleverTapAPI.getInstance(context);
            } catch (CleverTapMetaDataNotFoundException | CleverTapPermissionsNotSatisfied e) {
                e.printStackTrace();
            }
        }
        return cleverTapAPI;
    }

    private void sendGaEvents(GaEventModel eventModel) {
        if (sTracker == null) {
            return;
        }
        String newTimeStamp = eventModel.getTimeStamp().replaceAll("[\\s\\-()\\.\\:]", "");
        newTimeStamp = newTimeStamp.substring(0, newTimeStamp.length() - 3);
        long longTimeStamp = Long.parseLong(newTimeStamp);

        sTracker.send(new HitBuilders.EventBuilder()
                .setCategory(eventModel.getCategory())
                .setAction(eventModel.getAction())
                .setLabel(eventModel.getLabel() + " Name = " + eventModel.getUserName())
                .setValue(longTimeStamp)
                .setCustomDimension(CUSTOM_DIMEN_PARENT_APP, "NewsPoint")
                .build()
        );
    }

    private void sendFirebaseEvent(String event, Bundle bundle) {
        /*if (bundle == null) {
            bundle = new Bundle();
        }
        getFirebaseAnalytics().logEvent(event, bundle);*/
    }

    private void pushCleverTapEvent(String event, Map<String, Object> data) {
        try {
            if (cleverTapAPI != null) {
                data.put("isBaaziNewspoint", true);
                data.put("isBaaziPrime", false);
                cleverTapAPI.event.push(event, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getTimeStampInHHMMSSIST() {
        try {
            DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // Or whatever IST is supposed to be
            return formatter.format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public void logScreenView(int screenId) {
        if (listener != null) {
            listener.onScreenView(screenId);
        }
    }
}
