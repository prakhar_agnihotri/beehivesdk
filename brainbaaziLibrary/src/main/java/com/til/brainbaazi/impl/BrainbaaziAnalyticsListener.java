package com.til.brainbaazi.impl;

import android.util.Log;

import com.brainbaazi.component.AnalyticsListener;

/**
 * Created by prashant.rathore on 04/03/18.
 */

public class BrainbaaziAnalyticsListener implements AnalyticsListener {


    @Override
    public void onScreenView(int screenID) {
        Log.d("BBEVENT",String.valueOf(screenID));
    }
}
