package com.til.brainbaazi.impl;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.event.Answer;
import com.til.brainbaazi.entity.game.event.AnswerOptions;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.event.QuestionOptions;
import com.til.brainbaazi.entity.game.event.WinUser;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.GameResponse;
import com.til.brainbaazi.interactor.GameEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;

/**
 * Created by prashant.rathore on 19/02/18.
 */

public class GameEngineImpl implements GameEngine {
    @Override
    public Observable<GameResponse<?>> observeGameEvents(GameInfo gameInfo, User user) {
        return null;
    }

    @Override
    public void processEvent(InputEvent inputEvent) {

    }

    @Override
    public GameState getGameState(GameInfo gameInfo, User user) {
        return null;
    }

    @Override
    public boolean hasUserWon(GameInfo gameInfo, User user) {
        return false;
    }

    @Override
    public boolean hasGameFinished(GameInfo gameInfo, User user) {
        return false;
    }

    //    @Override
//    public Observable<GameResponse<?>> observeGameEvents(GameInfo gameInfo, User user) {
//
//        int questionNumber = 1;
//        boolean lifeAvailable = true;
//        int playState = GameState.PLAY_STATE_PLAYING;
//
//        List<GameResponse<?>> gameResponses = new ArrayList<>();
//
//        Question question = generateQuestion(questionNumber++);
//        gameResponses.add(generateResponse(GameResponse.TYPE_SHOW_QUESTION, question, playState, lifeAvailable));
//        QuestionInfo answer = generateAnswer(question, playState, 1, 1);
//        gameResponses.add(generateResponse(GameResponse.TYPE_SHOW_ANSWER, answer, playState, lifeAvailable));
//
//        playState = GameState.PLAY_STATE_LATE;
//        lifeAvailable = false;
//        gameResponses.add(generateResponse(GameResponse.TYPE_USER_STATE, null, playState, lifeAvailable));
//
//        question = generateQuestion(questionNumber++);
//        gameResponses.add(generateResponse(GameResponse.TYPE_SHOW_QUESTION, question, playState, lifeAvailable));
//        answer = generateAnswer(question, playState, 2, 2);
//        gameResponses.add(generateResponse(GameResponse.TYPE_SHOW_ANSWER, answer, playState, lifeAvailable));
//
//        playState = GameState.PLAY_STATE_ELIMINATED;
//        lifeAvailable = true;
//        gameResponses.add(generateResponse(GameResponse.TYPE_USER_STATE, null, playState, lifeAvailable));
//
//        question = generateQuestion(questionNumber++);
//        gameResponses.add(generateResponse(GameResponse.TYPE_SHOW_QUESTION, question, playState, lifeAvailable));
//        answer = generateAnswer(question, playState, 3, 3);
//        gameResponses.add(generateResponse(GameResponse.TYPE_SHOW_ANSWER, answer, playState, lifeAvailable));
//
//        gameResponses.add(generateResponse(GameResponse.TYPE_USER_INFO_KICKED_OUT, null, playState, lifeAvailable));
//
//        //Winner Result
//        Winner winner = generateWinner(20, true);
//        gameResponses.add(generateResponse(GameResponse.TYPE_SHOW_WINNER, winner, playState, lifeAvailable));//Winner Result
//
//        gameResponses.add(generateResponse(GameResponse.TYPE_SHOW_GAME_END, null, playState, lifeAvailable));
//
//        Observable<Long> interval = Observable.interval(15, TimeUnit.SECONDS);
//        BiFunction<GameResponse<?>, Long, GameResponse<?>> biFunction = new BiFunction<GameResponse<?>, Long, GameResponse<?>>() {
//            @Override
//            public GameResponse<?> apply(GameResponse<?> gameResponse, Long aLong) throws Exception {
//                return gameResponse;
//            }
//        };
//        return Observable.fromArray(gameResponses.toArray(new GameResponse<?>[gameResponses.size()]))
//                .zipWith(interval, biFunction);
//    }
//
//
//    public static QuestionInfo generateAnswer(Question question, int newState, int userAnswered, int correctAnswer, User user) {
//        AnswerOptions[] options = new AnswerOptions[3];
//        for (int i = 0; i < options.length; i++) {
//            options[i] = AnswerOptions.builder()
//                    .setText("Option " + (i + 1))
//                    .setPosition(i)
//                    .setOptionId(String.valueOf(i))
//                    .setCorrectOption(correctAnswer == i)
//                    .setCount(45)
//                    .build();
//        }
//
//        Answer answer = Answer.builder()
//                .setAnswerOptions(options)
//                .setCorrectAnswerId(correctAnswer)
//                .setQuestionId(question.getId())
//                .setMaxUserCount(100)
//                .build();
//
//        return QuestionInfo.builder()
//                .setNewUserState(newState)
//                .setUserAnswered(userAnswered)
//                .setQuestion(question)
//                .setAnswer(answer).build();
//    }
//
//    public static Question generateQuestion(int questionNumber) {
//        QuestionOptions[] options = new QuestionOptions[3];
//        for (int i = 0; i < options.length; i++) {
//            options[i] = QuestionOptions.builder()
//                    .setText("Option " + (i + 1))
//                    .setPosition(i)
//                    .setOptionId(String.valueOf(i)).build();
//        }
//        return Question.builder()
//                .setText("Question " + questionNumber)
//                .setId(questionNumber)
//                .setQuestionOptions(options)
//                .setQuestionNumber(questionNumber)
//                .build();
//    }
//
//    public static Winner generateWinner(int winnerCount, boolean isWon) {
//        Winner.Builder builder = Winner.builder();
//        if (winnerCount > 0) {
//            WinUser[] winUsers = new WinUser[winnerCount];
//            for (int i = 0; i < winUsers.length; i++) {
//                winUsers[i] = WinUser.builder()
//                        .setImgUrl("")
//                        .setName("Name " + (i + 1))
//                        .build();
//            }
//            builder.setWinUserList(winUsers);
//        }
//        builder.setPrizeAmount(100).setEarnExtraLifeList("").setEmpty(false);
//        return builder.build();
//    }
//
//    public static <T> GameResponse<?> generateResponse(int type, T objec, int playState, boolean lifeAvailable) {
//        GameState build = GameState.builderDefault()
//                .setUserPlayState(playState)
//                .setLifeAvailable(lifeAvailable ? 1 : 0).build();
//
//        return GameResponse.<T>builder()
//                .setEventType(type)
//                .setGameState(build)
//                .setValue(objec).build();
//    }
//
//
//    @Override
//    public void processEvent(GameInfo gameInfo, InputEvent inputEvent) {
//
//    }
//
//    @Override
//    public GameState getGameState(GameInfo gameInfo) {
//        return null;
//    }
//
//    @Override
//    public boolean hasUserWon(GameInfo gameInfo) {
//        return true;
//    }


}
