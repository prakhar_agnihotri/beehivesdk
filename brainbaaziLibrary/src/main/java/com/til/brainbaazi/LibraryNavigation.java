package com.til.brainbaazi;

import android.os.Bundle;

import com.brainbaazi.core.AppNavigation;
import com.brainbaazi.core.ScreenControllerFactory;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.entity.otp.PhoneNumber;
import com.til.brainbaazi.screen.controller.ScreenNavigation;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.dashboard.DashboardNavigation;
import com.til.brainbaazi.viewmodel.other.WebViewModel;
import com.til.brainbaazi.viewmodel.otp.OtpNavigation;
import com.til.brainbaazi.viewmodel.otp.OtpVerifyViewModel;
import com.til.brainbaazi.viewmodel.otp.ProfileViewModel;
import com.til.brainbaazi.viewmodel.splash.SplashNavigation;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by prashant.rathore on 23/02/18.
 */

public abstract class LibraryNavigation extends ScreenNavigation implements SplashNavigation, OtpNavigation, DashboardNavigation {

    private AppNavigation appNavigation;

    public LibraryNavigation(AppNavigation appNavigation) {
        this.appNavigation = appNavigation;
    }

    @Override
    public void navigateToDashboard() {
        clearStack();
        SegmentInfo segmentInfo = new SegmentInfo(ScreenControllerFactory.SCREEN_DASHBOARD, null);
        navigateToScreen(segmentInfo);
    }

    @Override
    public void navigateToRegistration() {
        SegmentInfo segmentInfo = new SegmentInfo(ScreenControllerFactory.SCREEN_GENERATE_OTP, null);
        addToBackStack(segmentInfo);
    }

    @Override
    public void navigateToProfile(PhoneNumber phoneNumber) {
        navigateToProfileScreen(phoneNumber);
    }

    @Override
    public void proceedToMarket() {
//        SegmentInfo segmentInfo = new SegmentInfo(ScreenControllerFactory.SCREEN_DASHBOARD,null);
//        navigateToScreen(segmentInfo);
    }

    @Override
    public void navigateToOTPEnterScreen(PhoneNumber phoneNumber) {
        Bundle param = new Bundle();
        param.putParcelable(OtpVerifyViewModel.PARAM_PHONE_NUMBER, phoneNumber);
        SegmentInfo segmentInfo = new SegmentInfo(ScreenControllerFactory.SCREEN_VERIFY_OTP, param);
        addToBackStack(segmentInfo);
    }

    @Override
    public void navigateToProfileScreen(PhoneNumber phoneNumber) {
        Bundle param = new Bundle();
        param.putParcelable(ProfileViewModel.PARAM_PHONE_NUMBER, phoneNumber);
        SegmentInfo segmentInfo = new SegmentInfo(ScreenControllerFactory.SCREEN_PROFILE, param);
        addToBackStack(segmentInfo);
    }

    @Override
    public void proceedToUpdate() {
        clearStack();
        SegmentInfo segmentInfo = new SegmentInfo(ScreenControllerFactory.SCREEN_UPDATE, null);
        navigateToScreen(segmentInfo);
    }

    @Override
    public void proceedToMaintenance() {
        clearStack();
        SegmentInfo segmentInfo = new SegmentInfo(ScreenControllerFactory.SCREEN_MAINTENANCE, null);
        navigateToScreen(segmentInfo);
    }

    @Override
    public void proceedToWebPage(WebData webData) {
        appNavigation.openWebScreen(webData);
    }


    @Override
    public void openUserBalanceScreen(User user) {
        appNavigation.openUserBalanceScreen(user);
    }

    @Override
    public void openZeroBalanceScreen(User user) {
        appNavigation.openZeroBalanceScreen(user);
    }

    @Override
    public void openPlayGameScreen(DashboardInfo dashboardInfo) {
        appNavigation.openPlayGameScreen(dashboardInfo);
    }

    @Override
    public void openLeaderBoard(User user) {
        appNavigation.openLeaderBoard(user);
    }

    @Override
    public void openWebScreen(WebData webData) {
        appNavigation.openWebScreen(webData);
    }

    @Override
    public void proceedToSplash() {
        clearStack();
        Bundle bundle = new Bundle();
        bundle.putBoolean("RESTORE", true);
        SegmentInfo segmentInfo = new SegmentInfo(ScreenControllerFactory.SCREEN_SPLASH, bundle);
        navigateToScreen(segmentInfo);
    }
}