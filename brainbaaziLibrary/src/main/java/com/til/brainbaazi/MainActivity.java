package com.til.brainbaazi;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.brainbaazi.core.AppNavigation;
import com.brainbaazi.core.BrainBaazi;
import com.brainbaazi.core.ScreenControllerFactory;
import com.brainbaazi.core.BrainBaaziConfig;
import com.til.brainbaazi.impl.BrainbaaziAnalyticsListener;
import com.til.brainbaazi.screen.controller.ScreenController;
import com.til.brainbaazi.screen.controller.ScreenControllerActivity;
import com.til.brainbaazi.screen.controller.ScreenNavigation;
import com.til.brainbaazi.screen.controller.SegmentInfo;
import com.til.brainbaazi.viewmodel.BaseViewModel;

public class MainActivity extends ScreenControllerActivity {

    AppNavigation appNavigation = new AppNavigation(this) {
        @Override
        public SegmentInfo navigateToScreen(SegmentInfo segmentInfo) {
            return changeScreen(segmentInfo);
        }
    };

    private LibraryNavigation libraryNavigation = new LibraryNavigation(appNavigation) {
        @Override
        public SegmentInfo navigateToScreen(SegmentInfo segmentInfo) {
            return changeScreen(segmentInfo);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.BBMainActivityThemeChangedBackground);
        super.onCreate(savedInstanceState);
    }

//    @Override
//    protected void changeView(View newView, Runnable onCompleHandler) {
//    }

    @Override
    protected ScreenController<? extends BaseViewModel> provideController(SegmentInfo segmentInfo) {
        ScreenController screenController = null;
        switch (segmentInfo.getId()) {
            case ScreenControllerFactory.SCREEN_DASHBOARD:
                screenController = ScreenControllerFactory.dashboard(segmentInfo, libraryNavigation, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_SPLASH:
                screenController = ScreenControllerFactory.splash(segmentInfo, libraryNavigation);
                break;
            case ScreenControllerFactory.SCREEN_GENERATE_OTP:
                screenController = ScreenControllerFactory.generateOtp(segmentInfo, libraryNavigation, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_VERIFY_OTP:
                screenController = ScreenControllerFactory.verifyOtp(segmentInfo, libraryNavigation, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_PROFILE:
                screenController = ScreenControllerFactory.profile(segmentInfo, libraryNavigation, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_UPDATE:
                screenController = ScreenControllerFactory.update(segmentInfo, libraryNavigation);
                break;
            case ScreenControllerFactory.SCREEN_WEB_VIEW:
                screenController = ScreenControllerFactory.webView(segmentInfo, getActivityInteractor());
                break;
            case ScreenControllerFactory.SCREEN_MAINTENANCE:
                screenController = ScreenControllerFactory.maintenance(segmentInfo, libraryNavigation);
                break;
        }
        return screenController;
    }

    @Override
    protected SegmentInfo provideDefaultScreenInfo() {
        return new SegmentInfo(ScreenControllerFactory.SCREEN_SPLASH, null);
    }

    @Override
    protected ScreenNavigation getScreenNavigation() {
        return this.libraryNavigation;
    }
}
