// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Authentication.proto

package com.example.proto;

public final class AuthenticationOuterClass {
  private AuthenticationOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }
  public interface AuthenticationOrBuilder extends
      // @@protoc_insertion_point(interface_extends:com.example.proto.Authentication)
      com.google.protobuf.MessageLiteOrBuilder {

    /**
     * <code>optional string appId = 1;</code>
     */
    java.lang.String getAppId();
    /**
     * <code>optional string appId = 1;</code>
     */
    com.google.protobuf.ByteString
        getAppIdBytes();

    /**
     * <code>optional string version = 2;</code>
     */
    java.lang.String getVersion();
    /**
     * <code>optional string version = 2;</code>
     */
    com.google.protobuf.ByteString
        getVersionBytes();

    /**
     * <code>optional string state = 3;</code>
     */
    java.lang.String getState();
    /**
     * <code>optional string state = 3;</code>
     */
    com.google.protobuf.ByteString
        getStateBytes();

    /**
     * <code>optional string token = 4;</code>
     */
    java.lang.String getToken();
    /**
     * <code>optional string token = 4;</code>
     */
    com.google.protobuf.ByteString
        getTokenBytes();
  }
  /**
   * Protobuf type {@code com.example.proto.Authentication}
   */
  public  static final class Authentication extends
      com.google.protobuf.GeneratedMessageLite<
          Authentication, Authentication.Builder> implements
      // @@protoc_insertion_point(message_implements:com.example.proto.Authentication)
      AuthenticationOrBuilder {
    private Authentication() {
      appId_ = "";
      version_ = "";
      state_ = "";
      token_ = "";
    }
    public static final int APPID_FIELD_NUMBER = 1;
    private java.lang.String appId_;
    /**
     * <code>optional string appId = 1;</code>
     */
    public java.lang.String getAppId() {
      return appId_;
    }
    /**
     * <code>optional string appId = 1;</code>
     */
    public com.google.protobuf.ByteString
        getAppIdBytes() {
      return com.google.protobuf.ByteString.copyFromUtf8(appId_);
    }
    /**
     * <code>optional string appId = 1;</code>
     */
    private void setAppId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      appId_ = value;
    }
    /**
     * <code>optional string appId = 1;</code>
     */
    private void clearAppId() {
      
      appId_ = getDefaultInstance().getAppId();
    }
    /**
     * <code>optional string appId = 1;</code>
     */
    private void setAppIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      appId_ = value.toStringUtf8();
    }

    public static final int VERSION_FIELD_NUMBER = 2;
    private java.lang.String version_;
    /**
     * <code>optional string version = 2;</code>
     */
    public java.lang.String getVersion() {
      return version_;
    }
    /**
     * <code>optional string version = 2;</code>
     */
    public com.google.protobuf.ByteString
        getVersionBytes() {
      return com.google.protobuf.ByteString.copyFromUtf8(version_);
    }
    /**
     * <code>optional string version = 2;</code>
     */
    private void setVersion(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      version_ = value;
    }
    /**
     * <code>optional string version = 2;</code>
     */
    private void clearVersion() {
      
      version_ = getDefaultInstance().getVersion();
    }
    /**
     * <code>optional string version = 2;</code>
     */
    private void setVersionBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      version_ = value.toStringUtf8();
    }

    public static final int STATE_FIELD_NUMBER = 3;
    private java.lang.String state_;
    /**
     * <code>optional string state = 3;</code>
     */
    public java.lang.String getState() {
      return state_;
    }
    /**
     * <code>optional string state = 3;</code>
     */
    public com.google.protobuf.ByteString
        getStateBytes() {
      return com.google.protobuf.ByteString.copyFromUtf8(state_);
    }
    /**
     * <code>optional string state = 3;</code>
     */
    private void setState(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      state_ = value;
    }
    /**
     * <code>optional string state = 3;</code>
     */
    private void clearState() {
      
      state_ = getDefaultInstance().getState();
    }
    /**
     * <code>optional string state = 3;</code>
     */
    private void setStateBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      state_ = value.toStringUtf8();
    }

    public static final int TOKEN_FIELD_NUMBER = 4;
    private java.lang.String token_;
    /**
     * <code>optional string token = 4;</code>
     */
    public java.lang.String getToken() {
      return token_;
    }
    /**
     * <code>optional string token = 4;</code>
     */
    public com.google.protobuf.ByteString
        getTokenBytes() {
      return com.google.protobuf.ByteString.copyFromUtf8(token_);
    }
    /**
     * <code>optional string token = 4;</code>
     */
    private void setToken(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      token_ = value;
    }
    /**
     * <code>optional string token = 4;</code>
     */
    private void clearToken() {
      
      token_ = getDefaultInstance().getToken();
    }
    /**
     * <code>optional string token = 4;</code>
     */
    private void setTokenBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      token_ = value.toStringUtf8();
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (!appId_.isEmpty()) {
        output.writeString(1, getAppId());
      }
      if (!version_.isEmpty()) {
        output.writeString(2, getVersion());
      }
      if (!state_.isEmpty()) {
        output.writeString(3, getState());
      }
      if (!token_.isEmpty()) {
        output.writeString(4, getToken());
      }
    }

    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;

      size = 0;
      if (!appId_.isEmpty()) {
        size += com.google.protobuf.CodedOutputStream
          .computeStringSize(1, getAppId());
      }
      if (!version_.isEmpty()) {
        size += com.google.protobuf.CodedOutputStream
          .computeStringSize(2, getVersion());
      }
      if (!state_.isEmpty()) {
        size += com.google.protobuf.CodedOutputStream
          .computeStringSize(3, getState());
      }
      if (!token_.isEmpty()) {
        size += com.google.protobuf.CodedOutputStream
          .computeStringSize(4, getToken());
      }
      memoizedSerializedSize = size;
      return size;
    }

    public static com.example.proto.AuthenticationOuterClass.Authentication parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return com.google.protobuf.GeneratedMessageLite.parseFrom(
          DEFAULT_INSTANCE, data);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return com.google.protobuf.GeneratedMessageLite.parseFrom(
          DEFAULT_INSTANCE, data, extensionRegistry);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return com.google.protobuf.GeneratedMessageLite.parseFrom(
          DEFAULT_INSTANCE, data);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return com.google.protobuf.GeneratedMessageLite.parseFrom(
          DEFAULT_INSTANCE, data, extensionRegistry);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageLite.parseFrom(
          DEFAULT_INSTANCE, input);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageLite.parseFrom(
          DEFAULT_INSTANCE, input, extensionRegistry);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return parseDelimitedFrom(DEFAULT_INSTANCE, input);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return parseDelimitedFrom(DEFAULT_INSTANCE, input, extensionRegistry);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageLite.parseFrom(
          DEFAULT_INSTANCE, input);
    }
    public static com.example.proto.AuthenticationOuterClass.Authentication parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageLite.parseFrom(
          DEFAULT_INSTANCE, input, extensionRegistry);
    }

    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(com.example.proto.AuthenticationOuterClass.Authentication prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }

    /**
     * Protobuf type {@code com.example.proto.Authentication}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageLite.Builder<
          com.example.proto.AuthenticationOuterClass.Authentication, Builder> implements
        // @@protoc_insertion_point(builder_implements:com.example.proto.Authentication)
        com.example.proto.AuthenticationOuterClass.AuthenticationOrBuilder {
      // Construct using com.example.proto.AuthenticationOuterClass.Authentication.newBuilder()
      private Builder() {
        super(DEFAULT_INSTANCE);
      }


      /**
       * <code>optional string appId = 1;</code>
       */
      public java.lang.String getAppId() {
        return instance.getAppId();
      }
      /**
       * <code>optional string appId = 1;</code>
       */
      public com.google.protobuf.ByteString
          getAppIdBytes() {
        return instance.getAppIdBytes();
      }
      /**
       * <code>optional string appId = 1;</code>
       */
      public Builder setAppId(
          java.lang.String value) {
        copyOnWrite();
        instance.setAppId(value);
        return this;
      }
      /**
       * <code>optional string appId = 1;</code>
       */
      public Builder clearAppId() {
        copyOnWrite();
        instance.clearAppId();
        return this;
      }
      /**
       * <code>optional string appId = 1;</code>
       */
      public Builder setAppIdBytes(
          com.google.protobuf.ByteString value) {
        copyOnWrite();
        instance.setAppIdBytes(value);
        return this;
      }

      /**
       * <code>optional string version = 2;</code>
       */
      public java.lang.String getVersion() {
        return instance.getVersion();
      }
      /**
       * <code>optional string version = 2;</code>
       */
      public com.google.protobuf.ByteString
          getVersionBytes() {
        return instance.getVersionBytes();
      }
      /**
       * <code>optional string version = 2;</code>
       */
      public Builder setVersion(
          java.lang.String value) {
        copyOnWrite();
        instance.setVersion(value);
        return this;
      }
      /**
       * <code>optional string version = 2;</code>
       */
      public Builder clearVersion() {
        copyOnWrite();
        instance.clearVersion();
        return this;
      }
      /**
       * <code>optional string version = 2;</code>
       */
      public Builder setVersionBytes(
          com.google.protobuf.ByteString value) {
        copyOnWrite();
        instance.setVersionBytes(value);
        return this;
      }

      /**
       * <code>optional string state = 3;</code>
       */
      public java.lang.String getState() {
        return instance.getState();
      }
      /**
       * <code>optional string state = 3;</code>
       */
      public com.google.protobuf.ByteString
          getStateBytes() {
        return instance.getStateBytes();
      }
      /**
       * <code>optional string state = 3;</code>
       */
      public Builder setState(
          java.lang.String value) {
        copyOnWrite();
        instance.setState(value);
        return this;
      }
      /**
       * <code>optional string state = 3;</code>
       */
      public Builder clearState() {
        copyOnWrite();
        instance.clearState();
        return this;
      }
      /**
       * <code>optional string state = 3;</code>
       */
      public Builder setStateBytes(
          com.google.protobuf.ByteString value) {
        copyOnWrite();
        instance.setStateBytes(value);
        return this;
      }

      /**
       * <code>optional string token = 4;</code>
       */
      public java.lang.String getToken() {
        return instance.getToken();
      }
      /**
       * <code>optional string token = 4;</code>
       */
      public com.google.protobuf.ByteString
          getTokenBytes() {
        return instance.getTokenBytes();
      }
      /**
       * <code>optional string token = 4;</code>
       */
      public Builder setToken(
          java.lang.String value) {
        copyOnWrite();
        instance.setToken(value);
        return this;
      }
      /**
       * <code>optional string token = 4;</code>
       */
      public Builder clearToken() {
        copyOnWrite();
        instance.clearToken();
        return this;
      }
      /**
       * <code>optional string token = 4;</code>
       */
      public Builder setTokenBytes(
          com.google.protobuf.ByteString value) {
        copyOnWrite();
        instance.setTokenBytes(value);
        return this;
      }

      // @@protoc_insertion_point(builder_scope:com.example.proto.Authentication)
    }
    protected final Object dynamicMethod(
        com.google.protobuf.GeneratedMessageLite.MethodToInvoke method,
        Object arg0, Object arg1) {
      switch (method) {
        case NEW_MUTABLE_INSTANCE: {
          return new com.example.proto.AuthenticationOuterClass.Authentication();
        }
        case IS_INITIALIZED: {
          return DEFAULT_INSTANCE;
        }
        case MAKE_IMMUTABLE: {
          return null;
        }
        case NEW_BUILDER: {
          return new Builder();
        }
        case VISIT: {
          Visitor visitor = (Visitor) arg0;
          com.example.proto.AuthenticationOuterClass.Authentication other = (com.example.proto.AuthenticationOuterClass.Authentication) arg1;
          appId_ = visitor.visitString(!appId_.isEmpty(), appId_,
              !other.appId_.isEmpty(), other.appId_);
          version_ = visitor.visitString(!version_.isEmpty(), version_,
              !other.version_.isEmpty(), other.version_);
          state_ = visitor.visitString(!state_.isEmpty(), state_,
              !other.state_.isEmpty(), other.state_);
          token_ = visitor.visitString(!token_.isEmpty(), token_,
              !other.token_.isEmpty(), other.token_);
          if (visitor == com.google.protobuf.GeneratedMessageLite.MergeFromVisitor
              .INSTANCE) {
          }
          return this;
        }
        case MERGE_FROM_STREAM: {
          com.google.protobuf.CodedInputStream input =
              (com.google.protobuf.CodedInputStream) arg0;
          com.google.protobuf.ExtensionRegistryLite extensionRegistry =
              (com.google.protobuf.ExtensionRegistryLite) arg1;
          try {
            boolean done = false;
            while (!done) {
              int tag = input.readTag();
              switch (tag) {
                case 0:
                  done = true;
                  break;
                default: {
                  if (!input.skipField(tag)) {
                    done = true;
                  }
                  break;
                }
                case 10: {
                  String s = input.readStringRequireUtf8();

                  appId_ = s;
                  break;
                }
                case 18: {
                  String s = input.readStringRequireUtf8();

                  version_ = s;
                  break;
                }
                case 26: {
                  String s = input.readStringRequireUtf8();

                  state_ = s;
                  break;
                }
                case 34: {
                  String s = input.readStringRequireUtf8();

                  token_ = s;
                  break;
                }
              }
            }
          } catch (com.google.protobuf.InvalidProtocolBufferException e) {
            throw new RuntimeException(e.setUnfinishedMessage(this));
          } catch (java.io.IOException e) {
            throw new RuntimeException(
                new com.google.protobuf.InvalidProtocolBufferException(
                    e.getMessage()).setUnfinishedMessage(this));
          } finally {
          }
        }
        case GET_DEFAULT_INSTANCE: {
          return DEFAULT_INSTANCE;
        }
        case GET_PARSER: {
          if (PARSER == null) {    synchronized (com.example.proto.AuthenticationOuterClass.Authentication.class) {
              if (PARSER == null) {
                PARSER = new DefaultInstanceBasedParser(DEFAULT_INSTANCE);
              }
            }
          }
          return PARSER;
        }
      }
      throw new UnsupportedOperationException();
    }


    // @@protoc_insertion_point(class_scope:com.example.proto.Authentication)
    private static final com.example.proto.AuthenticationOuterClass.Authentication DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new Authentication();
      DEFAULT_INSTANCE.makeImmutable();
    }

    public static com.example.proto.AuthenticationOuterClass.Authentication getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static volatile com.google.protobuf.Parser<Authentication> PARSER;

    public static com.google.protobuf.Parser<Authentication> parser() {
      return DEFAULT_INSTANCE.getParserForType();
    }
  }


  static {
  }

  // @@protoc_insertion_point(outer_class_scope)
}
