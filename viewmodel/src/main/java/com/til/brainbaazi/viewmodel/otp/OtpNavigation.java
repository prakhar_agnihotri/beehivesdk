package com.til.brainbaazi.viewmodel.otp;

import com.til.brainbaazi.entity.otp.PhoneNumber;

/**
 * Created by prashant.rathore on 16/02/18.
 */

public interface OtpNavigation {
    public void navigateToOTPEnterScreen(PhoneNumber phoneNumber);

    public void navigateToProfileScreen(PhoneNumber phoneNumber);

    public void navigateToDashboard();
}
