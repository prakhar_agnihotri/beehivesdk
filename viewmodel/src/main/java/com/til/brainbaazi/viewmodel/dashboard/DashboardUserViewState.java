package com.til.brainbaazi.viewmodel.dashboard;

import com.google.auto.value.AutoValue;
import com.til.brainbaazi.entity.User;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 15/02/18.
 */
@AutoValue
public abstract class DashboardUserViewState {

    public abstract int state();
    @Nullable
    public abstract User user();

    public static DashboardUserViewState create(int state, User user) {
        return new AutoValue_DashboardUserViewState(state, user);
    }

}
