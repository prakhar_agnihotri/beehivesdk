package com.til.brainbaazi.viewmodel.other;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

/**
 * Created by saurabh.garg on 2/21/18.
 */
@AutoFactory
public class WebViewModel extends BaseScreenModel {

    public static final String PARAM_WEB_DATA = "webData";

    private final ActivityInteractor activityInteractor;
    private final DataRepository dataRepository;

    private WebData webData;

    public WebViewModel(ActivityInteractor activityInteractor, @Provided ConnectionManager connectionManager, @Provided DataRepository dataRepository, @Provided Analytics analytics) {
        super(connectionManager,dataRepository, analytics);
        this.activityInteractor = activityInteractor;
        this.dataRepository = dataRepository;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.webData = ParcelableUtil.unmarshall(getParams().getByteArray(PARAM_WEB_DATA), WebData.creator());
    }

    public WebData getWebData() {
        return webData;
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }
}
