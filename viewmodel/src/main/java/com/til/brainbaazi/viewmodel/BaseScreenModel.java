package com.til.brainbaazi.viewmodel;

import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.analytics.GaEventModel;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by prashant.rathore on 14/02/18.
 */
@AutoFactory
public class BaseScreenModel extends BaseViewModel {

    private final ConnectionManager connectionManager;
    private final DataRepository dataRepository;
    private final Analytics analytics;

    private DisposableObserver<ConnectionInfo> networkObserver;

    private ConnectionInfo lastConnectionInfo;

    public BaseScreenModel(@Provided ConnectionManager connectionManager, @Provided DataRepository dataRepository, @Provided Analytics analytics) {
        this.connectionManager = connectionManager;
        this.dataRepository = dataRepository;
        this.analytics = analytics;
    }

    public ConnectionManager getConnectionManager() {
        return connectionManager;
    }

    @Override
    public void willShow() {
        super.willShow();
        observeNetworkChanges();
    }

    @Override
    public void willHide() {
        networkObserver.dispose();
        networkObserver = null;
        super.willHide();
    }

    protected void onNetworkChagned(ConnectionInfo connectionInfo) {

    }

    private void observeNetworkChanges() {
        networkObserver = new DisposableObserver<ConnectionInfo>() {
            @Override
            public void onNext(ConnectionInfo connectionInfo) {
                if (!connectionInfo.equals(lastConnectionInfo)) {
                    lastConnectionInfo = connectionInfo;
                    onNetworkChagned(connectionInfo);
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        };
        addDisposable(networkObserver);
        connectionManager.observeNetworkChanges().subscribe(networkObserver);

    }

    public Observable<BrainbaaziStrings> observeBrainbaaziStrings() {
        return dataRepository.observeBrainBaaziStrings();
    }

    public BrainbaaziStrings loadDefaultStrings() {
        return dataRepository.loadDefaultStrings();
    }

    public ConnectionInfo getLastConnectionInfo() {
        return lastConnectionInfo;
    }

    public Analytics getAnalytics() {
        return analytics;
    }

    public void logFireBaseScreen(int screenType) {
        analytics.logFireBaseScreen(screenType);
    }

    public void logFireBaseEvent(GaEventModel gaEventModel) {
        analytics.logFireBaseEvent(gaEventModel);
    }

    public void logFireBaseScreen(GaEventModel gaEventModel, Bundle bundle) {
        analytics.logFireBaseScreen(gaEventModel, bundle);
    }

    public void cleverTapEvent(String event, Map<String, Object> map) {
        analytics.cleverTapEvent(event, map);
    }
}
