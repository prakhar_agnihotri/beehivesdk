package com.til.brainbaazi.viewmodel.splash;

import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.entity.otp.PhoneNumber;

/**
 * Created by prashant.rathore on 20/02/18.
 */

public interface SplashNavigation {
    public void navigateToDashboard();

    public void navigateToRegistration();

    public void navigateToProfile(PhoneNumber phoneNumber);

    public void proceedToMarket();

    public void proceedToUpdate();

    public void proceedToMaintenance();

    public void proceedToWebPage(WebData webData);
}
