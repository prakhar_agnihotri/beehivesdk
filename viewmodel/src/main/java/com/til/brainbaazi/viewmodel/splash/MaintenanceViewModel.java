package com.til.brainbaazi.viewmodel.splash;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import io.reactivex.Scheduler;

/**
 * Created by saurabh.garg on 05/03/18.
 */
@AutoFactory
public class MaintenanceViewModel extends BaseScreenModel {

    public MaintenanceViewModel(@Provided ConnectionManager connectionManager, @Provided DataRepository dataRepository, @Provided Analytics analytics) {
        super(connectionManager, dataRepository, analytics);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void willShow() {
        super.willShow();
    }

    @Override
    public void willHide() {
        super.willHide();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
