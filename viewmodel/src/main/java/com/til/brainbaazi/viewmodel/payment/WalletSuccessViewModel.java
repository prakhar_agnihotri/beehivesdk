package com.til.brainbaazi.viewmodel.payment;

import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 09/03/18.
 */

@AutoFactory
public class WalletSuccessViewModel extends BaseScreenModel {

    private final PaymentRequests paymentRequests;
    private final PaymentNavigation paymentNavigation;
    private final Analytics analytics;

    private String amountToTransfer, phoneNumber, authToken;
    private User user;

    public static final String AUTH_TOKEN = "authToken";
    public static final String PARAM_USER = "user";

    private CompositeDisposable compositeDisposable;

    private BehaviorSubject<User> userInfoBehaviorSubject = BehaviorSubject.create();

    public WalletSuccessViewModel(@Provided ConnectionManager connectionManager,
                                  @Provided DataRepository dataRepository,
                                  @Provided Analytics analytics,
                                  @Provided PaymentRequests paymentRequests,
                                  PaymentNavigation paymentNavigation) {
        super(connectionManager, dataRepository, analytics);
        this.paymentRequests = paymentRequests;
        this.paymentNavigation = paymentNavigation;
        this.analytics= analytics;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        compositeDisposable = new CompositeDisposable();
        Bundle params = getParams();
        if(params != null) {
            this.authToken = params.getString(AUTH_TOKEN);
            observeUserData();
        }
    }

    private void observeUserData() {
        byte[] byteArray = getParams().getByteArray(PARAM_USER);
        User user = ParcelableUtil.unmarshall(byteArray, User.creator());
        userInfoBehaviorSubject.onNext(user);
        this.phoneNumber = user.getPhoneNumber();
        this.amountToTransfer = String.valueOf(user.getUserBalance());
    }


    public Observable<User> observeUserInfo() {
        return userInfoBehaviorSubject;
    }


    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    public String getAmountToTransfer() {
        return amountToTransfer;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAuthToken() {
        return authToken;
    }

    public PaymentNavigation getPaymentNavigation() {
        return paymentNavigation;
    }

    public PaymentRequests getPaymentRequests() {
        return paymentRequests;
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }


    public Analytics getAnalytics() {
        return analytics;
    }

    public User getUser() {
        return user;
    }
}
