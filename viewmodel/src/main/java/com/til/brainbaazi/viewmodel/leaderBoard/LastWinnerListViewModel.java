package com.til.brainbaazi.viewmodel.leaderBoard;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.leaderBoard.LastGameWinnerResponse;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by saurabh.garg on 3/04/18.
 */
@AutoFactory
public class LastWinnerListViewModel extends BaseScreenModel {
    private final DataRepository dataRepository;

    private BehaviorSubject<Response<LastGameWinnerResponse>> lastGameWinnerResponseBehaviorSubject = BehaviorSubject.create();

    public LastWinnerListViewModel(@Provided ConnectionManager connectionManager, @Provided DataRepository dataRepository, @Provided Analytics analytics) {
        super(connectionManager, dataRepository, analytics);
        this.dataRepository = dataRepository;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        loadLastGameWinnerData(null);
    }

    public void loadLastGameWinnerData(String appendURL) {
        final DisposableObserver<Response<LastGameWinnerResponse>> lastGameDataObserver = new DisposableOnNextObserver<Response<LastGameWinnerResponse>>() {
            @Override
            public void onNext(Response<LastGameWinnerResponse> lastGameWinnerResponseResponse) {
                lastGameWinnerResponseBehaviorSubject.onNext(lastGameWinnerResponseResponse);
            }
        };
        addDisposable(lastGameDataObserver);
        dataRepository.loadLastGameWinnerData(appendURL).subscribe(lastGameDataObserver);
    }


    public Observable<Response<LastGameWinnerResponse>> observableLastGameData() {
        return lastGameWinnerResponseBehaviorSubject;
    }

}
