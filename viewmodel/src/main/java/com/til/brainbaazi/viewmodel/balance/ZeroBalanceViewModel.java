package com.til.brainbaazi.viewmodel.balance;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by saurabh.garg on 2/15/18.
 */
@AutoFactory
public class ZeroBalanceViewModel extends BaseScreenModel {

    public static final String PARAM_USER = "user";
    private final ActivityInteractor activityInteractor;
    private final DataRepository dataRepository;

    private BehaviorSubject<User> userInfoBehaviorSubject = BehaviorSubject.create();

    public ZeroBalanceViewModel(ActivityInteractor activityInteractor,
                                @Provided ConnectionManager connectionManager,
                                @Provided DataRepository dataRepository,
                                @Provided Analytics analytics) {
        super(connectionManager, dataRepository, analytics);
        this.activityInteractor = activityInteractor;
        this.dataRepository = dataRepository;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        observeUserData();
    }

    private void observeUserData() {
        byte[] byteArray = getParams().getByteArray(PARAM_USER);
        User user = ParcelableUtil.unmarshall(byteArray, User.creator());
        userInfoBehaviorSubject.onNext(user);
//        DisposableObserver<Response<User>> userDataObserver = new DisposableObserver<Response<User>>() {
//            @Override
//            public void onNext(Response<User> user) {
//                userInfoBehaviorSubject.onNext(user.value());
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//            }
//
//            @Override
//            public void onComplete() {
//            }
//        };
//        addDisposable(userDataObserver);
//        dataRepository.loadUserInfo().subscribe(userDataObserver);
    }

    public Observable<User> observeUserInfo() {
        return userInfoBehaviorSubject;
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    public User getUser() {
        return userInfoBehaviorSubject.getValue();
    }
}
