package com.til.brainbaazi.viewmodel.gamePlay;

/**
 * Created by paras.mendiratta on 1/29/18.
 */

public enum UserState {
    NONE,
    READY_TO_PLAY,
    IN_WATCH_MODE,
    IS_PLAYING,
}


