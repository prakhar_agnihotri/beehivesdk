package com.til.brainbaazi.viewmodel.leaderBoard;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardUser;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by saurabh.garg on 2/21/18.
 */
@AutoFactory
public class LeaderBoardListViewModel extends BaseScreenModel {

    public static final String PARAM_DATA = "data";
    private final DataRepository dataRepository;

    private BehaviorSubject<List<LeaderBoardUser>> leaderBoardModelBehaviorSubject = BehaviorSubject.create();


    public LeaderBoardListViewModel(@Provided ConnectionManager connectionManager, @Provided DataRepository dataRepository, @Provided Analytics analytics) {
        super(connectionManager,dataRepository,analytics);
        this.dataRepository = dataRepository;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        loadLeaderBoardUserData();
    }


    private void loadLeaderBoardUserData() {
        List<LeaderBoardUser> data = getParams().getParcelableArrayList(PARAM_DATA);
        leaderBoardModelBehaviorSubject.onNext(data);
    }

    public Observable<List<LeaderBoardUser>> observeLeaderBoarModel() {
        return leaderBoardModelBehaviorSubject;
    }

}
