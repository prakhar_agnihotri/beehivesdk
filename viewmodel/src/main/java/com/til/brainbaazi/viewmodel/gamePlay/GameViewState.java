package com.til.brainbaazi.viewmodel.gamePlay;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 18/02/18.
 */

@AutoValue
public abstract class GameViewState {

    public abstract int getViewState();

    @Nullable
    public abstract Object getTranslations();

    public abstract int getUserPlayState();

    public abstract boolean getGameLive();

    public abstract int getConcurrentUserCount();

    public abstract boolean getLifeAvailable();

    @Nullable
    public abstract String getLiveStreamUrl();

    public abstract int getPrizeAmount();

    public abstract Builder toBuilder();

    public static Builder builder() {
        AutoValue_GameViewState.Builder builder = new AutoValue_GameViewState.Builder();
        builder.setGameLive(false)
                .setUserPlayState(0)
                .setConcurrentUserCount(-1)
                .setLifeAvailable(false);
        return builder;
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder setViewState(int viewState);

        public abstract Builder setTranslations(Object translations);

        public abstract Builder setGameLive(boolean live);

        public abstract Builder setUserPlayState(int type);

        public abstract Builder setLifeAvailable(boolean lifeAvailable);

        public abstract Builder setConcurrentUserCount(int concurrentUserCount);

        public abstract Builder setLiveStreamUrl(String liveStreamUrl);

        public abstract Builder setPrizeAmount(int prizeAmount);

        public abstract GameViewState build();

    }

}
