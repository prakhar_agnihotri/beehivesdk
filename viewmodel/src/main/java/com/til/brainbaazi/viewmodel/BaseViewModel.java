package com.til.brainbaazi.viewmodel;

import android.os.Bundle;

import com.google.auto.factory.AutoFactory;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by prashant.rathore on 02/02/18.
 */
@AutoFactory
public class BaseViewModel {

    private CompositeDisposable compositeDisposable;
    private Bundle params;
    private boolean freezeParams;

    public void setParams(Bundle params) {
        if(!freezeParams) {
            this.params = params;
        }
    }

    public Bundle getParams() {
        return params;
    }

    public void onCreate() {
        freezeParams = true;
        compositeDisposable = new CompositeDisposable();
    }

    public void willShow() {

    }

    public void restoreState(Bundle inBundle) {

    }

    public void resume() {

    }

    public void pause() {

    }

    public void saveState(Bundle outBundle) {

    }

    public void willHide() {

    }

    protected void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    public void onDestroy() {
        compositeDisposable.dispose();
        compositeDisposable = null;
    }

    public boolean handleBackPressed() {
        return false;
    }
}
