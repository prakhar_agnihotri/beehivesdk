package com.til.brainbaazi.viewmodel.otp;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.otp.CountryListModel;
import com.til.brainbaazi.entity.otp.CountryModel;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.otp.PhoneNumber;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 13/02/18.
 */
@AutoFactory
public class OtpGenerateViewModel extends BaseScreenModel {

    public static final int STATE_INPUT = 0;
    public static final int STATE_REQUESTING = 1;
    public static final int STATE_FAILED = 2;
    public static final int STATE_SUCCESS = 3;
    public static final int STATE_NO_NETWORK = 4;

    private final PhoneNumberAuthenticator phoneNumberAuthenticator;
    private final OtpNavigation navigation;
    private final Scheduler mainThreadScheduler;
    private final DataRepository dataRepository;
    private final ActivityInteractor activityInteractor;

    private OtpNavigation otpNavigation;

    private DisposableObserver<OTPResponse> otpRequestObservable;
    private DisposableObserver<ConnectionInfo> networkObserver;

    private BehaviorSubject<CountryListModel> observeCountryInfo;

    private int currentState = -1;

    private BehaviorSubject<Integer> viewStateObserver = BehaviorSubject.create();

    public OtpGenerateViewModel(ActivityInteractor activityInteractor
            , @Provided ConnectionManager connectionManager
            , @Provided @MainThreadScheduler Scheduler mainThreadScheduler
            , OtpNavigation navigation
            , @Provided PhoneNumberAuthenticator phoneNumberAuthenticator
            , @Provided DataRepository dataRepository
            , @Provided Analytics analytics) {

        super(connectionManager, dataRepository, analytics);
        this.activityInteractor = activityInteractor;
        this.mainThreadScheduler = mainThreadScheduler;
        this.navigation = navigation;
        this.phoneNumberAuthenticator = phoneNumberAuthenticator;
        this.otpNavigation = navigation;
        this.dataRepository = dataRepository;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sendCountryData();
        updateState(STATE_INPUT);
        observeCountryCodeData();
        observePhoneAuthenticationResponses();
    }

    private void sendCountryData() {
        CountryModel[] countryModels = new CountryModel[1];
        countryModels[0] = CountryModel.builder().setCode("IN").setName("India").setDialCode("+91").setImgUrl("").build();
        CountryListModel listModel = CountryListModel.builder().setCountryModels(countryModels).build();
        observeCountryInfo = BehaviorSubject.createDefault(listModel);
    }

    private void observeCountryCodeData() {
        final DisposableObserver<Response<CountryListModel>> leaderBoardDataObserver = new DisposableOnNextObserver<Response<CountryListModel>>() {
            @Override
            public void onNext(Response<CountryListModel> countryListModelResponse) {
                if (countryListModelResponse.success())
                    observeCountryInfo.onNext(countryListModelResponse.value());
            }
        };
        addDisposable(leaderBoardDataObserver);
        dataRepository.loadCountryListModel().subscribe(leaderBoardDataObserver);
    }

    private void observePhoneAuthenticationResponses() {
        otpRequestObservable = new DisposableOnNextObserver<OTPResponse>() {
            @Override
            public void onNext(OTPResponse response) {
                if (response.success) {
                    updateState(STATE_SUCCESS);
                    dispose();
                    proceedToOTPEnterScreen(response.phoneNumber);
                } else {
                    updateState(STATE_FAILED);
                }
            }
        };

        addDisposable(otpRequestObservable);
        phoneNumberAuthenticator.observeOtpCallbacks()
                .observeOn(mainThreadScheduler)
                .subscribe(otpRequestObservable);
    }


    private void updateState(int newState) {
        if (currentState != newState) {
            currentState = newState;
            viewStateObserver.onNext(newState);
        }
    }

    public void generateOtp(final String phoneNumber, final CountryModel countryModel) {
        updateState(STATE_REQUESTING);

        DisposableObserver<ConnectionInfo> disposableObserver = new DisposableOnNextObserver<ConnectionInfo>() {
            @Override
            public void onNext(ConnectionInfo connectionInfo) {
                if (connectionInfo.connectedToInternet()) {
                    viewStateObserver.onNext(STATE_REQUESTING);
                    String phoneNumberCC = countryModel.getDialCode() + phoneNumber;
                    PhoneNumber number = PhoneNumber.builder()
                            .setPhoneNumber(phoneNumberCC)
                            .setCountryModel(countryModel)
                            .build();
                    phoneNumberAuthenticator.generateOtp(number, false);
                } else {
                    viewStateObserver.onNext(STATE_NO_NETWORK);
                }
                dispose();
            }
        };
        addDisposable(disposableObserver);
        getConnectionManager()
                .observeNetworkChanges().observeOn(mainThreadScheduler)
                .subscribe(disposableObserver);

    }

    private void proceedToOTPEnterScreen(PhoneNumber phoneNumber) {
        otpNavigation.navigateToOTPEnterScreen(phoneNumber);
    }

    public Observable<CountryListModel> observeCountryCodeInfo() {
        return observeCountryInfo;
    }

    public Observable<Integer> observeViewState() {
        return viewStateObserver.distinctUntilChanged();
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    @Override
    public void onDestroy() {
        this.viewStateObserver.onComplete();
        this.viewStateObserver = null;
        super.onDestroy();
    }

    public void sendOTPStartEvent(String deviceID) {
        Map<String, Object> data = new HashMap<>();
        data.put("device_id", deviceID);
        cleverTapEvent("otp_initiated", data);

        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("otp_initiated").setCategory("Verify Number")
                .setAction("OTP intiated")
                .setLabel("")
                .setUserName("")
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        logFireBaseEvent(gaEventModel);
    }
}
