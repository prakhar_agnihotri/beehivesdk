package com.til.brainbaazi.viewmodel.payment;

import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 09/03/18.
 */

@AutoFactory
public class PaymentFailureViewModel extends BaseScreenModel {

    public static final int STATE_LOADING = 0;
    public static final int STATE_REMOVE_LOADING = 1;
    public static final int STATE_ERROR_MESSAGE = 2;

    private final Scheduler mainThreadScheduler;
    private final PaymentRequests paymentRequests;
    private final PaymentNavigation paymentNavigation;
    private CompositeDisposable compositeDisposable;

    private String source, authToken;
    private int resultCode;
    private User user;
    public static final int PAYMENT_FAILURE = 1004;
    public static final int PAYMENT_FAILURE_MOBIWIKI_CREATE_USER = 1005;

    private BehaviorSubject<Integer> screenStateBehaviorSubject = BehaviorSubject.create();
    private BehaviorSubject<PaymentResponseMessage> paymentResponseBehaviorSubject = BehaviorSubject.create();

    public static final String PAYMENT_RESULT_CODE = "paymentResultCode";
    public static final String PARAM_SOURCE = "source";
    public static final String AUTH_TOKEN = "auth_token";
    public static final String PARAM_USER = "user";

    public PaymentFailureViewModel(@Provided ConnectionManager connectionManager,
                                   @Provided @MainThreadScheduler Scheduler mainThreadScheduler,
                                   @Provided DataRepository dataRepository,
                                   @Provided Analytics analytics,
                                   @Provided PaymentRequests paymentRequests,
                                   PaymentNavigation paymentNavigation) {
        super(connectionManager, dataRepository, analytics);
        this.mainThreadScheduler = mainThreadScheduler;
        this.paymentRequests = paymentRequests;
        this.paymentNavigation = paymentNavigation;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        compositeDisposable = new CompositeDisposable();
        Bundle params = getParams();
        if(params != null) {
            byte[] byteArray = getParams().getByteArray(PARAM_USER);
            this.user = ParcelableUtil.unmarshall(byteArray, User.creator());
            this.source = params.getString(PARAM_SOURCE);
            this.resultCode = params.getInt(PAYMENT_RESULT_CODE);
            this.authToken = params.getString(AUTH_TOKEN);
        }
    }

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public String getAmountToTransfer() {
        return String.valueOf(user.getUserBalance());
    }

    public String getSource() {
        return source;
    }

    public String getPhoneNumber() {
        return user.getPhoneNumber();
    }

    public String getAuthToken() {
        return authToken;
    }

    public int getResultCode() {
        return resultCode;
    }

    public PaymentNavigation getPaymentNavigation() {
        return paymentNavigation;
    }

    public PaymentRequests getPaymentRequests() {
        return paymentRequests;
    }

    public void openMobikwikWalletScreen(){
        paymentNavigation.openMobikwikWalletScreen(user, authToken);
    }

    public void tryForRepayment(PaymentRequest paymentRequest) {
        showProgressLoader();
        DisposableObserver<PaymentResponseMessage> transferAmount = new DisposableObserver<PaymentResponseMessage>() {
            @Override
            public void onNext(PaymentResponseMessage paymentResponseModel) {
                hideProgressDialog();
                paymentResponseBehaviorSubject.onNext(paymentResponseModel);
            }

            @Override
            public void onError(Throwable e) {
                hideProgressDialog();
                screenStateBehaviorSubject.onNext(STATE_ERROR_MESSAGE);
            }

            @Override
            public void onComplete() {

            }
        };

        compositeDisposable.add(transferAmount);
        getPaymentRequests().transferAmount(getAuthToken(), paymentRequest).observeOn(mainThreadScheduler).subscribe(transferAmount);
    }

    public Observable<Integer> observeViewState() {
        return screenStateBehaviorSubject.observeOn(mainThreadScheduler);
    }

    public Observable<PaymentResponseMessage> observerPaymentResponse() {
        return paymentResponseBehaviorSubject;
    }

    private void showProgressLoader() {
        screenStateBehaviorSubject.onNext(STATE_LOADING);
    }

    private void hideProgressDialog() {
        screenStateBehaviorSubject.onNext(STATE_REMOVE_LOADING);
    }
}

