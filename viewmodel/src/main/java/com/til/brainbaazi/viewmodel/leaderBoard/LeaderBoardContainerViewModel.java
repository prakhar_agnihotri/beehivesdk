package com.til.brainbaazi.viewmodel.leaderBoard;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.leaderBoard.LeaderBoardListModel;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by saurabh.garg on 15/02/18.
 */
@AutoFactory
public class LeaderBoardContainerViewModel extends BaseScreenModel {

    public static final String PARAM_USER = "user";
    private final ActivityInteractor activityInteractor;
    private final DataRepository dataRepository;

    private BehaviorSubject<User> userInfoBehaviorSubject = BehaviorSubject.create();
    private BehaviorSubject<LeaderBoardListModel> leaderBoardModelBehaviorSubject = BehaviorSubject.create();

    public LeaderBoardContainerViewModel(ActivityInteractor activityInteractor,
                                         @Provided ConnectionManager connectionManager,
                                         @Provided DataRepository dataRepository,
                                         @Provided Analytics analytics) {
        super(connectionManager,dataRepository,analytics);
        this.activityInteractor = activityInteractor;
        this.dataRepository = dataRepository;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        User user = ParcelableUtil.unmarshall(getParams().getByteArray(PARAM_USER), User.creator());
        userInfoBehaviorSubject.onNext(user);
        observeLeaderBoardUserData();
    }

//    private void observeUserData() {
//        DisposableObserver<Response<User>> userDataObserver = new DisposableObserver<Response<User>>() {
//            @Override
//            public void onNext(Response<User> user) {
//                userInfoBehaviorSubject.onNext(user.value());
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//            }
//
//            @Override
//            public void onComplete() {
//            }
//        };
//        addDisposable(userDataObserver);
//        dataRepository.loadUserInfo().subscribe(userDataObserver);
//    }

    private void observeLeaderBoardUserData() {
        final DisposableObserver<Response<LeaderBoardListModel>> leaderBoardDataObserver = new DisposableOnNextObserver<Response<LeaderBoardListModel>>() {
            @Override
            public void onNext(Response<LeaderBoardListModel> leaderBoardModelResponse) {
                if (leaderBoardModelResponse.success())
                    leaderBoardModelBehaviorSubject.onNext(leaderBoardModelResponse.value());
            }
        };
        addDisposable(leaderBoardDataObserver);
        dataRepository.loadLeaderBoardData().subscribe(leaderBoardDataObserver);
    }

    public User getUserInfo() {
        return userInfoBehaviorSubject.getValue();
    }

    public Observable<User> observeUserInfo() {
        return userInfoBehaviorSubject;
    }

    public Observable<LeaderBoardListModel> observeLeaderBoardModel() {
        return leaderBoardModelBehaviorSubject;
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    public void cleverLeaderBoardScreenViewEvent(String type) {
        User user = userInfoBehaviorSubject.getValue();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        if (type.equals("Dashboard")) {
            data.put("source_screen", type);
            data.put("type", "Weekly");
        } else {
            data.put("type", type);
        }
        data.put("Event Time", getAnalytics().getTimeStampInHHMMSSIST());
        cleverTapEvent("leaderboard_visited", data);
    }

    public void sendAnalyticsEvent(String label) {
        User user = getUserInfo();
        if (user == null) {
            return;
        }
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Leaderboard Weekly").setCategory("")
                .setAction("Leaderboard")
                .setLabel(label)
                .setUserName(user.getUserName())
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getAnalytics().logFireBaseEvent(gaEventModel);
    }
}
