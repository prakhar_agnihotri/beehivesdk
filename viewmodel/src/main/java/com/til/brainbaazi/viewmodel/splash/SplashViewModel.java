package com.til.brainbaazi.viewmodel.splash;

import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.cache.Cache;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.entity.game.AppConfig;
import com.til.brainbaazi.entity.otp.PhoneNumber;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 13/02/18.
 */
@AutoFactory
public class SplashViewModel extends BaseScreenModel {

    public static final int STATE_LOADING = 0;
    public static final int STATE_UPDATE = 1;
    public static final int STATE_GET_STARTED = 2;
    public static final int STATE_NETWORK_ERROR = 3;
    public static final int STATE_SERVER_ERROR = 4;
    public static final int STATE_PROCEED_TO_DASHBOARD = 5;
    public static final int STATE_MAINTENANCE = 6;

    private final ConnectionManager connectionManager;
    private final SplashNavigation navigation;
    private final Scheduler mainThreadScheduler;
    private final DataRepository dataRepository;
    private final Cache cache;

    private BehaviorSubject<Integer> splashViewInfoBehaviorSubject = BehaviorSubject.create();
    private BehaviorSubject<AppConfig> appConfigBehaviorSubject = BehaviorSubject.create();

    private DisposableObserver<Response<AppConfig>> appStatusObserver;

    private CompositeDisposable compositeDisposable;

    private int currentState = STATE_LOADING;

    private AppHelper appHelper;

    public SplashViewModel(@Provided ConnectionManager connectionManager, @Provided @MainThreadScheduler Scheduler mainThreadScheduler, @Provided DataRepository dataRepository, SplashNavigation navigation, @Provided AppHelper appHelper,
                           @Provided Analytics analytics,
                           @Provided Cache cache) {
        super(connectionManager, dataRepository, analytics);
        this.dataRepository = dataRepository;
        compositeDisposable = new CompositeDisposable();
        this.mainThreadScheduler = mainThreadScheduler;
        this.connectionManager = connectionManager;
        this.navigation = navigation;
        this.appHelper = appHelper;
        this.cache = cache;
    }


    @Override
    public void restoreState(Bundle inBundle) {
        if (inBundle != null) {
            currentState = STATE_GET_STARTED;
        }
        super.restoreState(inBundle);
    }

    @Override
    public void willShow() {
        super.willShow();
        if (currentState == STATE_LOADING && getParams() != null && getParams().getBoolean("RESTORE")) {
            currentState = STATE_GET_STARTED;
        }
        splashViewInfoBehaviorSubject.onNext(currentState);
        checkAppStatus();
    }

    @Override
    protected void onNetworkChagned(ConnectionInfo connectionInfo) {
        super.onNetworkChagned(connectionInfo);
        if (currentState == STATE_LOADING || currentState == STATE_SERVER_ERROR) {
            if (connectionInfo.connectedToInternet()) {
                checkAppStatus();
            } else {
                updateState(STATE_NETWORK_ERROR);
            }
        }
    }

    @Override
    public void willHide() {
        super.willHide();
    }

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
        splashViewInfoBehaviorSubject = null;
        appStatusObserver = null;
        super.onDestroy();
    }


    private void checkAppStatus() {
        if (appStatusObserver != null && !appStatusObserver.isDisposed()) {
            return;
        }
        appStatusObserver = new DisposableOnNextObserver<Response<AppConfig>>() {
            @Override
            public void onNext(Response<AppConfig> appStatus) {
                final int state;
                if (appStatus.success()) {
                    AppConfig value = appStatus.value();
                    sendAppConfigEvent(null, value);
                    if (value.getVersion() > appHelper.getAppVersion()) {
                        state = STATE_UPDATE;
                    } else if (value.isInMaintenance()) {
                        state = STATE_MAINTENANCE;
                    } else if (value.isRegistered()) {
                        state = STATE_PROCEED_TO_DASHBOARD;
                    } else {
                        state = STATE_GET_STARTED;
                        appConfigBehaviorSubject.onNext(value);
                    }
                } else {
                    state = STATE_SERVER_ERROR;
                }
                updateState(state);
            }
        };
        addDisposable(appStatusObserver);
        connectionManager.observeNetworkChanges().filter(new Predicate<ConnectionInfo>() {
            @Override
            public boolean test(ConnectionInfo connectionInfo) throws Exception {
                return connectionInfo.connectedToInternet();
            }
        }).flatMap(new Function<ConnectionInfo, ObservableSource<Response<AppConfig>>>() {
            @Override
            public ObservableSource<Response<AppConfig>> apply(ConnectionInfo connectionInfo) throws Exception {
                return dataRepository.loadAppConfig();
            }
        }).observeOn(mainThreadScheduler).subscribe(appStatusObserver);
        compositeDisposable.add(appStatusObserver);
    }

    private void updateState(int newState) {
        if (currentState != newState) {
            splashViewInfoBehaviorSubject.onNext(newState);
            currentState = newState;
            if (currentState == STATE_PROCEED_TO_DASHBOARD) {
                appStatusObserver.dispose();
                proceedToDashboard();
            } else if (currentState == STATE_UPDATE) {
                appStatusObserver.dispose();
                proceedToUpdateScreen();
            } else if (currentState == STATE_MAINTENANCE) {
                appStatusObserver.dispose();
                proceedToMaintenanceScreen();
            }
        }
    }

    private void proceedToUpdateScreen() {
        navigation.proceedToUpdate();
    }


    public void proceedToDashboard() {
        navigation.navigateToDashboard();
    }

    private void proceedToMaintenanceScreen() {
        navigation.proceedToMaintenance();
    }

    public void proceedForRegistration() {
        DisposableObserver<ConnectionInfo> disposableObserver = new DisposableObserver<ConnectionInfo>() {
            @Override
            public void onNext(ConnectionInfo connectionInfo) {
                if (connectionInfo.connectedToInternet()) {
                    checkAndOpenRegisteration();
                }
                dispose();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
        connectionManager.observeNetworkChanges().subscribe(disposableObserver);
        addDisposable(disposableObserver);

    }

    private void checkAndOpenRegisteration() {
        String gToken = dataRepository.getGToken();
        PhoneNumber phoneNumber = cache.loadParcelable(Cache.CACHE_KEY_PHONE_NUMBER, PhoneNumber.creator());
        if (phoneNumber == null || gToken == null || gToken.isEmpty()) {
            navigation.navigateToRegistration();
        } else {
            navigation.navigateToProfile(phoneNumber);
        }
    }

    public Observable<Integer> observeViewState() {
        return splashViewInfoBehaviorSubject.distinctUntilChanged().observeOn(mainThreadScheduler);
    }

    public Observable<AppConfig> observeAppConfigState() {
        return appConfigBehaviorSubject.distinctUntilChanged().observeOn(mainThreadScheduler);
    }

    public void getStartedButtonClicked() {
        proceedForRegistration();
    }

    public void updateButtonClicked() {
        openMarket();
    }

    public void openMarket() {
        navigation.proceedToMarket();
    }

    public void retryNetworkConnection() {
        checkAppStatus();
    }

    public void openWebPages(WebData webData) {
        navigation.proceedToWebPage(webData);
    }

    public DataRepository getDataRepository() {
        return dataRepository;
    }

    public void sendAppConfigEvent(User user, AppConfig appConfig) {
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        } else {
            data.put("username", "NA");
            data.put("Phone", "NA");
        }
        data.put("Version", appHelper.getAppVersion());
        cleverTapEvent("Version Event", data);
    }

    public void cleverAppLaunchEvent(String firstLaunch, String deviceId, String city, String installSource) {
        getAnalytics().cleverAppLaunchEvent(firstLaunch, deviceId, city, installSource);
    }
}
