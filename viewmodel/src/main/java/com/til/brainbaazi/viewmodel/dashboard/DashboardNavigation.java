package com.til.brainbaazi.viewmodel.dashboard;

import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.WebData;

/**
 * Created by prashant.rathore on 20/02/18.
 */

public interface DashboardNavigation {
    public void openUserBalanceScreen(User user);

    public void openPlayGameScreen(DashboardInfo dashboardInfo);

    public void openLeaderBoard(User user);

    public void openZeroBalanceScreen(User user);

    public void openWebScreen(WebData webData);

    public void proceedToSplash();
}
