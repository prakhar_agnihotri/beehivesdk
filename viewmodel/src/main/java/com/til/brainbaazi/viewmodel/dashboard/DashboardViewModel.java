package com.til.brainbaazi.viewmodel.dashboard;

import android.net.Uri;
import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.UserStatus;
import com.til.brainbaazi.entity.WebData;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.user.UpdateUserImageModel;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by saurabh.garg on 15/02/18.
 */
@AutoFactory
public class DashboardViewModel extends BaseScreenModel {

    public static final int VIEW_STATE_LOADING = 0;
    public static final int VIEW_STATE_FAILED = 1;
    public static final int VIEW_STATE_SUCCESS = 2;
    public static final int VIEW_STATE_NO_NETWORK = 3;

    private final ActivityInteractor activityInteractor;
    private final DashboardNavigation navigation;
    private final PhoneNumberAuthenticator phoneNumberAuthenticator;

    private final DataRepository dataRepository;
    private final Scheduler mainThread;

    private BehaviorSubject<DashboardGameInfoViewState> gameInfoBehaviorSubject = BehaviorSubject.create();
    private BehaviorSubject<User> userImageResponseBehaviorSubject = BehaviorSubject.create();
    private BehaviorSubject<OTPResponse> otpResponseBehaviorSubject = BehaviorSubject.create();
    private BehaviorSubject<Integer> stateBehaviorSubject = BehaviorSubject.create();

    private CompositeDisposable compositeDisposable;

    private long openedGameID;
    DisposableObserver<UserStatus> userStatusDisposableObserver;
    private DisposableObserver<InputEvent> gameDataObserver;

    private static final String KEY_STATE_GAMEID = "gameId";

    public DashboardViewModel(@Provided @MainThreadScheduler Scheduler mainThread,
                              ActivityInteractor activityInteractor,
                              @Provided ConnectionManager connectionManager,
                              DashboardNavigation navigation,
                              @Provided DataRepository dataRepository,
                              @Provided PhoneNumberAuthenticator phoneNumberAuthenticator,
                              @Provided Analytics analytics) {
        super(connectionManager, dataRepository, analytics);
        this.mainThread = mainThread;
        this.phoneNumberAuthenticator = phoneNumberAuthenticator;
        this.activityInteractor = activityInteractor;
        this.navigation = navigation;
        this.dataRepository = dataRepository;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        gameInfoBehaviorSubject.onNext(DashboardGameInfoViewState.create(VIEW_STATE_LOADING, null));
    }

    @Override
    public void restoreState(Bundle inBundle) {
        if (inBundle != null) {
            openedGameID = inBundle.getLong(KEY_STATE_GAMEID, 0);
        }
        super.restoreState(inBundle);
    }

    @Override
    public void willShow() {
        super.willShow();
        compositeDisposable = new CompositeDisposable();
        observePhoneAuthenticationResponses();
        observeGameData();
        loadDashboardInfo();
        observeUserStatus();
    }

    private void observeUserStatus() {
        userStatusDisposableObserver = new DisposableOnNextObserver<UserStatus>() {
            @Override
            public void onNext(UserStatus userStatus) {
                if (userStatus.getStatus() == UserStatus.STATUS_LOGGED_OUT) {
                    navigation.proceedToSplash();
                }
            }
        };
        addDisposable(userStatusDisposableObserver);
        dataRepository.observeUserStatus().observeOn(mainThread).subscribe(userStatusDisposableObserver);
    }

    public void uploadImageToServer(final Uri filePath) {
        DisposableObserver<ConnectionInfo> disposableObserver = new DisposableOnNextObserver<ConnectionInfo>() {
            @Override
            public void onNext(ConnectionInfo connectionInfo) {
                if (connectionInfo.connectedToInternet()) {
                    phoneNumberAuthenticator.uploadUserImage(filePath);
                }
                dispose();
            }
        };
        addDisposable(disposableObserver);
        getConnectionManager().observeNetworkChanges().subscribe(disposableObserver);
    }

    public void updateUserImageToServer(final String imageUrl) {
        DisposableOnNextObserver<Response<User>> disposableOnNextObserver = new DisposableOnNextObserver<Response<User>>() {
            @Override
            public void onNext(Response<User> updateUserImageResponseResponse) {
                if (updateUserImageResponseResponse.value() != null) {
                    userImageResponseBehaviorSubject.onNext(updateUserImageResponseResponse.value());
                }
            }
        };
        addDisposable(disposableOnNextObserver);

        getConnectionManager().observeNetworkChanges().flatMap(new Function<ConnectionInfo, ObservableSource<Response<User>>>() {
            @Override
            public ObservableSource<Response<User>> apply(ConnectionInfo connectionInfo) throws Exception {
                if (connectionInfo.connectedToInternet()) {
                    UpdateUserImageModel updateUserImageModel = UpdateUserImageModel.builder().setUim(imageUrl).build();
                    return dataRepository.loadUpdateUserImageResponse(updateUserImageModel);
                } else {
                    Response<User> build = Response.builder().setSuccess(false).build();
                    return Observable.just(build);
                }
            }
        }).subscribe(disposableOnNextObserver);
    }

    private void observePhoneAuthenticationResponses() {
        DisposableObserver<OTPResponse> otpRequestObservable = new DisposableObserver<OTPResponse>() {
            @Override
            public void onNext(OTPResponse otpResponse) {
                otpResponseBehaviorSubject.onNext(otpResponse);
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        };
        addDisposable(otpRequestObservable);
        phoneNumberAuthenticator.observeOtpCallbacks().subscribe(otpRequestObservable);
    }

    @Override
    public void willHide() {
        removeGameDataObeserver();
        userStatusDisposableObserver.dispose();
        compositeDisposable.dispose();
        super.willHide();
    }

    @Override
    public void saveState(Bundle outBundle) {
        outBundle.putLong(KEY_STATE_GAMEID, openedGameID);
        super.saveState(outBundle);
    }

    public Observable<DashboardGameInfoViewState> observeDashBoardState() {
        return gameInfoBehaviorSubject;
    }

    public Observable<User> observeUserImageResponse() {
        return userImageResponseBehaviorSubject;
    }

    public Observable<OTPResponse> observeOTPResponse() {
        return otpResponseBehaviorSubject;
    }

    public Observable<Integer> observeStateBehaviorSubject() {
        return stateBehaviorSubject;
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    public void openUserBalanceScreen() {
        DashboardGameInfoViewState value = this.gameInfoBehaviorSubject.getValue();
        if (value != null && value.dashboardInfo() != null && value.dashboardInfo().getUser() != null) {
            User user = value.dashboardInfo().getUser();
            if (value.dashboardInfo().getUser().getUserBalance() > 0) {
                navigation.openUserBalanceScreen(user);
            } else {
                navigation.openZeroBalanceScreen(user);
            }
        }
    }

    public void openPlayGameScreen(boolean toSendAnalytics) {
        DashboardGameInfoViewState value = gameInfoBehaviorSubject.getValue();
        if (value != null && value.dashboardInfo() != null && value.dashboardInfo().getUser() != null) {
            User user = getUserInfo();
            Map<String, Object> data = new HashMap<>();
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
            data.put("game_id", value.dashboardInfo().getCurrentGameInfo().getCurrentGameID());
            data.put("question_id", "NA");
            data.put("Stream Viewed", toSendAnalytics ? "Watch" : "Auto");
            data.put("Event Time", getAnalytics().getTimeStampInHHMMSSIST());
            cleverTapEvent("Stream Viewed Event", data);

            if (toSendAnalytics) {
                String category = "Game Play- " + value.dashboardInfo().getCurrentGameInfo().getCurrentGameID();
                GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Stream Viewed").setCategory(category)
                        .setAction("Watch")
                        .setLabel("")
                        .setUserName(user.getUserName())
                        .setTimeStamp(AppUtils.getTimeStamp()).build();
                sendAnalyticsEvent(gaEventModel);
            }
            navigation.openPlayGameScreen(value.dashboardInfo());
        }
    }

    public void loadDashboardInfo() {
        final DisposableObserver<Response<DashboardInfo>> gameDataObserver;
        gameDataObserver = new DisposableOnNextObserver<Response<DashboardInfo>>() {
            @Override
            public void onNext(Response<DashboardInfo> dashboardInfoResponse) {
                DashboardInfo value = dashboardInfoResponse.value();
                if (dashboardInfoResponse.success()) {
                    int state = VIEW_STATE_SUCCESS;
                    gameInfoBehaviorSubject.onNext(DashboardGameInfoViewState.create(state, value));
                    if (value.getActive() && value.getCurrentGameInfo() != null) {
                        long currentGameId = value.getCurrentGameInfo().getCurrentGameID();
                        if (currentGameId != openedGameID) {
                            openedGameID = value.getCurrentGameInfo().getCurrentGameID();
                            openPlayGameScreen(false);
                        }
                    }
                } else {
                    int state = VIEW_STATE_FAILED;
                    gameInfoBehaviorSubject.onNext(DashboardGameInfoViewState.create(state, value));
                }
            }

        };
        compositeDisposable.add(gameDataObserver);
        dataRepository.loadDashboardInfo().subscribe(gameDataObserver);
    }

    public void openLeaderBoardScreen() {
        if (getUserInfo() == null) {
            return;
        }
        getConnectionManager().observeNetworkChanges().subscribe(new DisposableOnNextObserver<ConnectionInfo>() {
            @Override
            public void onNext(ConnectionInfo connectionInfo) {
                if (connectionInfo.connectedToInternet()) {
                    navigation.openLeaderBoard(getUserInfo());
                } else {
                    stateBehaviorSubject.onNext(VIEW_STATE_NO_NETWORK);
                }
                dispose();
            }
        });
    }

    public User getUserInfo() {
        DashboardGameInfoViewState value = this.gameInfoBehaviorSubject.getValue();
        if (value != null && value.dashboardInfo() != null && value.dashboardInfo().getUser() != null) {
            return value.dashboardInfo().getUser();
        }
        return null;
    }

    public String getUserName() {
        return dataRepository.getUserName();
    }

    public void openWebPages(WebData webData) {
        navigation.openWebScreen(webData);
    }

    public void signOutUser() {
        dataRepository.logOutUser();
    }

    private void observeGameData() {
        removeGameDataObeserver();
        gameDataObserver = new DisposableOnNextObserver<InputEvent>() {
            @Override
            public void onNext(InputEvent inputEvent) {
                handleGameEvent(inputEvent);
            }

        };
        addDisposable(gameDataObserver);
        dataRepository.observerGameEvents().subscribe(gameDataObserver);
    }

    private void removeGameDataObeserver() {
        if (gameDataObserver != null) {
            gameDataObserver.dispose();
        }
    }

    private void handleGameEvent(InputEvent inputEvent) {
        if (inputEvent.type() == InputEvent.TYPE_LIVE_TRIGGER || inputEvent.type() == InputEvent.TYPE_GAME_END) {
            loadDashboardInfo();
        }
    }

    public void sendAnalyticsEvent(GaEventModel gaEventModel) {
        getAnalytics().logFireBaseEvent(gaEventModel);
    }

    public DataRepository getDataRepository() {
        return dataRepository;
    }
}
