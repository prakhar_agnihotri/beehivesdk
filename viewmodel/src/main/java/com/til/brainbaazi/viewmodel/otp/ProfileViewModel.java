package com.til.brainbaazi.viewmodel.otp;

import android.net.Uri;
import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.cache.Cache;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.UserStatus;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.otp.PhoneNumber;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.entity.user.UsernameAvailableResponse;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by saurabh.garg on 2/23/18.
 */
@AutoFactory
public class ProfileViewModel extends BaseScreenModel {

    public static final String PARAM_PHONE_NUMBER = "phoneNumber";

    public static final int STATE_REQUESTING = 1;
    public static final int STATE_NO_NETWORK = 4;

    private final OtpNavigation navigation;
    private final DataRepository dataRepository;
    private final ActivityInteractor activityInteractor;
    private final PhoneNumberAuthenticator phoneNumberAuthenticator;
    private final Scheduler mainThreadScheduler;
    private final Cache cache;

    private BehaviorSubject<Integer> viewStateObserver = BehaviorSubject.create();
    private BehaviorSubject<OTPResponse> otpResponseBehaviorSubject = BehaviorSubject.create();
    private BehaviorSubject<User> userInfoBehaviorSubject = BehaviorSubject.create();

    private CompositeDisposable compositeDisposable;

    private int currentState = -1;
    private PhoneNumber phoneNumber;

    public ProfileViewModel(@Provided @MainThreadScheduler Scheduler mainThread,
                            ActivityInteractor activityInteractor,
                            @Provided ConnectionManager connectionManager,
                            OtpNavigation navigation,
                            @Provided DataRepository dataRepository,
                            @Provided PhoneNumberAuthenticator phoneNumberAuthenticator,
                            @Provided Analytics analytics,
                            @Provided Cache cache) {

        super(connectionManager, dataRepository, analytics);
        this.activityInteractor = activityInteractor;
        this.navigation = navigation;
        this.dataRepository = dataRepository;
        this.phoneNumberAuthenticator = phoneNumberAuthenticator;
        this.mainThreadScheduler = mainThread;
        this.cache = cache;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getUserPhoneData();
        observeUserStatus();
        observePhoneAuthenticationResponses();
    }

    private void getUserPhoneData() {
        this.phoneNumber = getParams().getParcelable(PARAM_PHONE_NUMBER);
        if (phoneNumber == null) {
            phoneNumber = cache.loadParcelable(Cache.CACHE_KEY_PHONE_NUMBER, PhoneNumber.creator());
        } else {
            cache.saveParcelable(Cache.CACHE_KEY_PHONE_NUMBER, phoneNumber);
        }
    }

    @Override
    public void willShow() {
        super.willShow();
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void willHide() {
        super.willHide();
        compositeDisposable.dispose();
    }

    private void observeUserStatus() {
        Disposable subscribe = dataRepository.observeUserStatus()
                .observeOn(mainThreadScheduler).subscribe(new Consumer<UserStatus>() {
                    @Override
                    public void accept(UserStatus userStatus) throws Exception {
                        switch (userStatus.getStatus()) {
                            case UserStatus.STATUS_LOGGED_IN:
                                navigation.navigateToDashboard();
                                break;
                        }
                    }
                });
        addDisposable(subscribe);
    }

    private void observePhoneAuthenticationResponses() {
        DisposableObserver<OTPResponse> otpRequestObservable = new DisposableOnNextObserver<OTPResponse>() {
            @Override
            public void onNext(OTPResponse otpResponse) {
                otpResponseBehaviorSubject.onNext(otpResponse);
            }

        };
        addDisposable(otpRequestObservable);
        phoneNumberAuthenticator.observeOtpCallbacks().subscribe(otpRequestObservable);
    }

    public void uploadImageToServer(final Uri filePath) {
        DisposableObserver<ConnectionInfo> disposableObserver = new DisposableOnNextObserver<ConnectionInfo>() {
            @Override
            public void onNext(ConnectionInfo connectionInfo) {
                if (connectionInfo.connectedToInternet()) {
                    phoneNumberAuthenticator.uploadUserImage(filePath);
                } else {
                    viewStateObserver.onNext(STATE_NO_NETWORK);
                }
                dispose();
            }

        };
        getConnectionManager().observeNetworkChanges().subscribe(disposableObserver);
    }

    public Observable<UsernameAvailableResponse> checkUserNameServer(final String userName, final int checkType) {
        return getConnectionManager().observeNetworkChanges().flatMap(new Function<ConnectionInfo, ObservableSource<UsernameAvailableResponse>>() {
            @Override
            public ObservableSource<UsernameAvailableResponse> apply(ConnectionInfo connectionInfo) throws Exception {
                if (connectionInfo.connectedToInternet()) {
                    return dataRepository.loadUsernameAvailableResponse(userName);
                } else {
                    UsernameAvailableResponse build = UsernameAvailableResponse.builder().setStatus(false).setCheckType(checkType).build();
                    return Observable.just(build);
                }
            }
        });
    }

    public void submitModifiedUserData(final UserRequestModel requestModel) {
        sendProfileSubmitEvent(requestModel);
        DisposableObserver<Response<UserStatus>> disposableObserver = new DisposableOnNextObserver<Response<UserStatus>>() {
            @Override
            public void onNext(Response<UserStatus> response) {
                if (response.success() && response.value().getStatus() == UserStatus.STATUS_LOGGED_IN) {
                    sendProfileFinishEvent(requestModel);
                    navigation.navigateToDashboard();
                }
            }

        };
        getConnectionManager().observeNetworkChanges().flatMap(new Function<ConnectionInfo, ObservableSource<Response<UserStatus>>>() {
            @Override
            public ObservableSource<Response<UserStatus>> apply(ConnectionInfo connectionInfo) throws Exception {
                return dataRepository.registerUser(requestModel);
            }
        }).observeOn(mainThreadScheduler).subscribe(disposableObserver);
    }


    public Observable<User> observeUserInfo() {
        return userInfoBehaviorSubject;
    }

    public Observable<OTPResponse> observeOTPResponse() {
        return otpResponseBehaviorSubject;
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    public Observable<Integer> observeViewState() {
        return viewStateObserver.distinctUntilChanged();
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public void onDestroy() {
        this.viewStateObserver.onComplete();
        this.viewStateObserver = null;
        super.onDestroy();
    }


    private void sendProfileFinishEvent(UserRequestModel requestModel) {
        Map<String, Object> data = new HashMap<>();
        data.put("referral_code", requestModel.getReferralId());
        data.put("username", requestModel.getUserName());
        data.put("photo", requestModel.getProfileImageUrl());
        data.put("Phone", phoneNumber.getPhoneNumber());
        cleverTapEvent("profile_creation_finish", data);

        Bundle bundle = new Bundle();
        bundle.putString("referral_code", requestModel.getReferralId());
        bundle.putString("username", requestModel.getUserName());
        bundle.putString("photo", requestModel.getProfileImageUrl());
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("profile_creation_finish").build();
        logFireBaseScreen(gaEventModel, bundle);
    }

    public void sendProfileImageEvent(String userNameString) {
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Photo Added").setCategory("Profile Creation")
                .setAction("Photo Added")
                .setLabel("New Photo")
                .setUserName(userNameString)
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getAnalytics().logFireBaseEvent(gaEventModel);
    }

    private void sendProfileSubmitEvent(UserRequestModel requestModel) {
        if (!AppUtils.isEmpty(requestModel.getReferralId())) {
            Map<String, Object> data = new HashMap<>();
            data.put("username", requestModel.getUserName());
            data.put("Phone", requestModel.getMobileNumber());
            data.put("referral_code", requestModel.getReferralId());
            cleverTapEvent("Add Referral Code", data);
        }

        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Profile Creation").setCategory("Profile Creation")
                .setAction("Referral Code " + requestModel.getReferralId())
                .setLabel("Clicked on Finish")
                .setUserName(requestModel.getUserName())
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getAnalytics().logFireBaseEvent(gaEventModel);

        gaEventModel = GaEventModel.builder().setMainEvent("Username Added").setCategory("Profile Creation")
                .setAction("Username Added")
                .setLabel("Clicked on Finish")
                .setUserName(requestModel.getUserName())
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getAnalytics().logFireBaseEvent(gaEventModel);
    }
}
