package com.til.brainbaazi.viewmodel.gamePlay;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.brainbaazi.component.repo.SocketStore;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.UserStatus;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.game.GameEventUtils;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.GameState;
import com.til.brainbaazi.entity.game.QuestionInfo;
import com.til.brainbaazi.entity.game.SubmitAnswer;
import com.til.brainbaazi.entity.game.SubmitAnswerRequestModel;
import com.til.brainbaazi.entity.game.SubmitPayload;
import com.til.brainbaazi.entity.game.chat.ChatMsg;
import com.til.brainbaazi.entity.game.chat.ChatPayLoad;
import com.til.brainbaazi.entity.game.chat.ChatSendObject;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.Question;
import com.til.brainbaazi.entity.game.event.UserAnswer;
import com.til.brainbaazi.entity.game.event.Winner;
import com.til.brainbaazi.entity.game.response.AmIWinnerResponse;
import com.til.brainbaazi.entity.game.response.GameResponse;
import com.til.brainbaazi.interactor.GameEngine;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by saurabh.garg on 2/16/18.
 */
@AutoFactory
public class LiveGamePlayViewModel extends BaseScreenModel {

    public static final int VIEW_STATE_LOADING = 0;
    public static final int VIEW_STATE_FAILED = 1;
    public static final int VIEW_STATE_VIDEO = 2;

    public static final int STATE_DIALOG_YOU_WON = 3;
    public static final int STATE_WINNER_LOADED = 4;
    public static final int STATE_DIALOG_QUIT = 5;

    private final ActivityInteractor activityInteractor;
    private final DataRepository dataRepository;
    private final GameEngine gameEngine;
    private final SocketStore socketStore;

    private PublishSubject<GameResponse<?>> liveGameEventsSubject = PublishSubject.create();
    private BehaviorSubject<GameViewState> gameViewStateSubject = BehaviorSubject.create();
    private BehaviorSubject<ChatMsg> chatMsgStateSubject = BehaviorSubject.create();
    private BehaviorSubject<Integer> dialogStateBehaviorSubject = BehaviorSubject.create();
    private DisposableObserver<UserStatus> userStatusDisposableObserver;
    private final Scheduler mainThread;
    private GameInfo gameInfo;

    private DisposableObserver<GameResponse<?>> gameDataObserver;

    private DashboardInfo dashboardInfo;
    private boolean prepareToQuit = false;

    public static final String PARAM_DASHBOARD_INFO = "dashboardInfo";
    private String KEY_STREAM_MODE_COACHMARK = "key_stream_mode_coachmark";

    public LiveGamePlayViewModel(@Provided @MainThreadScheduler Scheduler mainThread,
                                 @Provided ConnectionManager connectionManager,
                                 @Provided DataRepository dataRepository,
                                 @Provided GameEngine gameEngine,
                                 ActivityInteractor activityInteractor,
                                 @Provided SocketStore socketStore,
                                 @Provided Analytics analytics) {
        super(connectionManager, dataRepository, analytics);
        this.mainThread = mainThread;
        this.dataRepository = dataRepository;
        this.gameEngine = gameEngine;
        this.activityInteractor = activityInteractor;
        this.socketStore = socketStore;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.dashboardInfo = ParcelableUtil.unmarshall(getParams().getByteArray(PARAM_DASHBOARD_INFO), DashboardInfo.creator());
        observeGameInfo();
        observeUserStatus();
    }

    @Override
    protected void onNetworkChagned(ConnectionInfo connectionInfo) {
        super.onNetworkChagned(connectionInfo);
        if (!connectionInfo.connectedToInternet()) {
            sendNetworkStatusEvent(false);
        }
    }

    private void observeUserStatus() {
        userStatusDisposableObserver = new DisposableOnNextObserver<UserStatus>() {
            @Override
            public void onNext(UserStatus userStatus) {
                if (userStatus.getStatus() == UserStatus.STATUS_LOGGED_OUT) {
                    prepareToQuit = true;
                    getActivityInteractor().performBackPress();
                }
            }
        };
        addDisposable(userStatusDisposableObserver);
        dataRepository.observeUserStatus().observeOn(mainThread).subscribe(userStatusDisposableObserver);
    }

    public String getAbusiveString() {
        return dataRepository.getChatAbusiveText();
    }

    private void observerChatData() {
        DisposableObserver<ChatMsg> chatDataObserver = new DisposableOnNextObserver<ChatMsg>() {
            @Override
            public void onNext(ChatMsg chatMsgResponse) {
                chatMsgStateSubject.onNext(chatMsgResponse);
            }

        };
        addDisposable(chatDataObserver);
        dataRepository.observerChatMessages(gameInfo).subscribe(chatDataObserver);
    }

    private void observeGameInfo() {

        GameInfo oldGameInfo = gameInfo;
        gameInfo = dashboardInfo.getCurrentGameInfo();
        if (oldGameInfo == null || oldGameInfo.getCurrentGameID() != dashboardInfo.getCurrentGameInfo().getCurrentGameID()) {
            updateState(VIEW_STATE_LOADING);
            observeGameData(gameInfo);
            observerChatData();
        }
    }

    private void observeGameData(GameInfo gameInfo) {
        if (gameDataObserver != null) {
            gameDataObserver.dispose();
        }
        gameDataObserver = new DisposableOnNextObserver<GameResponse<?>>() {
            @Override
            public void onNext(GameResponse<?> gameResponse) {
                handleGameEvents(gameResponse);
            }
        };
        addDisposable(gameDataObserver);
        gameEngine.observeGameEvents(gameInfo, getUser()).subscribe(gameDataObserver);
    }

    private void handleGameEvents(GameResponse<?> gameResponse) {
        int event = gameResponse.eventType();
        switch ((event & GameResponse.MASK_SHOW_BITS)) {
            case GameResponse.TYPE_SHOW_WINNER:
                handleWinnerEvent(gameResponse);
                break;
            case GameResponse.TYPE_SHOW_QUESTION:
                cleverQuestionViewTraceEvent("LiveGameActivity");
            case GameResponse.TYPE_SHOW_ANSWER:
                if (gameResponse.value() instanceof QuestionInfo || gameResponse.value() == null) {
                    QuestionInfo value = (QuestionInfo) gameResponse.value();
                    if (value == null || value.question() == null) {
                        break;
                    }
                }
            case GameResponse.TYPE_USER_STATE:
            case GameResponse.TYPE_SHOW_GAME_END:
            default:
                liveGameEventsSubject.onNext(gameResponse);
                break;
        }

        updateViewState(gameResponse);
    }

    private void handleWinnerEvent(GameResponse<?> gameResponse) {
        if (hasUserWon()) {
            final long currentTimestamp = new Date().getTime();
            DisposableOnNextObserver<Response<AmIWinnerResponse>> disposableOnNextObserver = new DisposableOnNextObserver<Response<AmIWinnerResponse>>() {
                @Override
                public void onNext(Response<AmIWinnerResponse> winnerResponse) {
                    if (winnerResponse.success() && winnerResponse.value().isWin()) {
                        dialogStateBehaviorSubject.onNext(STATE_DIALOG_YOU_WON);
                        cleverTechAPIStatus(null, true, "", currentTimestamp);
                    } else {
                        cleverTechAPIStatus(null, false, "APP_WIN_API_NO_WIN", currentTimestamp);
                    }
                    submitGameData();
                }
            };
            addDisposable(disposableOnNextObserver);
            checkUserWinner().subscribe(disposableOnNextObserver);
        } else {
            submitGameData();
        }
        Winner winner = gameEngine.getGameState(dashboardInfo.getCurrentGameInfo(), getUser()).getWinner();
        if (winner == null || winner.isEmpty()) {
            loadWinnerData();
        } else {
            liveGameEventsSubject.onNext(gameResponse);
        }
    }

    private void loadWinnerData() {
        DisposableOnNextObserver<Response<Winner>> disposableOnNextObserver = new DisposableOnNextObserver<Response<Winner>>() {
            @Override
            public void onNext(Response<Winner> winnerResponse) {
                if (winnerResponse.success() && winnerResponse.value() != null) {
                    cleverWinnerEvent("API", winnerResponse.value().getWinUserList().size(), true);
                    InputEvent event = GameEventUtils.getInputEvent(InputEvent.TYPE_WINNER, winnerResponse.value(), dashboardInfo.getUser(), dashboardInfo.getCurrentGameInfo(), false, true);
                    gameEngine.processEvent(event);
                } else {
                    cleverWinnerEvent("API_WINNER_NULL", 0, true);
                }
            }
        };
        addDisposable(disposableOnNextObserver);
        loadGameWinnner().subscribe(disposableOnNextObserver);
    }


    private void updateState(int newViewState) {
        GameViewState value = gameViewStateSubject.getValue();
        GameViewState.Builder valueBuilder;
        if (value == null) {
            valueBuilder = GameViewState.builder();
        } else if (value.getViewState() == newViewState) {
            return;
        } else {
            valueBuilder = value.toBuilder();
        }

        if (gameInfo != null) {
            valueBuilder.setLiveStreamUrl(gameInfo.getStreamingUrl()).setPrizeAmount(gameInfo.getGamePrize());
//                    .setGameLive(gameInfo.isGoneLive());
        }
        GameState gameState = gameEngine.getGameState(gameInfo, getUser());
        valueBuilder.setLifeAvailable(gameState.getLifeAvailable() > gameState.getLifeUsed());
        GameViewState build = valueBuilder.setViewState(newViewState).build();
        gameViewStateSubject.onNext(build);
    }

    private void updateViewState(GameResponse<?> gameResponse) {
        GameState gameState = gameResponse.gameState();
        GameViewState build = GameViewState.builder()
                .setViewState(VIEW_STATE_VIDEO)
                .setConcurrentUserCount(gameState.getConcurrentUserCount())
                .setGameLive(gameState.isGameLive())
                .setLifeAvailable(gameState.getLifeAvailable() > gameState.getLifeUsed())
                .setLiveStreamUrl(gameInfo.getStreamingUrl())
                .setPrizeAmount(gameInfo.getGamePrize())
                .setUserPlayState(gameState.getUserPlayState())
                .setTranslations(new Object()).build();
        gameViewStateSubject.onNext(build);
    }

    public Observable<Response<AmIWinnerResponse>> checkUserWinner() {
        return getConnectionManager().observeNetworkChanges().flatMap(new Function<ConnectionInfo, ObservableSource<Response<AmIWinnerResponse>>>() {
            @Override
            public ObservableSource<Response<AmIWinnerResponse>> apply(ConnectionInfo connectionInfo) throws Exception {
                if (connectionInfo.connectedToInternet()) {
                    return dataRepository.sendWinnerRequest();
                } else {
                    Response<AmIWinnerResponse> build = Response.builder().setSuccess(false).build();
                    return Observable.just(build);
                }
            }
        });
    }

    public Observable<Response<Winner>> loadGameWinnner() {
        return getConnectionManager().observeNetworkChanges().flatMap(new Function<ConnectionInfo, ObservableSource<Response<Winner>>>() {
            @Override
            public ObservableSource<Response<Winner>> apply(ConnectionInfo connectionInfo) throws Exception {
                if (connectionInfo.connectedToInternet()) {
                    return dataRepository.loadGameWinners();
                } else {
                    Response<Winner> build = Response.builder().setSuccess(false).build();
                    return Observable.just(build);
                }
            }
        });
    }

    public Observable<GameViewState> observeGameViewState() {
        return gameViewStateSubject;
    }

    public Observable<ChatMsg> observeChatInfo() {
        return chatMsgStateSubject;
    }

    public Observable<GameResponse<?>> observeLiveGameEvents() {
        return liveGameEventsSubject;
    }

    public Observable<Integer> observeDialogStateBehaviorSubject() {
        return dialogStateBehaviorSubject;
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    public DashboardInfo getDashboardInfo() {
        return dashboardInfo;
    }

    public boolean hasUserWon() {
        return gameEngine.hasUserWon(dashboardInfo.getCurrentGameInfo(), getUser());
    }

    public User getUser() {
        return dashboardInfo.getUser();
    }

    public int getChatFrequency() {
        return dataRepository.getChatFrequency();
    }

    public void submitGameData() {
        //TODO submit user game data
    }

    public void submitSlikeQueEvent(String cueText, int inputType) {
        gameEngine.processEvent(GameEventUtils.extractPayloadData(dashboardInfo.getCurrentGameInfo(), cueText, inputType, dashboardInfo.getUser()));
    }

    public void submitUserChatMessage(final String message) {
        DisposableOnNextObserver<Boolean> disposableOnNextObserver = new DisposableOnNextObserver<Boolean>() {
            @Override
            public void onNext(Boolean isValid) {
                if (isValid) {
                    User user = dashboardInfo.getUser();
                    ChatPayLoad chatPayLoad = ChatPayLoad.builder()
                            .setAction("c")
                            .setMessage(message)
                            .setUserImg(user.getUserImgUrl())
                            .setUid(user.getUserName())
                            .build();

                    ChatSendObject chatSendObject = ChatSendObject.builder()
                            .setAction("c")
                            .setEvent("c")
                            .setPayLoad(chatPayLoad).build();

                    Map<String, Object> data = new HashMap<>();
                    data.put("username", user.getUserName());
                    data.put("Phone", user.getPhoneNumber());
                    cleverTapEvent("comment_added", data);

                    String category = "Game Play- " + dashboardInfo.getCurrentGameInfo().getCurrentGameID();
                    GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("comment_added").setCategory(category)
                            .setAction("comment_added")
                            .setLabel("")
                            .setUserName(dashboardInfo.getUser().getUserName())
                            .setTimeStamp(AppUtils.getTimeStamp()).build();
                    sendAnalyticsEvent(gaEventModel);
                    socketStore.sendChatMessage(chatSendObject);
                }
            }
        };
        addDisposable(disposableOnNextObserver);
        dataRepository.checkChatMessageValid(message).subscribe(disposableOnNextObserver);
    }

    public void submitUserAnswer(int userAnswer, String answer) {
        InputEvent inputEvent = gameEngine.getGameState(dashboardInfo.getCurrentGameInfo(), getUser()).getLastQuestionEvent();
        String quesID = String.valueOf(((Question) inputEvent.gameEvent()).getId());
        String gameID = String.valueOf(dashboardInfo.getCurrentGameInfo().getCurrentGameID());

        UserAnswer userAnswered = UserAnswer.builder().setQuestionId(Long.parseLong(quesID)).setUserAnswerOption(userAnswer).build();
        InputEvent userAnswerEvent = InputEvent.builder()
                .setUser(dashboardInfo.getUser())
                .setSource(0)
                .setType(InputEvent.TYPE_USER_ANSWERED)
                .setGameEvent(userAnswered)
                .setGameInfo(gameInfo)
                .setTrigger(false)
                .build();
        gameEngine.processEvent(userAnswerEvent);
        if (inputEvent.source() == InputEvent.SOURCE_SOCKET) {
            submitViaSocket(answer, gameID, quesID);
        } else {
            submitViaAPI(answer, gameID, quesID);
        }
    }

    private void submitViaAPI(String answer, String gameID, final String quesId) {
        SubmitAnswerRequestModel answerRequestModel = SubmitAnswerRequestModel.builder()
                .setTimeTaken("" + 122)
                .setGameId(gameID)
                .setQuesId(quesId)
                .setUid("0")
                .setUqid("0")
                .setOptId(answer).build();
        DisposableOnNextObserver<String> disposableOnNextObserver = new DisposableOnNextObserver<String>() {
            @Override
            public void onNext(String winnerResponse) {
                if (!AppUtils.isEmpty(winnerResponse)) {
                    cleverAnswerSubmitSourceEvent("API", quesId, true);
                } else {
                    cleverAnswerSubmitSourceEvent("API", quesId, false);
                }
            }
        };
        addDisposable(disposableOnNextObserver);
        dataRepository.submitUserAnswer(answerRequestModel).subscribe(disposableOnNextObserver);
    }

    private void submitViaSocket(String answer, String gameID, String quesId) {
        SubmitPayload submitPayload = SubmitPayload.builder()
                .setGameID(gameID)
                .setQuestionID(quesId)
                .setOptionID(answer)
                .setTime("122")
                .build();

        SubmitAnswer submitAnswer = SubmitAnswer.builder().setA("s").setP(submitPayload).build();
        socketStore.sendMessage(submitAnswer);
    }

    public void setPrepareToQuit(boolean prepareToQuit) {
        this.prepareToQuit = prepareToQuit;
    }

    @Override
    public boolean handleBackPressed() {
        if (prepareToQuit || gameEngine.hasGameFinished(dashboardInfo.getCurrentGameInfo(), dashboardInfo.getUser())) {
            return super.handleBackPressed();
        } else {
            dialogStateBehaviorSubject.onNext(STATE_DIALOG_QUIT);
            return true;
        }
    }

    public void cleverTechAPIStatus(String apiName, boolean success, String extraInfo, long timestamp) {
        User user = getUser();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        } else {
            data.put("username", "NA");
            data.put("Phone", "NA");
        }
        data.put("API name", apiName);
        data.put("success", success);
        data.put("EXTRA_INFO", extraInfo);
        data.put("Timestamp", timestamp);
        cleverTapEvent("Tech_API_Event", data);
    }

    public void sendVideoDataEvent(String dataName, String dataValue, boolean fromSocket) {
        User user = getUser();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("game_id", dashboardInfo.getCurrentGameInfo().getCurrentGameID());
        data.put("Event Time", getAnalytics().getTimeStampInHHMMSSIST());
        if (fromSocket) {
            data.put("Video Socket Data Name", dataName);
            cleverTapEvent("Socket Data Event", data);
        } else {
            data.put("Cue Data Name", dataName);
            cleverTapEvent("Cue Data Event", data);
        }
    }

    public void sendStreamErrorEvent(String exception) {
        User user = getUser();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("game_id", dashboardInfo.getCurrentGameInfo().getCurrentGameID());
        data.put("Exception", exception);
        data.put("Event Time", getAnalytics().getTimeStampInHHMMSSIST());
        cleverTapEvent("Stream/Player Error Event", data);
    }

    public void cleverQuestionViewTraceEvent(String fileName) {
        User user = getUser();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        } else {
            data.put("username", "NA");
            data.put("Phone", "NA");
        }

        data.put("fileName", fileName);
        cleverTapEvent("TECH_QuestionTraceEvent", data);
    }

    public void cleverAnswerSubmitSourceEvent(String source, String questionIndex, boolean success) {
        User user = getUser();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        } else {
            data.put("username", "NA");
            data.put("Phone", "NA");
        }

        data.put("QUESTION_INDEX", questionIndex);
        data.put("API name", source);
        data.put("success", success);
        cleverTapEvent("TECH_AnswerSubmittedFrom", data);
    }

    public void cleverWinnerEvent(String source, int winnerCount, boolean success) {
        User user = getUser();
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        } else {
            data.put("username", "NA");
            data.put("Phone", "NA");
        }

        data.put("WINNER_COUNT", winnerCount);
        data.put("API name", source);
        data.put("success", success);
        cleverTapEvent("TECH_AnswerSubmittedFrom", data);
    }

    public void cleverQuestionQuitEvent(String reason) {
        User user = getUser();
        boolean isEliminated = (gameEngine.hasUserWon(gameInfo, user));
        Map<String, Object> data = new HashMap<>();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("Reason", reason);
        data.put("Is_User_Eliminated", "" + isEliminated);
        data.put("game_id", dashboardInfo.getCurrentGameInfo().getCurrentGameID());
        data.put("Event Time", getAnalytics().getTimeStampInHHMMSSIST());
        cleverTapEvent("Game Quit", data);
    }

    public void cleverIAMAliveEvent(String bitRate, String latency, String deviceName, String versionCode) {
        Map<String, Object> data = new HashMap<>();
        User user = getUser();
        boolean isEliminated = (gameEngine.hasUserWon(gameInfo, user));
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        data.put("Timestamp", getAnalytics().getTimeStampInHHMMSSIST());
        data.put("game_id", dashboardInfo.getCurrentGameInfo().getCurrentGameID());
        data.put("Socket_Status", socketStore.getSocketStatus());
        data.put("Network Type", getLastConnectionInfo().name());
        data.put("Is_User_Eliminated", "" + isEliminated);
        data.put("Bit Rate", bitRate);
        data.put("Video_Latency", latency);
        data.put("Device_Name", deviceName);
        data.put("Version", versionCode);
        cleverTapEvent("I_AM_ALIVE", data);
    }

    public void sendQuitDialogAnalyticsEvent(boolean hasGameStarted) {
        String category = "Game Play- " + dashboardInfo.getCurrentGameInfo().getCurrentGameID();
        String action = (hasGameStarted ? "Post Question Pop Up" : "Pre First Question Pop Up");
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Back Button").setCategory(category)
                .setAction(action)
                .setLabel("Cancel")
                .setUserName(dashboardInfo.getUser().getUserName())
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        sendAnalyticsEvent(gaEventModel);
    }

    private void sendNetworkStatusEvent(boolean isConnected) {
        String status = (isConnected ? "Connected" : "Disconnected");
        Map<String, Object> data = new HashMap<>();
        User user = getUser();
        Question question = getLastQuestion();
        if (user != null) {
            data.put("username", user.getUserName());
            data.put("Phone", user.getPhoneNumber());
        }
        if (question != null) {
            data.put("question_id", question.getId());
            data.put("question_level", question.getQuestionNumber());
        }
        data.put("game_id", dashboardInfo.getCurrentGameInfo().getCurrentGameID());
        data.put("network_status", status);
        data.put("Event Time", getAnalytics().getTimeStampInHHMMSSIST());
        cleverTapEvent("Network_Staus_Event", data);
    }

    private Question getLastQuestion() {
        try {
            GameState gameState = (gameEngine.getGameState(gameInfo, getUser()));
            return (Question) gameState.getLastQuestionEvent().gameEvent();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;

    }

    public boolean isStreamModeCoachmarkToShown() {
        boolean isToShown = (AppUtils.isEmpty(dataRepository.getSharedPrefString(KEY_STREAM_MODE_COACHMARK, null)));
        if (isToShown) {
            dataRepository.setSharedPrefString(KEY_STREAM_MODE_COACHMARK, "shown");
        }
        return isToShown;
    }

    public void sendAnalyticsEvent(GaEventModel gaEventModel) {
        getAnalytics().logFireBaseEvent(gaEventModel);
    }
}
