package com.til.brainbaazi.viewmodel.payment;

import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;

/**
 * Created by prashant.rathore on 09/03/18.
 */

public interface PaymentNavigation {

    void proceedToHome();

    void openPaymentSuccessScreen(PaymentResponseMessage paymentResponseMessage, String source, int amount);

    void openPaymentFailureScreen(PaymentResponseMessage paymentResponseMessage, String source, User user,  String authToken);

    void openMobikwikWalletScreen(User user,  String authToken);

    void openMobikwikWalletSuccessScreen(User user, String authToken);

}
