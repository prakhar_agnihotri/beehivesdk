package com.til.brainbaazi.viewmodel.payment;

import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

/**
 * Created by prashant.rathore on 09/03/18.
 */

@AutoFactory
public class SuccessViewModel extends BaseScreenModel {

    private final PaymentRequests paymentRequests;
    private final PaymentNavigation paymentNavigation;

    private String amountToTransfer, source, message, orderId = "N/A";

    public static final String PARAM_AMOUNT = "amountToTransfer";
    public static final String PARAM_SOURCE = "source";
    public static final String PARAM_MESSAGE = "message";
    public static final String PARAM_ORDER_ID = "orderId";

    public SuccessViewModel(@Provided ConnectionManager connectionManager,
                            @Provided DataRepository dataRepository,
                            @Provided Analytics analytics,
                            @Provided PaymentRequests paymentRequests,
                            PaymentNavigation paymentNavigation) {
        super(connectionManager, dataRepository, analytics);
        this.paymentRequests = paymentRequests;
        this.paymentNavigation = paymentNavigation;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Bundle params = getParams();
        if(params != null) {
            this.amountToTransfer = params.getString(PARAM_AMOUNT);
            this.source = params.getString(PARAM_SOURCE);
            this.message = params.getString(PARAM_MESSAGE);
            this.orderId = params.getString(PARAM_ORDER_ID);
        }
    }

    public String getAmountToTransfer() {
        return amountToTransfer;
    }

    public String getSource() {
        return source;
    }

    public String getMessage() {
        return message;
    }

    public String getOrderId() {
        return orderId;
    }

    public PaymentNavigation getPaymentNavigation() {
        return paymentNavigation;
    }
}
