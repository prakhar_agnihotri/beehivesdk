package com.til.brainbaazi.viewmodel.balance;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.payment.PaymentRequest;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.entity.strings.BrainbaaziStrings;
import com.til.brainbaazi.entity.strings.PaymentStrings;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.BaseScreenModel;
import com.til.brainbaazi.viewmodel.payment.PaymentNavigation;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by saurabh.garg on 2/21/18.
 */
@AutoFactory
public class BalanceViewModel extends BaseScreenModel {

    public static final String PARAM_USER = "user";
    private final ActivityInteractor activityInteractor;
    private final DataRepository dataRepository;
    private final PaymentRequests paymentRequests;
    private final AppHelper appHelper;
    private final PaymentNavigation paymentNavigation;

    private BehaviorSubject<User> userInfoBehaviorSubject = BehaviorSubject.create();

    public BalanceViewModel(ActivityInteractor activityInteractor,
                            @Provided ConnectionManager connectionManager,
                            @Provided DataRepository dataRepository,
                            @Provided PaymentRequests paymentRequests,
                            @Provided AppHelper appHelper,
                            PaymentNavigation paymentNavigation,
                            @Provided Analytics analytics) {
        super(connectionManager,dataRepository,analytics);
        this.activityInteractor = activityInteractor;
        this.dataRepository = dataRepository;
        this.paymentRequests = paymentRequests;
        this.appHelper = appHelper;
        this.paymentNavigation = paymentNavigation;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        observeUserData();
    }

    private void observeUserData() {
        byte[] byteArray = getParams().getByteArray(PARAM_USER);
        User user = ParcelableUtil.unmarshall(byteArray, User.creator());
        userInfoBehaviorSubject.onNext(user);
    }

    public Observable<PaymentResponseMessage> startPaymentService(final User user, final String source) {
        return getConnectionManager().observeNetworkChanges().flatMap(new Function<ConnectionInfo, ObservableSource<PaymentResponseMessage>>() {
            @Override
            public ObservableSource<PaymentResponseMessage> apply(ConnectionInfo connectionInfo) throws Exception {
                if (connectionInfo.connectedToInternet()) {
                    return paymentRequests.transferAmount(dataRepository.getAuthToken(), createRequestData(user, source));
                } else {
                    PaymentResponseMessage build = PaymentResponseMessage.builder().build();
                    return Observable.just(build);
                }
            }
        });
    }

    private PaymentRequest createRequestData(User user, String source) {
        return PaymentRequest.builder().
                setAmt(String.valueOf(user.getUserBalance())).
                setMob(user.getPhoneNumber()).
                setIp(appHelper.getIpAddress()).
                setSrc(source).
                build();
    }

    public Observable<User> observeUserInfo() {
        return userInfoBehaviorSubject;
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    public void openAmazonPaymentScreen() {
//        appNavigation.openAmazonPaymentScreen();
    }

    public void navigateToSuccessScreen(PaymentResponseMessage paymentResponseMessage, String source, User user){
        if(paymentResponseMessage.isRequestSuccess()) {
            paymentNavigation.openPaymentSuccessScreen(paymentResponseMessage, source, user.getUserBalance());
        }else{
            paymentNavigation.openPaymentFailureScreen(paymentResponseMessage, source, user, dataRepository.getAuthToken());
        }
    }

    public void sendPaymentDialogAnalytics(User user, String mainEvent, String source, String action, String label) {
        String category = "Cashout " + source;
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent(mainEvent).setCategory(category)
                .setAction(action)
                .setLabel(label)
                .setUserName(user.getUserName())
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        getAnalytics().logFireBaseEvent(gaEventModel);
    }

    public void sendPaymentResponseAnalytics(PaymentResponseMessage paymentResponseMessage, User user, String source, String amountToTransfer) {
        if (paymentResponseMessage == null || !paymentResponseMessage.isRequestSuccess()) {
            String category = "Cashout " + source;
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Cashout Error Screen").setCategory(category)
                    .setAction("No Wallet ")
                    .setLabel("Go To Home")
                    .setUserName(user.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            getAnalytics().logFireBaseEvent(gaEventModel);
        } else {
            GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("WALLET PAGE").setCategory("Cashout")
                    .setAction(source)
                    .setLabel(amountToTransfer)
                    .setUserName(user.getUserName())
                    .setTimeStamp(AppUtils.getTimeStamp()).build();
            getAnalytics().logFireBaseEvent(gaEventModel);
        }
    }

}
