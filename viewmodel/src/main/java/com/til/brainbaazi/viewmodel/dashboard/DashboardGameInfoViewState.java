package com.til.brainbaazi.viewmodel.dashboard;

import com.google.auto.value.AutoValue;
import com.til.brainbaazi.entity.DashboardInfo;
import com.til.brainbaazi.entity.game.GameInfo;

import io.reactivex.annotations.Nullable;

/**
 * Created by prashant.rathore on 15/02/18.
 */
@AutoValue
public abstract class DashboardGameInfoViewState {

    public abstract int state();

    @Nullable
    public abstract DashboardInfo dashboardInfo();

    public static DashboardGameInfoViewState create(int state, DashboardInfo dashboardInfo) {
        return new AutoValue_DashboardGameInfoViewState(state,dashboardInfo);
    }

}
