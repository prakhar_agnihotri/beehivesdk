package com.til.brainbaazi.viewmodel.payment;

import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.payment.PaymentRequests;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.payment.PaymentResponseMessage;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.AppUtils;
import com.til.brainbaazi.utils.ParcelableUtil;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 09/03/18.
 */

@AutoFactory
public class MobikiwikWalletModel extends BaseScreenModel {

    public static final int STATE_LOADING = 0;
    public static final int STATE_REMOVE_LOADING = 1;
    public static final int STATE_SHOW_EMAIL_SCREEN = 2;
    public static final int STATE_SHOW_OTP_SCREEN = 3;
    public static final int STATE_SHOW_ERROR_TOAST = 4;

    private final Scheduler mainThreadScheduler;
    private final PaymentRequests paymentRequests;
    private final PaymentNavigation paymentNavigation;
    private CompositeDisposable compositeDisposable;
    private ActivityInteractor activityInteractor;

    private String authToken;
    private User user;
    private boolean isForOtpScreen;

    private BehaviorSubject<Integer> screenStateBehaviorSubject = BehaviorSubject.create();
    private BehaviorSubject<PaymentResponseMessage> paymentResponseBehaviorSubject = BehaviorSubject.create();

    public static final String AUTH_TOKEN = "auth_token";
    public static final String PARAM_USER = "user";

    public MobikiwikWalletModel(@Provided ConnectionManager connectionManager,
                                @Provided @MainThreadScheduler Scheduler mainThreadScheduler,
                                @Provided DataRepository dataRepository,
                                @Provided Analytics analytics,
                                @Provided PaymentRequests paymentRequests,
                                PaymentNavigation paymentNavigation,
                                @Provided ActivityInteractor activityInteractor) {
        super(connectionManager, dataRepository, analytics);
        this.mainThreadScheduler = mainThreadScheduler;
        this.paymentRequests = paymentRequests;
        this.paymentNavigation = paymentNavigation;
        this.activityInteractor = activityInteractor;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        compositeDisposable = new CompositeDisposable();
        Bundle params = getParams();
        if (params != null) {
            byte[] byteArray = getParams().getByteArray(PARAM_USER);
            this.user = ParcelableUtil.unmarshall(byteArray, User.creator());
            this.authToken = params.getString(AUTH_TOKEN);
        }
    }

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    public Observable<Integer> observeViewState() {
        return screenStateBehaviorSubject.observeOn(mainThreadScheduler);
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public String getPhoneNumber() {
        return user.getPhoneNumber();
    }

    public String getAuthToken() {
        return authToken;
    }

    public PaymentNavigation getPaymentNavigation() {
        return paymentNavigation;
    }

    public PaymentRequests getPaymentRequests() {
        return paymentRequests;
    }

    public void openSuccessScreen() {
        paymentNavigation.openMobikwikWalletSuccessScreen(user, authToken);
    }

    public boolean isForOtpScreen() {
        return isForOtpScreen;
    }

    public void setForOtpScreen(boolean forOtpScreen) {
        isForOtpScreen = forOtpScreen;
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    @Override
    public boolean handleBackPressed() {
        if (isForOtpScreen) {
            screenStateBehaviorSubject.onNext(STATE_SHOW_EMAIL_SCREEN);
            return true;
        } else {
            return super.handleBackPressed();
        }
    }

    public void generateOTP() {
        showProgressLoader();
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Cashout_Create_Wallet_Screen")
                .setCategory("Cashout_Create_Wallet Mobikwik")
                .setAction("Email")
                .setLabel("Submit")
                .setUserName("")
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        sendPaymentEvent(gaEventModel);
        DisposableObserver<PaymentResponseMessage> generateOTP = new DisposableObserver<PaymentResponseMessage>() {
            @Override
            public void onNext(PaymentResponseMessage response) {
                hideProgressDialog();
                screenStateBehaviorSubject.onNext(STATE_SHOW_OTP_SCREEN);
            }

            @Override
            public void onError(Throwable e) {
                hideProgressDialog();
                screenStateBehaviorSubject.onNext(STATE_SHOW_ERROR_TOAST);
            }

            @Override
            public void onComplete() {

            }
        };
        compositeDisposable.add(generateOTP);
        getPaymentRequests().generateMobikwikOTP(getAuthToken()).observeOn(mainThreadScheduler).subscribe(generateOTP);
    }

    public void regenerateOTP() {
        showProgressLoader();
        DisposableObserver<PaymentResponseMessage> regenerateOTP = new DisposableObserver<PaymentResponseMessage>() {
            @Override
            public void onNext(PaymentResponseMessage paymentResponseMessage) {
                hideProgressDialog();
                paymentResponseMessage = paymentResponseMessage.toBuilder().setRequestType(2).build();
                paymentResponseBehaviorSubject.onNext(paymentResponseMessage);
            }

            @Override
            public void onError(Throwable e) {
                hideProgressDialog();
                screenStateBehaviorSubject.onNext(STATE_SHOW_ERROR_TOAST);
            }

            @Override
            public void onComplete() {

            }
        };
        compositeDisposable.add(regenerateOTP);
        getPaymentRequests().generateMobikwikOTP(getAuthToken()).observeOn(mainThreadScheduler).subscribe(regenerateOTP);
    }

    public void verifyOTP(String email, String otp) {
        showProgressLoader();
        GaEventModel gaEventModel = GaEventModel.builder().setMainEvent("Cashout_Create_Wallet_Screen")
                .setCategory("Cashout_Create_Wallet Mobikwik")
                .setAction("OTP Screen")
                .setLabel("Submitted")
                .setUserName("")
                .setTimeStamp(AppUtils.getTimeStamp()).build();
        sendPaymentEvent(gaEventModel);
        DisposableObserver<PaymentResponseMessage> generateOTP = new DisposableObserver<PaymentResponseMessage>() {
            @Override
            public void onNext(PaymentResponseMessage paymentResponseMessage) {
                hideProgressDialog();
                paymentResponseMessage = paymentResponseMessage.toBuilder().setRequestType(1).build();
                paymentResponseBehaviorSubject.onNext(paymentResponseMessage);
            }

            @Override
            public void onError(Throwable e) {
                hideProgressDialog();
                screenStateBehaviorSubject.onNext(STATE_SHOW_ERROR_TOAST);
            }

            @Override
            public void onComplete() {

            }
        };
        compositeDisposable.add(generateOTP);
        getPaymentRequests().verifyMobikwikOTP(email, otp, getAuthToken()).observeOn(mainThreadScheduler).subscribe(generateOTP);
    }

    private void showProgressLoader() {
        screenStateBehaviorSubject.onNext(STATE_LOADING);
    }

    private void hideProgressDialog() {
        screenStateBehaviorSubject.onNext(STATE_REMOVE_LOADING);
    }

    public Observable<PaymentResponseMessage> observerPaymentResponse() {
        return paymentResponseBehaviorSubject;
    }

    private void sendPaymentEvent(GaEventModel gaEventModel) {
        getAnalytics().logFireBaseEvent(gaEventModel);
    }

}

