package com.til.brainbaazi.viewmodel.otp;

import android.os.Bundle;

import com.brainbaazi.component.Analytics;
import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.di.MainThreadScheduler;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.AppHelper;
import com.brainbaazi.component.repo.DataRepository;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.UserStatus;
import com.til.brainbaazi.entity.analytics.GaEventModel;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.entity.otp.PhoneNumber;
import com.til.brainbaazi.entity.user.UserRequestModel;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.utils.DisposableOnNextObserver;
import com.til.brainbaazi.viewmodel.BaseScreenModel;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by prashant.rathore on 15/02/18.
 */
@AutoFactory
public class OtpVerifyViewModel extends BaseScreenModel {

    public static final String PARAM_PHONE_NUMBER = "phoneNumber";

    public static final int STATE_INPUT = 0;
    public static final int STATE_VERIFING = 1;
    public static final int STATE_FAILED = 2;
    public static final int STATE_SUCCESS = 3;
    public static final int STATE_RESENT = 4;
    public static final int STATE_EXIT = 5;

    private final ActivityInteractor activityInteractor;
    private final OtpNavigation navigation;
    private final Scheduler mainThreadScheduler;
    private final AppHelper appHelper;

    private int currentState = -1;

    private final PhoneNumberAuthenticator phoneNumberAuthenticator;

    private BehaviorSubject<Integer> viewStatePublisher = BehaviorSubject.create();
    private BehaviorSubject<String> smsStateBehaviorSubject = BehaviorSubject.create();

    private DataRepository dataRepository;
    private PhoneNumber phoneNumber;

    public OtpVerifyViewModel(@Provided @MainThreadScheduler Scheduler mainThread
            , ActivityInteractor activityInteractor
            , @Provided ConnectionManager connectionManager
            , @Provided PhoneNumberAuthenticator phoneNumberAuthenticator
            , OtpNavigation navigation
            , @Provided DataRepository dataRepository
            , @Provided AppHelper appHelper
            , @Provided Analytics analytics) {

        super(connectionManager,dataRepository,analytics);
        this.activityInteractor = activityInteractor;
        this.phoneNumberAuthenticator = phoneNumberAuthenticator;
        this.navigation = navigation;
        this.dataRepository = dataRepository;
        this.mainThreadScheduler = mainThread;
        this.appHelper = appHelper;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.phoneNumber = getParams().getParcelable(PARAM_PHONE_NUMBER);
        updateState(STATE_INPUT);
        observerSmsEvents();
        observeUserStatus();
        observeOtpValidationEvents();
    }

    private void observerSmsEvents() {
        phoneNumberAuthenticator.observeSMSCodeCallbacks().observeOn(mainThreadScheduler).subscribe(new Consumer<String>() {
            @Override
            public void accept(String message) throws Exception {
                smsStateBehaviorSubject.onNext(message);
            }
        });
    }

    private void observeUserStatus() {
        Disposable subscribe = dataRepository.observeUserStatus()
                .observeOn(mainThreadScheduler).subscribe(new Consumer<UserStatus>() {
                    @Override
                    public void accept(UserStatus userStatus) throws Exception {
                        switch (userStatus.getStatus()) {
                            case UserStatus.STATUS_UNREGISTERED:
                                updateState(STATE_EXIT);
                                navigation.navigateToProfileScreen(phoneNumber);
                                break;
                            case UserStatus.STATUS_LOGGED_IN:
                                updateState(STATE_EXIT);
                                navigation.navigateToDashboard();
                                break;
                        }
                    }
                });
        addDisposable(subscribe);
    }

    private void observeOtpValidationEvents() {
        DisposableObserver<Response<UserStatus>> loginSuccessObserver = new DisposableOnNextObserver<Response<UserStatus>>() {
            @Override
            public void onNext(Response<UserStatus> response) {
                if (response.success()) {
                    viewStatePublisher.onNext(STATE_SUCCESS);
                    UserStatus userStatus = response.value();
                    switch (userStatus.getStatus()) {
                        case UserStatus.STATUS_UNREGISTERED:
                            updateState(STATE_EXIT);
                            navigation.navigateToProfileScreen(phoneNumber);
                            break;
                        case UserStatus.STATUS_LOGGED_IN:
                            updateState(STATE_EXIT);
                            navigation.navigateToDashboard();
                            break;
                    }
                } else {
                    viewStatePublisher.onNext(STATE_FAILED);
                }
            }

        };
        phoneNumberAuthenticator.observeOtpCallbacks().switchMap(new Function<OTPResponse, ObservableSource<Response<UserStatus>>>() {
            @Override
            public ObservableSource<Response<UserStatus>> apply(OTPResponse otpResponse) throws Exception {
                if(otpResponse.success) {
                    switch (otpResponse.event) {
                        case OTPResponse.EVENT_VERIFY:
                            viewStatePublisher.onNext(STATE_VERIFING);
                            UserRequestModel build1 = UserRequestModel.builder()
                                    .setMobileNumber(phoneNumber.getPhoneNumber())
                                    .setDeviceType("A")
                                    .setDeviceId(appHelper.getDeviceId())
                                    .setCountryCode(phoneNumber.getCountryModel().getDialCode())
                                    .build();
                            return dataRepository.loginUser(otpResponse.token, build1);
                        case OTPResponse.EVENT_SEND:
                            viewStatePublisher.onNext(STATE_RESENT);
                            return Observable.never();
                    }
                } else {
                    Response<UserStatus> build = Response.<UserStatus>builder().setSuccess(false).setException(otpResponse.exception).build();
                    return Observable.just(build);
                }
                return Observable.never();
            }
        }).observeOn(mainThreadScheduler).subscribe(loginSuccessObserver);

        addDisposable(loginSuccessObserver);

    }

    public void verifyOtp(String otpString) {
        updateState(STATE_VERIFING);
        phoneNumberAuthenticator.verifyOTP(otpString, phoneNumber);
    }

    public void resendOtp() {
        phoneNumberAuthenticator.generateOtp(phoneNumber, true);
    }

    public Observable<Integer> observeViewState() {
        return this.viewStatePublisher.distinctUntilChanged();
    }

    public Observable<String> observeSmsEvents() {
        return this.smsStateBehaviorSubject.distinctUntilChanged();
    }

    public ActivityInteractor getActivityInteractor() {
        return activityInteractor;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    private void updateState(int newState) {
        if (currentState != newState) {
            currentState = newState;
            this.viewStatePublisher.onNext(newState);
        }
    }

    public void openGenerateOTPScreen() {
        getActivityInteractor().performBackPress();
    }

    public void sendAnalyticsEvent(GaEventModel gaEventModel) {
        getAnalytics().logFireBaseEvent(gaEventModel);
    }

    public void sendAnalyticsScreen(GaEventModel gaEventModel, Bundle bundle) {
        getAnalytics().logFireBaseScreen(gaEventModel, bundle);
    }
}
