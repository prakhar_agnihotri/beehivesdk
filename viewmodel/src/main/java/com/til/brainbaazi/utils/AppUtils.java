package com.til.brainbaazi.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by saurabh.garg on 2/15/18.
 */

public class AppUtils {

    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    public static boolean isEmpty(String empty) {
        return (empty == null || empty.isEmpty());
    }

    public static String getTimeStamp() {
        return new Timestamp(System.currentTimeMillis()).toString();
    }

    public static String secondsToDateFormat(String seconds) {
        if (isEmpty(seconds)) {
            return null;
        }
        return millsToDateFormat(Long.parseLong(seconds) * 1000);
    }

    public static String millsToDateFormat(long mills) {
        Date date = new Date(mills);
        DateFormat formatter = new SimpleDateFormat("hh:mm a");
        formatter.setTimeZone(TimeZone.getDefault());
        return formatter.format(date); //note that it will give you the time in GMT+0
    }

    public static String Epoch2DateString(long epochSeconds, String formatString) {
        Date updatedate = new Date(epochSeconds * 1000);
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        return format.format(updatedate);
    }

    public static String formatWrap(long value) {
        String formatText = format(value);
        return (!isEmpty(formatText) ? formatText : "0");
    }

    public static String coolFormatWrap(long value) {
        String formatText = coolFormat(value);
        return (!isEmpty(formatText) ? formatText : "0");
    }

    public static String format(long value) {
        if (value > 0) {
            //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
            if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
            if (value < 0) return "-" + format(-value);
            if (value < 1000) return Long.toString(value); //deal with easy case

            Map.Entry<Long, String> e = suffixes.floorEntry(value);
            Long divideBy = e.getKey();
            String suffix = e.getValue();

            long truncated = value / (divideBy / 10); //the number part of the output times 10
            boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
            return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
        }
        return null;
    }

    public static String coolFormat(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.2f %c", count / Math.pow(1000, exp), "kMGTPE".charAt(exp - 1));
    }

    public static Character[] toCharacterArray(String s) {
        if (isEmpty(s)) {
            return null;
        }
        int len = s.length();
        Character[] array = new Character[len];
        for (int i = 0; i < len; i++) {
            array[i] = new Character(s.charAt(i));
        }

        return array;
    }

    public static boolean isMatches(String msg, String matcherString) {
        if (!isEmpty(matcherString) && !isEmpty(msg)) {
            Pattern p = Pattern.compile("(?<=\\|)(" + msg + ")(?=\\|)", Pattern.CASE_INSENSITIVE);
            Matcher matcher = p.matcher(matcherString);
            return matcher.find();
        }
        return false;
    }

    public static boolean isUserNameValid(String userName) {
        if (!isEmpty(userName) && userName.length() >= 3 && userName.length() <= 15) {
            Pattern p = Pattern.compile("^(?=.*\\w)(?=.*[a-z])[a-z0-9_/.]{1,15}$");
            Matcher matcher = p.matcher(userName);
            return matcher.matches();
        }
        return false;
    }

    public static boolean hasUpperCase(String str) {
        String lowercaseString = str.toLowerCase(Locale.ENGLISH);
        return !str.equals(lowercaseString);
    }

    public static boolean isAmountZero(String amount) {
        return (amount != null && (isEmpty(amount) || "--".equalsIgnoreCase(amount) || "0".equalsIgnoreCase(amount)));
    }

    public static int getStartIndex(String string, String substring) {
        if (string.contains(substring))
            return string.indexOf(substring);
        return -1;
    }

    public static int getEndIndex(String string, String substring) {
        if (string.contains(substring))
            return string.indexOf(substring) + substring.length();
        return 0;
    }
}
