package com.til.brainbaazi.utils;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by prashant.rathore on 24/02/18.
 */

public abstract class DisposableOnNextObserver<T> extends DisposableObserver<T> {

    @Override
    final public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    final public void onComplete() {

    }
}
