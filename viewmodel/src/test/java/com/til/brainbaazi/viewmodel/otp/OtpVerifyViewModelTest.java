package com.til.brainbaazi.viewmodel.otp;

import android.os.Bundle;

import com.brainbaazi.component.navigation.AppNavigation;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.brainbaazi.component.network.ConnectionManager;
import com.til.brainbaazi.entity.otp.OTPInteractor;
import com.til.brainbaazi.entity.otp.OTPResponse;
import com.til.brainbaazi.viewmodel.ObjectObserver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by prashant.rathore on 15/02/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class OtpVerifyViewModelTest {

    @Mock
    ConnectionManager connectionManager;
    @Mock
    ConnectionInfo connectionInfoConnected = mock(ConnectionInfo.class);

    @Mock
    ConnectionInfo connectionInfoDisconneced = mock(ConnectionInfo.class);

    @Mock
    OTPInteractor otpInteractor;

    @Mock
    AppNavigation appNavigation;

    private PublishSubject<OTPResponse> otpResponsePublisher = PublishSubject.create();
    private OtpVerifyViewModel otpVerifyViewModel;

    @Before
    public void setup() {
//        when(connectionInfoConnected.isConnectedToInternet()).thenReturn(true);
//        when(connectionInfoConnected.isConnectedToInternet()).thenReturn(false);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfoConnected));
        when(otpInteractor.observeOtpValidation()).thenReturn(otpResponsePublisher);
        this.otpVerifyViewModel = new OtpVerifyViewModel(connectionManager, otpInteractor, appNavigation);
    }


    @Test
    public void testLaunchOnline() throws Exception {
        this.otpVerifyViewModel.onCreate();

        ObjectObserver<Integer> viewStateObserver = new ObjectObserver<>();
        this.otpVerifyViewModel.observeViewState().subscribe(viewStateObserver);

        this.otpVerifyViewModel.restoreState(mock(Bundle.class));
        this.otpVerifyViewModel.willShow();
        this.otpVerifyViewModel.resume();


        destroyScreen();
    }

    @Test
    public void testLaunchOffline() throws Exception {
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfoDisconneced));
        launchScreen();
        destroyScreen();
    }


    @Test
    public void testVerifyOtpSend() throws Exception {
        launchScreen();
        String otpString = "testOtp";
        otpVerifyViewModel.verifyOtp(otpString);
        ObjectObserver<Integer> objectObserver = new ObjectObserver<>();
        otpVerifyViewModel.observeViewState().subscribeOn(Schedulers.trampoline()).observeOn(Schedulers.trampoline()).subscribe(objectObserver);
        assertEquals(OtpVerifyViewModel.STATE_VERIFING, (int) objectObserver.getObject());

        otpResponsePublisher.onNext(OTPResponse.requestSentFailed(new NullPointerException()));
        assertEquals(OtpVerifyViewModel.STATE_FAILED, (int) objectObserver.getObject());

        otpResponsePublisher.onNext(OTPResponse.requestSentSuccess());
        assertEquals(OtpVerifyViewModel.STATE_VERIFING, (int) objectObserver.getObject());

        otpResponsePublisher.onNext(OTPResponse.verificationSuccess("Test token"));
        assertEquals(OtpVerifyViewModel.STATE_SUCCESS, (int) objectObserver.getObject());


    }

    private void launchScreen() {
        this.otpVerifyViewModel.onCreate();
        this.otpVerifyViewModel.restoreState(mock(Bundle.class));
        this.otpVerifyViewModel.willShow();
        this.otpVerifyViewModel.resume();
    }

    private void destroyScreen() {
        this.otpVerifyViewModel.pause();
        this.otpVerifyViewModel.willHide();
        this.otpVerifyViewModel.onDestroy();
    }

    @After
    public void tearDown() {

    }

}