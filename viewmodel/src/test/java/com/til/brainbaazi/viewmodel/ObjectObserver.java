package com.til.brainbaazi.viewmodel;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by prashant.rathore on 15/02/18.
 */

public class ObjectObserver<T> extends DisposableObserver<T> {

    private T object;
    private Throwable error;
    private boolean complete;

    @Override
    public void onNext(T o) {
        this.object = o;
    }

    @Override
    public void onError(Throwable e) {
        this.error = e;
    }

    @Override
    public void onComplete() {
        this.complete = true;
    }

    public T getObject() {
        return object;
    }

    public boolean isComplete() {
        return complete;
    }

    public Throwable getError() {
        return error;
    }
}
