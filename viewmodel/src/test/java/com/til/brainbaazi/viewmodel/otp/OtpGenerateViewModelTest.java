package com.til.brainbaazi.viewmodel.otp;

import android.os.Bundle;

import com.brainbaazi.component.navigation.AppNavigation;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.brainbaazi.component.network.ConnectionManager;
import com.til.brainbaazi.entity.otp.OTPInteractor;
import com.til.brainbaazi.viewmodel.ObjectObserver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by prashant.rathore on 15/02/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class OtpGenerateViewModelTest {

    @Mock
    ConnectionManager connectionManager;
    @Mock
    AppNavigation appNavigation;
    @Mock
    OTPInteractor otpInteractor;
    @Mock
    OtpNavigation otpNavigation;

    private OtpGenerateViewModel otpGenerateViewModel;

    @Before
    public void setup() {
        this.otpGenerateViewModel = new OtpGenerateViewModel(connectionManager,Schedulers.trampoline(),  appNavigation, otpInteractor,otpNavigation);
    }

    @Test
    public void testOTPScreenLaunch() {

        ConnectionInfo connectionInfoConnected = mock(ConnectionInfo.class);
//        when(connectionInfoConnected.isConnectedToInternet()).thenReturn(true);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfoConnected));

        this.otpGenerateViewModel.onCreate();
        this.otpGenerateViewModel.restoreState(null);

        ObjectObserver<Integer> viewStateObserver = new ObjectObserver<>();
        this.otpGenerateViewModel.observeViewState().subscribe(viewStateObserver);
        assertEquals(OtpGenerateViewModel.STATE_INPUT, (int) viewStateObserver.getObject());
        viewStateObserver.dispose();

        this.otpGenerateViewModel.willShow();
        this.otpGenerateViewModel.resume();
        this.otpGenerateViewModel.pause();
        this.otpGenerateViewModel.saveState(mock(Bundle.class));
        this.otpGenerateViewModel.willHide();
        this.otpGenerateViewModel.onDestroy();


    }

    @Test
    public void testOTPSubmitIfNetworkOff() {

        ConnectionInfo connectionInfoConnected = mock(ConnectionInfo.class);
        when(connectionInfoConnected.connectedToInternet()).thenReturn(false);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfoConnected));

        this.otpGenerateViewModel.onCreate();
        this.otpGenerateViewModel.restoreState(null);
        this.otpGenerateViewModel.willShow();
        this.otpGenerateViewModel.resume();

        String phoneNumber = "+989181881882";

//        when(otpInteractor.generateOtp(anyString())).thenReturn(Observable.<Boolean>never());
        ObjectObserver<Integer> viewStateObserver = new ObjectObserver<>();
        otpGenerateViewModel.observeViewState().subscribe(viewStateObserver);
        otpGenerateViewModel.generateOtp(phoneNumber);
        assertEquals(OtpGenerateViewModel.STATE_FAILED, (int) viewStateObserver.getObject());

    }


    @Test
    public void testOTPSubmitIfNetworkONFailed() {

        prepareMockWithNetworkStatus(true);
        String phoneNumber = "+989181881882";

        when(otpInteractor.generateOtp(anyString())).thenReturn(Observable.just(false));
        ObjectObserver<Integer> viewStateObserver = new ObjectObserver<>();
        otpGenerateViewModel.observeViewState().subscribe(viewStateObserver);
        otpGenerateViewModel.generateOtp(phoneNumber);
        assertEquals(OtpGenerateViewModel.STATE_FAILED, (int) viewStateObserver.getObject());
    }

    @Test
    public void testOTPSubmitIfNetworkONSuccess() {

        prepareMockWithNetworkStatus(true);
        String phoneNumber = "+989181881882";

        when(otpInteractor.generateOtp(anyString())).thenReturn(Observable.just(true));
        ObjectObserver<Integer> viewStateObserver = new ObjectObserver<>();
        otpGenerateViewModel.observeViewState().subscribe(viewStateObserver);
        otpGenerateViewModel.generateOtp(phoneNumber);
        assertEquals(OtpGenerateViewModel.STATE_SUCCESS, (int) viewStateObserver.getObject());

        verify(otpNavigation).navigateToOTPEnterScreen(phoneNumber);

    }

    @Test
    public void testOTPSubmitInvalidInput() {

        prepareMockWithNetworkStatus(true);
        String phoneNumber = "+989181881882";

        when(otpInteractor.generateOtp(anyString())).thenReturn(Observable.<Boolean>never());
        ObjectObserver<Integer> viewStateObserver = new ObjectObserver<>();
        otpGenerateViewModel.observeViewState().subscribe(viewStateObserver);
        otpGenerateViewModel.generateOtp(phoneNumber);
        assertEquals(OtpGenerateViewModel.STATE_REQUESTING, (int) viewStateObserver.getObject());

    }



    private void prepareMockWithNetworkStatus(boolean connected) {
        ConnectionInfo connectionInfoConnected = mock(ConnectionInfo.class);
        when(connectionInfoConnected.connectedToInternet()).thenReturn(connected);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfoConnected));

        this.otpGenerateViewModel.onCreate();
        this.otpGenerateViewModel.restoreState(null);
        this.otpGenerateViewModel.willShow();
        this.otpGenerateViewModel.resume();
    }


    @After
    public void tearDown() {

    }

}