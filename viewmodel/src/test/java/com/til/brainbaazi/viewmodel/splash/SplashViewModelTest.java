package com.til.brainbaazi.viewmodel.splash;

import android.os.Bundle;

import com.brainbaazi.component.navigation.AppNavigation;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.brainbaazi.component.network.ConnectionManager;
import com.til.brainbaazi.interactor.CheckAppStatus;
import com.til.brainbaazi.entity.AppStatus;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by prashant.rathore on 13/02/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class SplashViewModelTest {


    @Mock
    ConnectionManager connectionManager;

    @Mock
    ConnectionInfo connectionInfo;

    @Mock
    CheckAppStatus checkAppStatus;

    @Mock
    AppNavigation appNavigation;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private SplashViewModel splashViewModel;


    @Before
    public void setUp() throws Exception {
        splashViewModel = new SplashViewModel(connectionManager, Schedulers.trampoline(), checkAppStatus, appNavigation);
    }

    @Test
    public void testDefaultLaunchWithNetworkOff() {
//        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>never().subscribeOn(Schedulers.trampoline()));

        when(connectionInfo.connectedToInternet()).thenReturn(false);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfo).subscribeOn(Schedulers.trampoline()));
        splashViewModel.onCreate();
        splashViewModel.willShow();

        ViewStateObserver viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_NETWORK_ERROR,viewStateObserver.value);

        viewStateObserver.dispose();
    }

    @Test
    public void testNetworkOnButStatusFailed() {
//        AppStatus appStatus = mock(AppStatus.class);
//        when(appStatus.updateAvailable).thenReturn(false);
        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>error(new NullPointerException()));
        when(connectionInfo.connectedToInternet()).thenReturn(true);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfo).subscribeOn(Schedulers.trampoline()));
        splashViewModel.onCreate();

        ViewStateObserver viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_SERVER_ERROR,viewStateObserver.value);

        viewStateObserver.dispose();
    }


    @Test
    public void testNetworkOnAndStatusUpdate() {
        AppStatus appStatus = mock(AppStatus.class);
        when(appStatus.isUpdateAvailable()).thenReturn(true);
        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>just(appStatus));
        when(connectionInfo.connectedToInternet()).thenReturn(true);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfo).subscribeOn(Schedulers.trampoline()));
        splashViewModel.onCreate();
        splashViewModel.willShow();

        ViewStateObserver viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_UPDATE,viewStateObserver.value);

        viewStateObserver.dispose();

        splashViewModel.updateButtonClicked();
    }


    @Test
    public void testNetworkOffAndThenSwitchOn() {
        PublishSubject<ConnectionInfo> networInfoSubject = PublishSubject.create();

        AppStatus appStatus = mock(AppStatus.class);
        when(appStatus.isUpdateAvailable()).thenReturn(true);
        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>just(appStatus));
        when(connectionInfo.connectedToInternet()).thenReturn(false);
        when(connectionManager.observeNetworkChanges()).thenReturn(networInfoSubject.subscribeOn(Schedulers.trampoline()));
        splashViewModel.onCreate();
        splashViewModel.willShow();
        splashViewModel.resume();
        networInfoSubject.onNext(connectionInfo);

        ViewStateObserver viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_NETWORK_ERROR,viewStateObserver.value);

        viewStateObserver.dispose();

        ConnectionInfo mockedConnectionInfo = mock(ConnectionInfo.class);
        when(mockedConnectionInfo.connectedToInternet()).thenReturn(true);
        networInfoSubject.onNext(mockedConnectionInfo);

        viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_UPDATE,viewStateObserver.value);

        viewStateObserver.dispose();
    }

    @Test
    public void testNetworkOnAndThenSwitchOff() {
        PublishSubject<ConnectionInfo> networInfoSubject = PublishSubject.create();

        AppStatus appStatus = mock(AppStatus.class);
        when(appStatus.isUpdateAvailable()).thenReturn(true);
        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>just(appStatus));
        when(connectionInfo.connectedToInternet()).thenReturn(true);
        when(connectionManager.observeNetworkChanges()).thenReturn(networInfoSubject.subscribeOn(Schedulers.trampoline()));
        splashViewModel.onCreate();
        splashViewModel.willShow();
        splashViewModel.resume();
        networInfoSubject.onNext(connectionInfo);

        ViewStateObserver viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_UPDATE,viewStateObserver.value);

        viewStateObserver.dispose();

        ConnectionInfo mockedConnectionInfo = mock(ConnectionInfo.class);
        when(mockedConnectionInfo.connectedToInternet()).thenReturn(false);
        networInfoSubject.onNext(mockedConnectionInfo);

        viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_UPDATE,viewStateObserver.value);


        viewStateObserver.dispose();
    }


    @Test
    public void testAllLifecycleMethods() {
        AppStatus appStatus = mock(AppStatus.class);
        when(appStatus.isUpdateAvailable()).thenReturn(true);
        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>just(appStatus));
        when(connectionInfo.connectedToInternet()).thenReturn(true);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfo).subscribeOn(Schedulers.trampoline()));

        splashViewModel.onCreate();
        splashViewModel.restoreState(null);
        splashViewModel.willShow();
        splashViewModel.resume();
        splashViewModel.pause();
        splashViewModel.saveState(Mockito.mock(Bundle.class));
        splashViewModel.willHide();
        splashViewModel.onDestroy();

        expectedException.expect(NullPointerException.class);
        splashViewModel.observeViewState().subscribe();
    }


    @Test
    public void testAllMethods() {
        AppStatus appStatus = mock(AppStatus.class);
        when(appStatus.isUpdateAvailable()).thenReturn(true);
        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>just(appStatus));
        when(connectionInfo.connectedToInternet()).thenReturn(true);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfo).subscribeOn(Schedulers.trampoline()));

        splashViewModel.onCreate();
        splashViewModel.restoreState(null);
        splashViewModel.willShow();
        splashViewModel.resume();
        splashViewModel.pause();
        splashViewModel.willHide();
        splashViewModel.saveState(Mockito.mock(Bundle.class));
        splashViewModel.onDestroy();


        expectedException.expect(NullPointerException.class);
        splashViewModel.observeViewState().subscribe();
    }

    @Test
    public void testNetworkOnUnregistered() {
        AppStatus appStatus = mock(AppStatus.class);
        when(appStatus.isUpdateAvailable()).thenReturn(false);
        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>just(appStatus));
        when(connectionInfo.connectedToInternet()).thenReturn(true);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfo).subscribeOn(Schedulers.trampoline()));
        splashViewModel.onCreate();

        ViewStateObserver viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_GET_STARTED,viewStateObserver.value);
        viewStateObserver.dispose();
        splashViewModel.getStartedButtonClicked();
    }


    @Test
    public void testNetworkOnRegistered() {
        AppStatus appStatus = mock(AppStatus.class);
        when(appStatus.isUpdateAvailable()).thenReturn(false);
        when(appStatus.isRegistered()).thenReturn(true);


        when(checkAppStatus.execute()).thenReturn(Observable.<AppStatus>just(appStatus));
        when(connectionInfo.connectedToInternet()).thenReturn(true);
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(connectionInfo).subscribeOn(Schedulers.trampoline()));

        splashViewModel.onCreate();

        ViewStateObserver viewStateObserver = new ViewStateObserver();
        splashViewModel.observeViewState().subscribe(viewStateObserver);

        Assert.assertEquals(SplashViewModel.STATE_PROCEED_TO_DASHBOARD,viewStateObserver.value);
        verify(appNavigation).navigateToDashboard();

        viewStateObserver.dispose();
    }


    @After
    public void tearDown() throws Exception {
    }


    static class ViewStateObserver extends DisposableObserver<Integer> {

        int value = -1;

        @Override
        public void onNext(Integer integer) {
            value = integer;
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }

}