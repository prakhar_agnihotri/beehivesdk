package com.til.brainbaazi.viewmodel.dashboard;

import android.os.Bundle;

import com.brainbaazi.component.auth.PhoneNumberAuthenticator;
import com.brainbaazi.component.navigation.AppNavigation;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.brainbaazi.component.network.ConnectionManager;
import com.brainbaazi.component.repo.DataRepository;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.Response;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.interactor.activity.ActivityInteractor;
import com.til.brainbaazi.viewmodel.ObjectObserver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by prashant.rathore on 16/02/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class DashboardViewModelTest {

    @Mock
    ConnectionManager connectionManager;
    @Mock
    DashboardNavigation appNavigation;
    @Mock
    DataRepository dataRepository;
    @Mock
    ActivityInteractor activityInteractor;
    @Mock
    PhoneNumberAuthenticator phoneNumberAuthenticator;

    private DashboardViewModel dashboardViewModel;

    @Before
    public void setUp() throws Exception {
        ConnectionInfo wifi = ConnectionInfo.builder().setType(ConnectionManager.TYPE_WIFI).setConnectedToInternet(true).setName("WIFI").build();
        when(connectionManager.observeNetworkChanges()).thenReturn(Observable.just(wifi));
        when(dataRepository.loadDashboardInfo()).thenReturn(Observable.<Response<GameInfo>>never());
        when(dataRepository.loadUserInfo()).thenReturn(Observable.<Response<User>>never());
        this.dashboardViewModel = new DashboardViewModel(activityInteractor, connectionManager, appNavigation, dataRepository, phoneNumberAuthenticator);
    }

    @Test
    public void testLifecycle() throws Exception {
        this.dashboardViewModel.onCreate();
        this.dashboardViewModel.restoreState(mock(Bundle.class));
        this.dashboardViewModel.willShow();
        this.dashboardViewModel.resume();
        this.dashboardViewModel.pause();
        this.dashboardViewModel.saveState(mock(Bundle.class));
        this.dashboardViewModel.willHide();
        this.dashboardViewModel.onDestroy();
    }

    @Test
    public void testUserInfo() throws Exception {
        Response<User> userInfo = Response.<User>builder().setSuccess(false).setValue(null).setException(new RuntimeException()).build();
        PublishSubject<Response<User>> responsePublisher = PublishSubject.create();
        when(dataRepository.loadUserInfo()).thenReturn(responsePublisher);
        this.dashboardViewModel.onCreate();
        this.dashboardViewModel.restoreState(mock(Bundle.class));
        this.dashboardViewModel.willShow();
        this.dashboardViewModel.resume();

        ObjectObserver<DashboardUserViewState> objectObserver = new ObjectObserver<>();
        this.dashboardViewModel.observeUserInfo().observeOn(Schedulers.trampoline()).subscribe(objectObserver);
        assertEquals(DashboardViewModel.VIEW_STATE_LOADING, objectObserver.getObject().state());

        responsePublisher.onNext(userInfo);
        assertEquals(DashboardViewModel.VIEW_STATE_FAILED, objectObserver.getObject().state());


        User user = User.builder().setUserName("Test User name").setUserImgUrl("image url").setUserBalance("999").setUserLifeTimeBalance("10000").setLives(5).setPhoneNumber("123456789").setWeeklyRank("1").build();
        userInfo = Response.<User>builder().setSuccess(true).setValue(user).setException(new RuntimeException()).build();
        responsePublisher.onNext(userInfo);
        assertEquals(DashboardViewModel.VIEW_STATE_SUCCESS, objectObserver.getObject().state());
        assertNotNull(objectObserver.getObject().user());
        objectObserver.dispose();
    }

    @Test
    public void testGameInfo() throws Exception {
        Response<GameInfo> gameInfo = Response.<GameInfo>builder().setSuccess(false).setValue(null).setException(new RuntimeException()).build();
        PublishSubject<Response<GameInfo>> responsePublisher = PublishSubject.create();
        when(dataRepository.loadGameInfo()).thenReturn(responsePublisher);
        this.dashboardViewModel.onCreate();
        this.dashboardViewModel.restoreState(mock(Bundle.class));
        this.dashboardViewModel.willShow();
        this.dashboardViewModel.resume();

        ObjectObserver<DashboardGameInfoViewState> objectObserver = new ObjectObserver<>();
        this.dashboardViewModel.observeDashBoardState().observeOn(Schedulers.trampoline()).subscribe(objectObserver);
        assertEquals(DashboardViewModel.VIEW_STATE_LOADING, objectObserver.getObject().state());

        responsePublisher.onNext(gameInfo);
        assertEquals(DashboardViewModel.VIEW_STATE_FAILED, objectObserver.getObject().state());


        GameInfo game = GameInfo.builder()
                .setActive_info("Active Info")
                .setCurrentGameID(1L)
                .setGamePrize(0)
                .setGoneLive(true)
                .setNextGameTime("Next Game Time ")
                .setServerTimestamp(1L)
                .setTimeStamp(1L)
                .build();
        gameInfo = Response.<GameInfo>builder().setSuccess(true).setValue(game).setException(null).build();
        responsePublisher.onNext(gameInfo);
        assertEquals(DashboardViewModel.VIEW_STATE_SUCCESS, objectObserver.getObject().state());
        objectObserver.dispose();
    }

    @After
    public void tearDown() throws Exception {
    }

}