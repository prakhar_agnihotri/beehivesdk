package com.til.brainbaazi.network.socket;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.brainbaazi.component.network.ConnectionManager;
import com.neovisionaries.ws.client.ThreadType;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketExtension;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;
import com.neovisionaries.ws.client.WebSocketListener;
import com.neovisionaries.ws.client.WebSocketState;
import com.til.brainbaazi.entity.ConnectionInfo;
import com.til.brainbaazi.entity.User;
import com.til.brainbaazi.entity.game.GameEventUtils;
import com.til.brainbaazi.entity.game.GameInfo;
import com.til.brainbaazi.entity.game.event.InputEvent;
import com.til.brainbaazi.entity.game.event.LiveGameInfo;
import com.til.brainbaazi.entity.game.event.SocketConnectError;
import com.til.brainbaazi.entity.game.event.SocketDisconnected;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by arpit.toshniwal on 25/02/18.
 */

public class NVSocketClient implements WebSocketListener {

    private final ConnectionManager connectionManager;
    private PublishSubject<String> eventPublisher = PublishSubject.create();
    private PublishSubject<InputEvent> liveInfoPublisher = PublishSubject.create();
    private PublishSubject<InputEvent> otherEventPublisher = PublishSubject.create();

    private Observable<InputEvent> inputEventObservable;
    private WebSocket webSocket;
    private Context mContext;
    private WebSocketFactory factory;
    private String mUrl;
    private String authToken;
    private User user;
    private GameInfo gameInfo;
    private boolean forcedDisconnection;
    private boolean isDisconnected;
    private boolean isConnectedToInternet;
    private Handler handler;

    private void initObservables() {
        inputEventObservable = eventPublisher.subscribeOn(Schedulers.newThread()).flatMap(new Function<String, Observable<InputEvent>>() {
            @Override
            public Observable<InputEvent> apply(String value) throws Exception {
                List<InputEvent> inputEvents = parseDataMessage(value);
                if (inputEvents != null && inputEvents.size() > 0) {
                    return Observable.fromIterable(inputEvents);
                } else {
                    return Observable.never();
                }
            }
        }).filter(new Predicate<InputEvent>() {
            @Override
            public boolean test(InputEvent inputEvent) throws Exception {
                boolean b = inputEvent.type() == InputEvent.TYPE_LIVE_INFO;
                if (b) {
                    LiveGameInfo gameEvent = (LiveGameInfo) inputEvent.gameEvent();
                    if (gameEvent.getConcurrentUserCount() > 0) {
                        liveInfoPublisher.onNext(inputEvent);
                    }
                }
                return !b;
            }
        });
    }

    public Observable<InputEvent> observeSocketEvents() {
        return this.inputEventObservable.mergeWith(liveInfoPublisher.throttleLast(1, TimeUnit.SECONDS)).mergeWith(otherEventPublisher);
    }

    public NVSocketClient(Context context, String url, ConnectionManager connectionManager) {
        this.mContext = context.getApplicationContext();
        this.mUrl = url;
        this.factory = new WebSocketFactory();
        this.eventPublisher = PublishSubject.create();
        this.connectionManager = connectionManager;
        handler = new Handler(Looper.getMainLooper());
        this.factory = new WebSocketFactory();
        this.eventPublisher = PublishSubject.create();
        observeNetworkChange();
        initObservables();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                createDummyLifeInfoEvent();
//            }
//        }).start();
    }

    private void createDummyLifeInfoEvent() {
        boolean flag = true;
        int multiplier = 10;
        while (flag) {
            LiveGameInfo liveGameInfo = LiveGameInfo.builder().
                    setConcurrentUserCount(multiplier).
                    build();
            multiplier = (multiplier + 1) % Integer.MAX_VALUE;
            liveInfoPublisher.onNext(GameEventUtils.getInputEvent(InputEvent.TYPE_LIVE_INFO, liveGameInfo, user, null, true, true));
        }
    }

    public boolean isConnectionOpen() {
        return webSocket != null && webSocket.isOpen();
    }

    public void disconnect() {
        Log.d("Arpit", "ordered to disconnect: ");
        if (webSocket != null) {
            forcedDisconnection = true;
            webSocket.disconnect();
        }
    }

    public void connect(String authToken, GameInfo gameInfo, User user) {
        this.gameInfo = gameInfo;
        this.user = user;
        if (this.authToken == null || !this.authToken.equals(authToken)) {
            this.authToken = authToken;
            disconnect();
            connect();
        }
    }

    public void connect() {
        try {
            Log.d("Arpit", "asking to connect: ");
            forcedDisconnection = false;
            if (webSocket == null || webSocket.getState() == WebSocketState.CLOSED) {
                webSocket = factory.createSocket(mUrl);
                webSocket.setMissingCloseFrameAllowed(false);
                webSocket.addListener(this);
                init();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendUpStreamMessage(String message) {
        if (webSocket != null)
            webSocket.sendText(message);
    }

    private void init() {
        webSocket.setMissingCloseFrameAllowed(false);
        webSocket.addHeader("authtoken", authToken);
        webSocket.addExtension(WebSocketExtension.PERMESSAGE_DEFLATE);
        webSocket.setPingInterval(1000);
        webSocket.connectAsynchronously();
//        test();
    }

    private void test() {
        String messages[] = {
                //[{"a":"a","p":{"qid":175,"luc":"0","opts":[{"cnt":0,"oid":1002},{"cnt":0,"oid":1003},{"cnt":0,"oid":1001}],"crt":1001},"e":"a","c":1}]
                "[{\"a\":\"a\",\"p\":{\"qid\":175,\"luc\":\"0\",\"opts\":[{\"cnt\":0,\"oid\":1002},{\"cnt\":0,\"oid\":1003},{\"cnt\":0,\"oid\":1001}],\"crt\":1001},\"e\":\"a\",\"c\":1}]",

                //[{"a":"a","e":"q","p":{"qt":"Which of these Indian organisations launches satellites?","qid":426,"lst":true,"gid":1399,"sqn":3,"ans":[{"oid":1756,"val":"SpaceX","pos":1},{"oid":1754,"val":"ISRO","pos":2},{"oid":1755,"val":"NASA","pos":3}]},"c":1}]
                "[{\"a\":\"a\",\"e\":\"q\",\"p\":{\"qt\":\"Which of these Indian organisations launches satellites?\",\"qid\":426,\"lst\":true,\"gid\":1399,\"sqn\":3,\"ans\":[{\"oid\":1756,\"val\":\"SpaceX\",\"pos\":1},{\"oid\":1754,\"val\":\"ISRO\",\"pos\":2},{\"oid\":1755,\"val\":\"NASA\",\"pos\":3}]},\"c\":1}]",


                //[{"a":"a","p":{"qid":426,"luc":"0","opts":[{"cnt":0,"oid":1756},{"cnt":0,"oid":1754},{"cnt":0,"oid":1755}],"crt":1754},"e":"a","c":1}]
                "[{\"a\":\"a\",\"p\":{\"qid\":426,\"luc\":\"0\",\"opts\":[{\"cnt\":0,\"oid\":1756},{\"cnt\":0,\"oid\":1754},{\"cnt\":0,\"oid\":1755}],\"crt\":1754},\"e\":\"a\",\"c\":1}]",

                //[{"a":"a","e":"w","p":{"pmt":5,"wc":0,"l":"","w":[]},"c":1}]
                "[{\"a\":\"a\",\"e\":\"w\",\"p\":{\"pmt\":5,\"wc\":0,\"l\":\"\",\"w\":[]},\"c\":1}]",

                ///[{"a":"a","p":{"a":0,"u":"http://slike-live-s3.akamaized.net/b/bbliveslike/playlist.m3u8","t":1519577073,"g":"1399"},"e":"l","c":1}]
                "[{\"a\":\"a\",\"p\":{\"a\":0,\"u\":\"http://slike-live-s3.akamaized.net/b/bbliveslike/playlist.m3u8\",\"t\":1519577073,\"g\":\"1399\"},\"e\":\"l\",\"c\":1}]",

                //[{"a":"a","e":"q","p":{"qt":"Mt Everest is the highest mountain of which country?","qid":175,"lst":false,"gid":1400,"sqn":2,"ans":[{"oid":1002,"val":"Bhutan","pos":1},{"oid":1003,"val":"Pakistan","pos":2},{"oid":1001,"val":"Nepal","pos":3}]},"c":1}]
                "[{\"a\":\"a\",\"e\":\"q\",\"p\":{\"qt\":\"Mt Everest is the highest mountain of which country?\",\"qid\":175,\"lst\":false,\"gid\":1400,\"sqn\":2,\"ans\":[{\"oid\":1002,\"val\":\"Bhutan\",\"pos\":1},{\"oid\":1003,\"val\":\"Pakistan\",\"pos\":2},{\"oid\":1001,\"val\":\"Nepal\",\"pos\":3}]},\"c\":1}]"
        };
        Observable.fromArray(messages).observeOn(Schedulers.io()).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                Thread.sleep(10000);
                parseDataMessage(s);
            }
        });
    }

    @Override
    public void onStateChanged(WebSocket webSocket, WebSocketState webSocketState) throws Exception {
    }

    @Override
    public void onConnected(WebSocket webSocket, Map<String, List<String>> map) throws Exception {
        isDisconnected = false;
        Log.d("Arpit", "onConnected: ");
    }

    @Override
    public void onConnectError(WebSocket webSocket, WebSocketException e) throws Exception {
        Log.d("Arpit", "onConnectError: ");
        e.printStackTrace();
        SocketConnectError socketConnectError = SocketConnectError.builder().
                setWebSocketException(e).
                build();
        InputEvent socketDisconnectedIe = InputEvent.builder().
                setUser(user).
                setType(InputEvent.TYPE_SOCKET_CONNECT_ERROR).
                setTrigger(false).
                setSource(InputEvent.SOURCE_SOCKET).
                setGameInfo(gameInfo).
                setGameEvent(socketConnectError).
                build();
        otherEventPublisher.onNext(socketDisconnectedIe);
    }

    @Override
    public void onDisconnected(WebSocket webSocket, final WebSocketFrame serverCloseFrame, final WebSocketFrame clientCloseFrame, final boolean closedByServer) throws Exception {
        Log.d("Arpit", "onDisconnected: " + closedByServer);
        handler.post(new Runnable() {
            @Override
            public void run() {
                isDisconnected = true;
                int errorCode = 1;
                if (serverCloseFrame != null) {
                    Log.d("Arpit", "onDisconnected: " + serverCloseFrame.getCloseCode() + " " + serverCloseFrame.getCloseReason());
                    errorCode = serverCloseFrame.getCloseCode();
                } else if (clientCloseFrame != null) {
                    errorCode = clientCloseFrame.getCloseCode();
                    Log.d("Arpit", "onDisconnected1: " + clientCloseFrame.getCloseCode() + " " + clientCloseFrame.getCloseReason());
                }
                SocketDisconnected socketDisconnected = SocketDisconnected.builder().
                        setErrorCode(errorCode).
                        build();
                InputEvent socketDisconnectedIe = InputEvent.builder().
                        setUser(user)
                        .setSource(InputEvent.SOURCE_SOCKET).
                                setType(InputEvent.TYPE_SOCKET_DISCONNECTED).
                                setTrigger(false).
                                setGameInfo(gameInfo).
                                setGameEvent(socketDisconnected).
                                build();
                otherEventPublisher.onNext(socketDisconnectedIe);
                Log.d("Arpit", "onDisconnected: isConnectedToInternet " + isConnectedToInternet + " errorCode " + errorCode + " forcedDisconnection: " + forcedDisconnection);
                if (!forcedDisconnection && errorCode != 4001 && isConnectedToInternet) {
                    connect();
                }
            }
        });
    }

    @Override
    public void onFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onContinuationFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {
    }

    @Override
    public void onTextFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {
    }

    @Override
    public void onBinaryFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onCloseFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onPingFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onPongFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onTextMessage(WebSocket webSocket, String message) throws Exception {
        eventPublisher.onNext(message);
    }

    @Override
    public void onBinaryMessage(WebSocket webSocket, byte[] bytes) throws Exception {

    }

    @Override
    public void onSendingFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onFrameSent(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onFrameUnsent(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onThreadCreated(WebSocket webSocket, ThreadType threadType, Thread thread) throws Exception {

    }

    @Override
    public void onThreadStarted(WebSocket webSocket, ThreadType threadType, Thread thread) throws Exception {

    }

    @Override
    public void onThreadStopping(WebSocket webSocket, ThreadType threadType, Thread thread) throws Exception {

    }

    @Override
    public void onError(WebSocket webSocket, WebSocketException e) throws Exception {

    }

    @Override
    public void onFrameError(WebSocket webSocket, WebSocketException e, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onMessageError(WebSocket webSocket, WebSocketException e, List<WebSocketFrame> list) throws Exception {

    }

    @Override
    public void onMessageDecompressionError(WebSocket webSocket, WebSocketException e, byte[] bytes) throws Exception {
        // AppLog.d("onMessageDecompressionError: ");

    }

    @Override
    public void onTextMessageError(WebSocket webSocket, WebSocketException e, byte[] bytes) throws Exception {

    }

    @Override
    public void onSendError(WebSocket webSocket, WebSocketException e, WebSocketFrame webSocketFrame) throws Exception {

    }

    @Override
    public void onUnexpectedError(WebSocket webSocket, WebSocketException e) throws Exception {

    }

    @Override
    public void handleCallbackError(WebSocket webSocket, Throwable throwable) throws Exception {

    }

    @Override
    public void onSendingHandshake(WebSocket webSocket, String s, List<String[]> list) throws Exception {

    }

    private List<InputEvent> parseDataMessage(String dataString) {
        try {
            Log.d("DATA_SOCKET", dataString);
            JSONArray jsonArray = new JSONArray(dataString);
            List<InputEvent> inputEvents = new LinkedList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String action = jsonObject.optString("e");
                String admin = jsonObject.optString("a");
                JSONObject payload = jsonObject.optJSONObject("p");

                InputEvent inputEvent = null;
                if ("a".equalsIgnoreCase(admin)) {
                    switch (action) {
                        case "l":
                            // GAME ON or OFF case handled here
                            inputEvent = (GameEventUtils.parseGameOnOff(payload, user, gameInfo, true, true));
                            break;
                        case "q":
                            // GAME QUESTION PAYLOAD
                            inputEvent = (GameEventUtils.parseQuestion(payload, user, gameInfo, true, false));
                            break;
                        case "a":
                            // GAME ANSWER PAYLOAD
                            inputEvent = (GameEventUtils.parseAnswerPayload(payload, user, gameInfo, true, false));
                            break;
                        case "w":
                            // GAME WINNER MESSAGE
                            inputEvent = (GameEventUtils.parseWinner(payload, user, gameInfo, true, false));
                            break;
                        case "c":
                            inputEvent = (GameEventUtils.parseChatMessage(payload, user, gameInfo, true, true));
                            break;
                        case "k":
                            // USER KICKED EVENT
                            inputEvent = (GameEventUtils.parseUserKickedEvent(payload, user, gameInfo, true, true));
                            break;

                    }
                    if (jsonObject.has("c")) {
                        inputEvents.add(GameEventUtils.parseLiveGameInfo(jsonObject, user, gameInfo, true, true));
                    }
                } else if ("c".equalsIgnoreCase(admin)) {
                    inputEvents.add(GameEventUtils.parseChatMessage(payload, user, gameInfo, true, true));
                    // Live USER COUNT value
                    inputEvents.add(GameEventUtils.parseLiveGameInfo(jsonObject, user, gameInfo, true, true));
                }
                if (inputEvent != null) {
                    inputEvents.add(inputEvent);
                }
                inputEvent = null;
            }
            return inputEvents;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private void observeNetworkChange() {
        connectionManager.observeNetworkChanges().subscribe(new Observer<ConnectionInfo>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(ConnectionInfo connectionInfo) {
                Log.d("Arpit", "isConnectedToInternet: " + connectionInfo.connectedToInternet());
                isConnectedToInternet = connectionInfo.connectedToInternet();
                if (isConnectedToInternet && isDisconnected && !forcedDisconnection) {
                    connect();
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        });
    }

}
